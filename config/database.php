<?php

//----------------------------------------------------------
// connectionsの設定
//----------------------------------------------------------
$_connections = [];

// Common DB
$_connection = [
	'driver'			=> 'mysql',
	'host'				=> env('DB_HOST_COMMON',		'127.0.0.1'),
	'port'				=> env('DB_PORT_COMMON',		'3306'),
	'database'			=> env('DB_DATABASE_COMMON',	'bbdw_common'),
	'username'			=> env('DB_USERNAME_COMMON',	'bbdw_user'),
	'password'			=> env('DB_PASSWORD_COMMON',	'bbdw_pass!'),
	'unix_socket'		=> env('DB_SOCKET_COMMON',		''),
	'charset'			=> 'utf8mb4',
	'collation'			=> 'utf8mb4_unicode_ci',
	'prefix'			=> '',
	'prefix_indexes'	=> true,
	'strict'			=> false,
	'engine'			=> null,
];
$_connections['common'] = $_connection;

// Game DB
$_game_num = env('DB_GAME_NUM', '1');
$_game = [];
for ($i = 1; $i <= $_game_num; $i++)
{
	$_prefix = sprintf('_%02d', $i);
	$_connection = [
		'driver'			=> 'mysql',
		'host'				=> env('DB_HOST_GAME' . $_prefix,		'127.0.0.1'),
		'port'				=> env('DB_PORT_GAME' . $_prefix,		'3306'),
		'database'			=> env('DB_DATABASE_GAME' . $_prefix,	'bbdw_game' . $_prefix),
		'username'			=> env('DB_USERNAME_GAME' . $_prefix,	'bbdw_user'),
		'password'			=> env('DB_PASSWORD_GAME' . $_prefix,	'bbdw_pass!'),
		'unix_socket'		=> env('DB_SOCKET_GAME' . $_prefix,		''),
		'charset'			=> 'utf8mb4',
		'collation'			=> 'utf8mb4_unicode_ci',
		'prefix'			=> '',
		'prefix_indexes'	=> true,
		'strict'			=> false,
		'engine'			=> null,
	];
	$_connections['game' . $_prefix] = $_connection;
}

// Log DB
$_connection = [
	'driver'			=> 'mysql',
	'host'				=> env('DB_HOST_LOG',		'127.0.0.1'),
	'port'				=> env('DB_PORT_LOG',		'3306'),
	'database'			=> env('DB_DATABASE_LOG',	'bbdw_log'),
	'username'			=> env('DB_USERNAME_LOG',	'bbdw_user'),
	'password'			=> env('DB_PASSWORD_LOG',	'bbdw_pass!'),
	'unix_socket'		=> env('DB_SOCKET_LOG',		''),
	'charset'			=> 'utf8mb4',
	'collation'			=> 'utf8mb4_unicode_ci',
	'prefix'			=> '',
	'prefix_indexes'	=> true,
	'strict'			=> false,
	'engine'			=> null,
];
$_connections['log'] = $_connection;

// GMS DB
$_connection = [
	'driver'			=> 'mysql',
	'host'				=> env('DB_HOST_GMS',		'127.0.0.1'),
	'port'				=> env('DB_PORT_GMS',		'3306'),
	'database'			=> env('DB_DATABASE_GMS',	'bbdw_gms'),
	'username'			=> env('DB_USERNAME_GMS',	'bbdw_user'),
	'password'			=> env('DB_PASSWORD_GMS',	'bbdw_pass!'),
	'unix_socket'		=> env('DB_SOCKET_GMS',		''),
	'charset'			=> 'utf8mb4',
	'collation'			=> 'utf8mb4_unicode_ci',
	'prefix'			=> '',
	'prefix_indexes'	=> true,
	'strict'			=> false,
	'engine'			=> null,
];
$_connections['gms'] = $_connection;



return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'gms'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => $_connections,

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DB', 0),
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => env('REDIS_CACHE_DB', 1),
        ],

    ],

];
