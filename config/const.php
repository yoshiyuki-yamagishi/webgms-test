<?php

return [
    //管理画面の権限レベル
    'AuthLevel' => [
        'VIEWER' => 1,
        'ADMIN'  => 2,
        'DEBUG' => 3,
        'CS' => 4
    ],

];
