<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        プレイヤー基本情報
      </h1>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;"><b>プレイヤー基本情報</b>
          <table border="1" height="50">
          {!! Form::model($player, array('action' => 'PlayerExpLogController@index')) !!}
            <tr>
              <th class="player_col">表示用プレイヤーID</th>
              <td>{!! Form::label('', $player['player_disp_id'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr style="background-color: #87cefa;">
              <th class="player_col">プレイヤーID</th>
              <td>{!! Form::label('0', $player['id'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">プレイヤー名</th>
              <td>{!! Form::label('0', $player['player_name'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">経験値</th>
              <td>{!! Form::label('0', $player['experience'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">レベル</th>
              <td>{!! Form::label('0', $player['player_lv'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">MAXAL</th>
              <td>{!! Form::label('0', $player['max_al'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">P$</th>
              <td>{!! Form::label('0', $player['platinum_dollar'], ['class' => 'player_data']) !!}</td>
            </tr>

            <tr>
              <th class="player_col">欠片パウダー</th>
              <td>{!! Form::label('0', $player['powder'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">魔素</th>
              <td>{!! Form::label('0', $player['magic_num'], ['class' => 'player_data']) !!}</td>
            </tr>

            <tr>
              <th class="player_col">フレンドポイント</th>
              <td>{!! Form::label('0', $player['friend_point'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">ゲーム内通貨（無償）</th>
              <td>{!! Form::label('0', $player['free_crystal'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">ゲーム内通貨（有償）</th>
              <td>{!! Form::label('0', $player['charged_crystal'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">AP回復日付</th>
              <td>{!! Form::label('9999-01-01', $player['al_recovery_at'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">魔道書所持枠</th>
              <td>{!! Form::label('0', $player['max_grimoire'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">お気に入りキャラ</th>
              <td>{!! Form::label('0', $player['favorite_character'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">最終プレイステージ</th>
              <td>{!! Form::label('未プレイ', $player['last_play_stage'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">ログイン日数</th>
              <td>{!! Form::label('0', $player['login_days'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">ゲーム開始日時</th>
              <td>{!! Form::label('9999-01-01', $player['first_login_at'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">最終ログイン日</th>
              <td>{!! Form::label('9999-01-01', $player['last_login_at'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">アクティブパーティーID </th>
              <td>{!! Form::label('0', $player['id'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">チュートリアル進捗</th>
              <td>{!! Form::label('0', $player['tutorial_progress'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">端末情報</th>
              <td>{!! Form::label('0', $player['unique_id'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">引継ぎコード</th>
              <td>{!! Form::label('未設定', $player['passcode'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">引継ぎコードパスワード</th>
              <td>{!! Form::label('未設定', $player['password'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">ゲーム内の生年月日</th>
              <td>{!! Form::label('9999-01-01', $player['birthday'], ['class' => 'player_data']) !!}</td>
            </tr>
            <tr>
              <th class="player_col">本人の生年月日</th>
              <td>{!! Form::label('9999-01-01', $player['legal_birthday'], ['class' => 'player_data']) !!}</td>
            </tr>
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">プレイヤー経験値ログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerLvLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">プレイヤーLvログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerFriendPointLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">プレイヤーフレンドポイントログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerLoginBonusLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">プレイヤーログインボーナスログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerItemBuyLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">ショップ利用ログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'ProductBuyLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">有償蒼の結晶購入ログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerMissionLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">ミッションログ画面</button>
            {!! Form::close() !!}

            {!! Form::model($player, array('action' => 'PlayerPresentLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">プレゼントログ画面</button>
            {!! Form::close() !!}
          </table>
        </div>
      </p>



      <style>
      .player_data {
        background-color: #CEECF5;
        width: 180px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_col {
        background-color: #87cefa;
        width: 180px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_name {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_lv {
        text-align: center;
        width: 40px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_last_login {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }

      </style>


      <!-- プレイヤー所持アイテム -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>所持アイテム</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">アイテム名</th>
            <th style="text-align: center;">所持数</th>
            <th style="text-align: center;">説明</th>
            <th style="text-align: center;">種別</th>
          </tr>
          @foreach ($pItems as $pItem)
          {!! Form::model($pItems, array('action' => 'PlayerItemLogController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $pItem->name, ['class' => 'player_item_name']) !!}</td>
            <td>{!! Form::label('', $pItem->num, ['class' => 'player_item_num']) !!}</td>
            <td>{!! Form::label('', $pItem->detail, ['class' => 'player_item_detail']) !!}</td>
            <td>{!! Form::label('', $pItem->category, ['class' => 'player_item_category']) !!}</td>
          </tr>

          @endforeach
          <button class="btn btn-success" type="submit">アイテムログ画面</button>
          {!! Form::hidden('player_id', $player['id']) !!}
          {!! Form::hidden('player_name', $player['player_name']) !!}
          {!! Form::close() !!}
        </table>
        </div>
      </p>

      <style>
        .player_item_name {
        text-align: center;
        width: 200px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_item_num {
        text-align: center;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_item_detail {
        text-align: center;
        width: 400px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_item_category {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>


      <!-- プレイヤー所持オーブ -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>所持オーブ</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">オーブ名</th>
            <th style="text-align: center;">所持数</th>
            <th style="text-align: center;">説明</th>
          </tr>
          @foreach ($pOrbs as $pOrb)
          {!! Form::model($pOrbs, array('action' => 'PlayerDetailsController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $pOrb->name, ['class' => 'player_orb_name']) !!}</td>
            <td>{!! Form::label('', $pOrb->num, ['class' => 'player_item_num']) !!}</td>
            <td>{!! Form::label('', $pOrb->detail, ['class' => 'player_orb_detail']) !!}</td>
          </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
      </p>

      <style>
        .player_orb_name {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_orb_detail {
        text-align: center;
        width: 500px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤー所持欠片 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>所持欠片</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">欠片名</th>
            <th style="text-align: center;">所持数</th>
            <th style="text-align: center;">説明</th>
          </tr>
          @foreach ($pFragments as $pFragment)
          {!! Form::model($pFragments, array('action' => 'PlayerDetailsController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $pFragment->name, ['class' => 'player_orb_name']) !!}</td>
            <td>{!! Form::label('', $pFragment->num, ['class' => 'player_item_num']) !!}</td>
            <td>{!! Form::label('', $pFragment->detail, ['class' => 'player_orb_detail']) !!}</td>
          </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
      </p>

      <style>
        .player_orb_name {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_orb_detail {
        text-align: center;
        width: 500px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤー所持キャラクター -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>所持キャラクター</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">入手日付</th>
            <th style="text-align: center;">キャラクターID</th>
            <th style="text-align: center;">キャラクター名</th>
            <th style="text-align: center;">レベル</th>
            <th style="text-align: center;">属性</th>
            <th style="text-align: center;">レアリティ</th>
            <th style="text-align: center;">スキル名1</th>
            <th style="text-align: center;">S1Lv</th>
            <th style="text-align: center;">スキル名2</th>
            <th style="text-align: center;">S2Lv</th>
          </tr>
          @foreach ($viewPlayerCharacters as $viewPlayerCharacter)
          {!! Form::model($pFragments, array('action' => 'PlayerCharacterLogController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $viewPlayerCharacter->acquired_at, ['class' => 'player_character_acquired_at']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->character_id, ['class' => 'player_character_id']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->name, ['class' => 'player_character_name']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->character_lv, ['class' => 'player_character_lv']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->element, ['class' => 'player_character_element']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->rarity, ['class' => 'player_character_rarity']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->as1Name, ['class' => 'player_character_skill_name']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->active_skill_1_lv, ['class' => 'player_character_skill_lv']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->as2Name, ['class' => 'player_character_skill_name']) !!}</td>
            <td>{!! Form::label('', $viewPlayerCharacter->active_skill_2_lv, ['class' => 'player_character_skill_lv']) !!}</td>
          </tr>

          @endforeach
          <button class="btn btn-success" type="submit">キャラクターログ画面</button>
          {!! Form::hidden('player_id', $player['id']) !!}
          {!! Form::hidden('player_name', $player['player_name']) !!}
          {!! Form::close() !!}
        </table>
        </div>
      </p>

      <style>
        .player_character_acquired_at {
        text-align: center;
        width: 160px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_character_id {
        text-align: center;
        width: 120px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_name {
        text-align: center;
        width: 350px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_lv {
        text-align: center;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_element {
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_rarity {
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_skill_name {
        text-align: center;
        width: 170px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_character_skill_lv {
        text-align: center;
        width: 50px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>


      <!-- プレイヤー所持魔道書 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>所持魔道書</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">取得日付</th>
            <th style="text-align: center;">プレイヤー魔道書ID</th>
            <th style="text-align: center;">魔道書ID</th>
            <th style="text-align: center;">魔道書名</th>
            <th style="text-align: center;">レアリティ</th>
            <th style="text-align: center;">覚醒数</th>
            <th style="text-align: center;">スキル1名</th>
            <th style="text-align: center;">効果1</th>
            <th style="text-align: center;">スキル2名</th>
            <th style="text-align: center;">効果2</th>
          </tr>
          @foreach ($viewPlayerGrimoires as $viewPlayerGrimoire)

          {!! Form::model($viewPlayerGrimoires, array('action' => 'PlayerGrimoireLogController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $viewPlayerGrimoire->acquired_at, ['class' => 'player_grimoire_acquired_at']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->id, ['class' => 'player_grimoire_base_id']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->grimoire_id, ['class' => 'player_grimoire_id']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->name, ['class' => 'player_grimoire_name']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->rarity, ['class' => 'player_grimoire_rarity']) !!}</td>
            <td>{!! Form::label('0', $viewPlayerGrimoire->awake, ['class' => 'player_grimoire_awake']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->ps1Name, ['class' => 'player_grimoire_skill']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->ps1Detail, ['class' => 'player_grimoire_detail']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->ps2Name, ['class' => 'player_grimoire_skill']) !!}</td>
            <td>{!! Form::label('', $viewPlayerGrimoire->ps2Detail, ['class' => 'player_grimoire_detail']) !!}</td>
          </tr>
          @endforeach

          {!! Form::model($player, array('action' => 'PlayerGrimoireLogController@index')) !!}
              {!! Form::hidden('player_id', $player['id']) !!}
              {!! Form::hidden('player_name', $player['player_name']) !!}
              <button class="btn btn-success" type="submit">魔道書ログ画面</button>
            {!! Form::close() !!}
        </table>
        </div>
      </p>

      <style>
        .player_grimoire_acquired_at {
        text-align: center;
        width: 160px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_base_id {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_grimoire_id {
        text-align: center;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_name {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_rarity {
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_awake {
        text-align: center;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_skill {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_grimoire_detail {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>


      <!-- パーティー編成一覧 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>パーティー編成一覧</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">パーティNo</th>
            <th style="text-align: center;">キャラクターID 1</th>
            <th style="text-align: center;">キャラクター名 1</th>
            <th style="text-align: center;">キャラクターID 2</th>
            <th style="text-align: center;">キャラクター名 2</th>
            <th style="text-align: center;">キャラクターID 3</th>
            <th style="text-align: center;">キャラクター名 3</th>
            <th style="text-align: center;">キャラクターID 4</th>
            <th style="text-align: center;">キャラクター名 4</th>
            <th style="text-align: center;">キャラクターID 5</th>
            <th style="text-align: center;">キャラクター名 5</th>
            <th style="text-align: center;">キャラクターID 6</th>
            <th style="text-align: center;">キャラクター名 6</th>
          </tr>
          @foreach ($playerPartys as $playerParty)
          {!! Form::model($playerPartys, array('action' => 'PlayerDetailsController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $playerParty->party_no, ['class' => 'player_party_no']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_1, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_1, ['class' => 'player_party_chara_name']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_2, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_2, ['class' => 'player_party_chara_name']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_3, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_3, ['class' => 'player_party_chara_name']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_4, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_4, ['class' => 'player_party_chara_name']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_5, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_5, ['class' => 'player_party_chara_name']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_character_id_6, ['class' => 'player_party_chara_id']) !!}</td>
            <td>{!! Form::label('', $playerParty->player_party_name_6, ['class' => 'player_party_chara_name']) !!}</td>
          </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
      </p>

      <style>
        .player_party_no {
        text-align: center;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_party_chara_id {
        text-align: center;
        width: 130px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_party_chara_name {
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>


      <!-- プレゼントボックス -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>プレゼントボックス</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">アイテムID</th>
            <th style="text-align: center;">アイテム名</th>
            <th style="text-align: center;">個数</th>
            <th style="text-align: center;">文言</th>
            <th style="text-align: center;">入手経路</th>
            <th style="text-align: center;">入手日時</th>
          </tr>
          @foreach ($playerPresents as $playerPresent)
          {!! Form::model($playerPresents, array('action' => 'PlayerDetailsController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $playerPresent->item_id, ['class' => 'player_present_item_id']) !!}</td>
            <td>{!! Form::label('', $playerPresent->name, ['class' => 'player_present_item_name']) !!}</td>
            <td>{!! Form::label('', $playerPresent->item_num, ['class' => 'player_present_num']) !!}</td>
            <td>{!! Form::label('', $playerPresent->message, ['class' => 'player_present_message']) !!}</td>
            <td>{!! Form::label('', $playerPresent->src_type, ['class' => 'player_present_src_type']) !!}</td>
            <td>{!! Form::label('', $playerPresent->created_at, ['class' => 'player_present_created_at']) !!}</td>
          </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
      </p>

      <style>
        .player_present_item_id {
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
        .player_present_item_name {
        text-align: center;
        width: 200px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_present_num {
        text-align: center;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_present_message {
        text-align: center;
        width: 200px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_present_src_type {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .player_present_created_at {
        text-align: center;
        width: 170px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>
        @if($authLevel == config('const.AuthLevel.ADMIN') || $authLevel == config('const.AuthLevel.DEBUG'))
          {!! Form::model($player, array('action' => 'DebugController@index')) !!}
          <button name="modify_player" class="btn btn-warning" type="submit">
            デバッグ
          </button>
          {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
      @endif
    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
