<html>
<head>
    <title>hello/index</title>
    <style>
        body {font-size:16pt; color:#999 };
        h1 { font-size:50pt; text-align:right; color:#f6f6f6;
        margin:-20px 0px -30px 0px; letter-spacing:-4pt; }
    </style>
</head>
<body>
    <h1>Blade/Index</h1>
        {!! Form::model($playerLevel, ['action' => 'HelloController@index']) !!}
        <div class="form-group">
            {!! Form::label('level', 'level') !!}
            {!! Form::text('level', '', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('experience', 'experience') !!}
            {!! Form::text('experience', '', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('max_al', 'max_al') !!}
            {!! Form::text('max_al', '', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('max_party_cost', 'max_party_cost') !!}
            {!! Form::text('max_party_cost', '', ['class' => 'form-control']) !!}
        </div>

    <button class="btn btn-success" type="submit">更新!</button>
        {!! Form::close() !!}

    <!-- <form method="POST" action="hello">
        {{ csrf_field() }}
        <input type="text" name="msg">
        <input type="submit">
        

    </form> -->
</body>
</html>