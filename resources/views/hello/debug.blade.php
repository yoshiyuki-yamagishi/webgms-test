<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="dist/css/skins/skin-red.min.css">
  <link rel="stylesheet" href="dist/css/skins/skin-green.min.css">
  <link rel="stylesheet" href="dist/css/skins/skin-green.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/IMG_2353.PNG" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>TEST USER</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="top.html"><i class="fa fa-link"></i> <span>トップ</span></a></li>
        <li><a href="maintenance.html"><i class="fa fa-link"></i> <span>メンテナンス</span></a></li>
        <li><a href="notice.html"><i class="fa fa-link"></i> <span>お知らせ管理</span></a></li>
        <li><a href="player_list.html"><i class="fa fa-link"></i> <span>プレイヤー一覧</span></a></li>
        <li><a href="present.html"><i class="fa fa-link"></i> <span>プレゼント</span></a></li>
        <li><a href="simulate.html"><i class="fa fa-link"></i> <span>シミュレート</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>ログアウト</span></a></li>
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>デバッグ（遷移用）</span></a></li>
        <li><a href="ban.html"><i class="fa fa-link"></i> <span>BAN（開発用）</span></a></li>
        <li><a href="player_base.html"><i class="fa fa-link"></i> <span>プレイヤー基本情報（開発用）</span></a></li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        デバッグ
      </h1>


      # $dataから順に値を取り出して$valに代入します。
      @foreach ($player as $val) {
 
        # $valの値を使い、その中にある各項目の値を表示します。
        {{ $val->oname }}
 
      }



      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 220px; top: 0px;"><b>ユーザ基本情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center; width: 60px">レベル</th>
              <th style="text-align: center; width: 90px">ログイン日</th>
              <th style="text-align: center; width: 150px">チュートリアル進捗</th>
              <th style="text-align: center; width: 100px">ゲーム開始日</th>
              <th style="text-align: center; width: 60px">経験値</th>
              <th style="text-align: center; width: 130px">フレンドポイント</th>
              <th style="text-align: center; width: 30px">AL</th>
              <th style="text-align: center; width: 50px">通貨</th>
              <th style="text-align: center; width: 30px">枠</th>
            </tr>
            <tr id="2" style="background-color: #CEECF5;">
              <th style="text-align: center; width: 60px"><div id="myTD1" contentEditable="false">10</div></th>
              <th style="text-align: center; width: 90px"><div id="myTD2" contentEditable="false">120</div></th>
              <th style="text-align: center; width: 150px"><div id="myTD3" contentEditable="false">ステップ3</div></th>
              <th style="text-align: center; width: 100px"><div id="myTD4" contentEditable="false">2019/11/25</div></th>
              <th style="text-align: center; width: 60px"><div id="myTD5" contentEditable="false">2000</div></th>
              <th style="text-align: center; width: 130px"><div id="myTD6" contentEditable="false">1000</div></th>
              <th style="text-align: center; width: 30px"><div id="myTD7" contentEditable="false">50</div></th>
              <th style="text-align: center; width: 50px"><div id="myTD8" contentEditable="false">50000</div></th>
              <th style="text-align: center; width: 30px"><div id="myTD9" contentEditable="false">100</div></th>
            </tr>
          </table>
        </div>
      </p>

      <form style="position: relative; left: 810px; top: 0px;">
        <button type="button" class="btn btn-primary" onClick= "button1()" style="text-align: center; vertical-align: middle; height:30px">編集</button>
        <button type="button" class="btn btn-primary" onClick= "button2()" style="text-align: center; vertical-align: middle; height:30px">完了</button>
      </form>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 220px; top: 0px;"><b>アイテム付与</b>
          <tr>
            <p style="margin-bottom:1em;">
              <div style="position: relative; left: 0px; top: 0px;">キャラクター　
                レア度<select name="example"style="position: relative; left: 10px; top: 0px; width: 60px" >
                  <option value="A2">A2</option>
                  <option value="A1">A1</option>
                  <option value="S">S</option>
                  <option value="SS">SS</option>
                  <option value="SS+">SS+</option>
                  <option value="SS++">SS++</option>
                  <option value="SS+++">SS+++</option>
                </select>
                  　　キャラクター名<select name="example"style="position: relative; left: 10px; top: 0px; width: 250px" >
                  <option value="シエル（エンブリオ＝リアクター）">シエル（エンブリオ＝リアクター）</option>
                  <option value="ラグナ＝ザ＝ブラッドエッジ">ラグナ＝ザ＝ブラッドエッジ</option>
                  <option value="ジン＝キサラギ">ジン＝キサラギ</option>
                  <option value="ノエル＝ヴァーミリオン">ノエル＝ヴァーミリオン</option>
                  <option value="レイチェル＝アルカード">レイチェル＝アルカード</option>
                  <option value="テイガー">テイガー</option>
                  <option value="タオカカ">タオカカ</option>
                </select>
                <button type="button" class="btn btn-primary" onClick= "button1()"
                style="line-height:10px; text-align:center; vertical-align: middle; position: relative; left: 30px;">付与</button>
              </div>
            </p>
            <p style="margin-bottom:1em;">
              <div style="position: relative; left: 0px; top: 0px;">
                    　　　魔導書　
                    レア度<select name="example"style="position: relative; left: 10px; top: 0px; width: 60px" >
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5" selected>5</option>
                </select>　　　　　 魔導書名
                <select name="example"style="position: relative; left: 10px; top: 0px; width: 150px" >
                  <option value="蒼の魔導書">蒼の魔導書</option>
                  <option value="技の顕現">技の顕現</option>
                  <option value="ディスカバーコール">ディスカバーコール</option>
                  <option value="武帝">武帝</option>
                  <option value="増幅陣・ゲインアート">増幅陣・ゲインアート</option>
                  <option value="幻想生物">幻想生物</option>
                  <option value="バニングレッド">バニングレッド</option>
                </select>
                    　　個数<input type="text" name="example"style="line-height:15px; position: relative; left: 10px; top: 0px; width: 50px">
                <button type="button" class="btn btn-primary" onClick= "button1()"
                        style="line-height:10px; text-align:center; vertical-align: middle; position: relative; left: 30px;">付与</button>
              </div>
            </p>
            <p>
              <div style="position: relative; left: 0px; top: 0px;">
                    　　　オーブ　
                <select name="example"style="position: relative; left: 10px; top: 0px; width: 150px" >
                  <option value="ATK+1">ATK+1</option>
                  <option value="DEF+1">DEF+1</option>
                    <option value="HP+10">HP+10</option>
                </select>
                    　　個数<input type="text" name="example"style="line-height:15px; position: relative; left: 10px; top: 0px; width: 50px">
                <button type="button" class="btn btn-primary" onClick= "button1()"
                        style="line-height:10px; text-align:center; vertical-align: middle; position: relative; left: 30px;">付与</button>
              </div>
            </p>
            <p>
              <div style="position: relative; left: 0px; top: 0px;">
                    　　　　欠片　
                <select name="example"style="position: relative; left: 10px; top: 0px; width: 150px" >
                  <option value="欠片1">欠片1</option>
                  <option value="欠片2">欠片2</option>
                  <option value="欠片3">欠片3</option>
                </select>
                    　　個数<input type="text" name="example"style="line-height:15px; position: relative; left: 10px; top: 0px; width: 50px">
                <button type="button" class="btn btn-primary" onClick= "button1()"
                        style="line-height:10px; text-align:center; vertical-align: middle; position: relative; left: 30px;">付与</button>
              </div>
            </p>
            <p>
              <div style="position: relative; left: 0px; top: 0px;">
                    　　アイテム　
                <select name="example"style="position: relative; left: 10px; top: 0px; width: 150px" >
                  <option value="経験値アイテム1">経験値アイテム1</option>
                  <option value="経験値アイテム2">経験値アイテム2</option>
                  <option value="ガチャチケット">ガチャチケット</option>
                  <option value="クエストスキップ">クエストスキップ</option>
                </select>
                    　　個数<input type="text" name="example"style="line-height:15px; position: relative; left: 10px; top: 0px; width: 50px">
                <button type="button" class="btn btn-primary" onClick= "button1()"
                        style="line-height:10px; text-align:center; vertical-align: middle; position: relative; left: 30px;">付与</button>
              </div>
            </p>
          </tr>
        </div>
      </p>
      <p style="margin-bottom:2em;">
          <div style="position: relative; left: 220px; top: 0px;"><b>キャラクター編集</b>
            <table border="1" height="50">
              <tr id="100" style="background-color: #87cefa;">
                <th style="text-align: center; width: 120px">キャラクターID</th>
                <th style="text-align: center; width: 120px">キャラクターLv</th>
                <th style="text-align: center; width: 70px">経験値</th>
                <th style="text-align: center; width: 70px">オーブ1</th>
                <th style="text-align: center; width: 70px">オーブ2</th>
                <th style="text-align: center; width: 70px">オーブ3</th>
                <th style="text-align: center; width: 70px">オーブ4</th>
                <th style="text-align: center; width: 70px">オーブ5</th>
                <th style="text-align: center; width: 70px">オーブ6</th>
                <th style="text-align: center; width: 70px">グレード</th>
                <th style="text-align: center; width: 90px">AS1レベル</th>
                <th style="text-align: center; width: 90px">AS2レベル</th>
              </tr>
              <tr id="11" style="background-color: #CEECF5;">
                <th style="text-align: center; width: 60px"><div id="myTD1" contentEditable="false">0001001</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD2" contentEditable="false">10</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD3" contentEditable="false">3000</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD4" contentEditable="false">4211001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD5" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD6" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD7" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD8" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD9" contentEditable="false">4115003</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD10" contentEditable="false">2</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD11" contentEditable="false">1</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD12" contentEditable="false">2</div></th>
              </tr>
              <tr id="12" style="background-color: #CEECF5;">
                <th style="text-align: center; width: 60px"><div id="myTD13" contentEditable="false">0002001</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD14" contentEditable="false">10</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD15" contentEditable="false">3000</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD16" contentEditable="false">4211001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD17" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD18" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD19" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD20" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD21" contentEditable="false">4115003</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD22" contentEditable="false">3</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD23" contentEditable="false">1</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD24" contentEditable="false">2</div></th>
              </tr>
              <tr id=13 style="background-color: #CEECF5;">
                <th style="text-align: center; width: 60px"><div id="myTD25" contentEditable="false">0003001</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD26" contentEditable="false">10</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD27" contentEditable="false">3000</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD28" contentEditable="false">4211001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD29" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD30" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD31" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD32" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD33" contentEditable="false">4115003</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD34" contentEditable="false">1</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD35" contentEditable="false">1</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD36" contentEditable="false">2</div></th>
              </tr>
              <tr id="14" style="background-color: #CEECF5;">
                <th style="text-align: center; width: 60px"><div id="myTD37" contentEditable="false">0004001</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD38" contentEditable="false">10</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD39" contentEditable="false">3000</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD40" contentEditable="false">4211001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD41" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD42" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD43" contentEditable="false">4115001</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD44" contentEditable="false">4115002</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD45" contentEditable="false">4115003</div></th>
                <th style="text-align: center; width: 70px"><div id="myTD46" contentEditable="false">2</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD47" contentEditable="false">1</div></th>
                <th style="text-align: center; width: 90px"><div id="myTD48" contentEditable="false">2</div></th>
              </tr>
            </table>
            
          </div>
        </p>
        <form style="position: relative; left: 1100px; top: 0px;">
            <button type="button" class="btn btn-primary" onClick= "button3()" style="text-align: center; vertical-align: middle; height:30px">編集</button>
            <button type="button" class="btn btn-primary" onClick= "button4()" style="text-align: center; vertical-align: middle; height:30px">完了</button>
        </form>
      <script>
        function button1()
        {
          myTD1.contentEditable=true
          myTD2.contentEditable=true
          myTD3.contentEditable=true
          myTD4.contentEditable=true
          myTD5.contentEditable=true
          myTD6.contentEditable=true
          myTD7.contentEditable=true
          myTD8.contentEditable=true
          myTD9.contentEditable=true
          $('#2').css('background-color', '#ffffff');
        }
        function button2()
        {
          myTD1.contentEditable=false
          myTD2.contentEditable=false
          myTD3.contentEditable=false
          myTD4.contentEditable=false
          myTD5.contentEditable=false
          myTD6.contentEditable=false
          myTD7.contentEditable=false
          myTD8.contentEditable=false
          myTD9.contentEditable=false
          $('#2').css('background-color', '#CEECF5');
        }
        function button3()
        {
          myTD1.contentEditable=true
          myTD2.contentEditable=true
          myTD3.contentEditable=true
          myTD4.contentEditable=true
          myTD5.contentEditable=true
          myTD6.contentEditable=true
          myTD7.contentEditable=true
          myTD8.contentEditable=true
          myTD9.contentEditable=true
          myTD10.contentEditable=true
          myTD11.contentEditable=true
          myTD12.contentEditable=true
          myTD13.contentEditable=true
          myTD14.contentEditable=true
          myTD15.contentEditable=true
          myTD16.contentEditable=true
          myTD17.contentEditable=true
          myTD18.contentEditable=true
          myTD19.contentEditable=true
          myTD20.contentEditable=true
          myTD21.contentEditable=true
          myTD22.contentEditable=true
          myTD23.contentEditable=true
          myTD24.contentEditable=true
          myTD25.contentEditable=true
          myTD26.contentEditable=true
          myTD27.contentEditable=true
          myTD28.contentEditable=true
          myTD29.contentEditable=true
          myTD30.contentEditable=true
          myTD31.contentEditable=true
          myTD32.contentEditable=true
          myTD33.contentEditable=true
          myTD34.contentEditable=true
          myTD35.contentEditable=true
          myTD36.contentEditable=true
          myTD37.contentEditable=true
          myTD38.contentEditable=true
          myTD39.contentEditable=true
          myTD40.contentEditable=true
          myTD41.contentEditable=true
          myTD42.contentEditable=true
          myTD43.contentEditable=true
          myTD44.contentEditable=true
          myTD45.contentEditable=true
          myTD46.contentEditable=true
          myTD47.contentEditable=true
          myTD48.contentEditable=true
          
          $('#11').css('background-color', '#ffffff');
          $('#12').css('background-color', '#ffffff');
          $('#13').css('background-color', '#ffffff');
          $('#14').css('background-color', '#ffffff');
        }
        function button4()
        {
          myTD1.contentEditable=false
          myTD2.contentEditable=false
          myTD3.contentEditable=false
          myTD4.contentEditable=false
          myTD5.contentEditable=false
          myTD6.contentEditable=false
          myTD7.contentEditable=false
          myTD8.contentEditable=false
          myTD9.contentEditable=false
          myTD10.contentEditable=false
          myTD11.contentEditable=false
          myTD12.contentEditable=false
          myTD13.contentEditable=false
          myTD14.contentEditable=false
          myTD15.contentEditable=false
          myTD16.contentEditable=false
          myTD17.contentEditable=false
          myTD18.contentEditable=false
          myTD19.contentEditable=false
          myTD20.contentEditable=false
          myTD21.contentEditable=false
          myTD22.contentEditable=false
          myTD23.contentEditable=false
          myTD24.contentEditable=false
          myTD25.contentEditable=false
          myTD26.contentEditable=false
          myTD27.contentEditable=false
          myTD28.contentEditable=false
          myTD29.contentEditable=false
          myTD30.contentEditable=false
          myTD31.contentEditable=false
          myTD32.contentEditable=false
          myTD33.contentEditable=false
          myTD34.contentEditable=false
          myTD35.contentEditable=false
          myTD36.contentEditable=false
          myTD37.contentEditable=false
          myTD38.contentEditable=false
          myTD39.contentEditable=false
          myTD40.contentEditable=false
          myTD41.contentEditable=false
          myTD42.contentEditable=false
          myTD43.contentEditable=false
          myTD44.contentEditable=false
          myTD45.contentEditable=false
          myTD46.contentEditable=false
          myTD47.contentEditable=false
          myTD48.contentEditable=false

          $('#11').css('background-color', '#CEECF5');
          $('#12').css('background-color', '#CEECF5');
          $('#13').css('background-color', '#CEECF5');
          $('#14').css('background-color', '#CEECF5');
        }


      </script>
    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>