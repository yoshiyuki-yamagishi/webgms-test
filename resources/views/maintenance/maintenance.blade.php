<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.css') }}"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.css') }}"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        メンテナンス
      </h1>


        @if($authLevel == config('const.AuthLevel.ADMIN'))
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal" style="margin-left:-3px; margin-top:15px">新規作成へ</button>

        <!-- 新規作成用モーダルの設定 -->
        <div class="modal fade" id="Modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="Modal">メンテナンス新規作成</h5>
                    </div>
                    {!! Form::open(array('action' => 'MaintenanceController@index')) !!}
                    {!! Form::hidden('action', 'create_maintenance') !!}
                    <div class="modal-body">
                        <div>
                            info_id
                            {{Form::text('info_id')}}
                        </div>
                        <div>
                            message
                            {{Form::textarea('message')}}
                        </div>
                        <div>
                            start_at
                            {{ Form::datetimeLocal('start_at')}}

                        </div>
                            end_at
                            {{ Form::datetimeLocal('end_at')}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                        <button type="submit" class="btn btn-primary">保存</button>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
        @endif
<!-- 検索フォーム、一旦いらないのでコメントアウト、必要になったら戻す -->
{{--        <p style="margin-bottom:2em;">--}}
{{--        <div>--}}
{{--          {!! Form::model($maintenances, array('action' => 'MaintenanceController@index')) !!}--}}
{{--          <div style="margin-bottom:1em;">--}}
{{--              発行開始日時--}}
{{--              {{ Form::datetimeLocal('start_at', "", ['class' => 'search_date'])}}--}}
{{--          </div>--}}
{{--          <div style="margin-bottom:1em;">--}}
{{--              掲載終了日時--}}
{{--              {{ Form::datetimeLocal('end_at', "",  ['class' => 'search_date'])}}--}}
{{--          </div>--}}
{{--          {!! Form::hidden('action', 'drop_result') !!}--}}
{{--          <button class="btn btn-success" name="search_maintenance" type="submit" style="margin-left:0px;">上記の期間で検索する</button>--}}
{{--          {!! Form::hidden('action', 'search_maintenance') !!}--}}
{{--        {!! Form::close() !!}--}}
{{--        </div>--}}
{{--      </p>--}}

{{--      <style>--}}
{{--      .search_date {--}}
{{--        margin-left:  20px;--}}
{{--        padding:  0;--}}
{{--        list-style:  none;--}}
{{--      }--}}

{{--      </style>--}}



      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;"><b>メンテナンス一覧(最新50件)</b>
        <table border="1" height="50">
              <tr id="1" style="background-color: #87cefa;">
                <th style="text-align: center;">id</th>
                <th style="text-align: center;">info_id</th>
                <th style="text-align: center;">メッセージ</th>
                <th style="text-align: center;">開始日時</th>
                <th style="text-align: center;">終了日時</th>
                @if($authLevel == config('const.AuthLevel.ADMIN'))
                  <th style="text-align: center; width: 70px">編集</th>
                  <th style="text-align: center; width: 70px">削除</th>
                @endif
              </tr>
            @foreach ($maintenances as $maintenance)
                <tr id="2" style="background-color: #CEECF5;">
                  <td>{!! $maintenance->id !!}</td>
                  <td>{!! $maintenance->info_id !!}</td>
                  <td>{!! $maintenance->message !!}</td>
                  <td>{!! $maintenance->start_at !!}</td>
                  <td>{!! $maintenance->end_at !!}</td>
                  @if($authLevel == config('const.AuthLevel.ADMIN'))
                    <td style="text-align: center; width: 70px"><button type="button" class="btn btn-primary" data-toggle="modal" data-target={{"#".$maintenance->id}}>編集</button></td>

                    {!! Form::model($maintenances, array('action' => 'MaintenanceController@index')) !!}
                      {!! Form::hidden('id', $maintenance->id) !!}
                      {!! Form::hidden('action', 'delete_maintenance') !!}
                      <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_info" type="submit">削除</button></td>
                    {!! Form::close() !!}
                  <!-- 編集用モーダルの設定 -->
                    <div class="modal fade" id={{$maintenance->id}} data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Modal">編集</h5>
                                </div>
                                {!! Form::model($maintenance, array('action' => 'MaintenanceController@index')) !!}
                                {!! Form::hidden('action', 'edit_maintenance') !!}
                                {!! Form::hidden('id', $maintenance->id) !!}
                                <div class="modal-body">
                                    <div>
                                        info_id
                                        {{Form::text('info_id')}}
                                    </div>
                                    <div>
                                        message
                                        {{Form::textarea('message')}}
                                    </div>
                                    <div>
                                        start_at
                                        {{ Form::datetimeLocal('start_at',$maintenance->editDisp_start_at)}}

                                    </div>
                                    end_at
                                    {{ Form::datetimeLocal('end_at',$maintenance->editDisp_end_at)}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>

                                    <button type="submit" class="btn btn-primary">保存</button>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                  @endif
              </tr>
            @endforeach
          </table>
        </div>
      </p>

      <style>
      .maintenance_date {
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }

      </style>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
