<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js) }}"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js) }}"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        プレゼント補填
      </h1>
      @if($authLevel == config('const.AuthLevel.ADMIN') || $authLevel == config('const.AuthLevel.DEBUG') || $authLevel == config('const.AuthLevel.CS'))
      <p style="margin-bottom:2em;">
        <div>
          <div style="margin-bottom:1em;">
            <b>
            配布対象
            </b>
          </div>
                {!! Form::model('', array('action' => 'DirectPresentController@multiItemPresentPlayers', 'enctype'=> 'multipart/form-data','accept-charset'=> 'UTF-8')) !!}
                <div style="">
                    {{Form::file('multiPresentPlayers', ['class' => 'field'])}}
                </div>
                <button class="btn btn-success" name="register_info" type="submit" style="margin-left:330px;"  >一括補填</button>
                {!! Form::close() !!}
                <div style="">
                -------------------------------------------------------------------------------------------------------------
                </div>
                {!! Form::model($present, array('action' => 'DirectPresentController@index')) !!}
          <div style="margin-bottom:1em;">
              {{Form::radio('selectPlayer', '1', true,  ['class' => 'field'])}}
              全てのプレイヤー
          </div>
          <div style="margin-bottom:5em;">
              {{Form::radio('selectPlayer', '2', false,  ['class' => 'field'])}}
              プレイヤー指定
              {{ Form::text('player_id', '', ['class' => 'sim_quest'])}}
          </div>


          <div>
          <b>
            配布アイテム選択
          </b>
          </div>


          <div class="row">
          {{Form::radio('itemType', 'chara', true,  ['class' => 'field'])}}
            <b>
            キャラクター
            </b>
              {{ Form::select('character_base_id', $charaNames, null, ['class' => 'row_character_name']) }}
          </div>

          <div class="row">
          {{Form::radio('itemType', 'grimoire', false,  ['class' => 'field'])}}
            <b>
            魔導書
            </b>
              {{ Form::select('grimoire_id', $grimoireNames, null, ['class' => 'row_grimoire_name']) }}
          </div>

          <div class="row">
          {{Form::radio('itemType', 'mana', false,  ['class' => 'field'])}}
            <b>
            魔素
            </b>
          </div>

          <div class="row">
          {{Form::radio('itemType', 'orb', false,  ['class' => 'field'])}}
            <b>
            オーブ
            </b>
              {{ Form::select('orb_id', $orbNames, null, ['class' => 'row_item_name']) }}
          </div>

          <div class="row">
          {{Form::radio('itemType', 'fragment', false,  ['class' => 'field'])}}
            <b>
            欠片
            </b>
              {{ Form::select('fragment_id', $fragmentNames, null, ['class' => 'row_item_name']) }}
          </div>

          <div class="row">
          {{Form::radio('itemType', 'fragment_powder', false,  ['class' => 'field'])}}
            <b>
            欠片パウダー
            </b>
          </div>

          <div class="row">
          {{Form::radio('itemType', 'item', false,  ['class' => 'field'])}}
            <b>
            アイテム
            </b>

              {{ Form::select('item_id', $itemNames, null, ['class' => 'row_item_name']) }}
          </div>

          <div class="row">
          {{Form::radio('itemType', 'money', false,  ['class' => 'field'])}}
            <b>
            P$
            </b>
          </div>

          <div class="row">
          {{Form::radio('itemType', 'friend_point', false,  ['class' => 'field'])}}
            <b>
            フレンドポイント
            </b>
          </div>

          <div class="row"  style="margin-bottom:3em;">
          {{Form::radio('itemType', 'blue_crystal', false,  ['class' => 'field'])}}
            <b>
            蒼の結晶
            </b>
          </div>

          <div class="row">
          <b>
            配布アイテム数量・個数
          </b>
          </div>

          <div class="row"  style="margin-bottom:5em;">
          個数{!! Form::number('num', 0, ['class' => 'row_item_num']) !!}
          </div>

          <div class="row" style="margin-bottom:1em;">
          <b>
            配布条件設定
          </b>
          </div>

          <div class="row" style="margin-bottom:1em;">
              開始日時
              {{ Form::date('start_at', "", ['class' => 'present_started_at'])}}
              時
              {{ Form::select('start_at_hh', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23], ['class' => 'info_started_at'])}}
              分
              {{ Form::select('start_at_mm', ['00' => 00, '10' => 10, '20' =>  20, '30' =>  30, '40' =>  40, '50' =>  50], ['class' => 'info_started_at'])}}
          </div>
          <div class="row" style="margin-bottom:1em;">
              終了日時
              {{ Form::date('end_at', "",  ['class' => 'present_expired_at'])}}
              時
              {{ Form::select('end_at_hh', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23], ['class' => 'info_started_at'])}}
              分
              {{ Form::select('end_at_mm', ['00' => 00, '10' => 10, '20' =>  20, '30' =>  30, '40' =>  40, '50' =>  50], ['class' => 'info_started_at'])}}
          </div>
          <div class="row" style="margin-bottom:1em;">
              受取可能日数
              {{ Form::number('takable_days', "1", ['class' => 'present_takable_days'])}}
          </div>
          <div class="row" style="margin-bottom:1em;">
              優先度
              {{ Form::number('priority', "1", ['class' => 'present_priority'])}}
          </div>
          <div class="row" style="margin-bottom:1em;">
            <div>
              本文
            </div>
              {{ Form::textarea('content', "", ['class' => 'info_content'])}}
          </div>
          {!! Form::hidden('action', 'give_present') !!}
          <button class="btn btn-success" name="register_info" type="submit" style="margin-left:630px;">配布</button>
          {!! Form::close() !!}
        </div>
      </p>

      <style>
      .row {
        margin-left:  0px;
        margin-top:   20px;

      }
      .info_type {
        margin-left:  20px;
        padding:  0;
      }
      .info_started_at {
        margin-left:  20px;
        width: 150px;
      }
      .info_expired_at {
        margin-left:  20px;
        width: 150px;
      }
      .info_priority {
        text-align: center;
        width: 50px;
        margin-left:  63px;
        list-style:  none;
      }
      .info_title {
        width: 500px;
        margin-left:  50px;
        list-style:  none;
      }
      .info_content {
        width: 700px;
        height:100px;
        margin-left:  0px;
        overflow:scroll;
        list-style:  none;
      }
      </style>


      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>補填リスト</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">対象</th>
            <th style="text-align: center;">アイテム名</th>
            <th style="text-align: center;">配布数</th>
            <th style="text-align: center;">期限</th>
            <th style="text-align: center;">文言</th>
          </tr>
          @foreach ($directPresents as $directPresent)
          {!! Form::model($directPresents, array('action' => 'DirectPresentController@index')) !!}
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::label('', $directPresent->player_id, ['class' => 'direct_type']) !!}</td>
            <td>{!! Form::label('', $directPresent->name, ['class' => 'direct_name']) !!}</td>
            <td>{!! Form::label('', $directPresent->item_num, ['class' => 'direct_item_num']) !!}</td>
            <td>{!! Form::label('', $directPresent->end_at, ['class' => 'direct_end_at']) !!}</td>
            <td>{!! Form::label('', $directPresent->message, ['class' => 'direct_message']) !!}</td>
          </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
      </p>
      @else
            <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>プレゼント補填は管理者ユーザーのみになります</b></div>
      @endif
      <style>
      .direct_type {
        font-weight: bold;
        text-align: center;
        width: 120px;
        margin:  0;
        padding:  0;
        list-style:  none;

      }
      .direct_name {
        font-weight: bold;
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;

      }
      .direct_item_num {
        font-weight: bold;
        text-align: center;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;

      }
      .direct_end_at {
        font-weight: bold;
        text-align: center;
        width: 180px;
        margin:  0;
        padding:  0;
        list-style:  none;

      }
      .direct_message {
        font-weight: bold;
        text-align: center;
        width: 300px;
        margin:  0;
        padding:  0;
        list-style:  none;

      }
    </style>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
