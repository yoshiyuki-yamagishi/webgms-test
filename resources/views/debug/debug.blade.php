<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        デバッグ
      </h1>

      <!-- プレイヤー基本情報 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>プレイヤー基本情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
            <th class="base_id">ID</th>
              <th style="text-align: center;">レベル</th>
              <th style="text-align: center;">ログイン日</th>
              <th style="text-align: center;">チュートリアル進捗</th>
              <th style="text-align: center;">ゲーム開始日</th>
              <th style="text-align: center;">経験値</th>

              <th style="text-align: center;">P$</th>
              <th style="text-align: center;">欠片パウダー</th>
              <th style="text-align: center;">魔素</th>

              <th style="text-align: center;">フレンドポイント</th>
              <th style="text-align: center;">蒼の結晶（無償）</th>
              <th style="text-align: center;">蒼の結晶（有償）</th>
              <th style="text-align: center;">AL</th>

              <th style="text-align: center;">所持魔道書枠</th>
              <th style="text-align: center; width: 70px">更新</th>
            </tr>
            <tr id="2" style="background-color: #CEECF5;">
            {!! Form::model($player, array('action' => 'DebugController@index')) !!}
              <td>{!! Form::label('player_id', $player->id, ['class' => 'base_id']) !!}</td>
              <td>{!! Form::number('player_lv', $player->player_lv, ['class' => 'base_lv']) !!}</td>
              <td>{!! Form::text('last_login_at', $player->last_login_at, ['class' => 'base_last_login_at']) !!}</td>
              <td>{!! Form::number('tutorial_progress', $player->tutorial_progress, ['class' => 'base_last_login_at']) !!}</td>
              <td>{!! Form::text('first_login_at', $player->first_login_at, ['class' => 'base_first_login_at']) !!}</td>
              <td>{!! Form::number('experience', $player->experience, ['class' => 'base_experience']) !!}</td>

              <td>{!! Form::number('platinum_dollar', $player->platinum_dollar, ['class' => 'base_platinum_dollar']) !!}</td>
              <td>{!! Form::number('powder', $player->powder, ['class' => 'base_powder']) !!}</td>
              <td>{!! Form::number('magic_num', $player->magic_num, ['class' => 'base_magic_num']) !!}</td>

              <td>{!! Form::number('friend_point', $player->friend_point, ['class' => 'base_friend_point']) !!}</td>
              <td>{!! Form::number('free_crystal', $freeCrystal, ['class' => 'base_blue_crystal']) !!}</td>
              <td>{!! Form::number('charged_crystal', $chargedCrystal, ['class' => 'base_blue_crystal']) !!}</td>
              <td>{!! Form::number('al', $player->al, ['class' => 'base_al']) !!}</td>

              <td>{!! Form::number('max_grimoire', $player->max_grimoire, ['class' => 'base_max_grimoire']) !!}</td>
              <td style="text-align: center; width: 70px"><button name="modify_player" class="btn btn-success" type="submit">更新</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('max_al', $player->max_al) !!}
              {!! Form::close() !!}
            </tr>
          </table>
        </div>
      </p>

      <style>
      .base_id {
        font-weight: bold;
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_lv {
        font-weight: bold;
        width: 70px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_last_login_at {
        font-weight: bold;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_tutorial_progress {
        font-weight: bold;
        width: 170px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_first_login_at {
        font-weight: bold;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_experience {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }

      .base_platinum_dollar {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_powder {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_magic_num {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }



      .base_friend_point {
        font-weight: bold;
        width: 140px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }

      .base_blue_crystal {
        font-weight: bold;
        width: 130px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }

      .base_al {
        font-weight: bold;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_platinum_dollar {
        font-weight: bold;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .base_max_grimoire {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤーキャラクター -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>キャラクター情報</b>
          <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">キャラクターID</th>
            <th style="text-align: center;">キャラクターLv</th>
            <th style="text-align: center;">経験値</th>
            <th style="text-align: center;">オーブ1</th>
            <th style="text-align: center;">オーブ2</th>
            <th style="text-align: center;">オーブ3</th>
            <th style="text-align: center;">オーブ4</th>
            <th style="text-align: center;">オーブ5</th>
            <th style="text-align: center;">オーブ6</th>
            <th style="text-align: center;">グレード</th>
            <th style="text-align: center;">リピートボーナス</th>
            <th style="text-align: center;">AS1Lv</th>
            <th style="text-align: center;">AS2Lv</th>
            <th style="text-align: center; width: 70px">更新</th>
            <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($characters as $character)
          {!! Form::model($characters, array('action' => 'DebugController@index')) !!}
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::label('character_id', $character->character_id, ['class' => 'character_id']) !!}</td>
              <td>{!! Form::number('character_lv', $character->character_lv, ['class' => 'character_lv']) !!}</td>
              <td>{!! Form::number('experience', $character->experience, ['class' => 'character_experience']) !!}</td>
              <td>{!! Form::text('player_orb_id_1', $character->player_orb_id_1, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_2', $character->player_orb_id_2, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_3', $character->player_orb_id_3, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_4', $character->player_orb_id_4, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_5', $character->player_orb_id_5, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_6', $character->player_orb_id_6, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::number('grade', $character->grade, ['class' => 'character_grade']) !!}</td>
              <td>{!! Form::number('repeat', $character->repeat, ['class' => 'character_repeat']) !!}</td>
              <td>{!! Form::number('active_skill_1_lv', $character->active_skill_1_lv, ['class' => 'character_as']) !!}</td>
              <td>{!! Form::number('active_skill_2_lv', $character->active_skill_2_lv, ['class' => 'character_as']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" name="modify_character" type="submit">更新</button></td>
              <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_character" type="submit">削除</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('player_character_id', $character->id) !!}
            </tr>
        {!! Form::close() !!}
        @endforeach
        </table>
        </div>
        {{ $characters->appends(Request::except('page'))->links() }}
      </p>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>キャラクター一括設定</b>
          {!! Form::open(['action' => 'DebugController@index']) !!}
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">キャラクターLv</th>
              <th style="text-align: center;">経験値</th>
              <th style="text-align: center;">オーブ1</th>
              <th style="text-align: center;">オーブ2</th>
              <th style="text-align: center;">オーブ3</th>
              <th style="text-align: center;">オーブ4</th>
              <th style="text-align: center;">オーブ5</th>
              <th style="text-align: center;">オーブ6</th>
              <th style="text-align: center;">グレード</th>
              <th style="text-align: center;">リピートボーナス</th>
              <th style="text-align: center;">AS1Lv</th>
              <th style="text-align: center;">AS2Lv</th>
            </tr>
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::number('character_lv', null, ['class' => 'character_lv']) !!}</td>
              <td>{!! Form::number('experience', null, ['class' => 'character_experience']) !!}</td>
              <td>{!! Form::text('player_orb_id_1', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_2', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_3', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_4', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_5', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::text('player_orb_id_6', null, ['class' => 'character_orb']) !!}</td>
              <td>{!! Form::number('grade', null, ['class' => 'character_grade']) !!}</td>
              <td>{!! Form::number('repeat', null, ['class' => 'character_repeat']) !!}</td>
              <td>{!! Form::number('active_skill_1_lv', null, ['class' => 'character_as']) !!}</td>
              <td>{!! Form::number('active_skill_2_lv', null, ['class' => 'character_as']) !!}</td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
            </tr>
          </table>
          <div>
            ※値が空の場所は更新されません
          </div>
          <div>
            <button class="btn btn-success" name="modify_all_character" type="submit">一括更新</button>
            <button style="margin-left: 10px" class="btn btn-danger" name="delete_all_character" type="submit">一括削除</button>
          </div>
          {!! Form::close() !!}
        </div>
     </p>
      <style>
      .character_id {
        font-weight: bold;
        text-align: center;
        width: 120px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_lv {
        font-weight: bold;
        width: 120px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_experience {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_orb {
        font-weight: bold;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_grade {
        font-weight: bold;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_repeat {
        font-weight: bold;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .character_as {
        font-weight: bold;
        width: 50px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤー魔道書 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>魔道書情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">プレイヤー魔導書ID</th>
              <th style="text-align: center;">魔導書ID</th>
              <th style="text-align: center;">覚醒数</th>
              <th style="text-align: center;">スロット1</th>
              <th style="text-align: center;">スロット1解放</th>
              <th style="text-align: center;">スロット2</th>
              <th style="text-align: center;">スロット2解放</th>
              <th style="text-align: center;">スロット3</th>
              <th style="text-align: center;">スロット3解放</th>
              <th style="text-align: center;">ロック</th>
              <th style="text-align: center; width: 70px">更新</th>
              <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($grimoires as $grimoire)
          {!! Form::model($grimoires, array('action' => 'DebugController@index')) !!}
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::label('id', $grimoire->id, ['class' => 'grimoire_base_id']) !!}</td>
              <td>{!! Form::label('grimoire_id', $grimoire->grimoire_id, ['class' => 'grimoire_id']) !!}</td>
              <td>{!! Form::number('awake', $grimoire->awake, ['class' => 'grimoire_awake']) !!}</td>
              <td>{!! Form::text('slot_1', $grimoire->slot_1, ['class' => 'grimoire_orb_slot']) !!}</td>
              <td>{!! Form::number('slot_1_flag', $grimoire->slot_1_flag, ['class' => 'grimoire_slot_flag']) !!}</td>
              <td>{!! Form::text('slot_2', $grimoire->slot_2, ['class' => 'grimoire_orb_slot']) !!}</td>
              <td>{!! Form::number('slot_2_flag', $grimoire->slot_2_flag, ['class' => 'grimoire_slot_flag']) !!}</td>
              <td>{!! Form::text('slot_3', $grimoire->slot_3, ['class' => 'grimoire_orb_slot']) !!}</td>
              <td>{!! Form::number('slot_3_flag', $grimoire->slot_3_flag, ['class' => 'grimoire_slot_flag']) !!}</td>
              <td>{!! Form::number('lock_flag', $grimoire->lock_flag, ['class' => 'grimoire_lock_flag']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" name="modify_grimoire" type="submit">更新</button></td>
              <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_grimoire" type="submit">削除</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('player_grimoire_id', $grimoire->id) !!}
            </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
        {{ $grimoires->appends(Request::except('page'))->links() }}
      </p>

      <p style="margin-bottom:2em;">
      <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>魔導書一括設定</b>
        {!! Form::open(['action' => 'DebugController@index']) !!}
        <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">覚醒数</th>
            <th style="text-align: center;">スロット1</th>
            <th style="text-align: center;">スロット1解放</th>
            <th style="text-align: center;">スロット2</th>
            <th style="text-align: center;">スロット2解放</th>
            <th style="text-align: center;">スロット3</th>
            <th style="text-align: center;">スロット3解放</th>
            <th style="text-align: center;">ロック</th>
          </tr>
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::number('awake', null, ['class' => 'grimoire_awake']) !!}</td>
            <td>{!! Form::text('slot_1', null, ['class' => 'grimoire_orb_slot']) !!}</td>
            <td>{!! Form::number('slot_1_flag', null, ['class' => 'grimoire_slot_flag']) !!}</td>
            <td>{!! Form::text('slot_2', null, ['class' => 'grimoire_orb_slot']) !!}</td>
            <td>{!! Form::number('slot_2_flag', null, ['class' => 'grimoire_slot_flag']) !!}</td>
            <td>{!! Form::text('slot_3', null, ['class' => 'grimoire_orb_slot']) !!}</td>
            <td>{!! Form::number('slot_3_flag', null, ['class' => 'grimoire_slot_flag']) !!}</td>
            <td>{!! Form::number('lock_flag', null, ['class' => 'grimoire_lock_flag']) !!}</td>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          </tr>
        </table>
        <div>
          ※値が空の場所は更新されません
        </div>
        <div>
          <button class="btn btn-success" name="modify_all_grimoire" type="submit">一括更新</button>
          <button style="margin-left: 10px" class="btn btn-danger" name="delete_all_grimoire" type="submit">一括削除</button>
        </div>
        {!! Form::close() !!}
      </div>
      </p>

      <style>
        .grimoire_base_id {
        font-weight: bold;
        text-align: center;
        width: 150px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_id {
        font-weight: bold;
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_awake {
        font-weight: bold;
        width: 70px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_orb_slot {
        font-weight: bold;
        width: 90px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_slot_flag {
        font-weight: bold;
        width: 110px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_lock_flag {
        font-weight: bold;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .grimoire_lv {
        font-weight: bold;
        width: 80px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤーアイテム オーブ -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>オーブ情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">アイテムID</th>
              <th style="text-align: center;">個数</th>
              <th style="text-align: center; width: 70px">更新</th>
              <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($pOrbs as $pOrb)
          {!! Form::model($pOrbs, array('action' => 'DebugController@index')) !!}
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::label('item_id', $pOrb->item_id, ['class' => 'orb_id']) !!}</td>
              <td>{!! Form::number('num', $pOrb->num, ['class' => 'orb_num']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" name="modify_orb" type="submit">更新</button></td>
              <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_orb" type="submit">削除</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('player_item_id', $pOrb->id) !!}
            </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
        {{ $pOrbs->appends(Request::except('page'))->links() }}
      </p>

      <p style="margin-bottom:2em;">
      <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>オーブ一括設定</b>
        {!! Form::open(['action' => 'DebugController@index']) !!}
        <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">個数</th>
          </tr>
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::number('num', null, ['class' => 'orb_num']) !!}</td>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          </tr>
        </table>
        <br>
        <div>
          <button class="btn btn-success" name="modify_all_orb" type="submit">一括更新</button>
          <button style="margin-left: 10px" class="btn btn-danger" name="delete_all_orb" type="submit">一括削除</button>
        </div>
        {!! Form::close() !!}
      </div>
      </p>

      <style>
      .orb_id {
        font-weight: bold;
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .orb_num {
        font-weight: bold;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤーアイテム 欠片 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>欠片情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">アイテムID</th>
              <th style="text-align: center;">個数</th>
              <th style="text-align: center; width: 70px">更新</th>
              <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($pFragments as $pFragment)
          {!! Form::model($pFragments, array('action' => 'DebugController@index')) !!}
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::label('item_id', $pFragment->item_id, ['class' => 'fragment_id']) !!}</td>
              <td>{!! Form::number('num', $pFragment->num, ['class' => 'fragment_num']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" name="modify_fragment" type="submit">更新</button></td>
              <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_fragment" type="submit">削除</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('player_item_id', $pFragment->id) !!}
            </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>
        {{ $pFragments->appends(Request::except('page'))->links() }}
      </p>

      <p style="margin-bottom:2em;">
      <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>欠片一括設定</b>
        {!! Form::open(['action' => 'DebugController@index']) !!}
        <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">個数</th>
          </tr>
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::number('num', null, ['class' => 'orb_num']) !!}</td>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          </tr>
        </table>
        <br>
        <div>
          <button class="btn btn-success" name="modify_all_fragment" type="submit">一括更新</button>
          <button style="margin-left: 10px" class="btn btn-danger" name="delete_all_fragment" type="submit">一括削除</button>
        </div>
        {!! Form::close() !!}
      </div>
      </p>
      <style>
      .fragment_id {
        font-weight: bold;
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .fragment_num {
        font-weight: bold;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- プレイヤーアイテム その他 -->
      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>アイテム情報</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">アイテムID</th>
              <th style="text-align: center;">個数</th>
              <th style="text-align: center; width: 70px">更新</th>
              <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($pItems as $pItem)
          {!! Form::model($pItems, array('action' => 'DebugController@index')) !!}
            <tr id="2" style="background-color: #CEECF5;">
              <td>{!! Form::label('item_id', $pItem->item_id, ['class' => 'item_id']) !!}</td>
              <td>{!! Form::number('num', $pItem->num, ['class' => 'item_num']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" name="modify_item" type="submit">更新</button></td>
              <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="delete_item" type="submit">削除</button></td>
              {!! Form::hidden('action', 'do') !!}
              {!! Form::hidden('player_id', $player->id) !!}
              {!! Form::hidden('player_item_id', $pItem->id) !!}
            </tr>
          {!! Form::close() !!}
          @endforeach
        </table>
        </div>

        {{ $pItems->appends(Request::except('page'))->links() }}
      </p>
      <p style="margin-bottom:2em;">
      <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>アイテム一括設定</b>
        {!! Form::open(['action' => 'DebugController@index']) !!}
        <table border="1" height="50">
          <tr id="1" style="background-color: #87cefa;">
            <th style="text-align: center;">個数</th>
          </tr>
          <tr id="2" style="background-color: #CEECF5;">
            <td>{!! Form::number('num', null, ['class' => 'orb_num']) !!}</td>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          </tr>
        </table>
        <br>
        <div>
          <button class="btn btn-success" name="modify_all_item" type="submit">一括更新</button>
          <button style="margin-left: 10px" class="btn btn-danger" name="delete_all_item" type="submit">一括削除</button>
        </div>
        {!! Form::close() !!}
      </div>
      </p>
      <style>
      .item_id {
        font-weight: bold;
        text-align: center;
        width: 100px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .item_num {
        font-weight: bold;
        width: 60px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      </style>

      <!-- アイテム付与 -->

      <p class="row_top">
        <b style="margin-bottom: 10px;">アイテム付与</b>
        <div class="row">
          <b>
          キャラクター
          </b>
          {!! Form::model($rowCharacter, array('action' => 'DebugController@index')) !!}
            {{ Form::select('character_base_id', $charaNames, null, ['class' => 'row_character_name']) }}
            <button class="btn btn-success" name='row_chara' type="submit">付与</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          <br>
          <button class="btn btn-success" name="give_all_character" type="submit">一括付与</button>
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          魔導書
          </b>
          {!! Form::model($rowGrimoire, array('action' => 'DebugController@index')) !!}
            {{ Form::select('grimoire_id', $grimoireNames, null, ['class' => 'row_grimoire_name']) }}<br>
          個数{!! Form::number('num', 1, ['class' => 'row_item_num']) !!}
            <button class="btn btn-success" name="row_grimoire" type="submit">付与</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          <br>
          <br>
          ※個数の入力をしないと付与されません
          <br>
          <button class="btn btn-success" name="give_all_grimoire" type="submit">一括付与</button>

          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          オーブ
          </b>
          {!! Form::model($rowOrb, array('action' => 'DebugController@index')) !!}
            {{ Form::select('item_id', $orbNames, null, ['class' => 'row_item_name']) }}<br>
            個数{!! Form::number('num', 1, ['class' => 'row_item_num']) !!}
            <button class="btn btn-success" name="row_orb" type="submit">付与</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          <br>
          ※個数の入力をしないと付与されません
          <br>
          <button class="btn btn-success" name="give_all_orb" type="submit">一括付与</button>
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          欠片
          </b>
          {!! Form::model($rowFragment, array('action' => 'DebugController@index')) !!}
            {{ Form::select('item_id', $fragmentNames, null, ['class' => 'row_item_name']) }}<br>
            個数{!! Form::number('num', 1, ['class' => 'row_item_num']) !!}
            <button class="btn btn-success" name="row_fragment" type="submit">付与</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          <br>
          ※個数の入力をしないと付与されません
          <br>
          <button class="btn btn-success" name="give_all_fragment" type="submit">一括付与</button>
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          アイテム
          </b>
          {!! Form::model($rowItem, array('action' => 'DebugController@index')) !!}
            {{ Form::select('item_id', $itemNames, null, ['class' => 'row_item_name']) }}<br>
            個数{!! Form::number('num', 1, ['class' => 'row_item_num']) !!}
            <button class="btn btn-success" name="row_item" type="submit">付与</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          <br>
          ※個数の入力をしないと付与されません
          <br>
          <button class="btn btn-success" name="give_all_item" type="submit">一括付与</button>
          {!! Form::close() !!}
        </div>
      </p>

      <style>
      .row_top {
        margin-top: 20px;
      }
      .row_character_name {
        padding: 0;
        margin:  10px;
        padding:  0;
        list-style:  none;
      }
      .row_grimoire_name {
        padding: 0;
        margin:  10px;
        padding:  0;
        list-style:  none;
      }
      .row_item_name {
        padding-top: 20px;
        margin:  0;
        padding:  0;
        list-style:  none;
      }
      .row_item_num {
        width: 60px;
        padding: 0;
        margin:  10px;
        padding:  0;
        list-style:  none;
      }
      </style>

      <p style="margin-bottom:2em;">
        <div class="row">
          <b>
          ストーリークエスト進捗
          </b>
          {!! Form::model($storyQuestNames, array('action' => 'DebugController@index')) !!}
            ストーリークエスト名{{ Form::select('quest_name_id', $storyQuestNames, null, ['class' => 'quest_name']) }}<br>
            サブミッション1　{{Form::checkbox('mission_flag_1', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション2　{{Form::checkbox('mission_flag_2', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション3　{{Form::checkbox('mission_flag_3', 1, null, ['class' => 'mission_flag'])}}<br>
            クリア回数{!! Form::number('num', 0, ['class' => 'row_item_num']) !!}<br>
            <button class="btn btn-success" type="submit" name="story_prg" style="margin-top:1em;">ここまでクリア済にする</button>
            <button class="btn btn-warning" type="submit" name="delete_story" style="margin-top:1em; margin-left:2em">ここから未クリアにする</button><br>
            <button class="btn btn-danger" type="submit" name="delete_all_story" style="margin-top:1em;">すべて未クリアにする</button>
        {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          キャラクタークエスト進捗
          </b>
          {!! Form::model($characterQuestNames, array('action' => 'DebugController@index')) !!}
            キャラクタークエスト名{{ Form::select('quest_name_id', $characterQuestNames, null, ['class' => 'quest_name']) }}<br>
            サブミッション1　{{Form::checkbox('mission_flag_1', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション2　{{Form::checkbox('mission_flag_2', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション3　{{Form::checkbox('mission_flag_3', 1, null, ['class' => 'mission_flag'])}}<br>
            クリア回数{!! Form::number('num', 0, ['class' => 'row_item_num']) !!}<br>
          <button class="btn btn-success" type="submit" name="chara_story_prg" style="margin-top:1em;">ここまでクリア済にする</button>
          <button class="btn btn-warning" type="submit" name="delete_chara_story" style="margin-top:1em; margin-left:2em">ここから未クリアにする</button><br>
          <button class="btn btn-danger" type="submit" name="delete_all_chara_story" style="margin-top:1em;">すべて未クリアにする</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          イベントクエスト進捗
          </b>
          {!! Form::model($eventQuestNames, array('action' => 'DebugController@index')) !!}
            イベントクエスト名{{ Form::select('quest_name_id', $eventQuestNames, null, ['class' => 'quest_name']) }}<br>
            サブミッション1　{{Form::checkbox('mission_flag_1', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション2　{{Form::checkbox('mission_flag_2', 1, null, ['class' => 'mission_flag'])}}<br>
            サブミッション3　{{Form::checkbox('mission_flag_3', 1, null, ['class' => 'mission_flag'])}}<br>
            クリア回数{!! Form::number('num', 0, ['class' => 'row_item_num']) !!}<br>
            <button class="btn btn-success" type="submit" name="event_prg" style="margin-top:1em;">ここまでクリア済にする</button>
          <button class="btn btn-warning" type="submit" name="delete_event" style="margin-top:1em; margin-left:2em">ここから未クリアにする</button><br>
          <button class="btn btn-danger" type="submit" name="delete_all_event" style="margin-top:1em;">すべて未クリアにする</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          ミッション進捗
          </b>
          {!! Form::model($missionNames, array('action' => 'DebugController@index')) !!}
            ミッション名{{ Form::select('mission_id', $missionNames, null, ['class' => 'quest_name']) }}<br>
            進捗数{!! Form::number('num', 0, ['class' => 'row_item_num']) !!}
            <button class="btn btn-success" name="mission" type="submit">更新</button><br>
            <button class="btn btn-danger" name="mission_reset" type="submit">全リセット</button>
            {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          ログインボーナス進捗
          </b>
          {!! Form::model($loginBonusNames, array('action' => 'DebugController@index')) !!}
            ログインボーナス名{{ Form::select('id', $loginBonusNames, null, ['class' => 'quest_name']) }}
            <button class="btn btn-success" name="login" type="submit">更新</button><br>
            <button class="btn btn-danger" name="login_reset" type="submit">全リセット</button>
          {!! Form::hidden('action', 'do') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>

        <div class="row">
          <b>
          リセット
          </b>
          {!! Form::model($player, array('action' => 'DebugController@index')) !!}
            <button class="btn btn-success" type="submit">ショップ購入履歴</button>
            {!! Form::hidden('action', 'shop') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
          <br>
          {!! Form::model($player, array('action' => 'DebugController@index')) !!}
            <button class="btn btn-success" type="submit">ガチャ履歴</button>
            {!! Form::hidden('action', 'gatha') !!}
            {!! Form::hidden('player_id', $player->id) !!}
          {!! Form::close() !!}
        </div>
      </p>


      <style>
      .quest_name {
        margin:  10px;
        list-style:  none;
      }
      .mission_flag {
        margin-top: 10px;
        margin:  10px;
        list-style:  none;
      }
      div.row {
        margin-bottom:2em;
        margin-left:  20px;
        list-style:  none;
      }

      </style>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
