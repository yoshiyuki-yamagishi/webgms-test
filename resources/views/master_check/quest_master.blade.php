<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        クエストマスターチェック
      </h1>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow-y:auto;"><b>ストーリークエスト</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">クエストID</th>
              <th style="text-align: center;">チャプター名・ID</th>
              <th style="text-align: center;">ステージ名</th>
              <th style="text-align: center;">クエスト名</th>
              <th style="text-align: center;">種別</th>
              <th style="text-align: center;">ミッション</th>
              <th style="text-align: center;">初クリア報酬</th>
              <th style="text-align: center;">固定報酬</th>
              <th style="text-align: center;">バトルID</th>
              <th style="text-align: center;">詳細</th>
            </tr>
            @foreach ($storyQuests as $storyQuest)
            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($storyQuests, array('action' => 'QuestMasterDetailsController@index')) !!}
              <td>{!! Form::label('id', $storyQuest->id, ['class' => 'quest_id']) !!}</td>
              <td>
              <div>{!! Form::label('chapter_name', $storyQuest->story_quest_chapter_id, ['class' => 'chapter_name']) !!}</div>
              <div>{!! Form::label('chapter_name', $storyQuest->chapter_name, ['class' => 'chapter_name']) !!}</div>
              </td>
              <td>{!! Form::label('stage_name', $storyQuest->stage_name, ['class' => 'stage_name']) !!}</td>
              <td>{!! Form::label('story_quest_name', $storyQuest->story_quest_name, ['class' => 'quest_name']) !!}</td>
              <td>{!! Form::label('type', $storyQuest->type, ['class' => 'story_quest_type']) !!}</td>
              <td>
              <div>{!! Form::label('story_mission_name_01', $storyQuest->story_mission_name_01, ['class' => 'mission_name']) !!}</div>
              <div>{!! Form::label('story_mission_name_02', $storyQuest->story_mission_name_02, ['class' => 'mission_name']) !!}</div>
              <div>{!! Form::label('story_mission_name_03', $storyQuest->story_mission_name_03, ['class' => 'mission_name']) !!}</div>
              </td>

              <td>
              <div>{!! Form::label('', $storyQuest->first_reward_item_name, ['class' => 'quest_reward']) !!}</div>
              <div>{!! Form::label('', $storyQuest->first_reward_item_num, ['class' => 'quest_reward']) !!}</div>
              </td>

              <td>
              <div>{!! Form::label('', $storyQuest->fix_reward_item_name, ['class' => 'quest_reward']) !!}</div>
              <div>{!! Form::label('', $storyQuest->fix_reward_item_num, ['class' => 'quest_reward']) !!}</div>
              </td>

              <td>{!! Form::label('story_quest_battle_id', $storyQuest->story_quest_battle_id, ['class' => 'battle_id']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" type="submit">詳細</button></td>
              </tr>
              {!! Form::hidden('battle_id', $storyQuest->story_quest_battle_id) !!}
              {!! Form::hidden('quest_name', $storyQuest->story_quest_name) !!}
              {!! Form::hidden('id', $storyQuest->id) !!}
              {!! Form::hidden('type', 1) !!}
              {!! Form::close() !!}
              @endforeach

          </table>
        </div>
      </p>

      <style>
      .quest_id {
        width: 80px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .chapter_name {
        width: 150px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .character_chapter_name {
        width: 80px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .stage_name {
        width: 250px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .quest_name {
        width: 200px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .story_quest_type {
        width: 130px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .mission_name {
        width: 300px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .quest_reward {
        width: 150px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .battle_id {
        width: 70px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }


      </style>


      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;  overflow-y:auto;"><b>キャラクタークエスト</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th class="player_disp_id" style="text-align: center;">クエストID</th>
              <th class="player_disp_id" style="text-align: center;">チャプターID</th>
              <th class="player_disp_id" style="text-align: center;">クエスト名</th>
              <th class="player_name" style="text-align: center;">ミッション</th>
              <th class="player_name" style="text-align: center;">初クリア報酬</th>
              <th class="player_name" style="text-align: center;">固定報酬</th>
              <th class="player_name" style="text-align: center;">バトルID</th>
              <th class="player_lv" style="text-align: center;">詳細</th>
            </tr>
            @foreach ($characterQuests as $characterQuest)
            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($characterQuests, array('action' => 'QuestMasterDetailsController@index')) !!}
              <td>{!! Form::label('id', $characterQuest->id, ['class' => 'quest_id']) !!}</td>
              <td>{!! Form::label('character_quest_chapter_id', $characterQuest->character_quest_chapter_id, ['class' => 'character_chapter_name']) !!}</td>
              <td>{!! Form::label('quest_name', $characterQuest->quest_name, ['class' => 'quest_name']) !!}</td>
              <td>
              <div>{!! Form::label('character_mission_name_01', $characterQuest->character_mission_name_01, ['class' => 'mission_name']) !!}</div>
              <div>{!! Form::label('character_mission_name_02', $characterQuest->character_mission_name_02, ['class' => 'mission_name']) !!}</div>
              <div>{!! Form::label('character_mission_name_03', $characterQuest->character_mission_name_03, ['class' => 'mission_name']) !!}</div>
              </td>
              <td>
              <div>{!! Form::label('', $characterQuest->first_reward_item_name, ['class' => 'quest_reward']) !!}</div>
              <div>{!! Form::label('', $characterQuest->first_reward_item_num, ['class' => 'quest_reward']) !!}</div>
              </td>

              <td>
              <div>{!! Form::label('', $characterQuest->fix_reward_item_name, ['class' => 'quest_reward']) !!}</div>
              <div>{!! Form::label('', $characterQuest->fix_reward_item_num, ['class' => 'quest_reward']) !!}</div>
              </td>
              <td>{!! Form::label('character_quest_battle_id', $characterQuest->character_quest_battle_id, ['class' => 'battle_id']) !!}</td>
              <td style="text-align: center; width: 70px"><button class="btn btn-success" type="submit">詳細</button></td>
              </tr>
              {!! Form::hidden('battle_id', $characterQuest->character_quest_battle_id) !!}
              {!! Form::hidden('quest_name', $characterQuest->quest_name) !!}
              {!! Form::hidden('id', $characterQuest->id) !!}
              {!! Form::hidden('type', 2) !!}
              {!! Form::close() !!}
              @endforeach

          </table>
        </div>
      </p>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
