<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        キャラクターマスターチェック
      </h1>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px; overflow:scroll;"><b>キャラクターマスター</b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">画像</th>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">キャラクター名</th>
              <th style="text-align: center;">★・コスト</th>
              <th style="text-align: center;">コマンド排出率</th>
              <th style="text-align: center;">アクティブスキル・AH・DD</th>
              <th style="text-align: center;">パッシブスキル</th>
              <th style="text-align: center;">フレーバー</th>
              <th style="text-align: center; width: 60px">詳細</th>
            </tr>
            @foreach ($characters as $character)
            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($characters, array('action' => 'CharacterMasterDetailsController@index')) !!}
              <td><img src="{{$character->img}}" width="128"></td>
              <td>{!! Form::label('id', $character->id, ['class' => 'character_id']) !!}</td>
              <td>{!! Form::label('character_name', $character->character_name, ['class' => 'character_name']) !!}</td>
              <td>
              <div>{!! Form::label('character_rarity', $character->character_rarity, ['class' => 'character_rarity']) !!}</div>
              <div>{!! Form::label('character_rarity', $character->cost, ['class' => 'character_rarity']) !!}</div>
              </td>
              <td>
              <div>{!! Form::label('r_command', $character->r_command, ['class' => 'character_command']) !!}</div>
              <div>{!! Form::label('d_command', $character->d_command, ['class' => 'character_command']) !!}</div>
              <div>{!! Form::label('sp_command', $character->sp_command, ['class' => 'character_command']) !!}</div>
              </td>

              <td>
              <div>{!! Form::label('skill_1', $character->skill_1, ['class' => 'skill_name']) !!}</div>
              <div>{!! Form::label('skill_2', $character->skill_2, ['class' => 'skill_name']) !!}</div>

              <div>{!! Form::label('ah_name', $character->ah_name, ['class' => 'skill_name']) !!}</div>
              <div>{!! Form::label('dd_name', $character->dd_name, ['class' => 'skill_name']) !!}</div>
              </td>
              <td>
              <div>{!! Form::label('passive_name_1', $character->passive_name_1, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_2', $character->passive_name_2, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_3', $character->passive_name_3, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_4', $character->passive_name_4, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_5', $character->passive_name_5, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_6', $character->passive_name_6, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_7', $character->passive_name_7, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_8', $character->passive_name_8, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_9', $character->passive_name_9, ['class' => 'passive_name']) !!}</div>
              <div>{!! Form::label('passive_name_10', $character->passive_name_10, ['class' => 'passive_name']) !!}</div>
              </td>
              <td>{!! Form::label('character_flavor', $character->character_flavor, ['class' => 'character_flavor']) !!}</td>

              {!! Form::hidden('id', $character->id) !!}
              {!! Form::hidden('character_name', $character->character_name) !!}
              {!! Form::hidden('character_initial_rarity', $character->character_initial_rarity) !!}
              <td style="text-align: center; width: 70px"><button class="btn btn-success" type="submit">詳細</button></td>
              </tr>
              {!! Form::close() !!}
              @endforeach
          </table>
        </div>
      </p>

      <style>
      .character_id {
        width: 50px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .character_name {
        width: 300px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .character_rarity {
        width: 80px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .character_command {
        width: 100px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .skill_name {
        width: 300px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .passive_name {
        width: 200px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .character_flavor {
        width: 400px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }


      </style>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
