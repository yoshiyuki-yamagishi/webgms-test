<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ガチャ詳細
      </h1>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;"><b></b>
          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">バナー</th>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">ガチャ名</th>
              <th style="text-align: center;">期間</th>
              <th style="text-align: center;">消費アイテム・ID</th>
              <th style="text-align: center;">消費数</th>
            </tr>

            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($gacha, array('action' => 'GachaMasterDetailsController@index')) !!}
              <td><img src="{{$gacha->img}}" width="128"></td>
              <td>{!! Form::label('', $gacha->id, ['class' => 'gacha_id']) !!}</td>
              <td>{!! Form::label('', $gacha->gacha_name, ['class' => 'gacha_name']) !!}</td>
              <td>
              <div>{!! Form::label('', $gacha->release_day, ['class' => 'gacha_day']) !!}</div>
              <div>{!! Form::label('', $gacha->end_day, ['class' => 'gacha_day']) !!}</div>
              </td>
              <td>
              <div>{!! Form::label('', $gacha->item_id, ['class' => 'use_item_name']) !!}</div>
              <div>{!! Form::label('', $gacha->use_item_name, ['class' => 'use_item_name']) !!}</div>
              </td>
              <td>
              <div>{!! Form::label('', $gacha->single_item, ['class' => 'use_item_num']) !!}</div>
              <div>{!! Form::label('', $gacha->ten_item, ['class' => 'use_item_num']) !!}</div>
              </td>
              {!! Form::hidden('gacha_banner', $gacha->gacha_banner) !!}
              </tr>
              {!! Form::close() !!}
          </table>
        </div>
      </p>

      <style>
      .gacha_id {
        width: 50px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .gacha_name {
        width: 200px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .gacha_day {
        width: 200px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .use_item_name {
        width: 200px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .use_item_num {
        width: 140px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }

      </style>


      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 20px; top: 0px;"><b></b>
          <div>
            <p style="margin-bottom:2em;">
              <div>
              </div>
              <div class="row">
                <b>
                ガチャシミュレート
                </b>
                {!! Form::model($gachaGroupList, array('action' => 'GachaMasterDetailsController@index')) !!}
                <div>
                {{Form::radio('gacha_select', 'single', true,  ['class' => 'field'])}}単発
                </div>
                <div>
                {{Form::radio('gacha_select', 'ten', false,  ['class' => 'field'])}}10連
                </div>
                  <div style="margin-bottom:1em;">
                    ガチャ回数{!! Form::number('count', 1, ['class' => 'sim_num']) !!}
                  </div>
                  <div>
                  <td style="text-align: center; width: 70px"><button name="gacha_try" class="btn btn-success" type="submit">ガチャ実行</button></td>
                  </div>
                  {!! Form::hidden('id', $gacha->id) !!}
                  {!! Form::hidden('gacha_name', $gacha->gacha_name) !!}
                  {!! Form::hidden('release_day', $gacha->release_day) !!}
                  {!! Form::hidden('end_day', $gacha->end_day) !!}
                  {!! Form::hidden('action', 'gacha_result') !!}
                {!! Form::close() !!}
              </div>
            </p>
          </div>
        </div>
      </p>

      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;"><b>ガチャ結果</b>

          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
            <th class="player_disp_id" style="text-align: center;">排出アイテムID</th>
              <th class="player_id" style="text-align: center;">排出アイテム名</th>
              <th class="player_id" style="text-align: center;">排出数</th>
              <th class="player_name" style="text-align: center;">排出率</th>
            </tr>
            @foreach ($sum as $group)
              {!! Form::model($sum, array('action' => 'GachaMasterDetailsController@index')) !!}
              @foreach ($group as $key => $item)
                      <tr id="2" style="background-color: #CEECF5;">
                <td>{!! Form::label('', $key, ['class' => 'gacha_group_id']) !!}</td>
                <td>{!! Form::label('', $item['name'], ['class' => 'gacha_group_name']) !!}</td>
                <td>{!! Form::label('', $item['num'], ['class' => 'gacha_group_rate']) !!}</td>
                <td>{!! Form::label('', $item['rate'], ['class' => 'gacha_group_rate']) !!}</td>
              </tr>
              @endforeach
              {!! Form::close() !!}
              @endforeach

          </table>
        </div>
      </p>



      <p style="margin-bottom:2em;">
        <div style="position: relative; left: 0px; top: 0px;"><b>ラインナップ</b>

          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
            <th class="player_disp_id" style="text-align: center;">排出アイテムID</th>
              <th class="player_id" style="text-align: center;">排出アイテム名</th>
              <th class="player_name" style="text-align: center;">排出率</th>
              <th class="player_lv" style="text-align: center;">ステータス</th>
            </tr>
            @foreach ($gachaGroupList as $gachaGroup)
            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($gachaGroupList, array('action' => 'GachaMasterDetailsController@index')) !!}
              <td>{!! Form::label('', $gachaGroup->content_id, ['class' => 'gacha_group_id']) !!}</td>
              <td>{!! Form::label('', $gachaGroup->item_name, ['class' => 'gacha_group_name']) !!}</td>
              <td>{!! Form::label('', $gachaGroup->rate, ['class' => 'gacha_group_rate']) !!}</td>
              <td>
              <div>{!! Form::label('', $gachaGroup->hp, ['class' => 'gacha_group_status']) !!}</div>
              <div>{!! Form::label('', $gachaGroup->atk, ['class' => 'gacha_group_status']) !!}</div>
              <div>{!! Form::label('', $gachaGroup->def, ['class' => 'gacha_group_status']) !!}</div>
              </td>


              </tr>
              {!! Form::close() !!}
              @endforeach

          </table>
        </div>
      </p>

      <style>
      .gacha_group_id {
        width: 120px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .gacha_group_name {
        width: 300px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .gacha_group_rate {
        width: 50px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }
      .gacha_group_status {
        width: 100px;
        margin-left:  10px;
        padding:  0;
        list-style:  none;
      }

      </style>

      <p style="margin-bottom:3em;">
        <div style="position: relative; left: 0px; top: 0px;"><b>確定枠ラインナップ</b>

          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
            <th class="player_disp_id" style="text-align: center;">排出アイテムID</th>
              <th class="player_id" style="text-align: center;">排出アイテム名</th>
              <th class="player_name" style="text-align: center;">排出率</th>
              <th class="player_lv" style="text-align: center;">ステータス</th>
            </tr>
            @foreach ($confirmList as $confirm)
            <tr id="2" style="background-color: #CEECF5;">
              {!! Form::model($confirmList, array('action' => 'GachaMasterDetailsController@index')) !!}
              <td>{!! Form::label('', $confirm->content_id, ['class' => 'gacha_group_id']) !!}</td>
              <td>{!! Form::label('', $confirm->item_name, ['class' => 'gacha_group_name']) !!}</td>
              <td>{!! Form::label('', $confirm->rate, ['class' => 'gacha_group_rate']) !!}</td>
              <td>
              <div>{!! Form::label('', $confirm->hp, ['class' => 'gacha_group_status']) !!}</div>
              <div>{!! Form::label('', $confirm->atk, ['class' => 'gacha_group_status']) !!}</div>
              <div>{!! Form::label('', $confirm->def, ['class' => 'gacha_group_status']) !!}</div>
              </td>


              </tr>
              {!! Form::close() !!}
              @endforeach

          </table>
        </div>
      </p>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
