<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GMS/管理ツール</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-blue.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-red.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-green.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js) }}"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js) }}"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition fixed skin-green sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <!--a href="index2.html" class="logo" -->
    <a class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>メニュー</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account Menu -->

          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">

        </div>
        <div class="pull-left info">
          <p></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        </div>
      </form>
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">メインメニュー</li>
        <!-- Optionally, you can add icons to the links -->
        <li>{{ link_to_action('PlayerListController@index', 'プレイヤー一覧') }}</li>
        <li>{{ link_to_action('PlayerBanController@index', 'BAN管理') }}</li>
        <li>{{ link_to_action('SimulateController@index', 'ドロップシミュレート') }}</li>
        <li>{{ link_to_action('InfoController@index', 'お知らせ管理') }}</li>
        <li>{{ link_to_action('InfoRegisterController@index', 'お知らせ作成') }}</li>
        <li>{{ link_to_action('MaintenanceController@index', 'メンテナンス') }}</li>
        <li>{{ link_to_action('DirectPresentController@index', 'プレゼント補填') }}</li>
        <li>{{ link_to_action('ItemMasterController@index', 'アイテムマスター') }}</li>
        <li>{{ link_to_action('CharacterMasterController@index', 'キャラクターマスター') }}</li>
        <li>{{ link_to_action('GrimoireMasterController@index', '魔道書マスター') }}</li>
        <li>{{ link_to_action('SkillMasterController@index', 'スキルマスター') }}</li>
        <li>{{ link_to_action('GachaMasterController@index', 'ガチャマスター') }}</li>
        <li>{{ link_to_action('QuestMasterController@index', 'クエストマスター') }}</li>
        <li>{{ link_to_action('RewardMasterController@index', '報酬マスター') }}</li>
        <li>{{ link_to_action('Auth\RegisterController@showRegistrationForm', 'ユーザー登録') }}</li>
        <!--
        <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#">Link in level 2</a></li>
            <li><a href="#">Link in level 2</a></li>
          </ul>
        </li>
      </ul>
      -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        お知らせ管理
      </h1>

      <p style="margin-bottom:2em;">
          {{Form::open(['action' => 'InfoController@index'])}}
          <div>
            <div style="font-size: medium; font-weight: bold">
              絞り込み
            </div>
            <div>
              公開先
            </div>
            <div>
              {{Form::checkbox('platform1', 1, $narrowData['platform1'] ? true : false)}}：iOS
              {{Form::checkbox('platform2', 2, $narrowData['platform2'] ? true : false)}}：Android
              {{Form::checkbox('platform4', 4, $narrowData['platform4'] ? true : false)}}：公式
              <br>
              {{Form::checkbox('platform3', 3, $narrowData['platform3'] ? true : false)}}：iOS / Android
              {{Form::checkbox('platform5', 5, $narrowData['platform5'] ? true : false)}}：iOS / 公式
              {{Form::checkbox('platform6', 6, $narrowData['platform6'] ? true : false)}}：Android / 公式
              <br>
              {{Form::checkbox('platform7', 7, $narrowData['platform7'] ? true : false)}}：iOS / Android / 公式
            </div>
            <div>
              種別
            </div>
            <div>
              {{Form::radio('infoType', '0', $narrowData['infoType'] == 0 ? true: false)}}すべて
              {{Form::radio('infoType', '1', $narrowData['infoType'] == 1 ? true: false)}}お知らせ
              {{Form::radio('infoType', '2', $narrowData['infoType'] == 2 ? true: false)}}メンテナンス
              {{Form::radio('infoType', '3', $narrowData['infoType'] == 3 ? true: false)}}アップデート
              {{Form::radio('infoType', '4', $narrowData['infoType'] == 4 ? true: false)}}不具合報告
            </div>
            <button type="submit" class="btn btn-success">絞り込み</button>
          </div>
          {{Form::close()}}

        <br>

          <table border="1" height="50">
            <tr id="1" style="background-color: #87cefa;">
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">タイトル</th>
              <th style="text-align: center;">公開先</th>
              <th style="text-align: center;">種別</th>
              <th style="text-align: center;">優先度</th>
              <th style="text-align: center;">画像ファイル名</th>
              <th style="text-align: center;">掲載開始日時</th>
              <th style="text-align: center;">掲載終了日時</th>
              <th style="text-align: center; width: 70px">編集</th>
              <th style="text-align: center; width: 70px">削除</th>
            </tr>
          @foreach ($infos as $info)
            <tr id="2" style="background-color: #CEECF5;">
              <td style="text-align: center;">{!! $info->id !!}</td>
              <td style="text-align: center;">{!! $info->title !!}</td>
              <td style="text-align: center;">{!! $info->disp_platform_flag !!}</td>
              <td style="text-align: center;">{!! $info->disp_info_type !!}</td>
              <td style="text-align: center;">{!! $info->priority !!}</td>
              <td style="text-align: center;">{!! $info->image_name !!}</td>
              <td style="text-align: center;">{!! $info->started_at !!}</td>
              <td style="text-align: center;">{!! $info->expired_at !!}</td>
              @if($authLevel == config('const.AuthLevel.ADMIN'))
                <td style="text-align: center; width: 70px"><button type="button" class="btn btn-success" data-toggle="modal" data-target={{"#".$info->id}}>編集</button></td>
                {!! Form::model($info, ['action' => 'InfoController@index']) !!}
                {!! Form::hidden('id', $info->id) !!}
                <td style="text-align: center; width: 70px"><button class="btn btn-danger" name="action" type="submit" value="delete">削除</button></td>
                {!! Form::close() !!}

                <!-- 編集用モーダル -->
                <div class="modal fade" id={{$info->id}} data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="Modal">お知らせ編集</h5>
                      </div>
                      {!! Form::model($info,['action' => 'InfoController@index']) !!}
                      {!! Form::hidden('action', 'edit') !!}
                      {!! Form::hidden('id', $info->id) !!}
                      <div class="modal-body">
                        <div>
                          タイトル
                          {{Form::text('title')}}
                        </div>
                        <div>
                          お知らせ種別
                          {{ Form::select('info_type', [1 => 'お知らせ', 2 => 'メンテナンス', 3 =>  'アップデート', 4 =>  '不具合報告'], $info->info_type)}}
                        </div>
                        <div>
                          公開先
                          {{ Form::select('platform_flag', [1 => 'iOS', 2 => 'Android', 4 => '公式', 3 =>  'iOS / Android', 5 =>  'iOS / 公式', 6 => 'Android / 公式' ,7 => 'iOS / Android / 公式'], $info->platform_flag)}}
                        </div>
                        <div>
                          画像名
                          {{ Form::text('image_name')}}
                        </div>
                        <div>
                          優先度
                          {{ Form::text('priority')}}
                        </div>
                        <div>
                          開始日時
                          {{ Form::datetimeLocal('started_at',$info->editDisp_started_at)}}
                        </div>
                        <div>
                          終了日時
                          {{ Form::datetimeLocal('expired_at',$info->editDisp_expired_at)}}
                        </div>
                        <div>
                          本文
                        </div>
                        <div>
                          {{ Form::textarea('content')}}
                        </div>
                      </div>
                      <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
                        <button class="btn btn-primary" name="action" type="submit" value="preview" formtarget="_blank">プレビュー(別タブで開く)</button>
                        <button type="submit" class="btn btn-success">保存</button>
                      </div>
                      {{Form::close()}}
                    </div>
                  </div>
                </div>

              @endif
            </tr>
        {!! Form::close() !!}
        @endforeach
        </table>

        {{$infos->appends(request()->input())->links()}}
        </div>
      </p>

      <style>
      .info_list_type {
        text-align: center;
        width: 100px;
        padding:  0;
      }
      .info_list_date {
        text-align: center;
        width:  180px;
        padding:  0;
      }
      .info_list_delete {
        margin-left:  20px;
        padding:  0;
      }
      </style>

    </section>
    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>
