@extends('info.webview')


@section('content')
    <div class="content">
        {{-- 画像 --}}
        @if($info->image_name !== "")
            <div class="title logo">
                <img class="logo" src="{{ asset('img/'.$info->image_name) }}" alt="logo">
            </div>
        @endif

        <hr />

        {{-- タイトル --}}
        <div class="title m-b-md">
            {!! $info->title !!}
        </div>

        <hr />

        {{-- 本文 --}}
        <div class="m-b-md">
            {!! $info->content !!}
        </div>

        <hr />

        <div class="links">
            <a href="#" onclick="window.close()">プレビューを閉じる</a>
        </div>
    </div>
@endsection
