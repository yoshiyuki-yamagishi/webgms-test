<?php

namespace App\GmsModels;

use App\Models\BaseCommonModel;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * maintenance:メンテナンスのモデル
 *
 */
class Maintenance extends BaseCommonModel
{
	protected $table = 'maintenance';
	protected $primaryKey = 'id';
    const LIMIT = 50;
	/**
	 * お知らせ一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getAll()
	{
        $model =
			self::get();

		return $model;
	}


    /**
     * 登録
     *
     * @param $infoId
     * @param $message
     * @param $startAt
     * @param $endAt
     * @return boolean true:成功,false:失敗
     */
	public static function register($infoId, $message, $startAt, $endAt)
	{

		$model = new self();
		$model->info_id = $infoId;
		$model->message = $message;
		$model->start_at = $startAt;
		$model->end_at = $endAt;

		return $model->save();
	}


    /**
     * 最新からリミット分取得
     * @return bool
     */
    public static function getLimit()
    {

        $model = self::orderBy('id', 'desc')
            ->limit(self::LIMIT)
            ->get();
        return $model;
    }

    /**
     * １件物理削除(PK)
     * @param $id
     * @return void
     */
    public static function physicalDeleteByPK($id)
    {
        self::where('id', $id)
            ->delete();
        return;
    }

    /**
     * １件更新（PK）
     * @param $id
     * @param $infoId
     * @param $message
     * @param $startAt
     * @param $endAt
     * @return void
     */
    public static function updateByPK($id,$infoId,$message,$startAt,$endAt)
    {
        self::where('id', $id)
            ->update(['info_id' => $infoId,
                'message' => $message,
                'start_at' => $startAt,
                'end_at' => $endAt]);
        return;
    }

}
