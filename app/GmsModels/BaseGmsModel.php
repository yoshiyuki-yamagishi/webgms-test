<?php

namespace App\GmsModels;


use Illuminate\Support\Facades\DB;
use App\Models\BaseModel;

/**
 * Gmsモデルの基底クラス
 *
 */
class BaseGmsModel extends BaseModel
{
	public static function createConnectionName()
	{
		$connection = 'gms';
		return $connection;
	}

	public function getConnectionName()
	{
		$connection = self::createConnectionName();
		return $connection;
	}

	public static function beginTransaction()
	{
		DB::connection(self::createConnectionName())->beginTransaction();
	}

	public static function commit()
	{
		DB::connection(self::createConnectionName())->commit();
	}

	public static function rollBack()
	{
		DB::connection(self::createConnectionName())->rollBack();
	}
}