<?php

namespace App\GmsModels;

use App\Http\Responses\ApiResponse;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * info:お知らせ のモデル
 *
 */
class Info extends BaseGmsModel
{
	protected $table = 'info';
	protected $primaryKey = 'id';

	/**
	 * お知らせ一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getAll()
	{
        $model =
			self::get();

		return $model;
	}

	public static function getByType($i)
	{
        $model =
			self::where('info_id', $i)
			->get();

		return $model;
	}

	public static function getMaintenance()
	{
        $model =
			self::where('info_id', 2)
			->get();

		return $model;
	}

	public static function isInfoId($infoId)
	{
        $count =
			self::where('fname', $infoId)
			->count();

		return $count > 0;
	}


	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $characterId キャラクタID
	 * @param integer $characterLv キャラクタレベル
	 * @param integer $experience 経験値
	 * @return boolean true:成功,false:失敗
	 */
	public static function register(
        $infoId, $fname, $startedAt, $expiredAt, $priority, $title, $content
    )
	{
		// $now = DateTimeUtil::getNOW();
		$now = DateTimeUtil::getNOW();
        
		$model = new self();
		$model->info_id = $infoId;
		$model->fname = $fname;
		$model->started_at = $startedAt;
		$model->expired_at = $expiredAt;
		$model->priority = $priority;
		$model->title = $title;
		$model->content = $content;
		$model->created_at = $now;
        
		return $model->save();
	}
    
}
