<?php

namespace App\GmsModels;

use App\Models\BaseCommonModel;

use App\Utils\DateTimeUtil;

/**
 * DirectPresent:直接プレゼント のモデル
 *
 */
class DirectPresent extends BaseCommonModel
{
	protected $table = 'direct_present';
	protected $primaryKey = 'id';

	/**
	 * お知らせ一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getAll()
	{
        $model =
			self::get();

		return $model;
	}

	public static function getByType($i)
	{
        $model =
			self::where('info_id', $i)
			->get();

		return $model;
	}

	public static function getMaintenance()
	{
        $model =
			self::where('info_id', 2)
			->get();

		return $model;
	}

	public static function isInfoId($infoId)
	{
        $count =
			self::where('fname', $infoId)
			->count();

		return $count > 0;
	}


	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $characterId キャラクタID
	 * @param integer $characterLv キャラクタレベル
	 * @param integer $experience 経験値
	 * @return boolean true:成功,false:失敗
	 */
	public static function regist(
        $infoId, $fname, $startedAt, $expiredAt, $priority, $title, $content
    )
	{
		$now = DateTimeUtil::getNOW();

		$model = new self();
		$model->info_id = $infoId;
		$model->fname = $fname;
		$model->started_at = $startedAt;
		$model->expired_at = $expiredAt;
		$model->priority = $priority;
		$model->title = $title;
		$model->content = $content;
		$model->created_at = $now;

		return $model->save();
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $characterId キャラクタID
	 * @param integer $characterLv キャラクタレベル
	 * @param integer $experience 経験値
	 * @return boolean true:成功,false:失敗
	 */
	public static function register(
		$type,
		$playerId,
		$date1,
		$date2,
		$itemType,
		$itemId,
		$itemNum,
		$message,
		$takableDays,
		$startAt,
		$endAt
    )
	{
		$now = DateTimeUtil::getNOW();

		$model = new self();
		$model->type = $type;
		$model->player_id = $playerId;
		$model->date1 = $date1;
		$model->date2 = $date2;
		$model->item_type = $itemType;
		$model->item_id = $itemId;
		$model->item_num = $itemNum;
		$model->info_type = 4;
		$model->info_id = 0;
		$model->message = $message;
		$model->takable_days = $takableDays;
		$model->delete_flag = 0;
		$model->start_at = $startAt;
		$model->end_at = $endAt;
		$model->created_at = $now;

		return $model->save();
	}
    /**
     * 登録
     * @param array directPresentデーター
     * @return boolean
     */
    public static function multiInsert(
        $insert_records
    )
    {
        //インサート試し
        $model = new self();
        return $model->insert($insert_records);
    }
}
