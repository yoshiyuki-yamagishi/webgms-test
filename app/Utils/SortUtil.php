<?php
/**
 * ソート用のユーティリティクラス
 *
 */

namespace App\Utils;

/**
 * ソート用のユーティリティクラス
 *
 */
class SortUtil
{
	/**
	 * 比較
	 *
	 * @param object $a 比較変数1
	 * @param object $b 比較変数2
	 * @return 1, -1, 0
	 */
	public static function val_cmp($a, $b)
    {
        if ($a > $b)
            return 1;
        if ($a < $b)
            return -1;
        return 0;
    }
}
