<?php

namespace App\Models;


use Illuminate\Support\Facades\DB;

/**
 * Commonモデルの基底クラス
 *
 */
class BaseCommonModel extends BaseModel
{
	public static function createConnectionName()
	{
		$connection = 'common';
		return $connection;
	}

	public function getConnectionName()
	{
		$connection = self::createConnectionName();
		return $connection;
	}

	public static function beginTransaction()
	{
		DB::connection(self::createConnectionName())->beginTransaction();
	}

	public static function commit()
	{
		DB::connection(self::createConnectionName())->commit();
	}

	public static function rollBack()
	{
		DB::connection(self::createConnectionName())->rollBack();
	}
}