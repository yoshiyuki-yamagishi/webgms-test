<?php

namespace App\Models;



use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_login_point:プレイヤログイン のモデル
 *
 */
class PlayerLoginPoint extends BaseGameModel
{
    const TYPE_FRIEND = 1; // フレンドポイント
    
	protected $table = 'player_login_point';
	protected $primaryKey = 'id';
// 	public $incrementing = false;
// 	public $timestamps = false;

    /*
	protected $table = 'Business';
	protected $primaryKey = 'id';
	//public $incrementing = false;
	//public $timestamps = false;

	protected $fillable = [
		//'id',
	];
	*/

	/**
	 * ログインを登録する
	 *
	 * @param integer $playerId プレイヤID
	 * @return boolean true: 既に付与済み
	 */
    public static function alreadyGive($playerId, $type, $now)
	{
        $start = DateTimeUtil::loginPointStartDate($now);
        
        $count =
               self::where('player_id', $playerId)
               ->where('type', $type)
               ->where('created_at', '>=', $start)
               ->count();
        
        return $count > 0;
	}

}
