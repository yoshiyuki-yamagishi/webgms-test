<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * item:アイテム のモデル
 *
 */
class Item extends BaseMasterModel
{
	protected $table = 'item';
	protected $primaryKey = 'id';

	const TYPE_CHARACTER = 1;
	const TYPE_GRIMOIRE = 2;
	const TYPE_ITEM = 3;

    const ID_FREE_BLUE_CRYSTAL = 1000000;
    const ID_SKIP_TICKET = 5000000;
    const ID_FRIEND_POINT = 8000000;
    const ID_BLUE_CRYSTAL = 9000000;
    const ID_P_DOLLAR = 10000000;
    const ID_POWDER = 12000000;
	const ID_ELEMENT = 11000000;
    
    const CATEGORY_RANGE = 1000000;
	const CATEGORY_FREE_BLUE_CRYSTAL = 1; // 無償蒼の結晶
	const CATEGORY_TICKET = 2; // ガチャチケット
	const CATEGORY_EXP = 3; // 経験値アイテム
	const CATEGORY_ORB = 4; // オーブ
	const CATEGORY_SKIP_TICKET = 5; // スキップチケット
	const CATEGORY_FRAGMENT = 6; // キャラクター欠片
	const CATEGORY_AL_RECOVER = 7; // 回復アイテム
	const CATEGORY_FRIEND_POINT = 8; // フレンドポイント
	const CATEGORY_BLUE_CRYSTAL = 9; // 有償青の結晶
	const CATEGORY_P_DOLLAR = 10; // P$
	const CATEGORY_ELEMENT = 11; // 魔素
	const CATEGORY_POWDER = 12; // 欠片パウダー
	const CATEGORY_EVENT = 13; // イベントアイテム

    // 汎用欠片は、下記 6 種類で、下 2 桁が属性
    // 6999901 - 6999906
    const GENERAL_PIECE_MOD = 100;
    const GENERAL_PIECE_MOD_ID = 69999;

	/**
	 * 全アイテムのリストを返す
	 *
	 * @return array App/Models/Thrift/Item アイテムの配列
	 */
    public static function getAll()
    {
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
    }
    
	/**
	 * アイテムIDから、カテゴリを計算する
	 *
	 * @param integer $id アイテムID
	 * @return integer アイテムカテゴリ
	 */
    public static function categoryFromId($id)
    {
        return intdiv($id, self::CATEGORY_RANGE);
    }

	/**
	 * 欠片のアイテムIDからキャラクターIDを取得
	 */
    public static function characterIdFrom($id)
    {
        $category = static::categoryFromId($id);
        if ($category != 6)
        {
            throw \App\Exceptions\GameException::make(
                'specified item_id is not fragment: ' . $id
            );
        }

        return $id % static::CATEGORY_RANGE;
    }

    public static function getByCategory($category, $orderBy = null)
    {
        $min = $category * self::CATEGORY_RANGE;
        $max = $min + self::CATEGORY_RANGE - 1;

        $all = self::getAll();
        $list = [];

        foreach ($all as $item)
        {
            if ($item->id >= $min && $item->id <= $max)
                $list[] = $item;
        }
        
        if (isset($orderBy))
        {
            usort($list, function ($a, $b) {
                return SortUtil::val_cmp($a->$orderBy, $b->$orderBy);
            });     
        }

        return $list;
    }

	/**
	 * アイテム種別の有効性判定
	 *
	 * @param integer $type アイテム種別
	 * @return true: 有効
	 */
    public static function isValidType($type)
    {
        switch ($type)
        {
        case Item::TYPE_CHARACTER:
        case Item::TYPE_GRIMOIRE:
        case Item::TYPE_ITEM:
            return true;
        }
        return false;
    }
    
}
