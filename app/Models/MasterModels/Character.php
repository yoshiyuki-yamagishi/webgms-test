<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;


/**
 * character:キャラクター のモデル
 *
 */
class Character extends BaseMasterModel
{
	protected $table = 'character';
	protected $primaryKey = 'id';

    const MAX_RARITY = 7;

    // [初期レア, 進化度] -> スパイン段階
    static $SPINE_LV_TABLE = [
        [ 0, 0, 0, 0, 1, 2, 2 ], // 初期レア 1
        [ 0, 0, 0, 1, 2, 2, 2 ], // 初期レア 2
        [ 0, 0, 1, 2, 2, 2, 2 ], // 初期レア 3
        [ 0, 1, 2, 2, 2, 2, 2 ], // 初期レア 4
    ];
    // [初期レア, 進化度] -> イラスト段階
    static $ILLUST_LV_TABLE = [
        [ 0, 0, 0, 0, 1, 2, 3 ], // 初期レア 1
        [ 0, 0, 0, 1, 2, 3, 3 ], // 初期レア 2
        [ 0, 0, 1, 2, 3, 3, 3 ], // 初期レア 3
        [ 0, 1, 2, 3, 3, 3, 3 ], // 初期レア 4
    ];
    
	/**
	 * キャラクタIDからシリーズIDを取得
	 */
	public static function seriesIdFrom($characterId)
	{
        return intdiv($characterId, 100);
	}
    
	/**
	 * キャラクタIDから欠片IDを取得
	 */
    public static function fragmentIdFrom($characterId)
    {
        return Item::CATEGORY_FRAGMENT * Item::CATEGORY_RANGE
            + $characterId;
    }
    
	/**
	 * キャラクター段階を計算する
	 * @param $table 対応表
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer キャラクター段階
	 */
    public static function calcReleaseLv(&$table, $initialRarity, $evolve)
    {
        if ($initialRarity < 1 || $initialRarity > 4)
        {
            throw \App\Exceptions\MasterException::makeInvalid(
                'character_base', 'character_initial_rarity', $initialRarity
            );
        }
        if ($evolve < 0 && $evolve > 6)
        {
            throw \App\Exceptions\DataException::makeInvalid(
                'player_character', 'evolve', $evolve
            );
        }

        return $table[$initialRarity - 1][$evolve];
    }

	/**
	 * スパイン開放段階を計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer スパイン開放段階(0-)
	 */
    public static function calcSpineLv($initialRarity, $evolve)
    {
        return self::calcReleaseLv(
            self::$SPINE_LV_TABLE, $initialRarity, $evolve
        );
    }

	/**
	 * イラスト開放段階を計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer イラスト開放段階(0-)
	 */
    public static function calcillustLv($initialRarity, $evolve)
    {
        return self::calcReleaseLv(
            self::$ILLUST_LV_TABLE, $initialRarity, $evolve
        );
    }
    
	/**
	 * スパインIDを計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer スパインID
	 */
    public static function calcSpineId($characterId, $initialRarity, $evolve)
    {
        return $characterId * 10 + self::calcSpineLv($initialRarity, $evolve);
    }
    
	/**
	 * イラストIDを計算する
	 * @param integer $initialRarity(1-)
	 * @param integer $evolve 進化度(0-)
	 * @return integer イラストID
	 */
    public static function calcIllustId($characterId, $initialRarity, $evolve)
    {
        return $characterId * 10 + self::calcIllustLv($initialRarity, $evolve);
    }
}
