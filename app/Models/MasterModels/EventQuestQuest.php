<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;

/**
 * event_quest_quest:イベントクエストクエスト のモデル
 *
 */
class EventQuestQuest extends BaseMasterModel
{
	protected $table = 'event_quest_quest';
    protected $primaryKey = ['id', 'event_id'];
    
	/**
	 * イベント毎取得
	 *
	 * @return App/Models/Thrift/EventQuestQuest クエストの配列
	 */
	public static function getByChapterId($chapterId)
	{
        $_this = new self();
        return self::_getAllEx(
			$_this->table,
			['event_id'],
			[$chapterId]
        );
	}

	/**
	 * 取得
	 *
	 * @return App/Models/Thrift/EventQuestQuest クエストの配列
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
			$_this->table
        );
	}
    
}
