<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Services\GiveOrPayParam;
use App\Http\Responses\GachaResultResponse;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * gachaGroup:ガチャグループ のモデル
 *
 */
class GachaGroup extends BaseMasterModel
{
	protected $table = 'gacha_group';
	// protected $primaryKey = '';

	/**
	 * ガチャグループ取得
	 *
	 * @param string $groupId ガチャグループID
	 * @return array ガチャグループの配列
	 */
	public static function getAll($groupId)
	{
        $_this = new self();
        $all = static::_getAllEx(
            $_this->table, ['id'], [$groupId]
        );

        usort($all, function ($a, $b) {
            return - SortUtil::val_cmp($a->priority, $b->priority);
        });

        return $all;
	}


	/**
	 * ガチャ結果の計算
	 *
	 * @param string $groupId ガチャグループID
	 * @return GachaResultResponse ガチャ結果のレスポンス
	 */
	public static function calcResultResponse($gachaGroup)
	{
        // 1 ガチャ、1 アイテムを想定している //

        if (!Item::isValidType($gachaGroup->content_type))
        {
            throw \App\Exceptions\MasterException::make(
                'gacha_group invalid content_type: ' . $gachaGroup->content_type
            );
        }
        
        return GachaResultResponse::make(
            $gachaGroup->content_type,
            $gachaGroup->content_id,
            $gachaGroup->get_count,
            GiveOrPayParam::TAKE_FLAG_NONE
        );
    }
    
}
