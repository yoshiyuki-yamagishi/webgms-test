<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * character_quest_battle:ストーリクエスト バトル のモデル
 *
 */
class CharacterQuestBattle extends BaseMasterModel
{
	protected $table = 'character_quest_battle';
    protected $primaryKey = ['id','wave','position'];

	/**
	 * 全取得
	 *
	 * @param integer $battleId バトルID
	 * @return array App/Models/Thrift/CharacterQuestBattle バトルのリスト
	 */
	public static function getAll($battleId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$battleId]
        );
	}
    
}
