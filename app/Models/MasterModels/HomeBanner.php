<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * home_banner:ホームバナー のモデル
 *
 */
class HomeBanner extends BaseMasterModel
{
	protected $table = 'home_banner';
	protected $primaryKey = ['id'];

	/**
	 * 全取得
	 *
	 * @param string $now 現在時刻
	 * @return array App/Models/Thrift/HomeBanner ホームバナー
	 */
	public static function getAll($now)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        usort($all, function ($a, $b)
        {
            $cmp = SortUtil::val_cmp($a->banner_type, $b->banner_type);
            if ($cmp != 0)
                return $cmp;
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        $ret = [];
        $_now = new \DateTime($now);
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;

            $ret[] = $item;
        }

        return $ret;
	}
    
}
