<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * item_dictionary:アイテムテキストのモデル
 *
 */
class ItemDictionary extends BaseMasterModel
{
	protected $table = 'item_dictionary';
	protected $primaryKey = 'id';

}
