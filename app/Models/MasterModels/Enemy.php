<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * enemy:エネミー のモデル
 *
 */
class Enemy extends BaseMasterModel
{
	protected $table = 'enemy';
	protected $primaryKey = 'id';

}
