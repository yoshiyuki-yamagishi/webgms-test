<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;

/**
 * grimoire_awake_coefficient:魔道書覚醒段階係数 のモデル
 *
 */
class GrimoireAwakeCoefficient extends BaseMasterModel
{
	protected $table = 'grimoire_awake_coefficient';
	protected $primaryKey = 'grimoire_awake';

	/**
	 * 全取得
	 *
	 * @return array 魔道書覚醒段階係数の配列
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}
    
}
