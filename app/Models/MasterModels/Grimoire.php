<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;


/**
 * grimoire:魔道書 のモデル
 *
 */
class Grimoire extends BaseMasterModel
{
    const CATEGORY_NORMAL = 1;
    const CATEGORY_AWAKE = 2;
    
    protected $table = "grimoire";
    protected $primaryKey = 'id';
    
    /**
	 * 全魔道書の取得
	 *
	 * @return App/Models/Thrift/Grimoire 魔道書のリスト
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}

    /**
	 * 覚醒専用か？
	 *
	 * @param object $grimoire 魔道書
	 * @return bool true: 覚醒専用
	 */
	public static function forAwake($grimoire)
	{
        return $grimoire->category == self::CATEGORY_AWAKE;
	}
    
}
