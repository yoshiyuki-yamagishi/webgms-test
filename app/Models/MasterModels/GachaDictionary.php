<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * gacha_dictionary:ガチャテキストのモデル
 *
 */
class GachaDictionary extends BaseMasterModel
{
	protected $table = 'gacha_name';
	protected $primaryKey = 'id';

}
