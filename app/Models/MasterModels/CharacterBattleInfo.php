<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;


/**
 * character_battle_info:キャラクターバトル情報 のモデル
 *
 */
class CharacterBattleInfo extends BaseMasterModel
{

    protected $table = "character_battle_info";
    protected $primaryKey = 'id';
    
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll(
            $_this->table
        );
	}
    
}
