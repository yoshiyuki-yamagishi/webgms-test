<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * gacha:ガチャ のモデル
 *
 */
class Gacha extends BaseMasterModel
{
	protected $table = 'gacha';
	protected $primaryKey = 'id';

    // ガチャ種別
    const TYPE_BLUE_CRYSTAL = 1;
    const TYPE_TICKET = 2;
    const TYPE_FRIEND_POINT = 3;

    // 引き直しフラグ
    const LOOP_CAN = 1;
    const LOOP_NO = 2;

    // 回数制限タイプ
    const LIMIT_TYPE_NONE = -1; // 負の場合
    const LIMIT_TYPE_PERIOD = 0; // 期間中
    const LIMIT_TYPE_DAY = 1; // 一日毎

    // NEW 判定に使用する日数
    const NEW_DAYS = 30;
    
	/**
	 * ガチャ一覧取得
	 *
	 * @param string $now 現在時刻
	 * @return array ガチャのリスト
	 */
	public static function getAll($now)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        usort($all, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        $ret = [];
        $_now = new \DateTime($now);
        foreach ($all as $item)
        {
            $releaseDay = new \DateTime($item->release_day);
            $endDay = new \DateTime($item->end_day);

            if ($releaseDay > $_now)
                continue;
            if ($endDay < $now)
                continue;

            $ret[] = $item;
        }

        return $ret;
    }

    /**
	 * 全ガチャ一覧取得
	 *
	 * @return array ガチャのリスト
	 */
	public static function getAllRaw()
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        usort($all, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        return $all;
    }

    /**
	 * ガチャ種別ごとの一覧取得
     * 
	 *@param string $type ガチャ種別
	 * @return array ガチャのリスト
	 */
	public static function getByType($type)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        usort($all, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        return $all;
    }

	/**
	 * 新しいガチャがあるか判定する
	 *
	 * @param string $now 現在時刻
	 * @return boolean true: 新しいガチャがある
	 */
	public static function isNew($now)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);

        $_now = new \DateTime($now);
        
        $newFrom = new \DateTime($now);
        $newFrom->modify('-' . self::NEW_DAYS . ' day');
        
        foreach ($all as $item)
        {
            $releaseDay = new \DateTime($item->release_day);
            $endDay = new \DateTime($item->end_day);

            if ($releaseDay > $_now)
                continue;
            if ($endDay < $now)
                continue;
            if ($releaseDay >= $newFrom)
                return true; // 新しいのが見つかったので終了
        }

        return false;
    }

	 /**
	 * 引き直し可能か？
	 *
	 * @param App/Models/Thrift/Gacha $gacha ガチャ
	 * @return boolean true 引き直し可能
	 */
    public static function canLoop($gacha)
    {
        return $gacha->gacha_loop == Gacha::LOOP_CAN;
    }

	 /**
	 * 引き直し可能か？
	 *
	 * @param App/Models/Thrift/Gacha $gacha ガチャ
	 * @param integer $requestCount 指定の回数
	 * @return integer 実際に回せる回数
	 */
    public static function calcRealCount($gacha, $requestCount)
    {
        if ($requestCount == 1 && $gacha->single_item_get_count > 0)
            return $gacha->single_item_get_count;
        if ($requestCount == 10 && $gacha->ten_item_get_count > 0)
            return $gacha->ten_item_get_count;

        return $requestCount;
    }
    
}
