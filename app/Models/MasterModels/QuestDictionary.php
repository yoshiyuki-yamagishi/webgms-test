<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * quest_dictionary:クエストテキストのモデル
 *
 */
class QuestDictionary extends BaseMasterModel
{
	protected $table = 'quest_dictionary';
	protected $primaryKey = 'id';

}
