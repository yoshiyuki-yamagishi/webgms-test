<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * product_shop:製品のモデル
 *
 */
class ProductShop extends BaseMasterModel
{
	protected $table = 'product_shop';
	protected $primaryKey = 'id';

    const DAILY_FLG_NORMAL = 1;
    const DAILY_FLG_DAILY = 2;

    const CATEGORY_ALL = 0;
    const CATEGORY_DLG = 1;
    const CATEGORY_SHOP = 2;
    const CATEGORY_BOTH = 3;

	/**
	 * 製品一覧取得
	 *
	 * @param string $now 現在日付
	 * @param integer $category カテゴリ
	 * @return array App/Models/Thrift/ProductShop の配列
	 */
	public static function getAll($now, $category = null)
	{
        if (is_null($category))
            $category = 0;

        $_this = new self();
        $all = self::_getAll(
            $_this->table
        );

        $_now = new \DateTime($now);

        $ret = [];
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;

            if ($category > 0 &&
                $item->product_category != self::CATEGORY_BOTH &&
                $item->product_category != $category)
                continue;
            
            $ret[] = $item;
        }
        
        usort($ret, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        return $ret;
	}

	/**
	 * デイリーパックとして正しいデータかチェックする
	 *
	 * @param App/Models/Thrift/ProductShop $model
	 */
	public static function checkDaily($model)
	{
        // daily_flg は 2 のはずだが、無くてもよい列なので無視する
        
        if ($model->grant_count <= 0) // 致命的なエラー
        {
            throw \App\Exceptions\MasterException::make(
                'product_shop grant_count must be positive for contents'
            );
        }
    }
    
}
