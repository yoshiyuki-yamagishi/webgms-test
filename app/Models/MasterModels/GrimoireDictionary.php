<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * grimoire_dictionary:魔導書テキストのモデル
 *
 */
class GrimoireDictionary extends BaseMasterModel
{
	protected $table = 'grimoire_name';
	protected $primaryKey = 'id';

}
