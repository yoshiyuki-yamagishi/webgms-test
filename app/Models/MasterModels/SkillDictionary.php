<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * character_dictionary:スキルテキストのモデル
 *
 */
class SkillDictionary extends BaseMasterModel
{
	protected $table = 'skill_dictionary';
	protected $primaryKey = 'id';

}
