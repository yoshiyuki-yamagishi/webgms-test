<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * quest_reward:クエスト報酬 のモデル
 *
 */
class QuestReward extends BaseMasterModel
{
	protected $table = 'quest_reward';
	protected $primaryKey = ['id', 'quest_reward_frame_count'];

	/**
	 * 全取得
	 *
	 * @param integer $questRewardId クエスト報酬ID
	 * @return array App/Models/Thrift/QuestReward クエスト報酬のリスト
	 */
	public static function getAll($id)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['id'], [$id]
        );
	}

	/**
	 * 全取得
	 *
	 * @param integer $questRewardId アイテムID
	 * @return array App/Models/Thrift/QuestReward クエスト報酬のリスト
	 */
	public static function getAllByItemId($id)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['quest_reward_item_id'], [$id]
        );
	}


}