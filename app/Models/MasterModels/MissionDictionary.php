<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * mission_dictionary:ミッションテキストのモデル
 *
 */
class MissionDictionary extends BaseMasterModel
{
	protected $table = 'mission_dictionary';
	protected $primaryKey = 'id';

}
