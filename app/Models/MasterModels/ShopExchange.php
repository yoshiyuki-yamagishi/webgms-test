<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\SortUtil;

/**
 * shopExchange:ショップ交換所のモデル
 *
 */
class ShopExchange extends BaseMasterModel
{
	protected $table = 'shop_exchange';
	protected $primaryKey = 'id';

    const CATEGORY_ITEM = 1;
    const CATEGORY_GRIMOIRE = 2;

	/**
	 * ショップ交換所リスト取得
	 *
	 * @param integer $shopId ショップカテゴリー
	 * @return array 本クラスの配列
	 */
	public static function getByShopId($shopId)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table, ['shop_id'], [$shopId]
        );
	}

	/**
	 * 表示順にソートする
	 * @param array $items ShopExchange マスタの配列
	 */
    public static function sort(&$items)
    {
        usort($items, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });
    }
    
	/**
	 * 取得
	 * @param integer $shop shop マスタ
	 * @return array 本クラスの配列
	 */
    public static function calcLineup($shop)
    {
		$all = self::getByShopId($shop->id);
        
        $limit = $shop->linenup_limit;
        if ($limit <= 0)
        {
            self::sort($all);
            return $all;
        }

        // リミットあり //

        $fixedItems = [];
        $randomItems = [];

        foreach ($all as $item)
        {
            if ($item->fixed_item > 0)
                $fixedItems[] = $item;
            else
                $randomItems[] = $item;
        }

        // 返り値

        $ret = [];
        
        // 固定アイテムを優先度の順番に追加する //

        usort($fixedItems, function ($a, $b) {
            return - SortUtil::val_cmp($a->priority, $b->priority);
        });

        $count = 0;

        foreach ($fixedItems as $item)
        {
            if ($count >= $limit)
                break;
            $ret[] = $item;
            ++ $count;
        }

        // ランダムアイテムを追加する //

        shuffle($randomItems);

        foreach ($randomItems as $item)
        {
            if ($count >= $limit)
                break;
            $ret[] = $item;
            ++ $count;
        }

        self::sort($ret);
        return $ret;
    }

	/**
	 * アイテム種別を取得
	 *
	 * @param object $shopExchange Thrift/ShopExchange
	 * @return integer Item::TYPE_XXX
	 */
	public static function itemType($shopExchange)
	{
        switch ($shopExchange->lineup_category)
        {
        case self::CATEGORY_ITEM:
            return Item::TYPE_ITEM;
        case self::CATEGORY_GRIMOIRE:
            return Item::TYPE_GRIMOIRE;
        }
        throw \App\Exceptions\MasterException::make(
            'unknown shop_exchange::lineup_category: '
            . $shopExchange->lineup_category
        );
	}

}
