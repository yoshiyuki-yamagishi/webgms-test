<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\DebugUtil;

/**
 * flavor_dictionary:フレーバーテキストのモデル
 *
 */
class FlavorDictionary extends BaseMasterModel
{
	protected $table = 'flavor';
	protected $primaryKey = 'id';

}
