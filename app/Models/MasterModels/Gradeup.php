<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * gradeup:オーブ装備のモデル
 *
 */
class Gradeup extends BaseMasterModel
{
    const UP_TYPE_HP = 1;
    const UP_TYPE_ATK = 2;
    const UP_TYPE_DEF = 3;
    
	protected $table = 'gradeup';
	protected $primaryKey = ['id', 'gradeup_stage'];

	/**
	 * オーブ装備リスト取得
	 *
	 * @param integer $id オーブ装備ID
	 * @return array オーブ装備の配列
	 */
	public static function getAll($id)
	{
        $_this = new self();
        $ret = self::_getAllEx(
            $_this->table,
            ['id'],
            [$id]
        );

        usort($ret, function ($a, $b) {
            return SortUtil::val_cmp($a->gradeup_stage, $b->gradeup_stage);
        });

        return $ret;
	}

	/**
	 * パラメータ上昇の文字列キーを取得
	 *
	 * @param integer $upType パラメータ上昇タイプ
	 * @return string パラメータ上昇キー
	 */
	public static function upTypeToKey($upType)
	{
        switch ($upType)
        {
        case self::UP_TYPE_HP:
            return 'hp';
        case self::UP_TYPE_ATK:
            return 'atk';
        case self::UP_TYPE_DEF:
            return 'def';
        }
        
        throw \App\Exceptions\MasterException::makeInvalid(
            'gradeup', 'up_type', $upType
        );
    }
    
    
}
