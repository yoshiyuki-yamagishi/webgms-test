<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * product_contents:製品のモデル
 *
 */
class ProductContents extends BaseMasterModel
{
    // TODO: マスタが仕様に追いついていない
	const GRANT_TYPE_NORMAL = 1;
	const GRANT_TYPE_DAILY = 2;
	const GRANT_TYPE_PRESENT_BOX = 999; // まだ無い

    // 本当の仕様
	// const GRANT_TYPE_NORMAL = 1;
	// const GRANT_TYPE_PRESENT_BOX = 2;
	// const GRANT_TYPE_DAILY = 3;
    
	protected $table = 'product_contents';
	// protected $primaryKey = 'id';

	/**
	 * 製品取得
	 *
	 * @param integer $id 製品ID
	 * @return App/Models/Thrift/ProductContents 製品
	 */
	public static function getAll($id)
	{
        $_this = new self();
        return self::_getAllEx(
            $_this->table,
            ['id'],
            [$id]
        );
	}

	/**
	 * デイリー付与商品かどうかの判定を行う
	 *
	 * @param App/Models/Thrift/ProductContents $model 製品
	 * @return true: デイリー付与商品
	 */
    public static function isDaily($model)
    {
        return ($model->grant_type == self::GRANT_TYPE_DAILY);
    }

}
