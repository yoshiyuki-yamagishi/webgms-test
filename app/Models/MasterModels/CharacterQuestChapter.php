<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;

/**
 * character_quest_chapter:ストーリクエスト章 のモデル
 *
 */
class CharacterQuestChapter extends BaseMasterModel
{
	protected $table = 'character_quest_chapter';
    protected $primaryKey = 'id';

}
