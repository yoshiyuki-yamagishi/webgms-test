<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;
use App\Utils\DateTimeUtil;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * login_bonus:ログインボーナス のモデル
 *
 */
class LoginBonus extends BaseMasterModel
{
	protected $table = 'login_bonus';
	protected $primaryKey = 'id';

	// ループ判定
	const LOOP_JUDGE_NO		= 1;	// 非ループ
	const LOOP_JUDGE_YES	= 2;	// ループ

	/**
	 * ログインボーナス一覧を取得
	 *
	 * @param string $now 現在日付
	 * @return array 有効なログインボーナスのリスト
	 */
	public static function getAll($now)
	{
        $_this = new self();
        $all = self::_getAll($_this->table);
        
        usort($all, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a->priority, $b->priority);
            if ($cmp != 0)
                return - $cmp;
            return SortUtil::val_cmp($a->id, $b->id);
        });

        $_now = new \DateTime($now);

        $ret = [];
        foreach ($all as $item)
        {
            $startDay = new \DateTime($item->start_day);
            $endDay = new \DateTime($item->end_day);

            if ($startDay > $_now)
                continue;
            if ($endDay < $now)
                continue;
            
            $ret[] = $item;
        }

        return $ret;
    }
    
}
