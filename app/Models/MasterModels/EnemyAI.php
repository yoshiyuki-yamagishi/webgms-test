<?php

namespace App\Models\MasterModels;

use App\Models\BaseMasterModel;



/**
 * enemy_ai:エネミーAI のモデル
 *
 */
class EnemyAI extends BaseMasterModel
{
	protected $table = 'enemy_ai';
	protected $primaryKey = 'id';
	public $incrementing = false;

}
