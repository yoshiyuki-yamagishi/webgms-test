<?php

namespace App\Models\MasterModels;
use App\Models\BaseMasterModel;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * skill:スキル のモデル
 *
 */
class Skill extends BaseMasterModel
{
	protected $table = 'skill';
	protected $primaryKey = 'id';

	/**
	 * スキル一覧取得
	 *
	 * @return array スキルのリスト
	 */
	public static function getAll()
	{
        $_this = new self();
        return self::_getAll($_this->table);
	}

}
