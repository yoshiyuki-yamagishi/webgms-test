<?php

namespace App\Models;
use App\Models\Player;
use App\Models\MasterModels\Constant;
use App\Services\FriendService;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * direct_present:プレイヤフレンド のモデル
 *
 */
class DirectPresent extends BaseCommonModel
{
    const TYPE_ALL = 1; // 全プレイヤー
    const TYPE_PLAYER = 2; // プレイヤー指定
    const TYPE_LOGIN = 3; // ログイン期間指定
    
	protected $table = 'direct_present';
	protected $primaryKey = 'id';
	// public $incrementing = false;
	// public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param array $notIds 処理済みのID配列
	 * @param string $now 処理日時
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId, $notIds, $now)
	{
		$model =
               self::whereNotIn('id', $notIds)
               ->where(function($query) use ($playerId) {
                   $query
                       ->orWhereNull('player_id')
                       ->orWhere('player_id', $playerId);
               })
               ->where('start_at', '<=', $now)
               ->where('end_at', '>=', $now)
               ->where('delete_flag', 0)
               ->get();

		return $model;
	}
}
