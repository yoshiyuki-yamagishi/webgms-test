<?php

namespace App\Models;
use App\Models\MasterModels\CharacterBase;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_character_log:プレイヤキャラクタログ のモデル
 *
 */
class PlayerCharacterLog extends BaseGameLogModel
{
    public static $target_id_name = 'player_character_id';
    
	protected $table = 'player_character_log';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_GIVE = 1;
    const TYPE_PAY = 2;
    const TYPE_EXP = 3;
    const TYPE_LV = 4;
    const TYPE_SKILL_LV = 5;
    const TYPE_ORB_EQUIP = 6;
    const TYPE_GRADE_UP = 7;
    const TYPE_EVOLVE = 8;

	/**
	 * 経験値取得ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $exp 取得経験値
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerExp(
        $playerCharacter, $exp, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_EXP;
        $model->value1 = $exp;
        $model->value2 = $playerCharacter->experience;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * レベル取得ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $lv 取得レベル
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerLv(
        $playerCharacter, $lv, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_LV;
        $model->value1 = $lv;
        $model->value2 = $playerCharacter->character_lv;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * スキルレベル取得ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $skillNo スキル番号
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerSkillLv(
        $playerCharacter, $skillNo, $srcType, $srcId
    )
	{
        $propName = 'active_skill_' . $skillNo . '_lv';
        
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_SKILL_LV;
        $model->value1 = 1;
        $model->value2 = $playerCharacter->$propName;
        $model->value3 = $skillNo;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * オーブ装備ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $orbFlags 今回装備フラグ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerOrbEquip(
        $playerCharacter, $orbFlags, $srcType, $srcId
    )
	{
        // 現在装備フラグの計算
        $orbFlagsNow = 0;
        for ($i = 1; $i <= PlayerCharacter::MAX_ORB_EQUIP; ++ $i)
        {
            $pcOrbProp = 'player_orb_id_' . $i;
            if (isset($playerCharacter->$pcOrbProp))
                $orbFlagsNow |=  (1 << ($i - 1));
        }
        
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_ORB_EQUIP;
        $model->value1 = $orbFlags;
        $model->value2 = $orbFlagsNow;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}
    
	/**
	 * グレードアップログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $orbFlags 今回装備フラグ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerGradeUp(
        $playerCharacter, $orbFlags, $srcType, $srcId
    )
	{
        // 現在装備は、必ず、全てオフになる //
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_GRADE_UP;
        $model->value1 = 1;
        $model->value2 = $playerCharacter->grade;
        $model->value3 = $orbFlags;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
	}

	/**
	 * 進化ログを登録
     *
	 * @param object $playerCharacter プレイヤキャラクタ
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerEvolve(
        $playerCharacter, $srcType, $srcId
    )
	{
		$model = new self();
        $model->player_id = $playerCharacter->player_id;
        $model->player_character_id = $playerCharacter->id;
        $model->type = self::TYPE_EVOLVE;
        $model->value1 = 1;
        $model->value2 = $playerCharacter->evolve;
        $model->value3 = $playerCharacter->character_id;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
		return $model;
    }
    

    /**
	 * キャラクターログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}


	/**
	 * キャラクター進化回数ログ取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array (character_id, evolve, evolve_count)
	 */
	public static function getEvolveCounts($playerId)
	{
        $query = self::where('player_id', $playerId)
               ->where('type', self::TYPE_EVOLVE)
               ->groupBy('value3', 'value2')
               ->orderBy('value3', 'asc')
               ->orderBy('value2', 'asc')
               ->select(
                   'value3 as character_id',
                   'value2 as evolve',
                   DB::raw('sum(value1) as evolve_count')
               );
        
		$model = $query->get();
		// DebugUtil::e_log('PCL', 'getEvolveCounts', $model);
		return $model;
	}

	/**
	 * キャラクターレアリティ別進化回数ログ計算
	 *
	 * @param array $evolveCounts キャラクター進化回数ログ
	 * @return array (rarity, evolve_count)
	 */
	public static function calcRarityEvolveCounts($evolveCounts)
	{
        $ret = [];
        
        foreach ($evolveCounts as $evolveCount)
        {
            if ($evolveCount->character_id <= 0)
            {
                // コードが古い場合に出力されるデータ //
                continue;
            }
            
            $character = CharacterBase::getOne_($evolveCount->character_id);
            
            $rarity0 = $character->character_initial_rarity;
            $rarity = $rarity0 + $evolveCount->evolve;

            if (!array_key_exists($rarity, $ret))
                $ret[$rarity] = 0;
            
            $ret[$rarity] += $evolveCount->evolve_count;
        }

		// DebugUtil::e_log('PCL', 'calcRarityEvolveCounts', $ret);
        return $ret;
    }

	/**
	 * キャラクターグレードアップ回数ログ取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array (grade, grade_up_count)
	 */
	public static function getGradeUpCounts($playerId)
	{
        $query = self::where('player_id', $playerId)
               ->where('type', self::TYPE_GRADE_UP)
               ->groupBy('value2')
               ->orderBy('value2', 'asc')
               ->select(
                   'value2 as grade',
                   DB::raw('sum(value1) as grade_up_count')
               );
        
		$model = $query->get();
		// DebugUtil::e_log('PCL', 'getGradeUpCounts', $model);
		return $model;
	}
    
}
