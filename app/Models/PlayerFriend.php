<?php

namespace App\Models;
use App\Models\Player;
use App\Models\MasterModels\Constant;
use App\Services\FriendService;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\DB;

/**
 * player_friend:プレイヤフレンド のモデル
 *
 */
class PlayerFriend extends BaseCommonModel
{
	protected $table = 'player_friend';
	protected $primaryKey = 'id';
	// public $incrementing = false;
	// public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 1 件取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $followId フォロープレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getOne($playerId, $followId)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('follow_id', $followId)
               ->first();

		return $model;
	}

	/**
	 * 複数取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $friendType フレンド種別
	 * @param string $searchId 検索する表示用プレイヤID
	 * @param string $now 現在日時
	 * @param integer $sortType ソート順序
	 * @param integer $from 何件目から
	 * @param integer $count 取得件数
	 * @param boolean $useFollow follow_id が変化する場合 true
	 * @return array 本クラスの配列
	 */
    public static function get(
        $playerId, $friendType, $searchId, $now, $sortType, $from, $count,
        &$useFollow
    )
    {
        // おすすめは特殊 //
        if ($friendType == FriendService::TYPE_RECOMEND)
        {
            return static::getRecomend(
                $playerId, $friendType, $searchId, $now, $sortType,
                $from, $count, $useFollow
            );
        }

        $sortCol = FriendService::sortColumn($sortType);
        // DebugUtil::e_log('PlayerFriend', 'sortCol', $sortCol);
        $sortOrder = FriendService::sortOrder($sortType);
        // DebugUtil::e_log('PlayerFriend', 'sortOrder', $sortOrder);

        $query = self::from('player_friend as a');
        // $query = DB::table('player_friend as a');

        $useFollow = static::useFollowId($friendType);
        $searchCol = $useFollow ? 'player_id' : 'follow_id';
        $useCol = $useFollow ? 'follow_id' : 'player_id';

        // 必要な情報をジョイン
        $query
            ->join(
                'player_common as c',
                'c.player_id', '=', 'a.' . $useCol
            )
            ->join(
                'player_common_cache as ca',
                'ca.player_id', '=', 'a.' . $useCol
            );

        switch ($friendType)
        {
        case FriendService::TYPE_FOLLOW:
        case FriendService::TYPE_FOLLOWER:
            // 選択列
            $query->select(
                'a.*',
                DB::raw($friendType . ' as friend_type'),
                'c.player_disp_id',
                'ca.player_name',
                'ca.player_lv',
                'ca.main_character',
                'ca.message',
                'ca.last_login_at'
            );
            
            // 検索条件
            $query->where('a.' . $searchCol, $playerId);

            // フレンドを除く
            $query->leftJoin('player_friend as b',
                             function($join) use ($useCol, $searchCol)
            {
                $join
                    ->on('b.' . $useCol, '=', 'a.' . $searchCol)
                    ->on('b.' . $searchCol, '=', 'a.' . $useCol);
            })->whereNull('b.' . $useCol);
            break;
        case FriendService::TYPE_FRIEND:
            // 選択列
            $query->select(
                'a.*',
                DB::raw($friendType . ' as friend_type'),
                'c.player_disp_id',
                'ca.player_name',
                'ca.player_lv',
                'ca.main_character',
                'ca.message',
                'ca.last_login_at'
            );
            
            $query
                ->where('a.player_id', $playerId)
                ->join('player_friend as b', function($join)
            {
                $join
                    ->on('b.follow_id', '=', 'a.player_id')
                    ->on('b.player_id', '=', 'a.follow_id');
            });
            break;
        case FriendService::TYPE_RECOMEND:
            assert(false);
            break;
        }

        // ソート条件
        $query->orderBy('ca.' . $sortCol, $sortOrder);

        // セレクト
		$model = $query->offset($from)->limit($count)->get();
		return $model;
    }

	/**
	 * おすすめ、プレイヤ検索
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $friendType フレンド種別
	 * @param string $searchId 検索する表示用プレイヤID
	 * @param string $now 現在日時
	 * @param integer $sortType ソート順序
	 * @param integer $from 何件目から
	 * @param integer $count 取得件数
	 * @param boolean $useFollow follow_id が変化する場合 true
	 * @return array 本クラスの配列
	 */
    public static function getRecomend(
        $playerId, $friendType, $searchId, $now, $sortType, $from, $count,
        &$useFollow
    )
    {
        $useFollow = false;
        
        // player_common を主体フォロー系と SQL を共通にすることもできるが
        // 相互フォローの join が複雑になりそうなので、別にする
        
        // 1 クエリでやると複雑なので Player は検索しておく //
        $player = Player::find_($playerId);

        // 定数取得

        $cs = Constant::getRange(
            Constant::FRIEND_RECOMMEND_LEVEL,
            Constant::FRIEND_RECOMMEND_LOGIN
        );

        // おすすめの閾値
        $lv_value1 = 5;
        $lv_value2 = -5;
        $days = 3;
        
        if (count($cs) > 0)
        {
            $lv_value1 = $cs[0]->value1;
            $lv_value2 = $cs[0]->value2;
        }
        if (count($cs) > 1)
        {
            $days = $cs[1]->value1;
        }

        // DebugUtil::e_log('PlayerFriend', 'lv_value1', $lv_value1);
        // DebugUtil::e_log('PlayerFriend', 'lv_value2', $lv_value2);
        // DebugUtil::e_log('PlayerFriend', 'days', $days);

        $query = self::from('player_common as c');
        // $query = DB::table('player_common as c');

        // 必要な情報をジョイン
        $query
            ->join(
                'player_common_cache as ca',
                'ca.player_id', '=', 'c.player_id'
            );
        
        if (!empty($searchId))
        {
            $query->select(
                'c.*',
                DB::raw(
                    "case when w.player_id is null then\n" .
                    "  case when wer.player_id is null then 0 else 2 end\n" .
                    "else\n" .
                    "  case when wer.player_id is null then 1 else 3 end\n" .
                    "end\n" .
                    "as friend_type\n"
                ),
                'ca.player_name',
                'ca.player_lv',
                'ca.main_character',
                'ca.message',
                'ca.last_login_at'
            );
            
            // 表示用プレイヤ ID で検索する
            $query->where('c.player_disp_id', $searchId);

            // フォローか確認用
            $query->leftJoin('player_friend as w',
                             function($join) use ($playerId)
            {
                $join
                    ->on('w.follow_id', '=', 'c.player_id') // 相手を
                    ->where('w.player_id', '=', $playerId); // 自分が
                
            });

            // フォロワーか確認用
            $query->leftJoin('player_friend as wer',
                             function($join) use ($playerId)
            {
                $join
                    ->on('wer.player_id', '=', 'c.player_id') // 相手が
                    ->where('wer.follow_id', '=', $playerId); // 自分を
            });
        }
        else
        {
            $query->select(
                'c.*',
                // DB::raw(
                // 'case when wer.player_id is null then 0 else 2 end ' .
                // 'as friend_type'
                // ),
                DB::raw('0 as friend_type'),
                'ca.player_name',
                'ca.player_lv',
                'ca.main_character',
                'ca.message',
                'ca.last_login_at'
            );
            
            $query->where('c.player_id', '!=', $playerId);

            $lvMax = $player->player_lv + $lv_value1;
            $lvMin = $player->player_lv + $lv_value2;
            if ($lvMin < 0)
                $lvMin = 1;
            
            $query
                ->where('ca.player_lv', '>=', $lvMin)
                ->where('ca.player_lv', '<=', $lvMax);

            $start = new \DateTime($now);
            $days = \DateInterval::createFromDateString($days . ' day');
            $start->sub($days);

            $query->where(
                'ca.last_login_at', '>=', DateTimeUtil::formatDB($start)
            );

            // フォローを除く
            $query->leftJoin('player_friend as w',
                             function($join) use ($playerId)
            {
                $join
                    ->on('w.follow_id', '=', 'c.player_id')
                    ->where('w.player_id', '=', $playerId);
            })->whereNull('w.player_id');

            // フォロワーを除く
            $query->leftJoin('player_friend as wer',
                             function($join) use ($playerId)
            {
                $join
                    ->on('wer.player_id', '=', 'c.player_id')
                    ->where('wer.follow_id', '=', $playerId);
            })->whereNull('wer.player_id');
        }

        // ランダム抽出条件、マッチ数が多いと遅い //
        $query->orderByRaw('RAND()');

        // セレクト
		$model = $query->offset($from)->limit($count)->get();
        return $model;
        
        // ソート条件は無視する
        /*
        $sortCol = FriendService::sortColumn($sortType);
        // DebugUtil::e_log('PlayerFriend', 'sortCol', $sortCol);
        $sortOrder = FriendService::sortOrder($sortType);
        // DebugUtil::e_log('PlayerFriend', 'sortOrder', $sortOrder);

        if ($sortOrder == 'desc')
            return $model->sortByDesc($sortCol);
        else
            return $model->sortBy($sortCol);
        */
    }

    public static function useFollowId($friendType)
    {
        switch ($friendType)
        {
        case FriendService::TYPE_FOLLOW:
            return true; // follow_id が変わる
        case FriendService::TYPE_FOLLOWER:
            return false; // player_id が変わる
        case FriendService::TYPE_FRIEND:
            return true; // follow_id が変わる
        case FriendService::TYPE_RECOMEND:
            return false; // playerid が変わる (どちらでもよいが)
        }
        assert(false);
        return true;
    }


	/**
	 * 件数取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $friendType フレンド種別
	 * @param string $now 現在日時
	 * @return integer 件数
	 */
    public static function getCount($playerId, $friendType)
    {
        if ($friendType == FriendService::TYPE_RECOMEND)
        {
            throw \App\Exceptions\UnknownException::make(
                'getCount is not supported for TYPE_RECOMEND'
            );
        }

        $query = self::from('player_friend as a');

        $useFollow = static::useFollowId($friendType);
        $searchCol = $useFollow ? 'player_id' : 'follow_id';

        switch ($friendType)
        {
        case FriendService::TYPE_FOLLOW:
        case FriendService::TYPE_FOLLOWER:
            // 検索条件
            $query->where('a.' . $searchCol, $playerId);
            break;
        case FriendService::TYPE_FRIEND:
            $query
                ->where('a.player_id', $playerId)
                ->join('player_friend as b', function($join)
            {
                $join
                    ->on('b.follow_id', '=', 'a.player_id')
                    ->on('b.player_id', '=', 'a.follow_id');
            });
            break;
        case FriendService::TYPE_RECOMEND:
        default:
            assert(false);
            break;
        }

        // セレクト
		return (int)$query->count();
    }

	/**
	 * ログインで毎日獲得できるポイント数を計算する
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer 獲得ポイント
	 */
    public static function calcLoginPoint($playerId)
    {
        $baseId = Constant::FRIEND_POINT_DAILY_MUTUAL;
        assert(Constant::FRIEND_POINT_DAILY_MUTUAL - $baseId == 0);
        assert(Constant::FRIEND_POINT_DAILY_FOLLOWER - $baseId == 1);
        assert(Constant::FRIEND_POINT_DAILY_FOLLOWED - $baseId == 2);
        assert(Constant::FRIEND_POINT_DAILY_LIMIT - $baseId == 3);

        $nums = [ 0, 0, 0 ];
        
        //  ナイーブな実装

        $nums[0] = static::getCount(
            $playerId, FriendService::TYPE_FRIEND
        );
        $nums[1] = static::getCount(
            $playerId, FriendService::TYPE_FOLLOW
        );
        $nums[2] = static::getCount(
            $playerId, FriendService::TYPE_FOLLOWER
        );

        $nums[1] -= $nums[0]; // フレンド数を引く
        $nums[2] -= $nums[0]; // フレンド数を引く

        // 定数取得

        $cs = Constant::getRange(
            $baseId, 
            Constant::FRIEND_POINT_DAILY_LIMIT
        );

        $value = 0;
        for ($i = 0; $i < 3; ++ $i)
        {
            $value += $nums[$i] * $cs[$i]->value1;
            // DebugUtil::e_log('PlayerFriend', 'count', $nums[$i]);
            // DebugUtil::e_log('PlayerFriend', 'score', $cs[$i]->value1);
            // DebugUtil::e_log('PlayerFriend', '$value', $value);
        }

        // デイリーフレンドポイント上限処理

        if ($value > $cs[3]->value1)
            $value = $cs[3]->value1;

        return $value;
    }

    /*
	public function player()
	{
		return $this->belongsTo(
            'App\Models\PlayerCommon', 'player_id', 'player_id'
        );
	}

	public function follow()
	{
		return $this->belongsTo(
            'App\Models\PlayerCommon', 'follow_id', 'player_id'
        );
	}

	public function scopePlayerIdEqual($query, $playerId)
	{
		return $query->where('player_id', $playerId);
	}

	public function scopeFollowerIdEqual($query, $playerId)
	{
		return $query->where('follower_id', $playerId);
	}
    */
}
