<?php

namespace App\Models;
use App\Utils\MathUtil;
use App\Http\Requests\BattleEndRequest;

/**
 * player_battle_result:プレイヤバトル結果 のモデル
 *
 */
class PlayerBattleResult extends BaseGameModel
{
	protected $table = 'player_battle_result';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */


    // とどめフラグ
    const FINISH_CODE_AH = 1;
    const FINISH_CODE_DD = 2;
    const FINISH_CODE_OD = 4;

	/**
	 * 追加 (バトルコンティニュー、終了)
	 *
	 * @param object $playerBattle プレイヤバトル モデル
	 * @param object $request リクエスト
	 * @return object self
	 */
	public static function regist(
        $playerBattle, $request
    )
	{
		$model = new self();
		$model->player_battle_id = $playerBattle->id;
        
        if ($request instanceof \App\Http\Requests\BattleEndRequest)
        {
            if ($request->result == BattleEndRequest::RESULT_WIN)
                $model->result = PlayerBattle::RESULT_WIN;
            else
                $model->result = PlayerBattle::RESULT_LOSE_END;
            
            $model->dead_flags = $request->dead_flags;
            $model->used_skill_count = $request->used_skill_count;
            $model->used_od_count = $request->used_od_count;
            $model->used_dd_count = $request->used_dd_count;
            $model->used_ah_count = $request->used_ah_count;
            $model->max_combo_count = $request->max_combo_count;
            $model->max_chain_count = $request->max_chain_count;
            $model->finish_character_no = $request->finish_character_no;
            $model->finish_skill_type = $request->finish_skill_type;
        }
        else if ($request instanceof \App\Http\Requests\BattleContinueRequest)
        {
            $model->result = PlayerBattle::RESULT_LOSE_CONTINUE;
        }
        else if ($request instanceof \App\Http\Requests\BattleSkipRequest)
        {
            $model->result = PlayerBattle::RESULT_SKIP;
        }
        else
        {
            assert(false);
            return;
        }
        
        if (!empty($request->wave_list))
        {
            for ($i = 1; $i <= 3; ++ $i)
            {
                if (array_key_exists($i - 1, $request->wave_list))
                {
                    $wave = $request->wave_list[$i - 1];
                    
                    $prop = 'turn_count_' . $i;
                    $model->$prop = $wave['turn_count'];
                    $prop = 'total_damage_' . $i;
                    $model->$prop = $wave['total_damage'];
                    $prop = 'total_receive_damage_' . $i;
                    $model->$prop = $wave['total_receive_damage'];
                }
            }
        }
        
		$model->save();
        return $model;
	}

	/**
	 * 死亡キャラクター数を返す
	 *
	 * @return integer 死亡キャラクター数
	 */
	public function deadCount()
    {
        return MathUtil::bit_count($this->dead_flags);
    }
    
}
