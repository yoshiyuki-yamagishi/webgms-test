<?php
namespace App\Models\Thrift;

/**
 * Autogenerated by Thrift Compiler (0.12.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
use Thrift\Base\TBase;
use Thrift\Type\TType;
use Thrift\Type\TMessageType;
use Thrift\Exception\TException;
use Thrift\Exception\TProtocolException;
use Thrift\Protocol\TProtocol;
use Thrift\Protocol\TBinaryProtocolAccelerated;
use Thrift\Exception\TApplicationException;

class GachaEffect
{
    static public $isValidate = false;

    static public $_TSPEC = array(
        10 => array(
            'var' => 'id',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        20 => array(
            'var' => 'effect01',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        30 => array(
            'var' => 'effect02',
            'isRequired' => false,
            'type' => TType::I32,
        ),
        40 => array(
            'var' => 'effect03',
            'isRequired' => false,
            'type' => TType::I32,
        ),
    );

    /**
     * @var int
     */
    public $id = null;
    /**
     * @var int
     */
    public $effect01 = null;
    /**
     * @var int
     */
    public $effect02 = null;
    /**
     * @var int
     */
    public $effect03 = null;

    public function __construct($vals = null)
    {
        if (is_array($vals)) {
            if (isset($vals['id'])) {
                $this->id = $vals['id'];
            }
            if (isset($vals['effect01'])) {
                $this->effect01 = $vals['effect01'];
            }
            if (isset($vals['effect02'])) {
                $this->effect02 = $vals['effect02'];
            }
            if (isset($vals['effect03'])) {
                $this->effect03 = $vals['effect03'];
            }
        }
    }

    public function getName()
    {
        return 'GachaEffect';
    }


    public function read($input)
    {
        $xfer = 0;
        $fname = null;
        $ftype = 0;
        $fid = 0;
        $xfer += $input->readStructBegin($fname);
        while (true) {
            $xfer += $input->readFieldBegin($fname, $ftype, $fid);
            if ($ftype == TType::STOP) {
                break;
            }
            switch ($fid) {
                case 10:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->id);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 20:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->effect01);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 30:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->effect02);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                case 40:
                    if ($ftype == TType::I32) {
                        $xfer += $input->readI32($this->effect03);
                    } else {
                        $xfer += $input->skip($ftype);
                    }
                    break;
                default:
                    $xfer += $input->skip($ftype);
                    break;
            }
            $xfer += $input->readFieldEnd();
        }
        $xfer += $input->readStructEnd();
        return $xfer;
    }

    public function write($output)
    {
        $xfer = 0;
        $xfer += $output->writeStructBegin('GachaEffect');
        if ($this->id !== null) {
            $xfer += $output->writeFieldBegin('id', TType::I32, 10);
            $xfer += $output->writeI32($this->id);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->effect01 !== null) {
            $xfer += $output->writeFieldBegin('effect01', TType::I32, 20);
            $xfer += $output->writeI32($this->effect01);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->effect02 !== null) {
            $xfer += $output->writeFieldBegin('effect02', TType::I32, 30);
            $xfer += $output->writeI32($this->effect02);
            $xfer += $output->writeFieldEnd();
        }
        if ($this->effect03 !== null) {
            $xfer += $output->writeFieldBegin('effect03', TType::I32, 40);
            $xfer += $output->writeI32($this->effect03);
            $xfer += $output->writeFieldEnd();
        }
        $xfer += $output->writeFieldStop();
        $xfer += $output->writeStructEnd();
        return $xfer;
    }
}
