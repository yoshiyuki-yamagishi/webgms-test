<?php

namespace App\Models;
use App\Models\MasterModels\ShopExchange;

/**
 * player_item_buy:プレイヤアイテム購入 のモデル
 *
 */
class PlayerItemBuy extends BaseGameModel
{
	protected $table = 'player_item_buy';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * プレイヤ購入リストを取得
	 *
	 * @param integer $playerId　プレイヤID
	 * @param string $lastResetTime　最近のリセットタイム
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId, $lastResetTime)
	{
		$model =
			self::where('player_id', $playerId)
				->where('created_at', '>=', $lastResetTime)
				->get();

		return $model;
	}

	/**
	 * 登録
	 *
	 * @param object $playerShopItem プレイヤショップアイテム
	 * @param object $shop Thrift/Shop
	 * @param object $shopExchange Thrift/ShopExchange
	 * @return array 本クラスの配列
	 */
	public static function regist($playerShopItem, $shop, $shopExchange)
	{
        $model = new self();
        $model->player_id = $playerShopItem->player_id;
        $model->shop_category = $shop->shop_category;
        $model->shop_id = $shopExchange->shop_id;
        $model->shop_exchange_id = $shopExchange->id;
        $model->item_type = ShopExchange::itemType($shopExchange);
        $model->item_id = $shopExchange->lineup_id;
        $model->get_count = $shopExchange->get_count;
        $model->pay_item_contents_id = $shopExchange->pay_item_contents_id;
        $model->pay_item_count = $shopExchange->pay_item_count;
        $model->save();
		return $model;
	}

	/**
	 * ショップ利用ログ一覧取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function logGetByPlayerId($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();

		return $model;
	}
    
}
