<?php

namespace App\Models;

use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Item;
use App\Utils\DebugUtil;

/**
 * player_item:プレイヤアイテム のモデル
 *
 */
class PlayerItem extends BaseGameModel
{
	protected $table = 'player_item';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	// 最大所持数
	const MAX_NUM = 999;

	/**
	 * プレイヤアイテム一覧を取得
	 *
	 * @param array $ids id の配列
	 * @return array 本クラスの配列
	 */
	public static function getByIds($ids)
	{
        return self::whereIn('id', $ids)->get();
    }
    
	/**
	 * プレイヤアイテム一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId, $category = null)
	{
        $q = self::where('player_id', $playerId);
        if (isset($category) && $category > 0)
        {
            $q->whereRaw(
                'item_id DIV ? = ?',
                [
                    Item::CATEGORY_RANGE,
                    $category,
                ]
            );
        }
        
		$model = $q->get();
		return $model;
	}

	/**
	 * プレイヤアイテム一覧を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param array $itemIds item_id の配列
	 * @return array 本クラスの配列
	 */
	public static function getByItemIds($playerId, $itemIds)
	{
        $q = self::where('player_id', $playerId);
        if (is_array($itemIds))
        {
            $q->whereIn('item_id', $itemIds);
        }
        return $q->get();
	}

	/**
	 * 取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $itemId アイテムID
	 * @return self 本クラス
	 */
	public static function getOne($playerId, $itemId)
	{
		$model =
			self::where('player_id', $playerId)
				->where('item_id', $itemId)
				->first();

		return $model;
	}

	/**
	 * 追加することができるカテゴリか？
	 *
	 * @param integer $category アイテムカテゴリ
	 * @return boolean 追加すべきかどうか
	 */
    public static function isTargetCategory($category)
    {
        switch ($category)
        {
            // case Item::CATEGORY_FREE_BLUE_CRYSTAL:
        case Item::CATEGORY_TICKET:
        case Item::CATEGORY_EXP:
        case Item::CATEGORY_ORB:
        case Item::CATEGORY_SKIP_TICKET:
        case Item::CATEGORY_FRAGMENT:
        case Item::CATEGORY_AL_RECOVER:
        case Item::CATEGORY_EVENT:
            // case Item::CATEGORY_FRIEND_POINT:
            // case Item::CATEGORY_BLUE_CRYSTAL:
            // case Item::CATEGORY_P_DOLLAR:
            // case Item::CATEGORY_ELEMENT:
            return true;
        }
        return false;
    }

	/**
	 * 追加することができるアイテムIDか？
	 *
	 * @param integer $itemId アイテムID
	 * @return boolean 追加すべきかどうか
	 */
    public static function isTargetItemId($itemId)
    {
        return self::isTargetCategory(
            Item::categoryFromId($itemId)
        );
    }
    
}
