<?php

namespace App\Models;
use App\Utils\DebugUtil;

/**
 * player_battle_grimoire:プレイヤバトル魔道書 のモデル
 *
 */
class PlayerBattleGrimoire extends BaseGameModel
{
	protected $table = 'player_battle_grimoire';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 登録
	 *
	 * @param string $playerBattleId プレイヤバトルID
	 * @param array $grimoireList 魔道書リスト
	 */
	public static function register(
        $playerBattle, $grimoireList
    )
	{
        // あらかじめ、SQL 1 回で装備アイテムの item_id を得る //
        
        // スロット装備は、リリース時に含まない
        /*
        $equipPids = [];
        foreach ($grimoireList as $grim)
        {
            for ($j = 0; $j < PlayerGrimoire::MAX_SLOT_EQUIP; ++ $j)
            {
                $prop = 'slot_' . ($j + 1);
                if (empty($grim->$prop))
                    continue;
                if (in_array($grim->$prop, $equipPids))
                    continue;
                
                $equipPids[] = $grim->$prop;
            }
        }
        // DebugUtil::e_log('PBC', 'equipPids', $equipPids);
        
        $equips = PlayerItem::getByIds($equipPids);
        // DebugUtil::e_log('PBC', 'equips', $equips);
        */

        // 登録 //

        $models = [];

        $count = count($grimoireList);
        for ($i = 0; $i < $count; ++ $i)
        {
            $grim = $grimoireList[$i];

            if (empty($grim))
                continue;

            $model = new self();
            $model->player_battle_id = $playerBattle->id;
            $model->position = ($i + 1);
            $model->grimoire_id = $grim->grimoire_id;
            $model->awake = $grim->awake;

            // スロット装備は、リリース時に含まない
            /*
            for ($j = 0; $j < PlayerGrimoire::MAX_SLOT_EQUIP; ++ $j)
            {
                $prop = 'slot_' . ($j + 1);

                if (empty($grim->$prop))
                    continue;

                $equip = $equips->find($grim->$prop);
                $model->$prop = $equip->item_id;
            }
            */
            
            $model->save();
            
            $models[] = $model;
        }
        
        return $models;
	}
    
}
