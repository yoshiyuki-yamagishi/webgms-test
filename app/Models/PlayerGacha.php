<?php

namespace App\Models;
use App\Models\MasterModels\Gacha;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * player_gacha:プレイヤガチャ のモデル
 *
 */
class PlayerGacha extends BaseGameModel
{
	protected $table = 'player_gacha';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    public static function regist(
        $playerId, $gacha, $gachaCount, $save = true
    )
    {
        $model = new self();
        $model->player_id = $playerId;
        $model->gacha_id = $gacha->id;
        $model->gacha_count = $gachaCount;
        $model->gacha_loop = $gacha->gacha_loop;

        if ($save)
            $model->save();

        return $model;
    }

	/**
	 * 1 行取得 (引き直し、確定用)
	 *
	 * @param integer $playerGachaId プレイヤガチャID
	 * @param integer $playerId プレイヤID
	 * @return self
	 */
    public static function getForRetryCommit($playerGachaId, $playerId)
    {
        $model = self::find($playerGachaId);
        if (!isset($model))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_gacha', 'id', $playerGachaId
            );
        }
        if ($model->player_id != $playerId)
        {
            throw \App\Exceptions\DataException::make(
                'player_id != player_gacha.player_id: ' . $model->player_id
            );
        }
        if (isset($model->taked_at))
        {
            throw \App\Exceptions\DataException::make(
                'player_gacha.taked_at is already set'
            );
        }

        return $model;
    }
    

	/**
	 * 期間内に引いた回数を返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param object $gacha ガチャマスタ
	 * @param string $now 現在日付
	 * @return integer 回数
	 */
    public static function getLimitCount($playerId, $gacha, $now)
    {
        if ($gacha->limit_type < 0)
            return 0; // 未定義

        $q = self::where('player_id', $playerId)
           ->where('gacha_id', $gacha->id)
           ->whereNotNull('taked_at');
           
        switch ($gacha->limit_type)
        {
        case Gacha::LIMIT_TYPE_PERIOD:
            break;
        case Gacha::LIMIT_TYPE_DAY:
            $_start = DateTimeUtil::dailyGachaStartDate($now);
            $start = DateTimeUtil::formatDB($_start);
            $q->where('created_at', '>=', $start);
            break;
        default:
            throw \App\Exceptions\MasterException::makeInvalid(
                'gacha', 'limit_type', $gacha->limit_type
            );
        }
        
		// DebugUtil::e_log('PG', 'getLimitCount', $q->toSql());
        return $q->count();
    }

	/**
	 * 回数チェックする
	 *
	 * @param integer $playerId プレイヤID
	 * @param object $gacha ガチャマスタ
	 * @param string $now 現在日付
	 * @return integer 回数
	 */
    public static function checkLimitCount($playerId, $gacha, $now)
    {
        if ($gacha->limit_type <= 0)
            return 0; // 未定義
        
        $limitCount = self::getLimitCount($playerId, $gacha, $now);
        if ($limitCount >= $gacha->gacha_limit)
        {
            throw \App\Exceptions\GameException::make(
                'already gacha_limit: '
                . $limitCount . ' / ' . $gacha->gacha_limit
            );
        }
        
        return $limitCount;
    }

}
