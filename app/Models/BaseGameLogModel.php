<?php

namespace App\Models;
use App\Http\Responses\ApiResponse;

/**
 * Gameログモデルの基底クラス
 *
 */
class BaseGameLogModel extends BaseGameModel
{
	/**
	 * 付与/消費ログを登録
     *
	 * @param object $target ログに対応するモデル
	 * @param integer $itemId アイテムID
	 * @param integer $itemCount アイテム数増減
	 * @param integer $itemValue 現在のアイテム数
	 * @param integer $srcType 取得種別
	 * @param integer $srcId 取得ID
	 * @return self 本クラス
	 */
	public static function registerGiveOrPay(
        $target, $itemId, $itemCount, $itemValue, $srcType, $srcId
    )
	{
        $targetIdName = static::$target_id_name;

        $type = static::TYPE_GIVE;
        if ($itemCount < 0)
        {
            $itemCount = - $itemCount;
            $type = static::TYPE_PAY;
        }

		$model = new static();
        
        if ($targetIdName != 'player_id')
            $model->player_id = $target->player_id;
        
        $model->$targetIdName = $target->id;
        $model->type = $type;
        $model->value1 = $itemCount;
        $model->value2 = $itemValue;
        $model->value3 = $itemId;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->save();
        
		return $model;
	}

    public function save(array $options = [])
    {
		$response = ApiResponse::getInstance();
        $now = $response->currentDateDB();
        
        $this->called_at = $now;
        
        return BaseGameModel::save($options);
    }

}