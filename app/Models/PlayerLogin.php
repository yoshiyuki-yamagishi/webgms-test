<?php

namespace App\Models;

use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\DB;


/**
 * player_login:プレイヤログイン のモデル
 *
 */
class PlayerLogin extends BaseGameModel
{
	protected $table = 'player_login';
	protected $primaryKey = 'id';
// 	public $incrementing = false;
// 	public $timestamps = false;

    /*
	protected $table = 'Business';
	protected $primaryKey = 'id';
	//public $incrementing = false;
	//public $timestamps = false;

	protected $fillable = [
		//'id',
	];
	*/

	/**
	 * ログインを登録する
	 *
	 * @param integer $playerId プレイヤID
	 * @return self 本モデル
	 */
	public static function register($playerId)
	{
		$model = new self();
		$model->player_id	= $playerId;
		$model->save();
        return $model;
	}

	/**
	 * 期間中のログイン回数を返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $start 期間開始
	 * @param string $end 期間終了
	 * @return integer ログイン回数
	 */
	public static function loginCount($playerId, $start, $end)
	{
        if (empty($start))
            $start = DateTimeUtil::getFarPast();
        if (empty($end))
            $end = DateTimeUtil::getFarFuture();

        $count = self::where('player_id', $playerId)
               ->where('created_at', '>=', $start)
               ->where('created_at', '<=', $end)
               ->count();
        
        return $count;
	}

	/**
	 * 期間中のログイン日数を返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $start 期間開始
	 * @param string $end 期間終了
	 * @return integer ログイン回数
	 */
	public static function loginDays($playerId, $start = null, $end = null)
	{
        if (empty($start))
            $start = DateTimeUtil::getFarPast();
        if (empty($end))
            $end = DateTimeUtil::getFarFuture();
        
        $result = self::where('player_id', $playerId)
                ->where('created_at', '>=', $start)
                ->where('created_at', '<=', $end)
                ->selectRaw(
                    'count(distinct(date(created_at))) as login_days'
                )->first();

        $count = (int)$result->login_days;
        // DebugUtil::e_log('PL', 'loginDays', $count);
        return $count;
	}
    
	/**
	 * 期間中にログインしたか返す
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $start 期間開始
	 * @param string $end 期間終了
	 * @param string $now 現在でチェックする場合、現在日時
	 * @return true: ログインした
	 */
	public static function isLogin($playerId, $start, $end, $now = null)
	{
        if (!empty($now))
        {
            if (DateTimeUtil::isBetween($now, $start, $end))
                return true;
        }
        return static::loginCount($playerId, $start, $end) > 0;
    }

}