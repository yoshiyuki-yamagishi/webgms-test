<?php

namespace App\Models\CacheModels;
use App\Models\BaseCacheModel;
use App\Models\MasterModels\StoryQuestQuest;
use App\Utils\DebugUtil;

/**
 * ストーリークエスト最終ID のモデル
 *
 */
class StoryQuestLastIds extends BaseCacheModel
{
    public static $table = 'storyQuestLastIds';

	/**
	 * データを用意する
     *
	 */
    public static function prepare()
    {
        $all = StoryQuestQuest::getAll();

        $keys = [];
        foreach ($all as $item)
        {
            $keys[$item->id] = 1;
            unset($keys[$item->release_trigger]);
        }

        $ids = array_keys($keys);
        static::set($ids);
    }

}
