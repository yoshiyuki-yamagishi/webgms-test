<?php

namespace App\Models\CacheModels;
use App\Models\BaseCacheModel;
use App\Models\MasterModels\CharacterQuestQuest;
use App\Utils\DebugUtil;

/**
 * キャラクタークエスト最終ID のモデル
 *
 */
class CharacterQuestLastIds extends BaseCacheModel
{
    public static $table = 'characterQuestLastIds';

	/**
	 * データを用意する
     *
	 */
    public static function prepare()
    {
        $all = CharacterQuestQuest::getAll();

        $keys = [];
        foreach ($all as $item)
        {
            $keys[$item->id] = 1;
            unset($keys[$item->release_trigger]);
        }

        $ids = array_keys($keys);
        static::set($ids);
    }

}
