<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

    class GmsUserModel extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password', 'manage_user_auth'
    ];
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'gms_user'; // Laravelの命名規則に従っていないテーブルでも、明示的に指定すればOK
    public $timestamps = false;
    protected $connection = 'gms';

}

?>
