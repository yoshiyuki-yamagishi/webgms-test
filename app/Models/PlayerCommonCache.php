<?php

namespace App\Models;



/**
 * player_common_cache:プレイヤ共通キャッシュ のモデル
 * player 情報のうち他者検索に必要な情報を、共通 DB にも持つ
 *
 */
class PlayerCommonCache extends BaseCommonModel
{
	protected $table = 'player_common_cache';
	protected $primaryKey = 'id';
	public $incrementing = true;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * プレイヤ共通キャッシュを更新する
	 *
	 * @param Player $player プレイヤ
	 * @return self 本クラス
	 */
	public static function updateCache($player)
	{
        $model = self::where('player_id', $player->id)->first();
        if (empty($model))
        {
            $model = new self();
            $model->player_id = $player->id;
        }

        if ($player->favorite_character)
        {
            $pc = PlayerCharacter::find_($player->favorite_character);
            $model->main_character = $pc->character_id;
        }
        else
        {
            $model->main_character = 0;
        }

        $model->player_name = $player->player_name;
        $model->player_lv = $player->player_lv;
        $model->message = $player->message;
        $model->last_login_at = $player->last_login_at;
        $model->save();
	}
    
}
