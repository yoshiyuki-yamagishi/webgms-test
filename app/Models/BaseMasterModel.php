<?php

namespace App\Models;
use App\Models\MasterModelManager;
use App\Utils\DebugUtil;

/**
 * Masterモデルの基底クラス
 *
 */
class BaseMasterModel
{
    /**
	 * モデル取得
	 *
	 * @param object $id 主キーの値
	 * @return object App/Models/Thrift/??? モデル
	 */
	public static function getOne($id)
	{
        $_this = new static();

        if (is_array($_this->primaryKey))
        {
            return self::_getOneEx(
                $_this->table,
                $_this->primaryKey,
                $id
            );
        }

        return self::_getOne(
            $_this->table,
            $_this->primaryKey,
            $id
        );
    }

    /**
	 * モデル取得 (例外スロー)
	 *
	 * @param object $id 主キーの値
	 * @return object App/Models/Thrift/??? モデル
	 */
	public static function getOne_($id)
	{
        $model = static::getOne($id);
        if (empty($model))
        {
            $_this = new static();
            throw \App\Exceptions\MasterException::makeNotFound(
                $_this->table, $_this->primaryKey, $id
            );
        }
        return $model;
    }
    
    /**
	 * リストのプロパティ名の接頭辞を計算
	 *
	 * @param string $table テーブル名
	 * @return string リストのプロパティ名の接頭辞
	 */
	protected static function _calcListPrefix($table)
    {
        $ret = '';
        
        $chars = str_split($table);
        $len = count($chars);
        $prevUnder = false;

        for ($i = 0; $i < $len; ++ $i)
        {
            if ($chars[$i] == '_')
            {
                $prevUnder = true;
                continue;
            }

            if ($prevUnder)
            {
                $prevUnder = false;
                $ret .= strtoupper($chars[$i]);
                continue;
            }

            $ret .= $chars[$i];
        }

        return $ret;
    }

    /**
	 * リストのプロパティ名を計算
	 *
	 * @param string $table テーブル名
	 * @return string リストのプロパティ名
	 */
	protected static function _calcListName($table)
    {
        return self::_calcListPrefix($table) . 'List';
    }
    
    
    /**
	 * モデル取得
	 *
	 * @param string $table テーブル名
	 * @param array $key 主キー
	 * @param object $id 主キーの値
	 * @return object App/Models/Thrift/??? モデル
	 */
	protected static function _getOne($table, $key, $id)
	{
        $mmm = MasterModelManager::getInstance();
        $list = $mmm->getData(self::_calcListName($table));

        foreach ($list as $item)
        {
            if ($item->$key == $id)
                return $item;
        }

        return null;
    }

    /**
	 * モデル取得 (例外スロー)
	 *
	 * @param string $table テーブル名
	 * @param array $key 主キー
	 * @param object $id 主キーの値
	 * @return object App/Models/Thrift/??? モデル
	 */
	protected static function _getOne_($table, $key, $id)
	{
        $model = static::_getOne($table, $key, $id);
        if (empty($model))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                $table, $key, $id
            );
        }
        return $model;
    }

    /**
	 * match を呼び出す準備を行う
	 *
	 * @param array $keys 主キーリスト
	 * @param array $ids 主キーの値リスト
	 * @return integer マッチする回数
	 */
	protected static function _initMatch($keys, $ids)
	{
        $keyCount = count($keys);
        $idsCount = count($ids);

        // TODO: 例外を飛ばしたい
        assert($keyCount == $idsCount);
        
        return min($keyCount, $idsCount);
    }
    

    /**
	 * モデルにマッチするか？
	 *
	 * @param object $item モデル
	 * @param array $keys 主キーリスト
	 * @param array $ids 主キーの値リスト
	 * @return boolean true: マッチ false: アンマッチ
	 */
	protected static function _match(&$item, &$keys, &$ids, $count)
	{
        for ($i = 0; $i < $count; ++ $i)
            if ($item->{$keys[$i]} != $ids[$i])
                return false;

        return true;
    }

    /**
	 * モデル取得
	 *
	 * @param string $table テーブル名
	 * @param array $keys 主キーリスト
	 * @param array $ids 主キーの値リスト
	 * @return object App/Models/Thrift/??? モデル
	 */
	protected static function _getOneEx($table, $keys, $ids)
	{
        $mmm = MasterModelManager::getInstance();
        $list = $mmm->getData(self::_calcListName($table));

        $count = self::_initMatch($keys, $ids);
        foreach ($list as $item)
        {
            if (self::_match($item, $keys, $ids, $count))
                return $item;
        }

        return null;
    }
    
    /**
	 * 全モデル取得
	 *
	 * @param string $table テーブル名
	 * @return array App/Models/Thrift/??? モデルのリスト
	 */
	protected static function _getAll($table)
	{
        $mmm = MasterModelManager::getInstance();
        return $mmm->getData(self::_calcListName($table));
    }

    /**
	 * 複数モデル取得
	 *
	 * @param string $table テーブル名
	 * @return array App/Models/Thrift/??? モデルのリスト
	 */
	protected static function _getAllEx($table, $keys, $ids)
	{
        $mmm = MasterModelManager::getInstance();
        $list = $mmm->getData(self::_calcListName($table));

        $ret = [];
        $count = self::_initMatch($keys, $ids);

        foreach ($list as $item)
        {
            if (self::_match($item, $keys, $ids, $count))
                $ret[] = $item;
        }

        return $ret;
    }
    
}
