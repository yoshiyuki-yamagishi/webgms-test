<?php

namespace App\Models;

/**
 * Cacheモデルの基底クラス (加工済みのマスタデータを Cache に保存する)
 *
 */
class BaseCacheModel
{
	/**
	 * キャッシュに保存
     *
	 * @param object $values 保存する値
	 */
    public static function set($values)
    {
        CacheModelManager::getInstance()->setData(static::$table, $values);
    }

	/**
	 * キャッシュから取得
     *
	 */
    public static function get()
    {
        return CacheModelManager::getInstance()->getData(static::$table);
    }
    
}
