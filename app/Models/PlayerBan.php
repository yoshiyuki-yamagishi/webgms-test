<?php

namespace App\Models;

/**
 * player_ban:プレイヤBAN のモデル
 *
 */
class PlayerBan extends BaseGameModel
{
    const STATUS_NONE = 0;
    const STATUS_WARN = 1;
    const STATUS_BAN = 2;
    const OFF_FLG = 0;
    const ON_FLG = 1;

    protected $table = 'player_ban';
	protected $primaryKey = 'id';

	/**
	 * 現在有効なプレイヤバン取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getOne($playerId, $now)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('start_at', '<=', $now)
               ->where('end_at', '>=', $now)
               ->where('delete_flag', self::OFF_FLG)
               ->orderBy('ban_status', 'dsc') // 大きいほど、重大
               ->orderBy('id', 'dsc') // 後に登録の方を前に
               ->first();

		return $model;
	}

	/**
 * プレイヤバン一覧取得
 *
 * @param integer $playerId プレイヤID
 * @return array 本クラスの配列
 */
    public static function getByPlayerId($playerId)
    {
        $model =
            self::where('player_id', $playerId)
                ->where('delete_flag', self::OFF_FLG)
                ->orderBy('id', 'asc')
                ->get();

        return $model;
    }

    /**
     * プレイヤBAN新規登録
     *
     * @param integer $playerId プレイヤID
     * @param $banStatus
     * @param $message
     * @param $startAt
     * @param $endAt
     * @return model 本クラスの配列
     */
    public static function insert($playerId,$banStatus,$message,$startAt,$endAt)
    {
        $model = new self();
        $model->player_id = $playerId;
        $model->ban_status = $banStatus;
        $model->message = $message;
        $model->delete_flag = 0;
        $model->start_at = $startAt;
        $model->end_at = $endAt;
        $model->save();

        return $model;
    }


    /**
     * 論理削除
     * @param $id
     */
    public static function logicalDelete($id)
    {
        self::where('id', $id)
            ->update(['delete_flag' => self::ON_FLG]);
    }


}
