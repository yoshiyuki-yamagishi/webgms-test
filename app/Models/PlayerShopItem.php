<?php

namespace App\Models;

use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\ShopExchange;
use App\Models\MasterModels\Item;
use App\Utils\SortUtil;
use App\Utils\DebugUtil;

/**
 * player_shop_item:プレイヤアイテム のモデル
 *
 */
class PlayerShopItem extends BaseGameModel
{
	protected $table = 'player_shop_item';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 取得
	 * @param integer $playerId プレイヤID
	 * @param integer $shopId ショップID
	 * @return array 本クラスの配列
	 */
	public static function getByShopId($playerId, $shopId)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('shop_id', $shopId)
               ->get();

		return $model;
	}

	/**
	 * ラインナップ更新
	 * @param integer $playerId プレイヤID
	 * @param integer $shop shop マスタ
	 * @param string $now 現在日付
	 */
    public static function updateLineup($playerId, $shop, $now)
    {
        // 削除
        
        self::where('player_id', $playerId)
            ->where('shop_id', $shop->id)
            ->delete();

        // 追加

        $items = ShopExchange::calcLineup($shop);

        foreach ($items as $item)
        {
            $model = new self();
            $model->player_id = $playerId;
            $model->shop_category = $shop->shop_category;
            $model->shop_id = $shop->id;
            $model->shop_exchange_id = $item->id;
            $model->priority = $item->priority;
            $model->buy_count = 0;
            $model->created_at = $now;
            $model->save();
        }
    }
    
}
