<?php

    namespace App\Models;

    use App\Utils\StrUtil;

    /**
     * Information:お知らせ のモデル
     *
     */
    class Information extends BaseCommonModel
    {
        protected $table = 'information';
        protected $primaryKey = 'id';
        public $incrementing = true;

        // お知らせタイプ
        const INFORMATION_TYPE_ALL = 1;
        const INFORMATION_TYPE_MAINTENANCE = 2;
        const INFORMATION_TYPE_UPDATE = 3;
        const INFORMATION_TYPE_BUG_REPORT = 4;

        // プラットフォームフラグ
        const PLATFORM_FLAG_IOS = 0b001;
        const PLATFORM_FLAG_ANDROID = 0b010;
        const PLATFORM_FLAG_BROWSE = 0b100;

        /**
         * 指定したIDのお知らせ取得
         *
         * @param $id
         * @return mixed
         */
        public static function getOne($id)
        {
            return self::find($id);
        }

        /**
         * タイプ分けなし表示リスト取得
         *
         * @param $platform
         * @param $now
         * @return mixed
         */
        public static function getAllViewList($platform, $now)
        {
            $platformFlag = self::_getPlatformFlag($platform);
            return self::whereRaw('`platform_flag` & ?', $platformFlag)
                ->where('started_at', '<=', $now)
                ->where('expired_at', '>=', $now)
                ->orderByRaw('priority DESC, updated_at DESC')
                ->get();
        }

        public static function getPagingList($infoType,$platform)
        {
            $model = self::query();
            if($infoType){
                $model->where('info_type', $infoType);
            }
            if($platform) {
                $model->where(function($model) use ($platform){
                    foreach ($platform as $key => $value) {
                        $model->orwhere('platform_flag', $value);
                    }
                });
            }
            $model->orderBy('id','DESC');
            return $model->paginate(20);
        }
        /**
         * お知らせタイプ別表示リスト取得
         *
         * @param $infoType
         * @param $platform
         * @param $now
         * @return mixed
         */
        public static function getTypeViewList($infoType, $platform, $now)
        {
            $platformFlag = self::_getPlatformFlag($platform);
            return self::where('info_type', $infoType)
                ->whereRaw('`platform_flag` & ?', $platformFlag)
                ->where('started_at', '<=', $now)
                ->where('expired_at', '>=', $now)
                ->orderByRaw('priority DESC, updated_at DESC')
                ->get();
        }

        /**
         * 新規登録
         * @param $infoType
         * @param $platformFlg
         * @param $title
         * @param $content
         * @param $imageName
         * @param $priority
         * @param $startedAt
         * @param $expiredAt
         */
        public static function register(
            $infoType,
            $platform,
            $title,
            $content,
            $imageName,
            $priority,
            $startedAt,
            $expiredAt
        ) {
            $model = new self();
            $model->info_type = $infoType;
            $model->platformFlg = $platform;
            $model->title = $title;
            $model->content = $content;
            $model->image_name = $imageName;
            $model->priority = $priority;
            $model->started_at = $startedAt;
            $model->expired_at = $expiredAt;
            $model->save();
            return;
        }

        /**
         * 削除
         * @param $id
         * @throws \Exception
         */
        public static function deleteByPK($id) {
            self::where('id', $id)
                ->delete();
            return;
        }

        /**
         * 編集
         * @param $id
         * @throws \Exception
         */
        public static function editUpdate($request, $startedAt, $expierdAt) {
            self::update([
                'id'=> $request->id,
                'info_type' => $request->info_type,
                'platform_flag' => $request->platform_flag,
                'title' => $request->title,
                'content' => $request->content,
                'image_name' => $request->image_name,
                'started_at' => $startedAt,
                'expired_at' => $expierdAt
                ]);
            return;
        }

        /**
         * PlatformTypeからFlagパラメータ
         *
         * @param $platform
         * @return int
         */
        private static function _getPlatformFlag($platform)
        {
            switch ($platform) {
                case StrUtil::PLATFORM_IOS:
                    return self::PLATFORM_FLAG_IOS;
                    break;
                case StrUtil::PLATFORM_ANDROID:
                    return self::PLATFORM_FLAG_ANDROID;
                    break;
                default:
                    return self::PLATFORM_FLAG_BROWSE;
                    break;
            }
        }
    }
