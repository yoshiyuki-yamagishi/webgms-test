<?php

namespace App\Models;
use App\Utils\DateTimeUtil;

/**
 * player_battle:プレイヤバトル のモデル
 *
 */
class PlayerBattle extends BaseGameModel
{
	protected $table = 'player_battle';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	// 勝敗
    const RESULT_NONE = 0; // 継続中
    const RESULT_LOSE_END = 1; // 敗北し、終了
    const RESULT_LOSE_CONTINUE = 2; // 敗北したが、コンティニュー
    const RESULT_WIN = 3; // 勝利
    const RESULT_SKIP = 4; // スキップ
    const RESULT_TERMINATE = 5; // 中断

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * 継続中のバトルを取得
	 * @param integer $playerId プレイヤID
	 * @return self 本クラス
	 */
	public static function getActive($playerId)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('result', self::RESULT_NONE)
               ->first();

		return $model;
	}

	/**
	 * バトルコードから取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $battleCode バトルコード
	 * @return self 本クラス
	 */
	public static function getByBattleCode($playerId, $battleCode)
	{
		$model =
               self::where('player_id', $playerId)
               ->where('battle_code', $battleCode)
               ->where('result', self::RESULT_NONE)
               ->first();

		return $model;
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $battleCode バトルコード
	 * @param integer $partyNo パーティ番号
	 * @param integer $questCategory クエスト種類
	 * @param integer $chapterId チャプタID
	 * @param integer $questId ステージID
	 * @return self
	 */

	public static function regist(
        $playerId, $battleCode, $partyNo, $questCategory, $chapterId, $questId
    )
	{
        $model = new self();
        $model->player_id = $playerId;
		$model->battle_code = $battleCode;
		$model->party_no = $partyNo;
		$model->quest_category = $questCategory;
		$model->chapter_id = $chapterId;
		$model->quest_id = $questId;
		$model->save();
        return $model;
	}

	/**
	 * 章別のクリア回数 (日毎) を返す
	 * @param integer $playerId プレイヤID
	 * @param integer $questCategory クエスト種類
	 * @param integer $chapterId 章ID
	 * @param string $now 現在日時
	 * @return self 本クラス
	 */
	public static function getChapterDailyClearCount(
        $playerId, $questCategory, $chapterId, $now
    )
	{
        $results = [self::RESULT_WIN, self::RESULT_SKIP];
        $start = DateTimeUtil::dailyQuestStartDate($now);
        
        return
            self::where('player_id', $playerId)
            ->where('quest_category', $questCategory)
            ->where('chapter_id', $chapterId)
            ->whereIn('result', $results)
            ->where('updated_at', '>=', $start)
            ->count();
	}

	/**
	 * 有効なバトルコードかチェックする
	 *
	 * @param self 本クラス
	 * @return ture: 有効、false:無効
	 */
	public static function isValidBattleCode($playerBattle)
	{
        return isset($playerBattle) && isset($playerBattle->battle_code);
    }

	public static function terminateAll($playerId)
    {
        self::where('player_id', $playerId)
            ->where('result', self::RESULT_NONE)
            ->update(['result' => self::RESULT_TERMINATE]);
    }
    
}
