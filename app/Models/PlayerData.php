<?php

namespace App\Models;
use App\Models\Player;



/**
 * player_data:プレイヤデータ のモデル
 *
 */
class PlayerData extends BaseGameModel
{
	protected $table = 'player_data';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	// 種別
	const TYPE_BGM = 1; // BGM音量
	const TYPE_SE = 2; // SE音量
	const TYPE_VOICE = 3; // VOICE音量
	const TYPE_SCREEN_ORIENTATION = 4; // 画面向き
	const TYPE_PUSH = 5; // PUSH通知設定
	const TYPE_ADV = 6; // ADV調整

	// 画面向き
	const SCREEN_ORIENTATION_LANDSCAPE = 0; // 横
	const SCREEN_ORIENTATION_PORTRAIT = 1; // 縦

	// PUSH通知
	const PUSH_NO = 0; // なし
	const PUSH_YES = 1; // あり

	// 課金上限設定
	// const BILLING_CAP = 1000000;

	private static $initial_value = [
		self::TYPE_BGM => 100, // BGM音量
		self::TYPE_SE => 100, // SE音量
		self::TYPE_VOICE => 100, // VOICE音量
		self::TYPE_SCREEN_ORIENTATION =>
            self::SCREEN_ORIENTATION_LANDSCAPE, // 画面向き
		self::TYPE_PUSH => self::PUSH_YES, // PUSH通知設定
		self::TYPE_ADV => 0, // ADV調整
	];

	/**
	 * プレイヤデータを登録する
	 *
	 * @param integer $playerId プレイヤID
	 */
	public static function regist($playerId)
	{
		foreach (self::$initial_value as $type => $value)
		{
			$playerData = new self();
			$playerData->player_id = $playerId;
			$playerData->type = $type;
			$playerData->value = $value;
			$playerData->save();
		}
	}

	/**
	 * プレイヤデータを取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
			self::where('player_id', $playerId)
				->get();

		return $model;
	}

	/**
	 * 種別からプレイヤデータを取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $type 種別
	 * @return self 本クラス
	 */
	public static function getByType($playerId, $type)
	{
		$model =
			self::where('player_id', $playerId)
				->where('type', $type)
				->first();

		return $model;
	}

}
