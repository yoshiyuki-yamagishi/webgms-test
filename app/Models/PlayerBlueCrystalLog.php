<?php

namespace App\Models;
use App\Utils\DateTimeUtil;

/**
 * player_blue_crystal_log:プレイヤ蒼の結晶ログ のモデル
 *
 */
class PlayerBlueCrystalLog extends BaseGameLogModel
{
    public static $target_id_name = 'player_blue_crystal_id';
    
	protected $table = 'player_blue_crystal_log';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_GIVE = 1;
    const TYPE_PAY = 2;
    
}
