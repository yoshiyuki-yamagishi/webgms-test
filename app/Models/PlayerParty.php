<?php

namespace App\Models;



/**
 * player_party:プレイヤパーティ のモデル
 *
 */
class PlayerParty extends BaseGameModel
{
	protected $table = 'player_party';
	protected $primaryKey = 'id';

    const MAX_MAIN_CHARACTERS = 4; // 6 -> 4 予定
    const MAX_SUB_CHARACTERS = 2; // 0 -> 2 予定
    const MAX_CHARACTERS = self::MAX_MAIN_CHARACTERS + self::MAX_SUB_CHARACTERS;
    
    const MAX_MAIN_POSITIONS = 6; // 6 ポジション
    const MAX_SUB_POSITIONS = 2; // 0 -> 2 予定
    const MAX_POSITIONS = self::MAX_MAIN_POSITIONS + self::MAX_SUB_POSITIONS;
    
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

	/**
	 * プレイヤパーティ取得
	 *
	 * @param integer $playerId プレイヤID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerId($playerId)
	{
		$model =
			self::where('player_id', $playerId)
				->get();

		return $model;
	}

	/**
	 * プレイヤキャラクタIDよりプレイヤパーティ取得
	 *
	 * @param string $playerCharacterId プレイヤキャラクタID
	 * @return array 本クラスの配列
	 */
	public static function getByPlayerCharacterId($playerCharacterId)
	{
		$model =
			self::where('player_character_id_1', $playerCharacterId)
				->orWhere('player_character_id_2', $playerCharacterId)
				->orWhere('player_character_id_3', $playerCharacterId)
				->orWhere('player_character_id_4', $playerCharacterId)
				->orWhere('player_character_id_5', $playerCharacterId)
				->orWhere('player_character_id_6', $playerCharacterId)
				->get();

		return $model;
	}

	/**
	 * パーティ番号よりプレイヤパーティ取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $partyNo パーティ番号
	 * @return self 本クラス
	 */
	public static function getByPartyNo($playerId, $partyNo)
	{
		$model =
			self::where('player_id', $playerId)
				->where('party_no', $partyNo)
				->first();

		return $model;
	}

	/**
	 * 編成中のパーティ数を取得する
	 *
	 * @param integer $playerId プレイヤID
	 * @return integer パーティ数
	 */
	public static function countParty($playerId)
	{
        $query = self::where('player_id', $playerId);
        
        // 控えのみのは、カウントしない
        $query->where(function($query)
        {
            $query
                ->orWhereNotNull('player_character_id_1')
                ->orWhereNotNull('player_character_id_2')
                ->orWhereNotNull('player_character_id_3')
                ->orWhereNotNull('player_character_id_4')
                ->orWhereNotNull('player_character_id_5')
                ->orWhereNotNull('player_character_id_6');
        });

        return (int)$query->count();
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $partyNo パーティ番号
	 * @param integer $playerCharacterId1 プレイヤキャラクタID1
	 * @param integer $playerCharacterId2 プレイヤキャラクタID2
	 * @param integer $playerCharacterId3 プレイヤキャラクタID3
	 * @param integer $playerCharacterId4 プレイヤキャラクタID4
	 * @param integer $playerCharacterId5 プレイヤキャラクタID5
	 * @param integer $playerCharacterId6 プレイヤキャラクタID6
	 * @return boolean true:成功,false:失敗
	 */
	public static function regist(
        $playerId, $partyNo,
        $playerCharacterId1, $playerCharacterId2, $playerCharacterId3,
        $playerCharacterId4, $playerCharacterId5, $playerCharacterId6
    )
	{
		// プレイヤパーティ取得
		$model = self::getByPartyNo(
            $playerId, $partyNo
        );
		if (!isset($model))
		{
			// 新規作成
			$model = new self();
			$model->player_id = $playerId;
			$model->party_no = $partyNo;
		}

		$model->player_character_id_1	= $playerCharacterId1;
		$model->player_grimoire_id_1	= null;
		$model->player_character_id_2	= $playerCharacterId2;
		$model->player_grimoire_id_2	= null;
		$model->player_character_id_3	= $playerCharacterId3;
		$model->player_grimoire_id_3	= null;
		$model->player_character_id_4	= $playerCharacterId4;
		$model->player_grimoire_id_4	= null;
		$model->player_character_id_5	= $playerCharacterId5;
		$model->player_grimoire_id_5	= null;
		$model->player_character_id_6	= $playerCharacterId6;
		$model->player_grimoire_id_6	= null;
        
		return $model->save();
	}

	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $playerGrimoireId 魔道書ID
	 * @return boolean true:装備中
	 */
	public static function isUsedGrimoire(
        $playerId, $playerGrimoireId
    )
    {
        return
            self::where('player_id', $playerId)
			->orWhere('player_grimoire_id_1', $playerGrimoireId)
            ->orWhere('player_grimoire_id_2', $playerGrimoireId)
            ->orWhere('player_grimoire_id_3', $playerGrimoireId)
            ->orWhere('player_grimoire_id_4', $playerGrimoireId)
            ->orWhere('player_grimoire_id_5', $playerGrimoireId)
            ->orWhere('player_grimoire_id_6', $playerGrimoireId)
            ->exists();
    }

	/**
	 * キャラクタ設定
	 *
	 * @param integer $no キャラクター位置 1 〜 6
	 * @param integer $playerCharacterId プレイヤキャラクタID
	 * @return なし
	 */
	public function setCharacter($no, $playerCharacterId)
	{
        $funcName = 'player_character_id_' . $no;

        if ($playerCharacterId <= 0)
            $this->$funcName = null;
        else
            $this->$funcName = $playerCharacterId;
    }

	/**
	 * 魔道書設定
	 *
	 * @param integer $no キャラクター位置 1 〜 6
	 * @param integer $playerGrimoireId プレイヤ魔道書ID
	 * @return なし
	 */
	public function setGrimoire($no, $playerGrimoireId)
	{
        $funcName = 'player_grimoire_id_' . $no;

        if ($playerGrimoireId <= 0)
            $this->$funcName = null;
        else
            $this->$funcName = $playerGrimoireId;
    }

	/**
	 * 編成数の取得
	 *
	 * @param string $code 'character' / 'grimoire'
	 * @return 編成数
	 */
	public function getCount($code)
	{
        $count = 0;
        for ($i = 1; $i <= 6; ++ $i)
        {
            $prop = 'player_' . $code . '_id_' . $i;
            if (!isset($this->$prop) || $this->$prop <= 0)
                continue;

            ++ $count;
        }
        return $count;
    }

	/**
	 * 編成キャラクター数の取得
	 *
	 * @return 編成キャラクター数
	 */
	public function getCharacterCount()
	{
        return self::getCount('character');
    }

	/**
	 * 編成魔道書数の取得
	 *
	 * @return 編成魔道書数
	 */
	public function getGrimoireCount()
	{
        return self::getCount('grimoire');
    }
    
}
