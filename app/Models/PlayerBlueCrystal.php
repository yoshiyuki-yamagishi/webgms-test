<?php

namespace App\Models;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * player_blue_crystal:プレイヤ蒼の結晶 のモデル
 *
 */
class PlayerBlueCrystal extends BaseGameModel
{
	protected $table = 'player_blue_crystal';
	protected $primaryKey = 'id';
	//	public $incrementing = false;
	//	public $timestamps = false;

	/*
	 protected $fillable = [
	 //'id',
	 ];
	 */

    const TYPE_FREE = 1; // 無償
    const TYPE_CHARGED = 2; // 有償
    const TYPE_BOTH = 3; // 両方

	/**
	 * 蒼の結晶の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $types 取得タイプ
	 * @param string $now DB 形式の日付
	 * @return integer 蒼の結晶の所持数
	 */
	public static function _getNum($playerId, $types, $now)
	{
        $q =
           self::where('player_id', $playerId)
           ->where('delete_flag', '0')
           ->where('expired_at', '>=', $now);

        switch ($types)
        {
        case self::TYPE_FREE:
            $q->where('payment_type', '=', 0);
            break;
        case self::TYPE_CHARGED:
            $q->where('payment_type', '>', 0);
            break;
        case self::TYPE_BOTH:
            break;
        default:
            assert(false);
            return -1;
        }

        // 返り値の型が string //
        $sum = $q->sum("num");
        // DebugUtil::e_log_ex('sum', 'sum', $sum);
        return intval($sum);
    }

	/**
	 * 無償蒼の結晶の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now DB 形式の日付
	 * @return integer 蒼の結晶の所持数
	 */
	public static function getFreeNum($playerId, $now)
	{
        return self::_getNum($playerId, self::TYPE_FREE, $now);
    }
    
	/**
	 * 有償蒼の結晶の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now DB 形式の日付
	 * @return integer 蒼の結晶の所持数
	 */
	public static function getChargedNum($playerId, $now)
	{
        return self::_getNum($playerId, self::TYPE_CHARGED, $now);
    }

	/**
	 * 蒼の結晶の所持数を取得
	 *
	 * @param integer $playerId プレイヤID
	 * @param string $now DB 形式の日付
	 * @return integer 蒼の結晶の所持数
	 */
	public static function getNum($playerId, $now)
	{
        return self::_getNum($playerId, self::TYPE_BOTH, $now);
    }

	/**
	 * 蒼の結晶の所持数を取得
	 *
	 * @param integer $freeNum 蒼の結晶の所持数 (無償)
	 * @param integer $playerId プレイヤID
	 * @param string $now DB 形式の日付
	 * @return integer 蒼の結晶の所持数 (無償と有償)
	 */
	public static function getBothNum(&$freeNum, $playerId, $now)
	{
        // 両方を 1 SQL で取得する //
        
        $ret = self::selectRaw(
            'sum(num) as total_num,' . 
            'sum(case when payment_type = 0 then num else 0 end) as free_num'
        )
             ->where('player_id', $playerId)
             ->where('delete_flag', '0')
             ->where('expired_at', '>=', $now)
             ->first();

        $freeNum = intval($ret->free_num);
        return intval($ret->total_num);
    }
    
	/**
	 * 蒼の結晶消費用にセレクトする
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $types 有償/無償 種別
	 */
	public static function getForPay($playerId, $types)
	{
        // 無償優先、古い順に消費する
        //
        // 石によって、使用期限が違う場合は、
        // 消費期限に近いものからかもしれないが、現状は大丈夫

        $q =
           self::where('player_id', $playerId)
           ->where('delete_flag', 0)
           ->where('num', '>', 0);

        switch ($types)
        {
        case self::TYPE_FREE:
            $q->where('payment_type', '=', 0);
            break;
        case self::TYPE_CHARGED:
            $q->where('payment_type', '>', 0);
            break;
        case self::TYPE_BOTH:
            $q->orderByRaw('payment_type = 0 desc');
            break;
        default:
            throw \App\Exceptions\ParamException::makeInvalid(
                'PlayerBlueCrystal::getForPay', 'types', $types
            );
        }

        $q->orderBy('created_at', 'asc');
        return $q->get();
    }

	/**
	 * プレイヤが所持している蒼の結晶を減らす
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $num 減らす蒼の結晶数
	 * @return integer 減らす蒼の結晶数の残り
	 */
    /*
	public static function substract($playerId, $types, $num)
	{
        // 無償優先、古い順に消費する
        //
        // 石によって、使用期限が違う場合は、
        // 消費期限に近いものからかもしれないが、現状は大丈夫

        $q =
           self::where('player_id', $playerId)
           ->where('delete_flag', 0)
           ->where('num', '>', 0);

        switch ($types)
        {
        case self::TYPE_FREE:
            $q->where('payment_type', '=', 0);
            break;
        case self::TYPE_CHARGED:
            $q->where('payment_type', '>', 0);
            break;
        case self::TYPE_BOTH:
            $q->orderByRaw('payment_type = 0 desc');
            break;
        default:
            throw \App\Exceptions\ParamException::makeInvalid(
                'PlayerBlueCrystal::substract', 'types', $types
            );
        }

        $q->orderBy('created_at', 'asc');

        // 処理開始

		$blueCrystalList = $q->get();

		foreach ($blueCrystalList as $blueCrystal)
		{
			// 回しているレコード数が減らす数以上だったら、そのまま減らして終了
            
			if ($blueCrystal->num >= $num)
			{
				// 蒼の結晶数を減らす
				$blueCrystal->num -= $num;
				$blueCrystal->save();

				$num = 0;
				break;
			}
            
			// 減らす数の方が大きかったら、
            // そのレコードは 0 にして次のレコードを減らす
            
            $substractNum = $blueCrystal->num;
            $num -= $substractNum;
            assert($num > 0);

            $blueCrystal->num = 0;
            $blueCrystal->save();
		}

		return $num;
	}
    */
    
	/**
	 * 登録
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $paymentId 決済ID
	 * @param integer $paymentNum 購入数
	 * @return boolean true:成功,false:失敗
	 */
	public static function regist(
        $playerId, $paymentType, $srcType, $srcId, $paymentNum, $expiredAt
    )
	{
        if (!isset($expiredAt) || strlen($expiredAt) <= 0)
            $expiredAt = DateTimeUtil::getFarFuture();
        if (!isset($paymentId) || strlen($paymentId) <= 0)
            $paymentId = null;
        
		$model = new self();
		$model->player_id = $playerId;
        $model->payment_type = $paymentType;
        $model->src_type = $srcType;
        $model->src_id = $srcId;
        $model->payment_num = $paymentNum;
        $model->num = $paymentNum;
        $model->delete_flag = 0;
        $model->expired_at = $expiredAt;
      
		$model->save();
        return $model;
	}

    public static function bothOrCharged($onlyChanged)
    {
        return $onlyChanged ? self::TYPE_CHARGED : self::TYPE_BOTH;
    }
    
    public function isCharged()
    {
        return $this->payment_type > 0;
    }
    
}
