<?php
/**
 * GMS クエスト のサービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * GMS ログインボーナス のサービス
 *
 */
class GmsLoginBonusService extends BaseService
{
	/**
	 * ログインボーナスの進捗度を編集 
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function setLoginBonusProgress($playerId, $loginBonuses, $id)
	{
		$now = DateTimeUtil::getNOW();
		// ログボ編集
		// テーブルのデータを削除してから入れなおす
		$findLoginBonus = LoginBonusReward::getOne($id);
		DebugUtil::e_log('findLoginBonus', 'findLoginBonus', $findLoginBonus);

		PlayerLoginBonus::where('player_id', $playerId)
                             ->where('login_bonus_id', $findLoginBonus->login_bonus_id)
                             ->delete();


		foreach ($loginBonuses as $loginBonus)
		{
			if ($loginBonus->login_bonus_id != $findLoginBonus->login_bonus_id)
				continue;
			if ($loginBonus->count > $findLoginBonus->count)
				break;

			PlayerLoginBonus::regist(
				$playerId, $loginBonus, $loginBonus->count, $now
			);
		}
	}
}