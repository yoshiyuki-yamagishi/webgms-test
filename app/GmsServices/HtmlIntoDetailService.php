<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * GMS サービス
 *
 */
class HtmlIntoDetailService extends HtmlOutPutService
{
	/**
	 * お知らせ詳細
	 *
	 * @param integer $dbNo データベース番号
	 */
	public static function write($infoType, $infos, $now)
	{
		$dir = storage_path("temp_html");
		//DebugUtil::e_log('dir', 'dir', $dir);
		DebugUtil::e_log('infosService', 'infosService', $infos);

		$nowFname = $now->format('Y_m_d_H_i_s');

		$dir .= '/' . $nowFname;
		$typeName = self::$typeNames[$infoType - 1];
		$dir .= '/' . $typeName;
		{
			if (!file_exists($dir))
			{
				$mode = 0777;
				mkdir($dir, $mode, TRUE);
			}
		}

		


		// fwrite($handle, '<table>');
		foreach ($infos as $info)
		{

			$path = $dir . '/' . $info->fname . '.html';

			$handle = fopen($path, 'wt');
			$content = $info->content;
			$title = $info->title;
			fwrite($handle, 
			'<h1>' . 
			$title . 
			'</h1>' . 
			'<tr><td>' . 
			$content .  
			'</td></tr>'

			
		);
		
		}
		// fwrite($handle, '</table>');

	}
}