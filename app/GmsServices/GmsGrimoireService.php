<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Models\PlayerCharacter;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\GrimoireAwakeCoefficient;
use App\Models\MasterModels\LevelCoefficient;
use App\Models\MasterModels\RarityCoefficient;
use App\Utils\DebugUtil;


/**
 * GMS サービス
 *
 */
class GmsGrimoireService extends BaseService
{

	public static function maxGrimoireAwake()
	{
		// TODO master参照
		return 5;
	}

		/**
		 * パラメーターを計算する
		 *
		 * @param integer $baseValue 基本値
		 * @param array $coeffs 倍率
		 * @return integer パラメーター
		 */
		public static function calcParam($baseValue, $coeffs)
		{
			$value = floatval($baseValue); // 一応、変換
			foreach ($coeffs as $coeff)
				$value *= $coeff;

			return (int)round($value);
		}
		

	/**
	 * 
	 *魔道書パラメータ計算
	 * 
	 */
	public static function calcGrimoireParam($grimoireId, $awake)
	{

        // パラメータの計算 【廃止】
        // 魔道書取得
        
        $grimoire = Grimoire::getOne($grimoireId);
        if (empty($grimoire))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'grimoire', 'id', $grimoireId
            );
        }

        // 旧) 覚醒は、0 〜 4 だが、マスタは、1 〜 5
        //
        // 新) 覚醒は、0 〜 5 で、マスタは、1 〜 5 が用意されている
		// このコードは (旧) なので、復活する場合は要修正
		
		if ($awake > 0)
		{
			
			$grimoireAwake = GrimoireAwakeCoefficient::getOne(
				$awake
			);
			
			DebugUtil::e_log('grimoireAwake', 'grimoireAwake', $grimoireAwake);

			if (empty($awake))
			{
				throw \App\Exceptions\MasterException::makeNotFound(
					'grimoire_awake_coefficient', 'grimoire_awake',
					$playerGrimoire->awake
				);
			}

			$hp = self::calcParam(
				$grimoire->min_hp, [$grimoireAwake->hp_coefficient]
			);
			
			$atk = self::calcParam(
				$grimoire->min_atk, [$grimoireAwake->atk_coefficient]
			);
			
			$def = self::calcParam(
				$grimoire->min_def, [$grimoireAwake->def_coefficient]
			);

		}
		else
		{
			$hp = $grimoire->min_hp;
			$atk = $grimoire->min_atk;
			$def = $grimoire->min_def;
		}
		DebugUtil::e_log('grimoireSt', 'awake', $awake);
		DebugUtil::e_log('grimoireSt', 'hp', $hp);
		DebugUtil::e_log('grimoireSt', 'atk', $atk);
		DebugUtil::e_log('grimoireSt', 'def', $def);
		return [
			'awake' => $awake,
			'hp' => $hp,
			'atk' => $atk,
			'def' => $def,
		];
	}

}