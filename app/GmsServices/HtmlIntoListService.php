<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use DateTime;
use App\Utils\DateTimeUtil;
use App\Utils\AwsS3Util;


// お知らせリストを作成
/**
 * GMS サービス
 *
 */
class HtmlIntoListService extends HtmlOutPutService
{

	/**
	 * お知らせリスト
	 *
	 * @param string $fname ファイル名
	 * @param string $value 値
	 * @return なし
	 */
	public static function write($infoType, $infos, $now)
    {

		// $dir = storage_path("temp_html");
		//DebugUtil::e_log('dir', 'dir', $dir);
		DebugUtil::e_log('infosService', 'infosService', $infos);

		// $nowFname = $now->format('Y_m_d_H_i_s');

		// $dir .= '/' . $nowFname;
		$dir = '/gms_test';
		$typeName = self::$typeNames[$infoType - 1];
		$dir .= '/' . $typeName;
		$infoPath = $dir;

		// $indexPath = str_replace(' ', '/', $indexPath);

		// {
		// 	if (!file_exists($dir))
		// 	{
		// 		$mode = 0777;
		// 		mkdir($dir, $mode, TRUE);
		// 	}
		// }

		// DebugUtil::e_log('dir', 'dir', $dir);
		// $dir .= '/' . $fname;
		// DebugUtil::e_log('dir', 'dir', $dir);

		
		$path = $dir . '/' . 'index.html';
		DebugUtil::e_log('path', 'path', $path);
		
		// $handle = fopen($path, 'wt');

		// $path = '/gms_test/abc.html';

		$nowTitle = $now->format('Y/m/d H:i:s');		

		$contents = '';
		$contents .= '<table>';
		foreach ($infos as $info)
		{
			$title = $info->title;
			$contents .=  
				'<tr><td>' . 
				$nowTitle . 
				'</td>' . 
				'<td>' . 
				'<a href"' . $infoPath . '/' . $info->fname . '.html">' . $title . '<a>' . 
				'</td></tr>';
		}
		$contents .= '</table>';
		// $s3Util = new AwsS3Util();
		// $s3Util->put($path, $contents);
	}
}