<?php
/**
 * GMS クエスト のサービス
 *
 */

namespace App\GmsServices;

use App\Models\PlayerQuest;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Models\PlayerMission;

use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * GMS ミッション のサービス
 *
 */
class GmsMissionService extends BaseService
{
	/**
	 * ミッションを登録
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function register($playerId, $missionId, $missionType, $now, $num, $mission)
	{
		if ($missionType == 1)
		{
			PlayerMission::where('player_id', $playerId)
							->where('mission_id', $missionId)
							->where('start_day', '<=', $now)
							->delete();
		}
		elseif ($missionType == 2)
		{
			PlayerMission::where('player_id', $playerId)
							->where('mission_id', $missionId)
							->delete();
		}

		if ($num == 0)
		{
			return;
		}

		$playerMission = new PlayerMission();
		$playerMission->player_id = $playerId;
		$playerMission->mission_id = $missionId;
		$playerMission->mission_type = $missionType;
		$playerMission->progress_count = $num;
		$playerMission->count = $mission->count;
		$playerMission->remuneration = $mission->mission_remuneration;
        $playerMission->remuneration_type = $mission->remuneration_item_type;
		$playerMission->remuneration_count = $mission->mission_remuneration_count;
		$playerMission->start_day = $mission->start_day;
		$playerMission->end_day = $mission->end_day;

		if ($num == $mission->count)
		{
			$playerMission->report_flag = 1;
		}
		else
		{
			$playerMission->report_flag = 0;
		}
		$playerMission->save();
	}

}