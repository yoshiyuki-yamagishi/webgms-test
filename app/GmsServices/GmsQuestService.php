<?php
/**
 * GMS クエスト のサービス
 *
 */

namespace App\GmsServices;

use App\Models\PlayerQuest;
use App\Services\QuestService;
use App\Services\BaseService;

use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * GMS クエスト のサービス
 *
 */
class GmsQuestService extends BaseService
{
	/**
	 * クエストの進捗度を編集 (ストーリー)
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function setQuestProgress($playerId, $storyQuests, $questId, $missionFlag1, $missionFlag2, $missionFlag3, $clearNum)
	{
		if ($clearNum <= 0)
		{
			throw \App\Exceptions\GameException::make(
				'clearNum is less than 1'
			);
		}
		// ストーリークエスト進捗
		$questCategory = QuestService::QUEST_CATEGORY_STORY;

		$storyQuestService = QuestService::make($questCategory);

		PlayerQuest::where('player_id', $playerId)
					->where('quest_category', $questCategory)
					->delete();

		$yes = QuestService::CLEAR_FLAG_YES;
		$no = QuestService::CLEAR_FLAG_NO;

		$requestMissionFlags = [$yes, $yes, $yes];

		if ($missionFlag1 == null)
			$requestMissionFlags[0] = $no;

		if ($missionFlag2 == null)
			$requestMissionFlags[1] = $no;

		if ($missionFlag3 == null)
			$requestMissionFlags[2] = $no;

		foreach ($storyQuests as $storyQuest)
		{
			DebugUtil::e_log('storyQuest', 'storyQuest', $storyQuest);
			$last = $storyQuest->id == $questId;
			if ($last)
			{
				$missionFlags = $requestMissionFlags;
				$num = $clearNum;
			} else
			{
				$missionFlags = [$yes, $yes, $yes];
				$num = 1000;
			}
			static::setProgress(
				$storyQuestService, $playerId, $storyQuest->story_quest_chapter_id, 
				$storyQuest->id, $yes,
				$missionFlags, $num

			);
			if ($last)
			{
				break;
			}
		}
	}


	/**
	 * クエストの進捗度を編集 (キャラクター)
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function setCharaQuestProgress($playerId, $characterQuests, $questId, $missionFlag1, $missionFlag2, $missionFlag3, $clearNum)
	{
		if ($clearNum <= 0)
		{
			throw \App\Exceptions\GameException::make(
				'clearNum is less than 1'
			);
		}
		// キャラクタークエスト進捗
		$questCategory = QuestService::QUEST_CATEGORY_CHARACTER;

		$characterQuestService = QuestService::make($questCategory);

		PlayerQuest::where('player_id', $playerId)
					->where('quest_category', $questCategory)
					->delete();

		$yes = QuestService::CLEAR_FLAG_YES;
		$no = QuestService::CLEAR_FLAG_NO;

		$requestMissionFlags = [$yes, $yes, $yes];

		if ($missionFlag1 == null)
			$requestMissionFlags[0] = $no;

		if ($missionFlag2 == null)
			$requestMissionFlags[1] = $no;

		if ($missionFlag3 == null)
			$requestMissionFlags[2] = $no;

		foreach ($characterQuests as $characterQuest)
		{
			$last = $characterQuest->id == $questId;
			if ($last)
			{
				$missionFlags = $requestMissionFlags;
				$num = $clearNum;
			} else
			{
				$missionFlags = [$yes, $yes, $yes];
				$num = 1000;
			}
			static::setProgress(
				$characterQuestService, $playerId, $characterQuest->character_quest_chapter_id, 
				$characterQuest->id, $yes, $missionFlags, $num

			);
			if ($last)
			{
				break;
			}
		}
	}

	/**
	 * クエストの進捗度を編集 (イベント)
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function setEventQuestProgress($playerId, $eventQuests, $questId, $missionFlag1, $missionFlag2, $missionFlag3, $clearNum)
	{
		if ($clearNum <= 0)
		{
			throw \App\Exceptions\GameException::make(
				'clearNum is less than 1'
			);
		}
		// イベントクエスト進捗
		$questCategory = QuestService::QUEST_CATEGORY_EVENT;

		$eventQuestService = QuestService::make($questCategory);

		PlayerQuest::where('player_id', $playerId)
					->where('quest_category', $questCategory)
					->delete();

		$yes = QuestService::CLEAR_FLAG_YES;
		$no = QuestService::CLEAR_FLAG_NO;

		$requestMissionFlags = [$yes, $yes, $yes];

		if ($missionFlag1 == null)
			$requestMissionFlags[0] = $no;

		if ($missionFlag2 == null)
			$requestMissionFlags[1] = $no;

		if ($missionFlag3 == null)
			$requestMissionFlags[2] = $no;

		foreach ($eventQuests as $eventQuest)
		{
			$last = $eventQuest->id == $questId;
			if ($last)
			{
				$missionFlags = $requestMissionFlags;
				$num = $clearNum;
			} else
			{
				$missionFlags = [$yes, $yes, $yes];
				$num = 1000;
			}
			static::setProgress(
				$eventQuestService, $playerId, $eventQuest->event_id, 
				$eventQuest->id, $yes, $missionFlags, $num

			);
			if ($last)
			{
				break;
			}
		}
	}


	/**
	 * クエストの進捗度を編集
	 *
	 * @param ReceiptCheckRequest $request リクエスト
	 * @return なし
	 */
	public static function setProgress(
		$questService, $playerId, $chapterId, $questId, $clearFlag, $missionFlags, $num
	)
	{
		$now = DateTimeUtil::getNOW();

		// PlayerQuest の準備

		$playerQuest = $questService->getPlayerQuest(
			$playerId,
			$chapterId, 
			$questId
		);

		// 無ければ新規登録 (普通は無い)

		$questService->preparePlayerQuest(
			$playerQuest, 
			$playerId,
			$chapterId, 
			$questId
		);
		
		// クリア済みにして保存

		$playerQuest->clear_flag = $clearFlag;
		$playerQuest->cleared_at = $now;

		// ミッションフラグ設定

		$mfCount = isset($missionFlags) ? count($missionFlags) : 0;

        for ($i = 0; $i < $mfCount; ++ $i)
        {
            $propName = "mission_flag_" . ($i + 1);
            $playerQuest->$propName = $missionFlags[$i];
        }

		// クリア数を増加する
		$playerQuest->clear_count = $num;
		
		$playerQuest->save();
	}
}