<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

/**
 * GMS サービス
 *
 */
class GmsService extends BaseService
{
	/**
	 * データベース番号の設定
	 *
	 * @param integer $dbNo データベース番号
	 */
	public static function updateDbNo($playerId)
	{
		// playerCommonテーブルから DB番号取得して
		// setDbNo を呼ぶ
		$playerCommon = PlayerCommon::getByPlayerId($playerId);
		$dbNo = $playerCommon->db_no;
		SessionService::setDbNo($dbNo);
		DebugUtil::e_log('playerCommon', 'playerCommon', $playerCommon);
	}
}