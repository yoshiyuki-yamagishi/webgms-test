<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Models\PlayerGachaResult;
use App\Services\GachaService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Models\PlayerCharacter;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\GachaLot;
use App\Models\MasterModels\GachaGroup;
use App\Models\MasterModels\GrimoireAwakeCoefficient;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\LevelCoefficient;
use App\Models\MasterModels\RarityCoefficient;
use App\Utils\DebugUtil;


/**
 * GMS サービス
 *
 */
class GmsGachaService extends BaseService
{

	public static function get()
	{
		// ガチャを取得
		$gacha = Gacha::getOne($gachaId);
		if (!isset($gacha))
		{
			throw \App\Exceptions\MasterException::makeNotFound(
				'gacha', 'gacha_id', $gachaId
			);
		}
	}
	

	/**
	 *ガチャ実行
	 * 
	 */
	public static function try($gachaId, $isTen, $count)
	{
		$gacha = Gacha::getOne($gachaId);
		$confirmTotalRate = 0;
        $realGachaCount = 0;
        $gachaResult = [];
		// ガチャレート取得
		$gachaLotList = GachaLot::getAll($gacha->gacha_lot_id);
		// DebugUtil::e_log('tryGachaLot', 'tryGachaLot', $gachaLotList);
		$totalRate = GachaLot::calcRateSum($gachaLotList);

        $realGachaCount = 0;
        $payCount = 0;
        $isConfirm = false;
        self::calcGachaParams(
            $realGachaCount,
            $payCount,
            $isConfirm,
            $gacha,
            $isTen
        );


		$confirmLotList = [];
        if ($gacha->confirm > 0 && $isConfirm)
            $confirmLotList = GachaLot::getAll($gacha->confirm);


		// DebugUtil::e_log('totalRate', 'totalRate', $totalRate);
		if (count($confirmLotList) > 0)
            $confirmTotalRate = GachaLot::calcRateSum($confirmLotList);

        if ($confirmTotalRate > 0)
            -- $realGachaCount;


        // DebugUtil::e_log('tryCount', 'tryCount', $count);
        for ($loop = 1; $loop <= $count; ++ $loop)
        {
            // ガチャ結果計算 (確定枠)

            if ($confirmTotalRate > 0)
            {
                $resultList[] = static::calcResult(
                    $gacha, $confirmLotList, $confirmTotalRate,
                    PlayerGachaResult::FLAG_CONFIRMED
                );
            }

            // ガチャ結果計算

            for ($i = 0; $i < $realGachaCount; ++ $i)
            {
                $resultList[] = static::calcResult(
                    $gacha, $gachaLotList, $totalRate,
                    PlayerGachaResult::FLAG_NONE
                );
            }
        }
        // DebugUtil::e_log('resultList', 'resultList', $resultList);
        return $resultList;
    }
    

    /**
	 * ガチャパラメーター計算処理
	 *
	 */
	public static function calcGachaParams(
        &$realGachaCount,
        &$payCount,
        &$isConfirm,
        $gacha,
        $isTen
    )
	{
		$payCount = 0;
        $isConfirm = false;

		if ($isTen)
        {
            $isConfirm = true; // 確定枠あり
			$payCount = $gacha->ten_item;
            $realGachaCount = $gacha->ten_item_get_count;
            //　10連エラーチェック
        }
        else
        {
            $payCount = $gacha->single_item;
            // single ないときのエラーチェック
            $realGachaCount = $gacha->single_item_get_count;
        }
    }


	public static function calcResult(
        $gacha, $gachaLotList, $totalRate, $resultFlag
    )
	{
        // 確率でガチャグループ ID を決める

        $resultLot = null;
        {
            $r = mt_rand(1, $totalRate);

            $rateSum = 0;
            foreach ($gachaLotList as $gachaLot)
            {
                $rateSum += $gachaLot->rate;
                if ($r <= $rateSum)
                {
                    $resultLot = $gachaLot;
                    break;
                }
            }
        }

        assert(isset($resultLot));
        // DebugUtil::e_log('Gacha', 'resultLot', $resultLot);

        // ガチャグループを取得

        $gachaGroupList = GachaGroup::getAll(
            $resultLot->gacha_group_id
        );
        // DebugUtil::e_log('Gacha', 'gachaGroupList', $gachaGroupList);
		// DebugUtil::e_log('gachaGroupList', 'gachaGroupList', $gachaGroupList);
        // 個数チェック

        $groupCount = count($gachaGroupList);
        if ($groupCount <= 0)
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'gacha_group_list', 'gacha_group_id',
                $resultLot->gacha_group_id
            );
        }

        // 確率で出るアイテムを決定する

        $groupIndex = mt_rand(0, $groupCount - 1);
        $resultGroup = $gachaGroupList[$groupIndex];

        // DebugUtil::e_log('Gacha', 'resultGroup', $resultGroup);

        return $resultGroup;

        // ガチャ結果生成

        // return PlayerGachaResult::make(
        //     $playerGacha,
        //     $resultGroup,
        //     $resultFlag
        // );
    }

    public static function calcRates($gachaLotList)
    {
        $totalRate = 0;
		foreach ($gachaLotList as $gachaLot)
		{
			$totalRate += $gachaLot->rate;
		}
		DebugUtil::e_log('totalRate', 'totalRate', $totalRate);
        // ガチャレート、ガチャグループ取得
        $gachaGroupList = [];
		foreach ($gachaLotList as $gachaLot)
		{
			$gachaGroups = GachaGroup::getAll($gachaLot->gacha_group_id);
			$rate = $gachaLot->rate*100/$totalRate/count($gachaGroups);
			DebugUtil::e_log('rate', 'rate', $rate);
			foreach ($gachaGroups as $gachaGroup)
			{
                $gachaResult = clone $gachaGroup;

				$gachaResult->rate = round($rate, 2).'%';
				$contentType = $gachaGroup->content_type;
				$contentId = $gachaGroup->content_id;
				// DebugUtil::e_log('contentId', 'contentId', $contentId);
				$gachaLot->get_item_id = $contentId;
				if ($contentType == 1)
				{
					$getItem = CharacterBase::getOne($contentId);
					$gachaResult->hp = 'HP：'.$getItem->min_hp;
					$gachaResult->atk = 'ATK：'.$getItem->min_atk;
					$gachaResult->def = 'DEF：'.$getItem->min_def;
					$getItemD = CharacterDictionary::getOne($getItem->character_name_id);
					$gachaResult->item_name = $getItemD->dictionary_ja;
				}
				elseif ($contentType == 2)
				{
					$getItem = Grimoire::getOne($contentId);
					$gachaResult->hp = 'HP：'.$getItem->min_hp;
					$gachaResult->atk = 'ATK：'.$getItem->min_atk;
					$gachaResult->def = 'DEF：'.$getItem->min_def;
					$getItemD = GrimoireDictionary::getOne($getItem->grimoire_name_id);
					$gachaResult->item_name = $getItemD->dictionary_ja;
				}
				elseif ($contentType == 3)
				{
					$getItem = Item::getOne($contentId);
					$gachaResult->hp = 'HP：なし';
					$gachaResult->atk = 'ATK：なし';
					$gachaResult->def = 'DEF：なし';
					$getItemD = ItemDictionary::getOne($getItem->item_name_id);
					$gachaResult->item_name = $getItemD->dictionary_ja;
				}
				else
				{
					$gachaResult->hp = 'HP：なし';
					$gachaResult->atk = 'ATK：なし';
					$gachaResult->def = 'DEF：なし';
					$gachaResult->item_name = '不明なアイテム';
				}
				// DebugUtil::e_log('gachaLot', 'gachaLot', $gachaLot);
				$gachaGroupList[] = $gachaResult;
			}

        }
        return $gachaGroupList;
    }



}