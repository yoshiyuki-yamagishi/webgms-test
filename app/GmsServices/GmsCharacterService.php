<?php
/**
 * GMS サービス
 *
 */

namespace App\GmsServices;

use App\Models\MasterModels\LoginBonusReward;
use App\Models\PlayerCommon;
use App\Models\PlayerLoginBonus;
use App\Services\QuestService;
use App\Services\BaseService;
use App\Services\SessionService;
use App\Models\PlayerCharacter;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\LevelCoefficient;
use App\Models\MasterModels\RarityCoefficient;
use App\Utils\DebugUtil;


/**
 * GMS サービス
 *
 */
class GmsCharacterService extends BaseService
{

	public static function maxCharacterLv()
	{
		// TODO master参照
		return 100;
	}

		/**
		 * パラメーターを計算する
		 *
		 * @param integer $baseValue 基本値
		 * @param array $coeffs 倍率
		 * @return integer パラメーター
		 */
		public static function calcParam($baseValue, $coeffs)
		{
			$value = floatval($baseValue); // 一応、変換
			foreach ($coeffs as $coeff)
				$value *= $coeff;

			return (int)round($value);
		}



	/**
	 * 
	 *キャラクターパラメータ計算
	 * 
	 */
	public static function calcCharacterParam($characterId, $evolve, $lv)
	{
		$charaBase = CharacterBase::getOne($characterId);
        $rarity0 = $charaBase->character_initial_rarity;
		// パラメータの計算 【廃止】
        $rarity = $rarity0 + $evolve;

        $rc = RarityCoefficient::getOne($rarity);
        if (!isset($rc))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'rarity_coefficient', 'rarity_coefficient_id', $rarity
            );
        }
        DebugUtil::e_log('PCR', 'rc', $rc);

        $lc = LevelCoefficient::getOne($lv);
        if (!isset($lc))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'level_coefficient', 'level_coefficient_id', $lv
            );
        }
		DebugUtil::e_log('PCR', 'lc', $lc);
			

        
        $hp = self::calcParam(
    		$charaBase->min_hp, [$rc->hp_coefficient, $lc->hp_coefficient]
		);
        
        $atk = self::calcParam(
            $charaBase->min_atk, [$rc->atk_coefficient, $lc->atk_coefficient]
        );
        
        $def = self::calcParam(
            $charaBase->min_def, [$rc->def_coefficient, $lc->def_coefficient]
		);

		return [
			'lv' => $lv,
			'hp' => $hp,
			'atk' => $atk,
			'def' => $def,
		];
	}




}