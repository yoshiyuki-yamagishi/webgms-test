<?php
/**GMSサービス
 *
 */

namespace App\GmsServices;


use App\Services\BaseService;

use App\Models\MasterModels\Item;
use App\Utils\DateTimeUtil;


class GmsDirectPresentService extends BaseService
{
    /**
     * 入力内容をもとにアイテムタイプでを返す
     * @param $item_type
     * @return int アイテムのタイプ
     */
    private static function getItemType($item_type)
    {
        $_item_type = 0;
        switch($item_type){
            case 'chara':
                $_item_type = ITEM::TYPE_CHARACTER;
                break;
            case 'grimoire':
                $_item_type = Item::TYPE_GRIMOIRE;
                break;
            case 'item':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'mana':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'fragment':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'fragment_powder':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'money':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'friend_point':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'blue_crystal':
                $_item_type = Item::TYPE_ITEM;
                break;
            case 'orb':
                $_item_type = Item::TYPE_ITEM;
                break;
          default :
                throw \App\Exceptions\DataException::make(
                '不正なデータータイプ : '.$item_type
                );
                break;
        }
        return $_item_type;
    }

    /**
     * タイプが資金系?はIDを渡す。出ないと０にする
     * @param $item_type
     * @return Integer
     */
    private static function getItemIdByType($item_type)
    {
        $_itemId = 0;
        switch($item_type){
            case 'mana':
                $_itemId = Item::ID_ELEMENT;
                break;
            case 'fragment_powder':
                $_itemId = Item::ID_POWDER;
                break;
            case 'money':
                $_itemId = Item::ID_P_DOLLAR;
                break;
            case 'friend_point':
                $_itemId = Item::ID_FRIEND_POINT;
                break;
            case 'blue_crystal':
                $_itemId = Item::ID_BLUE_CRYSTAL;
                break;
        }
        return $_itemId;
    }

    /**
     * @param array $csv_datas
     * @return array
     */
    public static function makeRecordData($csv_datas){
        $insert_record_datas = array();

        $insert_record_datas = self::_makeRecordData($csv_datas);

        return $insert_record_datas;
    }



    /**
     * レコード作成処理
     * @param array $csv_datas
     * @return array インサート用レコード
     */
    private static function _makeRecordData($csv_datas)
    {
        $insert_record_data = array();
        $column_count = 0;
        $priority = 0;

        $now = DateTimeUtil::getNOW();
        foreach ($csv_datas as $record_id => $column)
        {
            $column_count = count($column);
            $tmp_item_id = self::getItemIdByType($column[1]);
            $tmp_item_type = self::getItemType($column[1]);
            if ($column_count == 9)
            {
                $insert_record_data = [
                    'player_id' => $column[0],
                    'item_type' => $tmp_item_type,
                    'item_id' => $tmp_item_id !== 0 ? $tmp_item_id : $column[2],
                    'item_num' => $column[3],
                    'takable_days' => $column[6],
                    'type' => 3,
                    'info_type' => 1,
                    'info_id' => 0,
                    'delete_flag' => 0,
                    'start_at' => $column[4],
                    'end_at' => $column[5],
                    'message' => $column[8],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
                $priority = $column[7];

            }
            else if ($column_count == 8)
            {
                $insert_record_data = [
                    'player_id' =>  $column[0],
                    'item_type' => $tmp_item_type,
                    'item_id' => $tmp_item_id,
                    'item_num' => $column[2],
                    'takable_days' => $column[5],
                    'type' => 3,
                    'info_type' => 1,
                    'info_id' => 0,
                    'delete_flag' => 0,
                    'start_at' => $column[3],
                    'end_at' => $column[4],
                    'message' => $column[7],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
                $priority = $column[6];
            }

            $tmp_insert_record_datas[$priority][] = $insert_record_data;

        }
        //付与された時に見える順番に修正
        ksort($tmp_insert_record_datas);
        //マルチインサート用データー作り直し
        foreach ($tmp_insert_record_datas as $priority => $datas)
        {
            foreach($datas as $data)
            {
                $insert_record_datas[] = $data;
            }
        }
        return $insert_record_datas;
    }

}
