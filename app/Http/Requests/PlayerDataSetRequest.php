<?php
/**
 * プレイヤデータ設定 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤデータ設定 のリクエストパラメータ
 *
 */
class PlayerDataSetRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'type'						=> 'required',
			'value'						=> 'required',
		];
	}

}
