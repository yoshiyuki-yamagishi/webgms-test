<?php
/**
 * フォロワー削除 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * フォロワー削除 のリクエストパラメータ
 *
 */
class FollowerDeleteRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id'					=> 'required',
			'follower_player_id'		=> 'required',
		];
	}

}
