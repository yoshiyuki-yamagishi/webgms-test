<?php
/**
 * バトル中断 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * バトル中断 のリクエストパラメータ
 *
 */
class BattleInterruptRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			// 'battle_code' => 'required',
		];
	}

}
