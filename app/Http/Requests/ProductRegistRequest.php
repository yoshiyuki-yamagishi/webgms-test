<?php
/**
 * 製品登録 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品登録 のリクエストパラメータ
 *
 */
class ProductRegistRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_buy_id' => 'required',
			'receipt' => 'required',
		];
	}

}
