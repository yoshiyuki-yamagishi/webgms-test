<?php
/**
 * プレイヤアイテム一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム一覧 のリクエストパラメータ
 *
 */
class PlayerItemListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'item_category' => 'nullable|integer|min:0',
		];
	}

}
