<?php
/**
 * フォロー削除 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * フォロー削除 のリクエストパラメータ
 *
 */
class FollowDeleteRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id' => 'required',
			'follow_player_id' => 'required',
		];
	}

}
