<?php
/**
 * プレイヤアイテム欠片パウダー精製 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム欠片パウダー精製 のリクエストパラメータ
 *
 */
class PlayerItemPowderPurifyRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'powder' => 'required|integer|min:1',
			'player_item_list' => 'required',
			'player_item_list.*.player_item_id' => 'required|integer',
			'player_item_list.*.item_num' => 'required|integer',
		];
	}

}
