<?php
/**
 * プレイヤミッション報酬受取 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤミッション報酬受取 のリクエストパラメータ
 *
 */
class PlayerMissionRewardTakeRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'take_type'					=> 'required',
		];
	}

}
