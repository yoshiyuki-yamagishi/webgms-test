<?php
/**
 * プレイヤデータ取得 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤデータ取得 のリクエストパラメータ
 *
 */
class PlayerDataGetRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
		];
	}

}
