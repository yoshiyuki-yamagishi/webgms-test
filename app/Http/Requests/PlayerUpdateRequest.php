<?php
/**
 * プレイヤ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ更新 のリクエストパラメータ
 *
 */
class PlayerUpdateRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'nullable|string',
			'player_id' => 'required',
			'player_name' => 'nullable|string',
			'gender' => 'nullable|integer|min:1|max:2',
			'message' => 'nullable|string',
			'birthday' => 'nullable|date_format:Y-m-d',
			'legal_birthday' => 'nullable|date_format:Y-m-d',
		];
	}

}
