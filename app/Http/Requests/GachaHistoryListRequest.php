<?php
/**
 * ガチャ履歴一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ履歴一覧 のリクエストパラメータ
 *
 */
class GachaHistoryListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
		];
	}

}
