<?php
/**
 * プレイヤ魔道書売却 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ魔道書売却 のリクエストパラメータ
 *
 */
class PlayerGrimoireSellRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'									=> 'required',
			'player_id'									=> 'required',
			'player_grimoire_list'						=> 'required',
			'player_grimoire_list.*.player_grimoire_id'	=> 'required',
		];
	}

}
