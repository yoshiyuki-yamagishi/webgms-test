<?php
/**
 * ログイン のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ログイン のリクエストパラメータ
 *
 */
class LoginRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id'					=> 'required',
		];
	}

}
