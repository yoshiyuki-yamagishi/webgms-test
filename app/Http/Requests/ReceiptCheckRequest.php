<?php
/**
 * レシートチェック のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * レシートチェック のリクエストパラメータ
 *
 */
class ReceiptCheckRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'receipt'					=> 'required',
			'product_id'				=> 'required',
		];
	}

}
