<?php
/**
 * フォロー追加 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

/**
 * フォロー追加 のリクエストパラメータ
 *
 */
class FollowAddRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id' => 'required',
			'follow_player_id' => 'required',
		];
	}

}
