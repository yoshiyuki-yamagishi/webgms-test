<?php
/**
 * 製品キャンセル のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品キャンセル のリクエストパラメータ
 *
 */
class ProductCancelRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_buy_id' => 'required',
		];
	}

}
