<?php
/**
 * フレンド一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

/**
 * フレンド一覧 のリクエストパラメータ
 *
 */
class FriendListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
            'friend_type' => 'required|integer',
            'sort_order' => 'required|integer|min:1|max:4',
            'from' => 'required|integer',
            'count' => 'required|integer',
		];
	}

}
