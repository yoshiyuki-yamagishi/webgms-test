<?php
/**
 * ガチャ実行 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ実行 のリクエストパラメータ
 *
 */
class GachaExecRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'gacha_id' => 'required',
			'gacha_count' => 'required',
		];
	}

}
