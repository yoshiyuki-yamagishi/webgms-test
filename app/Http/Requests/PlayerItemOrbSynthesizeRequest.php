<?php
/**
 * プレイヤアイテムオーブ合成 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテムオーブ合成 のリクエストパラメータ
 *
 */
class PlayerItemOrbSynthesizeRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'item_id' => 'required|integer|min:1',
			'count' => 'required|integer|min:1',
			'player_item_list' => 'required',
			'player_item_list.*.player_item_id' => 'required|integer',
			'player_item_list.*.item_num' => 'required|integer',
			'platinum_dollar' => 'required|integer|min:1',
		];
	}

}
