<?php
/**
 * プレイヤアカウント復元 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤアカウント復元 のリクエストパラメータ
 *
 */
class PlayerAccountRestoreRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'os_type' => 'required|integer|min:1|max:2',
            'token' => 'required',
		];
	}

}
