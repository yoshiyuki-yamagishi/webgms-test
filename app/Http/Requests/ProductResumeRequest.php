<?php
/**
 * 製品レジューム のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品レジューム のリクエストパラメータ
 *
 */
class ProductResumeRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_buy_id' => 'required',
			'receipt' => 'required',
		];
	}

}
