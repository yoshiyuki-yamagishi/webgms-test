<?php
/**
 * ショップ購入・交換 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ショップ購入・交換 のリクエストパラメータ
 *
 */
class ShopBuyRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_shop_item_id' => 'required',
		];
	}

}
