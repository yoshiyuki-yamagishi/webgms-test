<?php
/**
 * 製品購入 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品購入 のリクエストパラメータ
 *
 */
class ProductBuyRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'store_id' => 'required',
			'product_id' => 'required',
			'num' => 'required',
			'currency' => 'required',
			'amount' => 'required',
		];
	}

}
