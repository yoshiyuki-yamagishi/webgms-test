<?php
/**
 * プレイヤキャラクタ進化 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタ進化 のリクエストパラメータ
 *
 */
class PlayerCharacterEvolveRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_character_id' => 'required',
			'player_item_list' => 'required',
			'player_item_list.*.player_item_id' => 'required|integer',
			'player_item_list.*.item_num' => 'required|integer',
		];
	}

}
