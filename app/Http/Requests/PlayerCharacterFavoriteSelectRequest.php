<?php
/**
 * プレイヤキャラクタお気に入り選択 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクお気に入り選択 のリクエストパラメータ
 *
 */
class PlayerCharacterFavoriteSelectRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'player_character_id'		=> 'required',
			'favorite_flag'				=> 'required|min:0|max:1',
		];
	}

}
