<?php
/**
 * プレイヤキャラクタ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタ更新 のリクエストパラメータ
 *
 */
class PlayerCharacterUpdateRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_character_id' => 'required',
            'spine' => 'required|integer|min:0|max:2',
		];
	}

}
