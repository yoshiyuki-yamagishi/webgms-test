<?php
/**
 * 製品一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 製品一覧 のリクエストパラメータ
 *
 */
class ProductListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'category' => 'nullable|integer|min:0|max:2',
		];
	}

}
