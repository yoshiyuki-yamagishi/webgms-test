<?php
/**
 * プレイヤ引継ぎ元 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤ引継ぎ元 のリクエストパラメータ
 *
 */
class PlayerTransitSrcRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
// 			'id'						=> 'required',
// 			'app_id'					=> 'nullable|string',
// 			'client_id'					=> 'required|integer',
// 			'trans_client_id'			=> 'nullable|string',
// 			'trans_client_secret'		=> 'nullable|string',
// 			'shop_name'					=> 'required|string',
// 			'location'					=> 'nullable|string',
// 			'genre'						=> 'nullable|string',
// 			'postal_cd'					=> 'nullable|string',
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'password'					=> 'required',
		];
	}

}
