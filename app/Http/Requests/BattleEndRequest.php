<?php
/**
 * バトル終了 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * バトル終了 のリクエストパラメータ
 *
 */
class BattleEndRequest extends BaseRequest
{
    const RESULT_LOSE = 0; // 敗北
    const RESULT_WIN = 1; // 勝利
    
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'battle_code' => 'required',
			'result' => 'required|integer|min:0|max:1',
			'dead_flags' => 'required',
		];
	}

}
