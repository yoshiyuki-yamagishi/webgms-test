<?php
/**
 * プレイヤプレゼント一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤプレゼント一覧 のリクエストパラメータ
 *
 */
class PlayerPresentListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'take_flag'					=> 'required',
			'count'					    => 'required',
		];
	}

}
