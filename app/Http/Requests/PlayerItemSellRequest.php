<?php
/**
 * プレイヤアイテム売却 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤアイテム売却 のリクエストパラメータ
 *
 */
class PlayerItemSellRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_item_id' => 'required',
			'num' => 'required',
		];
	}

}
