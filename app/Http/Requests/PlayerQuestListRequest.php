<?php
/**
 * プレイヤクエスト一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

/**
 * プレイヤクエスト一覧 のリクエストパラメータ
 *
 */
class PlayerQuestListRequest extends BaseRequest
{
    const FLAG_NEED_CLOSED_EVENT = 1;
    
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'quest_category' => 'required|integer|min:0|max:3',
			'flags' => 'nullable|integer|min:0|max:1',
		];
	}

}
