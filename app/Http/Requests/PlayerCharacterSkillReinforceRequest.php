<?php
/**
 * プレイヤキャラクタスキル強化 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * プレイヤキャラクタスキル強化 のリクエストパラメータ
 *
 */
class PlayerCharacterSkillReinforceRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_character_id' => 'required',
			'skill_id' => 'required',
			'platinum_dollar' => 'required',
		];
	}

}
