<?php
/**
 * ガチャ確定 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ確定 のリクエストパラメータ
 *
 */
class GachaCommitRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_gacha_id' => 'required',
		];
	}

}
