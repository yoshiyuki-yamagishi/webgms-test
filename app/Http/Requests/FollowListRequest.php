<?php
/**
 * フォロー一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * フォロー一覧 のリクエストパラメータ
 *
 */
class FollowListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id'					=> 'required',
		];
	}

}
