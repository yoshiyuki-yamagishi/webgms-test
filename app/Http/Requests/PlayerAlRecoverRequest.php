<?php
/**
 * AL回復 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * AL回復 のリクエストパラメータ
 *
 */
class PlayerAlRecoverRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
			'crystal_num'				=> 'required_without_all:item_id',
			'item_id'					=> 'required_without_all:crystal_num',
			'item_num'					=> 'required_with:item_id',
		];
	}

}
