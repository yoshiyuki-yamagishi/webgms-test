<?php
/**
 * ガチャ引き直し のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * ガチャ引き直し のリクエストパラメータ
 *
 */
class GachaRetryRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'player_gacha_id' => 'required',
		];
	}

}
