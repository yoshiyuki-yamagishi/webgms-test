<?php
/**
 * プレイヤオーブ一覧 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * プレイヤオーブ一覧 のリクエストパラメータ
 *
 */
class PlayerOrbListRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code'					=> 'required',
			'player_id'					=> 'required',
		];
	}

}
