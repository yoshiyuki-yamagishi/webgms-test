<?php
/**
 * バトルスキップ のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * バトルスキップ のリクエストパラメータ
 *
 */
class BattleSkipRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'quest_category' => 'required|integer|min:1|max:3',
			'chapter_id' => 'required',
			'quest_id' => 'required',
			// 'party_no' => 'required',
			'skip_ticket_id' => 'required|integer',
			'skip_count' => 'required',
		];
	}

}
