<?php
/**
 * 認証コード更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;



/**
 * 認証コード更新 のリクエストパラメータ
 *
 */
class LoginUpdateRequest extends BaseRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'player_id'					=> 'required',
		];
	}

}
