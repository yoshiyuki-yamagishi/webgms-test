<?php
/**
 * ショップラインナップ更新 のリクエストパラメータ
 *
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;



/**
 * ショップラインナップ更新 のリクエストパラメータ
 *
 */
class ShopLineupUpdateRequest extends BaseRequest
{
    const PAY_ITEM_NO = 0;
    const PAY_ITEM_YES = 1;
    
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'auth_code' => 'required',
			'player_id' => 'required',
			'shop_id' => 'required',
		];
	}

	/**
	 * アイテムを払って手動で更新するか判定する
	 *
	 * @return true: 払う、false: 払わない
	 */
	public function payItem()
    {
        if (empty($this->pay_item))
            return false;
        return $this->pay_item != self::PAY_ITEM_NO;
    }

}
