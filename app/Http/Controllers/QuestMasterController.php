<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\MissionDictionary;
use App\Models\MasterModels\StoryQuestChapter;
use App\Models\MasterModels\CharacterQuestChapter;
use App\Models\MasterModels\QuestDictionary;
use App\Models\MasterModels\QuestReward;

use App\GmsServices\GmsQuestService;
use App\Services\QuestService;

use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class QuestMasterController extends Controller
{
	/**
	 * BBDW_MasterData_story_quest.xlsm チェック
	 * BBDW_MasterData_character_quest.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		$storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
		$characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);
		
		$storyQuests = $storyQuestService->getAllQuests();
		$characterQuests = $characterQuestService->getAllQuests();
		
		
		

		foreach ($storyQuests as $storyQuest)
		{
			$storyQuest->story_quest_chapter_id = 'ID：'.$storyQuest->story_quest_chapter_id;
			$storyChapterD = QuestDictionary::getOne($storyQuest->story_quest_chapter_id);
			$storyQuest->chapter_name = $storyChapterD->dictionary_ja;

			$stageNameD = QuestDictionary::getOne($storyQuest->stage_name_id);
			$storyQuest->stage_name = $stageNameD->dictionary_ja;

			$storyNameD = QuestDictionary::getOne($storyQuest->quest_name_id);
			$storyQuest->story_quest_name = $storyNameD->dictionary_ja;

			$storyQuest->first_reward_item_name = '';
			$storyQuest->first_reward_item_num = '';
			$storyQuest->fix_reward_item_name = '';
			$storyQuest->fix_reward_item_num = '';
			// 初クリア報酬
			$firstRewards = QuestReward::getAll($storyQuest->first_reward_id);
			foreach ($firstRewards as $firstReward)
			{
				$itemCategory = Item::categoryFromId($firstReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);
				// $storyQuest->item_category = $itemCategory;
				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($firstReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					$storyQuest->first_reward_item_name = $itemD->dictionary_ja;
					$storyQuest->first_reward_item_num = '個数：'.$firstReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($firstReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$storyQuest->first_reward_item_name = $itemD->dictionary_ja;
					$storyQuest->first_reward_item_num = '個数：'.$firstReward->quest_reward_count;
				}
			}

			// 固定報酬
			$fixRewards = QuestReward::getAll($storyQuest->fix_reward_id);
			foreach ($fixRewards as $fixReward)
			{
				$itemCategory = Item::categoryFromId($fixReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);
				// $storyQuest->item_category = $itemCategory;
				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($fixReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					$storyQuest->fix_reward_item_name = $itemD->dictionary_ja;
					$storyQuest->fix_reward_item_num = '個数：'.$fixReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($fixReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$storyQuest->fix_reward_item_name = $itemD->dictionary_ja;
					$storyQuest->fix_reward_item_num = '個数：'.$fixReward->quest_reward_count;
				}
			}

			// // ドロップ報酬
			// $dropRewards = QuestReward::getAll($storyQuest->drop_reward_id);
			// foreach ($dropRewards as $dropReward)
			// {
			// 	$itemCategory = Item::categoryFromId($dropReward->quest_reward_item_id);
			// 	DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);
			// 	// $storyQuest->item_category = $itemCategory;
			// 	if ($itemCategory == 1 || $itemCategory == 3)
			// 	{
			// 		$item = Item::getOne($dropReward->quest_reward_item_id);
			// 		$itemD = ItemDictionary::getOne($item->item_name_id);
			// 		$storyQuest->drop_reward_item_name = $itemD->dictionary_ja;
			// 		$storyQuest->drop_reward_item_num = $dropReward->quest_reward_count;
			// 	}
			// 	elseif ($itemCategory == 2)
			// 	{
			// 		$item = Grimoire::getOne($firstReward->quest_reward_item_id);
			// 		$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
			// 		$storyQuest->drop_reward_item_name = $itemD->dictionary_ja;
			// 		$storyQuest->drop_reward_item_num = $dropReward->quest_reward_count;
			// 	}
			// }

			// DebugUtil::e_log('firstRewards', 'firstRewards', $firstRewards);

			// $itemCategory = Item::categoryFromId($firstReward->quest_reward_item_id);
			// DebugUtil::e_log('firstRewardItem', 'firstRewardItem', $firstRewardItem);
			
			// $firstRewardItem = Item::getOne($firstReward->quest_reward_item_id);
			// DebugUtil::e_log('firstRewardItem', 'firstRewardItem', $firstRewardItem);



			if ($storyQuest->quest_mission_count == -1)
			{
				$storyQuest->story_mission_name_01 ='１：なし';
				$storyQuest->story_mission_name_02 ='２：なし';
				$storyQuest->story_mission_name_03 ='３：なし';
			}
			else
			{
				$m1 = $storyQuest->quest_mission_id_01;
				$m2 = $storyQuest->quest_mission_id_02;
				$m3 = $storyQuest->quest_mission_id_03;


				$storyMission1 = Mission::getOne($storyQuest->quest_mission_id_01);
				$storyMission1D = MissionDictionary::getOne($storyMission1->mission_detail);
				$storyQuest->story_mission_name_01 = '１：'.$m1.'：'.$storyMission1D->dictionary_ja;

				$storyMission2 = Mission::getOne($storyQuest->quest_mission_id_02);
				$storyMission2D = MissionDictionary::getOne($storyMission2->mission_detail);
				$storyQuest->story_mission_name_02 = '２：'.$m2.'：'.$storyMission2D->dictionary_ja;

				$storyMission3 = Mission::getOne($storyQuest->quest_mission_id_03);
				$storyMission3D = MissionDictionary::getOne($storyMission3->mission_detail);
				$storyQuest->story_mission_name_03 = '３：'.$m3.'：'.$storyMission3D->dictionary_ja;
			}
			
			if ($storyQuest->quest_type == 0)
			{
				$storyQuest->type = 'ストーリーのみ';
			}
			elseif ($storyQuest->quest_type == 1)
			{
				$storyQuest->type = 'ストーリー＋戦闘';
			}
			elseif ($storyQuest->quest_type == 2)
			{
				$storyQuest->type = '戦闘のみ';
			}

		}
		DebugUtil::e_log('storyQuests', 'storyQuests', $storyQuests);


		foreach ($characterQuests as $characterQuest)
		{
			$characterQuestNameD = QuestDictionary::getOne($characterQuest->quest_name_id);
			$characterQuest->quest_name = $characterQuestNameD->dictionary_ja;

			$characterQuest->first_reward_item_name = '';
			$characterQuest->first_reward_item_num = '';
			$characterQuest->fix_reward_item_name = '';
			$characterQuest->fix_reward_item_num = '';

			// 初クリア報酬
			$firstRewards = QuestReward::getAll($characterQuest->first_reward_id);
			foreach ($firstRewards as $firstReward)
			{
				$itemCategory = Item::categoryFromId($firstReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);
				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($firstReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					$characterQuest->first_reward_item_name = $itemD->dictionary_ja;
					$characterQuest->first_reward_item_num = '個数：'.$firstReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($firstReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$characterQuest->first_reward_item_name = $itemD->dictionary_ja;
					$characterQuest->first_reward_item_num = '個数：'.$firstReward->quest_reward_count;
				}
			}

			// 固定報酬
			$fixRewards = QuestReward::getAll($characterQuest->fix_reward_id);
			foreach ($fixRewards as $fixReward)
			{
				$itemCategory = Item::categoryFromId($fixReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);
				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($fixReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					$characterQuest->fix_reward_item_name = $itemD->dictionary_ja;
					$characterQuest->fix_reward_item_num = '個数：'.$fixReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($fixReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$characterQuest->fix_reward_item_name = $itemD->dictionary_ja;
					$characterQuest->fix_reward_item_num = '個数：'.$fixReward->quest_reward_count;
				}
			}


			if ($characterQuest->quest_mission_count == -1)
			{
				$characterQuest->character_mission_name_01 = '１：なし';
				$characterQuest->character_mission_name_02 = '２：なし';
				$characterQuest->character_mission_name_03 = '３：なし';
			}
			else
			{
				$m1 = $characterQuest->quest_mission_id_01;
				$m2 = $characterQuest->quest_mission_id_02;
				$m3 = $characterQuest->quest_mission_id_03;

				$characterMission1 = Mission::getOne($characterQuest->quest_mission_id_01);
				$characterMission1D = MissionDictionary::getOne($characterMission1->mission_detail);
				$characterQuest->character_mission_name_01 = '１：'.$m1.'：'.$characterMission1D->dictionary_ja;
			
				$characterMission2 = Mission::getOne($characterQuest->quest_mission_id_02);
				$characterMission2D = MissionDictionary::getOne($characterMission2->mission_detail);
				$characterQuest->character_mission_name_02 = '２：'.$m2.'：'.$characterMission2D->dictionary_ja;

				$characterMission3 = Mission::getOne($characterQuest->quest_mission_id_03);
				$characterMission3D = MissionDictionary::getOne($characterMission3->mission_detail);
				$characterQuest->character_mission_name_03 = '３：'.$m3.'：'.$characterMission3D->dictionary_ja;
			}
		}

		DebugUtil::e_log('characterQuests', 'characterQuests', $characterQuests);

		$params = [
			'storyQuests' => $storyQuests,
			'characterQuests' => $characterQuests,
		];

		return view('master_check.quest_master', $params);
	}





}
