<?php

namespace App\Http\Controllers;

use App\Models\PlayerGrimoireLog;

use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerGrimoireService;
use App\Services\SessionService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerGrimoireLogController extends Controller
{
	
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$grimoireLogs = PlayerGrimoireLog::getByPlayerId($playerId);
		

		foreach ($grimoireLogs as $grimoireLog)
		{
			if ($grimoireLog->type == 1)
			{
				$grimoireLog->_type = '取得';
			}
			elseif ($grimoireLog->type == 2)
			{
				$grimoireLog->_type = '消費';
			}
			elseif ($grimoireLog->type == 3)
			{
				$grimoireLog->_type = '覚醒';
			}
			elseif ($grimoireLog->type == 4)
			{
				$grimoireLog->_type = 'スロット解放';
			}
			elseif ($grimoireLog->type == 5)
			{
				$grimoireLog->_type = 'スロット装備';
			}
			$grimoire = Grimoire::getOne($grimoireLog->value3);
			$grimoireD = GrimoireDictionary::getOne($grimoire->grimoire_name_id);
			DebugUtil::e_log('grimoireD', 'grimoireD', $grimoireD);
			$grimoireLog->grimoire_name = $grimoireD->dictionary_ja;
		}
		
		DebugUtil::e_log('grimoireLogs', 'grimoireLogs', $grimoireLogs);

		$params = [
            'grimoireLogs' => $grimoireLogs,
        ];

		return view('player_log.player_grimoire_log', $params);

	}



	
}
