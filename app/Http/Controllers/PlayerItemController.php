<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;



class PlayerItemController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerItemListRequest $request
	 * @return
	 */
	public function list(PlayerItemListRequest $request)
	{
		$response = PlayerItemService::list($request);
		return $response->toResponse();
	}

	/**
	 * 売却
	 * @param PlayerItemSellRequest $request
	 * @return
	 */
	public function sell(PlayerItemSellRequest $request)
	{
		$response = PlayerItemService::sell($request);
		return $response->toResponse();
	}
}
