<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\FlavorDictionary;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class GrimoireMasterController extends Controller
{
	/**
	 * BBDW_MasterData_grimoire.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		$grimoires = Grimoire::getAll();
		// DebugUtil::e_log('dc', 'request', $request->all());
		DebugUtil::e_log('host', 'getHost', $request->getHost());
		DebugUtil::e_log('host', 'getHttpHost', $request->getHttpHost());
		$host = $request->getHost();
		DebugUtil::e_log('$host', '$host', $request->getHost());

		foreach ($grimoires as $grimoire)
		{
			$grimoireNameId = $grimoire->grimoire_name_id;
			$flavorId = $grimoire->flavor_id;
			$passive1 = $grimoire->passive_id_1;
			$passive2 = $grimoire->passive_id_2;
			$grimoire->cost = 'コスト：'.$grimoire->cost;
			$grimoire->grimoire_initial_rarity = '★：'.$grimoire->grimoire_initial_rarity;

			$grimoireNameD = GrimoireDictionary::getOne($grimoireNameId);
			$grimoire->grimoire_name = $grimoireNameD->dictionary_ja;
			$grimoireFlavorD = FlavorDictionary::getOne($flavorId);
			$grimoire->flavor = $grimoireFlavorD->flavor_dictionary_ja;
			
			$pSkill1 = Skill::getOne($passive1);
			$pSkill2 = Skill::getOne($passive2);

			$pSkill1D = SkillDictionary::getOne($pSkill1->skill_description_id);
			$pSkill2D = SkillDictionary::getOne($pSkill2->skill_description_id);

			DebugUtil::e_log('pSkill1D', 'pSkill1D', $pSkill1D);

			$grimoire->skill_1 = 'PS1：'.$pSkill1D->dictionary_ja;
			$grimoire->skill_2 = 'PS2：'.$pSkill2D->dictionary_ja;

			// アイコン表示
			if ($host == 'localhost')
			{
				// ローカル用
				$grimoire->img = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com:8080/assetbundle_rsc/Reinforce/Grimoire/rif_gri_'.$grimoire->id.'.png';
			}
			else
			{
				// 開発用サーバー用
				$grimoire->img = '/assetbundle_rsc/Reinforce/Grimoire/rif_gri_'.$grimoire->id.'.png';
			}
			
		}

		DebugUtil::e_log('grimoires', 'grimoires', $grimoires);
		$params = [
			'grimoires' => $grimoires,

		];

		return view('master_check.grimoire_master', $params);
	}





}
