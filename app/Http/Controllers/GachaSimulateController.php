<?php

namespace App\Http\Controllers;

use App\Http\Responses\BattleEnemyListResponse;
use App\Models\MasterModels\QuestDictionary;
use App\Models\MasterModels\GachaDictionary;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\GachaLot;
use App\Models\MasterModels\GachaGroup;
use App\Services\GachaService;
use App\Services\QuestService;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class GachaSimulateController extends Controller
{
	/**
	 * GMS ガチャシミュレート
	 * @param Request $request
	 * @return
	 */
    
    public function index(Request $request)
    {

        $gachaList = Gacha::getAllRaw();
        $gachaTypes = [];

        foreach ($gachaList as $gacha)
        {
            $gachaName = GachaDictionary::getOne($gacha->gacha_name_id);
            $gacha->gacha_name = $gachaName->dictionary_ja;
            $key = $gacha->gacha_type;
            $gachaTypes[$key] = true;
        }
        DebugUtil::e_log('gachaTypes', 'gachaTypes', $gachaTypes);


        $i = 0;
        $type = [];
        $type[0] = '全て';
        foreach ($gachaTypes as $gachaType)
        {
            $i = ++ $i;
            DebugUtil::e_log('gachaType', 'gachaType', $gachaType);
            $type[$i] = $i;
        }
        DebugUtil::e_log('type', 'type', $type);
        // ガチャロット取得
		// $gachaLotList = GachaLot::getAll($gacha->gacha_lot_id);
        DebugUtil::e_log('gachaList', 'gachaList', $gachaList);
        // DebugUtil::e_log('gachaLotList', 'gachaLotList', $gachaLotList);
        $gachaType = [];

        // $gachaType[$type->id] = 
        // $orbNames[$mItem->id] = $itemD->dictionary_ja;


        $params = [
            'gachaList' => $gachaList,
            'type' => $type,

        ];

        return view('simulate.gacha_simulate', $params);
        
    }
}
