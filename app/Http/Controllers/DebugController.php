<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerCharacter;
use App\Models\PlayerGacha;
use App\Models\PlayerGachaResult;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerItemBuy;
use App\Models\PlayerLoginBonus;
use App\Models\PlayerMission;
use App\Models\PlayerProductBuy;
use App\Models\PlayerQuest;
use App\Models\PlayerShopItem;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\EventQuestDictionary;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\MissionDictionary;
use App\Models\MasterModels\LoginBonusDictionary;
use App\Models\MasterModels\LoginBonusReward;
use App\Models\MasterModels\QuestDictionary;

use App\Services\GiveOrPayParam;
use App\Services\PlayerService;
use App\Services\QuestService;
use App\Services\SessionService;
use App\GmsServices\GmsLoginBonusService;
use App\GmsServices\GmsMissionService;
use App\GmsServices\GmsQuestService;
use App\GmsServices\GmsService;
use Illuminate\Http\Request;
use App\Utils\DateTimeUtil;

class DebugController extends Controller
{
	/**
	 * GMS デバッグ
	 * @param Request $request
	 * @return
	 */

    public $player = null;
    public $storyChapters = [];
    public $characterChapters = [];
    

    public function modify(Request $request)
    {
        $playerId = $request->player_id;
        $req = $request->all();
        // if ($request->action == 'modify_player')
        if (array_key_exists('modify_player', $req))
        {

            $declineAl = $request->max_al - $request->al;

            $alRecover = Constant::getOne_(Constant::AL_RECOVER_TIME);

            // マスターのAL１あたりの回復時間取得（300秒）
            $alRecoverTime = $alRecover->value1;

            $_alRecoverTime = $alRecoverTime * $declineAl - 60;
            $now = DateTimeUtil::getNOW();
            $alRecoveryAt = DateTimeUtil::addSecondsToDate($now, $_alRecoverTime);

            $this->player->player_lv=$request->player_lv;
            $this->player->last_login_at=$request->last_login_at;
            $this->player->tutorial_progress=$request->tutorial_progress;
            $this->player->first_login_at=$request->first_login_at;
            $this->player->experience=$request->experience;

            $this->player->platinum_dollar=$request->platinum_dollar;
            $this->player->powder=$request->powder;
            $this->player->magic_num=$request->magic_num;

            $this->player->player_lv=$request->player_lv;
            $this->player->friend_point=$request->friend_point;

            $this->player->al=$request->al;
            $this->player->al_recovery_at=$alRecoveryAt;
            $this->player->platinum_dollar=$request->platinum_dollar;
            $this->player->max_grimoire=$request->max_grimoire;
            $this->player->save();

            // 蒼の結晶 計算
            $freeCrystalNum = $request->free_crystal - $this->freeCrystal;
            $chargedCrystalNum = $request->charged_crystal - $this->chargedCrystal;

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemType = Item::TYPE_ITEM;
            $param->srcType = 16;
            $param->srcId = 0;

            if ($freeCrystalNum != 0)
            {
                $param->itemId = Item::ID_FREE_BLUE_CRYSTAL;
                $param->count = $freeCrystalNum;
                $param->paymentType = PlayerProductBuy::STORE_NONE;
                PlayerService::giveOrPay($param);
            }
            if ($chargedCrystalNum != 0)
            {
                $param->itemId = Item::ID_BLUE_CRYSTAL;
                $param->count = $chargedCrystalNum;
                $param->paymentType = PlayerProductBuy::STORE_GOOGLE;
                PlayerService::giveOrPay($param);
            }

        }
        elseif (array_key_exists('modify_character', $req))
        {
            $id = $request->player_character_id;
            $character = PlayerCharacter::find($id);
            // $character->character_id=$request->character_id;
            $character->character_lv=$request->character_lv;
            $character->experience=$request->experience;
            $character->player_orb_id_1=$request->player_orb_id_1;
            $character->player_orb_id_2=$request->player_orb_id_2;
            $character->player_orb_id_3=$request->player_orb_id_3;
            $character->player_orb_id_4=$request->player_orb_id_4;
            $character->player_orb_id_5=$request->player_orb_id_5;
            $character->player_orb_id_6=$request->player_orb_id_6;
            $character->grade=$request->grade;
            $character->repeat=$request->repeat;
            $character->active_skill_1_lv=$request->active_skill_1_lv;
            $character->active_skill_2_lv=$request->active_skill_2_lv;
            $character->save();
        }
        elseif (array_key_exists('delete_character', $req))
        {
            $charaId = $request->player_character_id;
            PlayerCharacter::where('player_id', $playerId)
					->where('id', $charaId)
					->delete();
        }
        elseif (array_key_exists('modify_all_character', $req))
        {
            // キャラクター全更新
            $updateColumn = [];
            if($request->character_lv !== null){
                $updateColumn['character_lv'] = $request->character_lv;
            }
            if($request->experience !== null){
                $updateColumn['experience'] = $request->experience;
            }
            if($request->player_orb_id_1 !== null){
                $updateColumn['player_orb_id_1'] = $request->player_orb_id_1;
            }
            if($request->player_orb_id_2 !== null){
                $updateColumn['player_orb_id_2'] = $request->player_orb_id_2;
            }
            if($request->player_orb_id_3 !== null){
                $updateColumn['player_orb_id_3'] = $request->player_orb_id_3;
            }
            if($request->player_orb_id_4 !== null){
                $updateColumn['player_orb_id_4'] = $request->player_orb_id_4;
            }
            if($request->player_orb_id_5 !== null){
                $updateColumn['player_orb_id_5'] = $request->player_orb_id_5;
            }
            if($request->player_orb_id_6 !== null){
                $updateColumn['player_orb_id_6'] = $request->player_orb_id_6;
            }
            if($request->character_lv !== null){
                $updateColumn['character_lv'] = $request->character_lv;
            }
            if($request->grade !== null){
                $updateColumn['grade'] = $request->grade;
            }
            if($request->repeat !== null){
                $updateColumn['repeat'] = $request->repeat;
            }
            if($request->active_skill_1_lv !== null){
                $updateColumn['active_skill_1_lv'] = $request->active_skill_1_lv;
            }
            if($request->active_skill_2_lv !== null){
                $updateColumn['active_skill_2_lv'] = $request->active_skill_2_lv;
            }
            if($updateColumn){
                PlayerCharacter::where('player_id',$playerId)
                    ->update($updateColumn);
            }
        }
        elseif (array_key_exists('delete_character', $req))
        {
            $charaId = $request->delete_character;
            PlayerCharacter::where('player_id', $playerId)
                ->where('id', $charaId)
                ->delete();
        }
        elseif (array_key_exists('delete_all_character', $req))
        {
            PlayerCharacter::where('player_id', $playerId)
                ->delete();
        }
        elseif (array_key_exists('modify_grimoire', $req))
        {
            $id = $request->player_grimoire_id;
            $grimoire = PlayerGrimoire::find($id);
            $grimoire->awake=$request->awake;
            $grimoire->slot_1=$request->slot_1;
            $grimoire->slot_1_flag=$request->slot_1_flag;
            $grimoire->slot_2=$request->slot_2;
            $grimoire->slot_2_flag=$request->slot_2_flag;
            $grimoire->slot_3=$request->slot_3;
            $grimoire->slot_3_flag=$request->slot_3_flag;
            $grimoire->lock_flag=$request->lock_flag;
            $grimoire->save();
        }
        elseif (array_key_exists('delete_grimoire', $req))
        {
            $playerGrimoireId = $request->player_grimoire_id;
            PlayerGrimoire::where('player_id', $playerId)
                            ->where('id', $playerGrimoireId)
                            ->delete();
        }
        elseif (array_key_exists('modify_all_grimoire', $req))
        {
            // グリモア全更新
            $updateColumn = [];
            if($request->awake !== null){
                $updateColumn['awake'] = $request->awake;
            }
            if($request->slot_1 !== null){
                $updateColumn['slot_1'] = $request->slot_1;
            }
            if($request->slot_1_flag !== null){
                $updateColumn['slot_1_flag'] = $request->slot_1_flag;
            }
            if($request->slot_2 !== null){
                $updateColumn['slot_2'] = $request->slot_2;
            }
            if($request->slot_2_flag !== null){
                $updateColumn['slot_2_flag'] = $request->slot_2_flag;
            }
            if($request->slot_3 !== null){
                $updateColumn['slot_3'] = $request->slot_3;
            }
            if($request->slot_3_flag !== null){
                $updateColumn['slot_3_flag'] = $request->slot_3_flag;
            }
            if($request->lock_flag !== null){
                $updateColumn['lock_flag'] = $request->lock_flag;
            }

            if($updateColumn){
                PlayerGrimoire::where('player_id',$playerId)
                    ->update($updateColumn);
            }
        }
        elseif (array_key_exists('delete_all_grimoire', $req))
        {
            PlayerGrimoire::where('player_id', $playerId)
                ->delete();
        }
        elseif (array_key_exists('modify_orb', $req))
        {
            $id = $request->player_item_id;
            $playerItem = PlayerItem::find($id);
            $playerItem->num=$request->num;
            $playerItem->save();
        }
        elseif (array_key_exists('delete_orb', $req))
        {
            $playerItemId = $request->player_item_id;
            PlayerItem::where('player_id', $playerId)
                            ->where('id', $playerItemId)
                            ->delete();
        }
        elseif (array_key_exists('modify_all_orb', $req))
        {
            // オーブ全更新
            $updateColumn = [];
            if($request->num !== null){
                $updateColumn['num'] = $request->num;
            }
            if($updateColumn){
                PlayerItem::where('player_id', $playerId)
                    ->whereRaw('item_id DIV ? = ?', [Item::CATEGORY_RANGE, Item::CATEGORY_ORB])
                    ->update($updateColumn);
            }
        }
        elseif (array_key_exists('delete_all_orb', $req))
        {
            PlayerItem::where('player_id', $playerId)
                ->whereRaw('item_id DIV ? = ?', [Item::CATEGORY_RANGE, Item::CATEGORY_ORB])
                ->delete();
        }
        elseif (array_key_exists('modify_fragment', $req))
        {
            $id = $request->player_item_id;
            $playerItem = PlayerItem::find($id);
            $playerItem->num=$request->num;
            $playerItem->save();
        }
        elseif (array_key_exists('delete_fragment', $req))
        {
            $playerItemId = $request->player_item_id;
            PlayerItem::where('player_id', $playerId)
                            ->where('id', $playerItemId)
                            ->delete();
        }
        elseif (array_key_exists('modify_all_fragment', $req))
        {
            // オーブ全更新
            $updateColumn = [];
            if($request->num !== null){
                $updateColumn['num'] = $request->num;
            }
            if($updateColumn){
                PlayerItem::where('player_id', $playerId)
                    ->whereRaw('item_id DIV ? = ?', [Item::CATEGORY_RANGE, Item::CATEGORY_FRAGMENT])
                    ->update($updateColumn);
            }
        }
        elseif (array_key_exists('delete_all_fragment', $req))
        {
            PlayerItem::where('player_id', $playerId)
                ->whereRaw('item_id DIV ? = ?', [Item::CATEGORY_RANGE, Item::CATEGORY_FRAGMENT])
                ->delete();
        }
        elseif (array_key_exists('modify_item', $req))
        {
            $id = $request->player_item_id;
            $playerItem = PlayerItem::find($id);
            $playerItem->num=$request->num;
            $playerItem->save();
        }
        elseif (array_key_exists('delete_item', $req))
        {
            $playerItemId = $request->player_item_id;
            PlayerItem::where('player_id', $playerId)
                            ->where('id', $playerItemId)
                            ->delete();
        }
        elseif (array_key_exists('modify_all_item', $req))
        {
            // アイテム全更新
            $updateColumn = [];
            if($request->num !== null){
                $updateColumn['num'] = $request->num;
            }
            if($updateColumn){
                PlayerItem::where('player_id', $playerId)
                    ->whereRaw('item_id DIV ? IN (?, ?, ?, ?)', [Item::CATEGORY_RANGE, Item::CATEGORY_TICKET, Item::CATEGORY_EXP, Item::CATEGORY_SKIP_TICKET,Item::CATEGORY_AL_RECOVER])
                    ->update($updateColumn);
            }
        }
        elseif (array_key_exists('delete_all_item', $req))
        {
            PlayerItem::where('player_id', $playerId)
                ->whereRaw('item_id DIV ? IN (?, ?, ?, ?)', [Item::CATEGORY_RANGE, Item::CATEGORY_TICKET, Item::CATEGORY_EXP, Item::CATEGORY_SKIP_TICKET,Item::CATEGORY_AL_RECOVER])
                ->delete();
        }
        elseif (array_key_exists('row_character', $req))
        {
            // キャラ付与
            $param = new GiveOrPayParam();
            $param->playerId = $playerId;
            $param->itemType = Item::TYPE_CHARACTER;
            $param->itemId = $request->character_base_id;
            $param->count = 1;
            PlayerService::giveOrPay($param);
        }
        elseif (array_key_exists('give_all_character', $req))
        {
            $allCharaData = CharacterBase::getAll();
            foreach ($allCharaData as $charaData){
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemType = Item::TYPE_CHARACTER;
                $param->itemId = $charaData->id;
                $param->count = 1;
                PlayerService::giveOrPay($param);
            }
        }
        elseif (array_key_exists('row_grimoire', $req))
        {
            // 魔導書付与

            if($request->num > 0) {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemType = Item::TYPE_GRIMOIRE;
                $param->itemId = $request->grimoire_id;
                $param->count = $request->num;
                PlayerService::giveOrPay($param);
            }
        }
        elseif (array_key_exists('give_all_grimoire', $req))
        {
            $allGrimoiresData = Grimoire::getAll();
            if ($request->num > 0) {
                foreach ($allGrimoiresData as $grimoireData) {
                    // 魔導書付与
                    $param = new GiveOrPayParam();
                    $param->playerId = $playerId;
                    $param->itemType = Item::TYPE_GRIMOIRE;
                    $param->itemId = $grimoireData->id;
                    $param->count = $request->num;
                    PlayerService::giveOrPay($param);
                }
            }
        }
        elseif (array_key_exists('row_orb', $req))
        {
            // オーブ付与
            $param = new GiveOrPayParam();
            $param->playerId = $playerId;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = $request->item_id;
            $param->count = $request->num;

            PlayerService::giveOrPay($param);
        }
        elseif (array_key_exists('give_all_orb', $req))
        {
            if($request->num > 0) {
                $allItemData = Item::getAll();
                foreach ($allItemData as $itemData) {
                    $itemCat = intdiv($itemData->id, 1000000);
                    // オーブ
                    if ($itemCat == Item::CATEGORY_ORB) {
                        $param = new GiveOrPayParam();
                        $param->playerId = $playerId;
                        $param->itemType = Item::TYPE_ITEM;
                        $param->itemId = $itemData->id;
                        $param->count = $request->num;
                        PlayerService::giveOrPay($param);
                    }
                }
            }
        }
        elseif (array_key_exists('row_fragment', $req))
        {
            // 欠片付与
            $param = new GiveOrPayParam();
            $param->playerId = $playerId;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = $request->item_id;
            $param->count = $request->num;

            PlayerService::giveOrPay($param);

        }
        elseif (array_key_exists('give_all_fragment', $req))
        {
            if($request->num > 0) {
                $allItemData = Item::getAll();
                foreach ($allItemData as $itemData) {
                    $itemCat = intdiv($itemData->id, 1000000);
                    // オーブ
                    if ($itemCat == Item::CATEGORY_FRAGMENT) {
                        $param = new GiveOrPayParam();
                        $param->playerId = $playerId;
                        $param->itemType = Item::TYPE_ITEM;
                        $param->itemId = $itemData->id;
                        $param->count = $request->num;
                        PlayerService::giveOrPay($param);
                    }
                }
            }
        }
        elseif (array_key_exists('row_item',$req))
        {
            // アイテム付与
            $param = new GiveOrPayParam();
            $param->playerId = $playerId;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = $request->item_id;
            $param->count = $request->num;

            PlayerService::giveOrPay($param);

        }
        elseif (array_key_exists('give_all_item', $req))
        {
            if($request->num > 0) {
                $allItemData = Item::getAll();
                foreach ($allItemData as $itemData) {
                    $itemCat = intdiv($itemData->id, 1000000);
                    // オーブ
                    switch($itemCat){
                        case Item::CATEGORY_TICKET:
                        case Item::CATEGORY_EXP:
                        case Item::CATEGORY_SKIP_TICKET:
                        case Item::CATEGORY_AL_RECOVER:
                            $param = new GiveOrPayParam();
                            $param->playerId = $playerId;
                            $param->itemType = Item::TYPE_ITEM;
                            $param->itemId = $itemData->id;
                            $param->count = $request->num;
                            PlayerService::giveOrPay($param);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        elseif (array_key_exists('story_prg', $req))
        {
            // ストーリークエスト進捗
            GmsQuestService::setQuestProgress(
                $playerId, $this->storyQuests,
                $request->quest_name_id,
                $request->mission_flag_1,
                $request->mission_flag_2,
                $request->mission_flag_3,
                $request->num
            );

        }
        elseif (array_key_exists('delete_story', $req))
        {
            $start = false;
            $ids = [];
            foreach ($this->storyQuests as $storyQuest){
                if($storyQuest->id == $request->quest_name_id){
                    $start = true; //ON_FLGとかないから一旦これで
                }
                if($start){
                    $ids[] = $storyQuest->id;
                }
            }

            PlayerQuest::where('player_id', $playerId)
            ->where('quest_category', PlayerQuest::QUEST_CATEGORY_STORY)
            ->whereIn('quest_id', $ids)
            ->delete();
        }
        elseif (array_key_exists('delete_all_story', $req))
        {
            PlayerQuest::where('player_id', $playerId)
                ->where('quest_category', PlayerQuest::QUEST_CATEGORY_STORY)
                ->delete();
        }
        elseif (array_key_exists('chara_story_prg', $req))
        {
            // キャラクタークエスト進捗
            GmsQuestService::setCharaQuestProgress(
                $playerId, $this->characterQuests,
                $request->quest_name_id,
                $request->mission_flag_1,
                $request->mission_flag_2,
                $request->mission_flag_3,
                $request->num
            );

        }
        elseif (array_key_exists('delete_chara_story', $req))
        {
            $start = false;
            $ids = [];
            foreach ($this->characterQuests as $charaQuest){
                if($charaQuest->id == $request->quest_name_id){
                    $start = true; //ON_FLGとかないから一旦これで
                }
                if($start){
                    $ids[] = $charaQuest->id;
                }
            }

            PlayerQuest::where('player_id', $playerId)
                ->where('quest_category', PlayerQuest::QUEST_CATEGORY_CHARACTER)
                ->whereIn('quest_id', $ids)
                ->delete();
        }
        elseif (array_key_exists('delete_all_chara_story', $req))
        {
            PlayerQuest::where('player_id', $playerId)
                ->where('quest_category', PlayerQuest::QUEST_CATEGORY_CHARACTER)
                ->delete();
        }
        elseif (array_key_exists('event_prg', $req))
        {
            // イベントクエスト進捗
            GmsQuestService::setEventQuestProgress(
                $playerId, $this->eventQuests,
                $request->quest_name_id,
                $request->mission_flag_1,
                $request->mission_flag_2,
                $request->mission_flag_3,
                $request->num
            );
        }
        elseif (array_key_exists('delete_event', $req))
        {
            //TODO:
            $start = false;
            $ids = [];
            foreach ($this->eventQuests as $charaQuest){
                if($charaQuest->id == $request->quest_name_id){
                    $start = true; //ON_FLGとかないから一旦これで
                }
                if($start){
                    $ids[] = $charaQuest->id;
                }
            }

            PlayerQuest::where('player_id', $playerId)
                ->where('quest_category', PlayerQuest::QUEST_CATEGORY_EVENT)
                ->whereIn('quest_id', $ids)
                ->delete();
        }
        elseif (array_key_exists('delete_all_event', $req)){
            PlayerQuest::where('player_id', $playerId)
                ->where('quest_category', PlayerQuest::QUEST_CATEGORY_EVENT)
                ->delete();
        }
        elseif (array_key_exists('mission', $req))
        {
            // ミッション進捗
            $now = DateTimeUtil::getNOW();
            $missionId = $request->mission_id;
            $playerId = $request->player_id;
            $num = $request->num;

            $mission = Mission::getOne($missionId);

            $missionType = $mission->mission_type;
            GmsMissionService::register($playerId, $missionId, $missionType, $now, $num, $mission);

        }
        elseif (array_key_exists('mission_reset', $req))
        {
            // ミッション進捗
            PlayerMission::where('player_id',$playerId)
                ->delete();
        }
        elseif (array_key_exists('login', $req))
        {
            GmsLoginBonusService::setLoginBonusProgress(
            $playerId,
            $this->loginBonuses,
            $request->id
            );
        }
        elseif (array_key_exists('login_reset', $req))
        {
            PlayerLoginBonus::where('player_id', $playerId)
                            ->delete();
        }
        elseif ($request->action == 'shop')
        {
            PlayerItemBuy::where('player_id', $playerId)
                                ->delete();

            PlayerShopItem::where('player_id', $playerId)
                                ->delete();

        }
        elseif ($request->action == 'gatha')
        {
            // ガチャ履歴削除
            // getAll
            $playerGachaList =
                PlayerGacha::where('player_id', $playerId)
               ->orderBy('id', 'asc')
               ->get();
            foreach ($playerGachaList as $playerGacha)
            {
                $playerGachaId = $playerGacha->id;
                PlayerGachaResult::where('player_gacha_id', $playerGachaId)
                                ->delete();
            }
            PlayerGacha::where('player_id', $playerId)
                        ->delete();
        }
    }




    public function index(Request $request)
    {
        SessionService::start(0, SessionService::SS_GMS);

        if (isset($request->player_id))
        {
            $playerId = $request->player_id;
        }
        else
        {
            //$playerId = 101;
        }

        // プレイヤーごとにゲームDBを切り替える 
        GmsService::updateDbNo($playerId);

        $this->player = Player::find($playerId);
        $now = DateTimeUtil::getNOW();
        // 有償・無償の石を取得
        // プレイヤー情報に追加
        $this->freeCrystal = PlayerBlueCrystal::getFreeNum($playerId, $now);
        $this->chargedCrystal = PlayerBlueCrystal::getChargedNum($playerId, $now);

        $storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
        $characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);
        $eventQuestService = QuestService::make(QuestService::QUEST_CATEGORY_EVENT);
        $this->storyQuests = $storyQuestService->getAllQuests();
        $this->characterQuests = $characterQuestService->getAllQuests();
        $this->eventQuests = $eventQuestService->getAllQuests();

        $missionTypes = array(Mission::TYPE_DAILY, Mission::TYPE_NORMAL, Mission::TYPE_EVENT);
        $this->missions = Mission::getAll($now, $missionTypes);

        $this->loginBonuses = LoginBonusReward::getAllCount();

        if (isset($request->action))
        {
            $this->modify($request);
        }

        // 有償・無償の石を取得
        // プレイヤー情報に追加
        $this->freeCrystal = PlayerBlueCrystal::getFreeNum($playerId, $now);
        $this->chargedCrystal = PlayerBlueCrystal::getChargedNum($playerId, $now);

        $mCharacters = CharacterBase::getAll();
        $mGrimoires = Grimoire::getAll();
        $mItems = Item::getAll();
        $playerItems = PlayerItem::getByPlayerId($playerId);

        $characters = PlayerCharacter::where('player_id', '=', $playerId)->paginate(10, ["*"], 'characterPage');
        $grimoires = PlayerGrimoire::where('player_id', '=', $playerId)->paginate(10, ["*"], 'grimoirePage');

        // キャラクター付与
        $charaNames = [];
        foreach ($mCharacters as $mCharacter)
        {
            $cd = CharacterDictionary::getOne($mCharacter->character_name_id);
            $charaNames[$mCharacter->id] = $cd->dictionary_ja;
        }

        // 魔導書付与
        $grimoireNames = [];
        foreach ($mGrimoires as $mGrimoire)
        {
            $gd = GrimoireDictionary::getOne($mGrimoire->grimoire_name_id);
            $grimoireNames[$mGrimoire->id] = $gd->dictionary_ja;
        }

        // アイテム種別分け
        $pOrbs = [];
        $pFragments = [];
        $pItems = [];
        foreach ($playerItems as $playerItem)
        {
            $pItemCat = intdiv($playerItem->item_id, 1000000);
            // オーブ
            if($pItemCat == 4)
            {
                $pOrbs[] = $playerItem;
            }
            // 欠片
            elseif ($pItemCat == 6)
            {
                $pFragments[] = $playerItem;
            }
            // アイテム
            elseif ($pItemCat == 2 || $pItemCat == 3 || $pItemCat == 5 || $pItemCat == 7)
            {
                $pItems[] = $playerItem;
            }

        }


        $itemNames = [];
        $orbNames = [];
        $fragmentNames = [];
        foreach ($mItems as $mItem)
        {
            $mItemCat = intdiv($mItem->id, 1000000);
            $itemD = ItemDictionary::getOne($mItem->item_name_id);
            // オーブ
            if($mItemCat == 4)
            {
                $orbNames[$mItem->id] = $itemD->dictionary_ja;
            }
            // 欠片
            elseif ($mItemCat == 6)
            {

                $fragmentNames[$mItem->id] = $itemD->dictionary_ja;
            }
            // アイテム
            elseif ($mItemCat == 2 || $mItemCat == 3 || $mItemCat == 5 || $mItemCat == 7)
            {
                $itemNames[$mItem->id] = $itemD->dictionary_ja;
            }
        }
        
        
        $storyQuestNames = [];
        $characterQuestNames = [];
        $eventQuestNames = [];


        $qd = QuestDictionary::getOne(9901001);
        

        foreach ($this->storyQuests as $storyQuest)
        {
            $qd = QuestDictionary::getOne($storyQuest->id);
            if (isset($qd))
            {
                $name = $qd->dictionary_ja;
            }
            else
            {
                $name = 'クエスト名がありません: ' . $storyQuest->id;
            }
            
            $storyQuestNames[$storyQuest->id] = $name;
        }

        foreach ($this->characterQuests as $characterQuest)
        {
            $qd = QuestDictionary::getOne($characterQuest->id);
            $characterQuestNames[$characterQuest->id] = $qd->dictionary_ja;
        }

        foreach ($this->eventQuests as $eventQuest)
        {
            $qd = EventQuestDictionary::getOne($eventQuest->id);
            $eventQuestNames[$eventQuest->id] = $qd->dictionary_ja;
        }


        $missionNames = [];

        foreach ($this->missions as $mission)
        {
            $missionD = MissionDictionary::getOne($mission->mission_detail);
            $missionNames[$mission->id] = $mission->id . ' ： ' . $missionD->dictionary_ja;
        }


        
        $loginBonusNames = [];
        $prevLoginId = 0;
        $prevLoginCnt = 0;

        foreach ($this->loginBonuses as $loginBonus)
        {
            if ($loginBonus->login_bonus_id == $prevLoginId && 
                $loginBonus->count == $prevLoginCnt)
                continue;
            $lbd = LoginBonusDictionary::getOne($loginBonus->login_bonus_message);
            $loginBonusNames[$loginBonus->id] = $lbd->dictionary_ja;

            $prevLoginId = $loginBonus->login_bonus_id;
            $prevLoginCnt = $loginBonus->count;
        }


        $pOrbs = PlayerItem::where('player_id', $playerId)
                                    ->whereRaw(
                                        'item_id DIV ? = ?',
                                        [
                                            Item::CATEGORY_RANGE,
                                            Item::CATEGORY_ORB,
                                        ]
                                    )
                                    ->paginate(10, ["*"], 'orbPage');

        $pFragments = PlayerItem::where('player_id', $playerId)
                                    ->whereRaw(
                                        'item_id DIV ? = ?',
                                        [
                                            Item::CATEGORY_RANGE,
                                            Item::CATEGORY_FRAGMENT,
                                        ]
                                    )
                                    ->paginate(10, ["*"], 'fragmentPage');

        $pItems = PlayerItem::where('player_id', $playerId)
                                    ->whereRaw(
                                        'item_id DIV ? IN (?, ?, ?, ?)',
                                        [
                                            Item::CATEGORY_RANGE,
                                            Item::CATEGORY_TICKET,
                                            Item::CATEGORY_EXP,
                                            Item::CATEGORY_SKIP_TICKET,
                                            Item::CATEGORY_AL_RECOVER,
                                        ]
                                    )
                                    ->paginate(10, ["*"], 'itemPage');


        $params = [
            'player' => $this->player,
            'freeCrystal' => $this->freeCrystal,
            'chargedCrystal' => $this->chargedCrystal,
            'characters' => $characters,
            'grimoires' => $grimoires,
            'pOrbs'=> $pOrbs,
            'pFragments'=> $pFragments,
            'pItems'=> $pItems,
            'charaNames' => $charaNames,
            'grimoireNames' => $grimoireNames,
            'orbNames' => $orbNames,
            'fragmentNames' => $fragmentNames,
            'itemNames' => $itemNames,
            'rowCharacter' => '',
            'rowGrimoire' => '',
            'rowOrb' => '',
            'rowFragment' => '',
            'rowItem' => '',
            'storyQuestNames' => $storyQuestNames,
            'characterQuestNames' => $characterQuestNames,
            'eventQuestNames' => $eventQuestNames,
            'loginBonusNames' => $loginBonusNames,
            'missionNames' => $missionNames,
            

        ];

        return view('debug.debug', $params);

    }

}
