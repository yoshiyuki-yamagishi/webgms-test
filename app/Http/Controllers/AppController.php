<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AppVersionRequest;
use App\Services\AppVersionService;

class AppController extends Controller
{
	public function version(AppVersionRequest $request)
	{
		$response = AppVersionService::get($request);
// 		return $response->toArray();
		return $response->toResponse();
	}
}
