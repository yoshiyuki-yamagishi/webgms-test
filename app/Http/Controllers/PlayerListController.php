<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\PlayerCommon;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerService;
use App\Services\SessionService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerListController extends Controller
{
	/**
	 * GMS プレイヤー一覧
	 * @param Request $request
	 * @return
	 */

    public $player = null;

    public function search(Request $request)
    {

        SessionService::start(0, SessionService::SS_GMS);
        // $playerItems = PlayerItem::getByPlayerId($playerId);
        $query = PlayerCommon::join(
                                    'player_common_cache',
                                    'player_common_cache.player_id',
                                    '=',
                                    'player_common.player_id'
                                    )
                                    ->where('valid_flag', 1);

        if ($request->select_search == 'id')
        {
            $idWord = $request->search_id_word;
            $query->where('player_common.player_id', 'like', "%{$idWord}%");
        }

        if ($request->select_search == 'disp')
        {
            $dispWord = $request->search_disp_word;
            $query->where('player_disp_id', 'like', "%{$dispWord}%");
        }

        if ($request->select_search == 'name')
        {
            $nameWord = $request->search_name_word;
            $query->where('player_name', 'like', "%{$nameWord}%");
        }

        $playerCommonList = $query->orderBy('player_disp_id')->get();

        DebugUtil::e_log('playerCommonList', 'playerCommonList', $playerCommonList);

        $this->playerList = $playerCommonList;



        // $playerList = [];
        // foreach ($playerCommonList as $playerCommon)
        // {
        //     $player = [];
        //     $player['player_id'] = $playerCommon->player_id;
        //     $player['player_disp_id'] = $playerCommon->player_disp_id;

        //     // プレイヤーごとにゲームDBを切り替える
        //     $dbNo = $playerCommon->db_no;
        //     $player['db_no'] = $playerCommon->db_no;
        //     SessionService::setDbNo($dbNo);

        //     // DebugUtil::e_log('playerCommon', 'playerCommon', $playerCommon);
        //     // DebugUtil::e_log('search_name_word', 'search_name_word', $request->search_name_word);
        //     $nameWord = $request->search_name_word;
        //     $idWord = $request->search_id_word;
        //     $dispWord = $request->search_disp_word;

        //     if ($request->select_search == 'name')
        //     {
        //         $_player = Player::where('id', $playerCommon->player_id)
        //         ->where('player_name', 'like', "%{$nameWord}%")
        //         ->first();
        //     }

        //     if ($request->select_search == 'id')
        //     {
        //         $_player = Player::where('id', $playerCommon->player_id)
        //         ->where('id', 'like', "%{$idWord}%")
        //         ->first();
        //     }

        //     if ($request->select_search == 'disp')
        //     {
        //         $_player = PlayerCommon::where('player_id', $playerCommon->player_id)
        //         ->where('player_disp_id', 'like', "%{$dispWord}%")
        //         ->first();
        //     }

        //     // DebugUtil::e_log('_player', '_player', $_player);

        //     if (isset($_player))
        //     {
        //         $player['player_name'] = $_player->player_name;
        //         $player['player_lv'] = $_player->player_lv;
        //         $player['last_login_at'] = $_player->last_login_at;

        //         $this->playerList[] = $player;
        //     }
        // };
        // DebugUtil::e_log('playerList', 'playerList', $this->playerList);
        // // DebugUtil::e_log('request', 'request', $request->all());
    }


    public function index(Request $request)
    {
        $this->middleware('auth');
        $this->playerList = [];
        if (isset($request->action))
        {
            $this->search($request);
        }

        $params = [
            'playerList' => $this->playerList,

        ];
        return view('browse.playerList', $params);
    }

}
