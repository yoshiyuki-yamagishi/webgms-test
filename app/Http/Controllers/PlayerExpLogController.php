<?php

namespace App\Http\Controllers;

use App\Models\PlayerCharacterLog;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Models\PlayerLog;
use App\Utils\DebugUtil;

class PlayerExpLogController extends Controller
{
	
	public function index(Request $request)
	{
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$playerExpLogs = PlayerLog::getPlayerExpLog($playerId);
		DebugUtil::e_log('playerExpLogs', 'playerExpLogs', $playerExpLogs);

		$params = [
            'playerExpLogs' => $playerExpLogs,
        ];

		return view('player_log.player_exp_log', $params);

	}

}