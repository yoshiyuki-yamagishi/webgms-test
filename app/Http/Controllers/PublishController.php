<?php

namespace App\Http\Controllers;

use App\GmsModels\Info;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\SessionService;
use App\GmsServices\HtmlIntoListService;
use App\GmsServices\HtmlIntoDetailService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use DateTime;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\Auth;

class PublishController extends Controller
{
	/**
	 * GMS お知らせ公開
	 * @param Request $request
	 * @return
	 */

    public function upHtml(Request $request)
    {
        $now = new DateTime();
        for ($i = 1; $i <= 4; ++ $i)
        {
            $infos = Info::getByType($i);
            DebugUtil::e_log('infos', 'infos', $infos);
            // DebugUtil::e_log('request', 'request', $request->all());

            // HtmlIntoListService HtmlIntoDetailServiceでhtml登録処理
            // $req = $request->all();
            DebugUtil::e_log('request', 'request', $request->all());

            HtmlIntoListService::write($i, $infos, $now);
            HtmlIntoDetailService::write($i, $infos, $now);
            // 全件の本文をテキストに書いて排出
            // foreach ($infos as $font)
            // {
            //     $content = $font->content;
            //     $title = $font->title;
            //     $fname = $font->fname;
            //     DebugUtil::e_log('content', 'content', $content);
            //     DebugUtil::e_log('title', 'title', $title);
            //     DebugUtil::e_log('fname', 'fname', $fname);

            // }
        }

    }

    public function index(Request $request)
    {
        DebugUtil::e_log('action', 'action', $request->action);


        $infoId = [];
        $infoID [1] = 'お知らせ';
        $infoID [2] = 'メンテナンス';
        $infoID [3] = 'アップデート';
        $infoID [4] = '不具合報告';

        $infos = Info::getAll();

        foreach ($infos as $info)
        {
            if ($info->info_id == 1)
            {
                $info->info_id = 'お知らせ';
            }
            elseif($info->info_id == 2)
            {
                $info->info_id = 'メンテナンス';
            }
            elseif($info->info_id == 3)
            {
                $info->info_id = 'アップデート';
            }
            elseif($info->info_id == 4)
            {
                $info->info_id = '不具合報告';
            }
        }

        DebugUtil::e_log('infos', 'infos', $infos);
        DebugUtil::e_log('request', 'request', $request->all());
        if (isset($request->action))
        {
            DebugUtil::e_log('a', 'a', 'a');
            $this->upHtml($request);
        }
        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;
        $params = [
            'infos' => $infos,
            'infoID' => $infoID,
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
        ];

        return view('info.publish', $params);

    }

}
