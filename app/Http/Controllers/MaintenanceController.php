<?php

namespace App\Http\Controllers;

use App\GmsModels\Maintenance;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class MaintenanceController extends Controller
{
	/**
	 * メンテナンスDBの修正系
	 * @param Request $request
	 * @return
	 */
    public function modify(Request $request)
    {
        //datetime-localは形の変換が必要
        $startAt = Carbon::parse($request->start_at)->format("Y-m-d H:i:s");
        $endAt = Carbon::parse($request->end_at)->format("Y-m-d H:i:s");
        if ($request->action == 'delete_maintenance')
        {
            Maintenance::physicalDeleteByPK($request->id);
        }
        elseif ($request->action == 'edit_maintenance')
        {
            Maintenance::updateByPK($request->id, $request->info_id, $request->message, $startAt, $endAt);
        }
        elseif ($request->action == 'create_maintenance')
        {
            Maintenance::register($request->info_id, $request->message, $startAt, $endAt);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->maintenances = [];
        if(isset($request)){
            $this->modify($request);
        }
        //最新50件あれば検索いらないよね
        $this->maintenances = Maintenance::getLimit();
        //datetime-localは"Y-m-d H:i:s"が許されないみたいなので変換
        foreach ($this->maintenances as $maintenance){
            $maintenance->editDisp_start_at = Carbon::parse($maintenance->start_at)->format('Y-m-d\TH:i');
            $maintenance->editDisp_end_at = Carbon::parse($maintenance->end_at)->format('Y-m-d\TH:i');
        }
        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;
        $params = [
            'maintenances' => $this->maintenances,
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
        ];

        return view('maintenance.maintenance', $params);
    }

}
