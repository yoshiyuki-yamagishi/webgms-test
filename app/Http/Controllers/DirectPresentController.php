<?php

namespace App\Http\Controllers;

use App\GmsModels\DirectPresent;

use App\GmsServices\GmsDirectPresentService;
use Illuminate\Http\Request;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;


use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Support\Facades\Auth;


class DirectPresentController extends Controller
{

	public function modify(Request $request)
    {
        DebugUtil::e_log('all', 'request', $request->all());
        $type = $request->selectPlayer;

        if ($type == 2)
        {
            $playerId = $request->player_id;
        }
        else
        {
            $playerId = null;
        }

        $startAt = $request->start_at . ' ' . $request->start_at_hh . ':' .  $request->start_at_mm;
        $endAt = $request->end_at . ' ' . $request->end_at_hh . ':' .  $request->end_at_mm;

        if ($type == 3)
        {
            $date1 = $request->date1 . ' ' . $request->date1_at_hh . ':' .  $request->date1_at_mm;
            $date2 = $request->date2 . ' ' . $request->date2_at_hh . ':' .  $request->date2_at_mm;
        }
        else
        {
            $date1 = null;
            $date2 = null;
        }


        $_itemType = $request->itemType;
        if ($_itemType == 'chara')
        {
            $itemType = Item::TYPE_CHARACTER;
            $itemId = $request->character_base_id;
        }
        else if ($_itemType == 'grimoire')
        {
            $itemType = Item::TYPE_GRIMOIRE;
            $itemId = $request->grimoire_id;
        }
        else if ($_itemType == 'mana')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = Item::ID_ELEMENT;
        }
        else if ($_itemType == 'orb')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = $request->orb_id;
        }
        else if ($_itemType == 'fragment')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = $request->fragment_id;
        }
        else if ($_itemType == 'fragment_powder')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = Item::ID_POWDER;
        }
        else if ($_itemType == 'item')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = $request->item_id;
        }
        else if ($_itemType == 'money')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = Item::ID_P_DOLLAR;
        }
        else if ($_itemType == 'friend_point')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = Item::ID_FRIEND_POINT;
        }
        else if ($_itemType == 'blue_crystal')
        {
            $itemType = Item::TYPE_ITEM;
            $itemId = Item::ID_BLUE_CRYSTAL;
        }


        // $itemId = $request->item_id;

        $message = $request->content;
        $takableDays = $request->takable_days;

        if ($itemType != Item::TYPE_ITEM)
        {
            $itemNum = 1;
        }
        else
        {
            $itemNum = $request->num;
        }


        DirectPresent::register(
            $type,
            $playerId,
            $date1,
            $date2,
            $itemType,
            $itemId,
            $itemNum,
            $message,
            $takableDays,
            $startAt,
            $endAt
        );
	}
	/**
	 * GMS 直接プレゼント
	 * @param Request $request
	 * @return
	 */
	public function index(request $request)
	{
        SessionService::start(0, SessionService::SS_GMS);

        if (isset($request->action))
        {
            $this->modify($request);
        }

        $mCharacters = CharacterBase::getAll();

        $mGrimoires = Grimoire::getAll();

        $mItems = Item::getAll();


        $charaNames = [];
        foreach ($mCharacters as $mCharacter)
        {
            $cd = CharacterDictionary::getOne($mCharacter->character_name_id);
            $charaNames[$mCharacter->id] = $cd->dictionary_ja;
        }

        $grimoireNames = [];
        foreach ($mGrimoires as $mGrimoire)
        {
            $gd = GrimoireDictionary::getOne($mGrimoire->grimoire_name_id);
            $grimoireNames[$mGrimoire->id] = $gd->dictionary_ja;
        }


		$itemNames = [];
        $orbNames = [];
        $fragmentNames = [];
        foreach ($mItems as $mItem)
        {
            $mItemCat = intdiv($mItem->id, 1000000);
            $itemD = ItemDictionary::getOne($mItem->item_name_id);
            // オーブ
            if($mItemCat == 4)
            {
                $orbNames[$mItem->id] = $itemD->dictionary_ja;
            }
            // 欠片
            elseif ($mItemCat == 6)
            {
                $fragmentNames[$mItem->id] = $itemD->dictionary_ja;
            }
            // アイテム
            elseif ($mItemCat == 2 || $mItemCat == 3 || $mItemCat == 5 || $mItemCat == 7)
            {
                $itemNames[$mItem->id] = $itemD->dictionary_ja;
            }
        }


        // レコード表示

        $directPresents = DirectPresent::getAll();

        foreach ($directPresents as $directPresent)
        {
            if ($directPresent->type == 1)
            {
                $directPresent->type = '全プレイヤー';
            }
            elseif ($directPresent->type == 2)
            {
                $directPresent->type = 'プレイヤー指定';
            }


            if ($directPresent->item_type == 1)
            {
                $character = CharacterBase::getOne($directPresent->item_id);
                $name = CharacterDictionary::getOne($character->character_name_id);
            }
            elseif ($directPresent->item_type == 2)
            {
                $grimoire = Grimoire::getOne($directPresent->item_id);
                $name = GrimoireDictionary::getOne($grimoire->grimoire_name_id);
            }
            elseif ($directPresent->item_type == 3)
            {
                $item = Item::getOne($directPresent->item_id);
                $name = ItemDictionary::getOne($item->item_name_id);
            }
            $directPresent->name = $name->dictionary_ja;

        }
        DebugUtil::e_log('directPresents', 'directPresents', $directPresents);

        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;

        $params = [
            'charaNames' => $charaNames,
            'grimoireNames' => $grimoireNames,
            'orbNames' => $orbNames,
            'fragmentNames' => $fragmentNames,
            'itemNames' => $itemNames,
            'present' => '',
			'directPresents' => $directPresents,
            'authLevel' => $authLevel,
        ];

        return view('present.present', $params);
	}
    /**
     * csv読み取って配布する
     * @param Request $request
     * @return
     */
    public function multiItemPresentPlayers(Request $request)
    {

        $file = $request->multiPresentPlayers;

        $csv_datas = array();
        //データーが大きくなるとメモリー負荷がかかるかも
        $fp = fopen($file, "r");

        $count = 0;
        //文字コード精査
        while (($tmp_array = fgetcsv($fp, 0, ',')) !== FALSE) {
            mb_convert_variables('UTF-8', "ASCII,UTF-8,SJIS-win", $tmp_array);

            //php bom対策　なぜか最初だけ変なデーターが入るので入れる
            if($count === 0){
                $tmp_array[0] = preg_replace('/^\xEF\xBB\xBF/', '',$tmp_array[0]);
            }
            $csv_datas[] = $tmp_array;
            $count++;
        }

        fclose($fp);

        $insert_record_datas = array();
        $insert_record_datas = GmsDirectPresentService::makeRecordData($csv_datas);

        DirectPresent::multiInsert($insert_record_datas);

              header('Location: ./index');
              exit;
   }

}
