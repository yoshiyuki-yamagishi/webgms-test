<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\FlavorDictionary;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class SkillMasterController extends Controller
{
	/**
	 * BBDW_MasterData_grimoire.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		$skills = Skill::getAll();
		// DebugUtil::e_log('dc', 'request', $request->all());

		foreach ($skills as $skill)
		{
			$skillName = SkillDictionary::getOne($skill->skill_name_id);
			DebugUtil::e_log('skillName', 'skillName', $skillName);
			$skill->skill_name = $skillName->dictionary_ja;

			$skillDescription = SkillDictionary::getOne($skill->skill_description_id);
			$skill->skill_description = $skillDescription->dictionary_ja;

			for ($i = 1; $i <= 10; ++ $i)
			{
				$effectDetail = 'effect_detail_'.$i;
				$skill->$effectDetail = $i.'：'.$skill->$effectDetail;
			}

			if ($skill->usetype == 1)
			{
				$skill->skill_type = 'アクティブ';
			}
			elseif ($skill->usetype == 2)
			{
				$skill->skill_type = 'パッシブ';
			}
			elseif ($skill->usetype == 3)
			{
				$skill->skill_type = 'SP追加効果';
			}
			elseif ($skill->usetype == 4)
			{
				$skill->skill_type = 'エネミー効果';
			}
			elseif ($skill->usetype == 5)
			{
				$skill->skill_type = 'DD追加効果';
			}
			elseif ($skill->usetype == 6)
			{
				$skill->skill_type = 'AH追加効果';
			}
			else
			{
				$skill->use_type = 'その他';
			}

			if ($skill->recast == -1)
			{
				$skill->recast = '';
			}
			else
			{
				$skill->recast = 'リキャスト：'.$skill->recast;
			}

		}

		DebugUtil::e_log('skills', 'skills', $skills);
		$params = [
			'skills' => $skills,

		];

		return view('master_check.skill_master', $params);
	}





}
