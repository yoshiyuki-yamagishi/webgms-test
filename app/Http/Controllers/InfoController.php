<?php

namespace App\Http\Controllers;


use App\Models\Information;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;


class InfoController extends Controller
{
    /**
     * GMS お知らせ管理
     * @param Request $request
     * @return
     * @throws \Exception
     */
    public function modify(Request $request)
    {
        $startedAt = Carbon::parse($request->started_at)->format("Y-m-d H:i:s");
        $expiredAt = Carbon::parse($request->expired_at)->format("Y-m-d H:i:s");

        if ($request->action == 'update')
        {
            Information::editUpdate($request, $startedAt, $expiredAt);
        }
        elseif ($request->action == 'delete')
        {
            Information::deleteByPK($request->id);
        }

    }

    public function index(Request $request)
    {

        $infoID [1] = 'お知らせ';
        $infoID [2] = 'メンテナンス';
        $infoID [3] = 'アップデート';
        $infoID [4] = '不具合報告';


        if (isset($request->action))
        {
         if($request->action == "preview") {
             $param = [
                 'info' => $request
             ];
             return view('info.preview', $param);
         }
         $this->modify($request);
        }

        $platform = [];

        $narrowData = [
            'platform1' => 0,
            'platform2' => 0,
            'platform3' => 0,
            'platform4' => 0,
            'platform5' => 0,
            'platform6' => 0,
            'platform7' => 0,
        ];

        foreach ($narrowData as $key => $value){
             if(isset($request->{$key})){
                 $narrowData[$key] = $request->{$key};
                 if($request->{$key}) {
                     $platform[] = $request->{$key};
                 }
             }
        }

        $infoType = 0;
        $narrowData['infoType'] = $infoType;
        if(isset($request->infoType)){
            $infoType = $request->infoType;
            $narrowData['infoType'] = $request->infoType;
        }
        $infos = Information::getPagingList($infoType , $platform);

        foreach ($infos as $info)
        {
            switch ($info->info_type){
                case 1:
                    $info->disp_info_type = 'お知らせ';
                    break;
                case 2:
                    $info->disp_info_type = 'メンテナンス';
                    break;
                case 3:
                    $info->disp_info_type = 'アップデート';
                    break;
                case 4:
                    $info->disp_info_type = '不具合報告';
                    break;
                default:
                    $info->disp_info_type = 'おかしな数値が入ってるよ';
                    break;
            }

            switch($info->platform_flag){
                case 1:
                    $info->disp_platform_flag = 'iOS';
                        break;
                case 2:
                    $info->disp_platform_flag = 'Android';
                        break;
                case 3:
                    $info->disp_platform_flag = 'iOS / Android';
                        break;
                case 4:
                    $info->disp_platform_flag = '公式';
                        break;
                case 5:
                    $info->disp_platform_flag = 'iOS / 公式';
                        break;
                case 6:
                    $info->disp_platform_flag = 'Android / 公式';
                        break;
                case 7:
                    $info->disp_platform_flag = 'iOS / Android / 公式';
                        break;
                default:
                    $info->disp_info_type = 'おかしな数値が入ってるよ';
                    break;
            }
            $info->editDisp_started_at = Carbon::parse($info->started_at)->format('Y-m-d\TH:i');
            $info->editDisp_expired_at = Carbon::parse($info->expired_at)->format('Y-m-d\TH:i');
        }


        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;
        $params = [
            'infos' => $infos,
            'infoID' => $infoID,
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
            'narrowData' => $narrowData
        ];

        return view('info.info', $params);

    }

}
