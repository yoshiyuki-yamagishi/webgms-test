<?php

namespace App\Http\Controllers;

use App\Models\PlayerMission;

use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\MissionDictionary;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerRegistRequest;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;

class PlayerMissionLogController extends Controller
{
	
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);

		$now = DateTimeUtil::getNOW();
		$playerId = $request->player_id;

		$missionLogs = PlayerMission::getByPlayerId($playerId, $now);
		
		foreach ($missionLogs as $missionLog)
		{
			$mission = Mission::getOne($missionLog->mission_id);
			$item = Item::getOne($missionLog->remuneration);
			$itemD = ItemDictionary::getOne($item->item_name_id);
			$missionD = MissionDictionary::getOne($mission->mission_detail);
			$missionLog->item_name = $itemD->dictionary_ja;
			$missionLog->mission_name = $missionD->dictionary_ja;

			if ($missionLog->report_flag == 0)
			{
				$missionLog->report = '未報告';
			}
			elseif ($missionLog->report_flag == 1)
			{
				$missionLog->report = '報告済み';
			}
		}

		$params = [
            'missionLogs' => $missionLogs,
        ];

		return view('player_log.player_mission_log', $params);

	}

}