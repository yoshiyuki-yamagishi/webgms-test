<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class ItemMasterController extends Controller
{
	
	public function detail(Request $request)
	{

	}
	/**
	 * BBDW_MasterData_item.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		$items = Item::getAll();
		$host = $request->getHost();
		DebugUtil::e_log('$host', '$host', $request->getHost());

		foreach ($items as $item)
		{
			$itemNameD = ItemDictionary::getOne($item->item_name_id);
			$item->item_name = $itemNameD->dictionary_ja;
			$itemDetailD = ItemDictionary::getOne($item->item_detail_id);
			$item->item_detail = $itemDetailD->dictionary_ja;
			$itemCategoryNum = Item::categoryFromId($item->id);

			if ($itemCategoryNum == Item::CATEGORY_FREE_BLUE_CRYSTAL)
			{
				$item->type = '蒼の結晶（無償）';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_TICKET)
			{
				$item->type = 'ガチャチケット';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_EXP)
			{
				$item->type = '経験値アイテム';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_ORB)
			{
				$item->type = 'オーブ';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_SKIP_TICKET)
			{
				$item->type = 'スキップチケット';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_FRAGMENT)
			{
				$item->type = 'キャラクター欠片';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_AL_RECOVER)
			{
				$item->type = '回復アイテム';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_FRIEND_POINT)
			{
				$item->type = 'フレンドポイント';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_BLUE_CRYSTAL)
			{
				$item->type = '蒼の結晶（有償）';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_P_DOLLAR)
			{
				$item->type = 'P$';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_ELEMENT)
			{
				$item->type = '魔素';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_POWDER)
			{
				$item->type = '欠片パウダー';
			}
			elseif ($itemCategoryNum == Item::CATEGORY_EVENT)
			{
				$item->type = 'イベントアイテム';
			}

			// アイコン表示
			if ($host == 'localhost')
			{
				// ローカル用
				$item->img = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com:8080/assetbundle_rsc/ItemIcon/'.$item->item_pic.'.png';
			}
			else
			{
				// 開発用サーバー用
				$item->img = '/assetbundle_rsc/ItemIcon/'.$item->item_pic.'.png';

			}

		}

		DebugUtil::e_log('items', 'items', $items);
		$params = [
			'items' => $items,

		];
		return view('master_check.item_master', $params);
	}


}
