<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Services\PlayerGrimoireService;

class PlayerGrimoireController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerGrimoireListRequest $request
	 * @return
	 */
	public function list(PlayerGrimoireListRequest $request)
	{
		$response = PlayerGrimoireService::list($request);
		return $response->toResponse();
	}

	/**
	 * 強化
	 * @param PlayerGrimoireReinforceRequest $request
	 * @return
	 */
	public function reinforce(PlayerGrimoireReinforceRequest $request)
	{
		$response = PlayerGrimoireService::reinforce($request);
		return $response->toResponse();
	}

	/**
	 * 枠増加
	 * @param PlayerGrimoireLimitUpRequest $request
	 * @return
	 */
	public function limitUp(PlayerGrimoireLimitUpRequest $request)
	{
		$response = PlayerGrimoireService::limitUp($request);
		return $response->toResponse();
	}

	/**
	 * 売却
	 * @param PlayerGrimoireSellRequest $request
	 * @return
	 */
	public function sell(PlayerGrimoireSellRequest $request)
	{
		$response = PlayerGrimoireService::sell($request);
		return $response->toResponse();
	}
}
