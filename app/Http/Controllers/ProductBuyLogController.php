<?php

namespace App\Http\Controllers;

use App\Models\PlayerBlueCrystal;
use App\Models\PlayerBlueCrystalLog;
use App\Models\PlayerProductBuy;

use App\Models\MasterModels\ProductShop;
use App\Models\MasterModels\ShopDictionary;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\SessionService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class ProductBuyLogController extends Controller
{
	
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);

		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;

		$query = PlayerBlueCrystalLog::join(
											'player_product_buy',
											'player_product_buy.id',
											'=',
											'player_blue_crystal_log.src_id'
											)
											->where('player_blue_crystal_log.player_id', $playerId);

		$playerProductBuyLogs = $query->orderBy('player_blue_crystal_log.id')->get();

		// DebugUtil::e_log('playerProductBuyLogs', 'playerProductBuyLogs', $playerProductBuyLogs);

		


		foreach ($playerProductBuyLogs as $playerProductBuyLog)
		{
			if ($playerProductBuyLog->store_id == 1)
			{
				$playerProductBuyLog->store_name = 'Android';
			}
			elseif ($playerProductBuyLog->store_id == 1)
			{
				$playerProductBuyLog->store_name = 'iOS';
			}

			$product = ProductShop::getOne($playerProductBuyLog->product_id);
			$productD = ShopDictionary::getOne($product->product_name_id);
			$playerProductBuyLog->product_name = $productD->dictionary_ja;


		}

		DebugUtil::e_log('playerProductBuyLogs', 'playerProductBuyLogs', $playerProductBuyLogs);
		$params = [
			'playerProductBuyLogs' => $playerProductBuyLogs,
		];

		return view('player_log.product_buy_log', $params);

	}
}
