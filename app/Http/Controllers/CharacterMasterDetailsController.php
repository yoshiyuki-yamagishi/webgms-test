<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterBattleInfo;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\CharacterFlavorDictionary;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\GmsServices\GmsCharacterService;
use App\Services\PlayerItemService;
use App\Services\SessionService;

use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class CharacterMasterDetailsController extends Controller
{
	/**
	 * BBDW_MasterData_character.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('dc', 'request', $request->all());
		$characterId = $request->id;
		$characterName = $request->character_name;
		$character = $characterId.'：'.$characterName;

		if (!isset($evolve))
		{
			$evolve = 1;
		}

		$maxLv = GmsCharacterService::maxCharacterLv();

		$characterParams = [];
		for ($i = 1; $i <= $maxLv; ++ $i)
		{
			$characterParams[] = GmsCharacterService::calcCharacterParam($characterId, $evolve, $i);
		}
		// DebugUtil::e_log('characterParam', 'characterParam', $characterParam);

		$params = [
			'characterParams' => $characterParams,
			'characterId' => $characterId,
			'character' => $character
		];

		return view('master_check.character_detail_master', $params);
	}





}
