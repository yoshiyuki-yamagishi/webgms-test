<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Services\LoginService;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
	/**
	 * ログイン
	 */
	public function login(LoginRequest $request) {
		// ログイン
		$response = LoginService::login($request);
		return $response->toResponse();
	}
}
