<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterQuestQuest;
use App\Models\MasterModels\DropReward;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\QuestDictionary;
use App\Models\MasterModels\QuestReward;
use App\Models\MasterModels\StoryQuestQuest;
use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use App\Services\QuestService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class ItemMasterDetailsController extends Controller
{
	/**
	 * BBDW_MasterData_item.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('dc', 'request', $request->all());

		$itemId = $request->item_id;
		
		// if (!isset($itemId))
		// {
		// 	$itemId = 0;
		// }

		$storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
		$characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);

		$dropRewards = DropReward::getByItemId($itemId);
		DebugUtil::e_log('dropRewards', 'dropRewards', $dropRewards);

		$quests = [];
		foreach ($dropRewards as $reward)
		{
			$storyDrop = StoryQuestQuest::getByDropRewardId($reward->id);
			DebugUtil::e_log('storyDrop', 'storyDrop', $storyDrop);
			foreach ($storyDrop as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->drop_reward_frame_count,
					'reward_type' => 'ドロップ報酬',
					'category_name' => 'ストーリー',
					'quest_name' => '',
				];

				$quests[] = $item;
			}

			$characterDrop = CharacterQuestQuest::getByDropRewardId($reward->id);
			foreach ($characterDrop as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->drop_reward_frame_count,
					'reward_type' => 'ドロップ報酬',
					'category_name' => 'キャラクター',
					'quest_name' => '',
				];

				$quests[] = $item;
			}
			DebugUtil::e_log('characterQuests', 'characterQuests', $quests);
		}


		$questRewards = QuestReward::getAllByItemId($itemId);
		DebugUtil::e_log('questRewards', 'questRewards', $questRewards);
		
		foreach ($questRewards as $reward)
		{
			$storyFirst = StoryQuestQuest::getByFirstRewardId($reward->id);

			foreach ($storyFirst as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->quest_reward_frame_count,
					'reward_type' => '初回報酬',
					'category_name' => 'ストーリー',
					'quest_name' => '',
				];

				$quests[] = $item;
				
			}

			DebugUtil::e_log('storyFirst', 'storyFirst', $storyFirst);

			$storyFix = StoryQuestQuest::getByFixRewardId($reward->id);

			foreach ($storyFix as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->quest_reward_frame_count,
					'reward_type' => '固定報酬',
					'category_name' => 'ストーリー',
					'quest_name' => '',
				];

				$quests[] = $item;
			}

			DebugUtil::e_log('storyFix', 'storyFix', $storyFix);

			
			$characterFirst = CharacterQuestQuest::getByFirstRewardId($reward->id);
			foreach ($characterFirst as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->quest_reward_frame_count,
					'reward_type' => '初回報酬',
					'category_name' => 'キャラクター',
					'quest_name' => '',
				];

				$quests[] = $item;
			}

			DebugUtil::e_log('characterFirst', 'characterFirst', $characterFirst);

			$characterFix = CharacterQuestQuest::getByFixRewardId($reward->id);
			foreach ($characterFix as $quest)
			{
				$item = [
					'quest_id' => $quest->id,
					'frame_count' => $reward->quest_reward_frame_count,
					'reward_type' => '固定報酬',
					'category_name' => 'キャラクター',
					'quest_name' => '',
				];

				$quests[] = $item;
			}
			DebugUtil::e_log('characterFix', 'characterFix', $characterFix);
		
		}
		

		

		foreach ($quests as &$quest)
		{
			$questD = QuestDictionary::getOne($quest['quest_id']);
			DebugUtil::e_log('questD', 'questD', $questD);
			$quest['quest_name'] = $questD->dictionary_ja;
			DebugUtil::e_log('quest_name', 'quest_name', $quest['quest_name']);
		}

		DebugUtil::e_log('quests', 'quests', $quests);



		DebugUtil::e_log('itemId', 'itemId', $itemId);
		$params = [
			'quests' => $quests,
			'itemId' => $itemId,
		];
		DebugUtil::e_log('params', 'params', $params);
		return view('master_check.item_detail_master', $params);
	}


}
