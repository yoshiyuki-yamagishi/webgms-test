<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PlayerCharacterService;
use App\Http\Requests\PlayerCharacterEvolveRequest;
use App\Http\Requests\PlayerCharacterFavoriteSelectRequest;
use App\Http\Requests\PlayerCharacterLeaderSelectRequest;
use App\Http\Requests\PlayerCharacterListRequest;
use App\Http\Requests\PlayerCharacterOrbEquipRequest;
use App\Http\Requests\PlayerCharacterReinforceRequest;
use App\Http\Requests\PlayerCharacterSkillReinforceRequest;

class PlayerCharacterController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerCharacterListRequest $request
	 * @return
	 */
	public function list(PlayerCharacterListRequest $request)
	{
		$response = PlayerCharacterService::list($request);
		return $response->toResponse();
	}

	/**
	 * 進化
	 * @param PlayerCharacterEvolveRequest $request
	 * @return
	 */
	public function evolve(PlayerCharacterEvolveRequest $request)
	{
		$response = PlayerCharacterService::evolve($request);
		return $response->toResponse();
	}

	/**
	 * 強化
	 * @param PlayerCharacterReinforceRequest $request
	 * @return
	 */
	public function reinforce(PlayerCharacterReinforceRequest $request)
	{
		$response = PlayerCharacterService::reinforce($request);
		return $response->toResponse();
	}

	/**
	 * スキル強化
	 * @param PlayerCharacterSkillReinforceRequest $request
	 * @return
	 */
	public function skillReinforce(PlayerCharacterSkillReinforceRequest $request)
	{
		$response = PlayerCharacterService::skillReinforce($request);
		return $response->toResponse();
	}

	/**
	 * オーブ装備
	 * @param PlayerCharacterOrbEquipRequest $request
	 * @return
	 */
	public function orbEquip(PlayerCharacterOrbEquipRequest $request)
	{
		$response = PlayerCharacterService::orbEquip($request);
		return $response->toResponse();
	}

	/**
	 * リーダ選択
	 * @param PlayerCharacterLeaderSelectRequest $request
	 * @return
	 */
	public function leaderSelect(PlayerCharacterLeaderSelectRequest $request)
	{
		$response = PlayerCharacterService::leaderSelect($request);
		return $response->toResponse();
	}

	/**
	 * お気に入り選択
	 * @param PlayerCharacterFavoriteSelectRequest $request
	 * @return
	 */
	public function favoriteSelect(PlayerCharacterFavoriteSelectRequest $request)
	{
		$response = PlayerCharacterService::favoriteSelect($request);
		return $response->toResponse();
	}
}
