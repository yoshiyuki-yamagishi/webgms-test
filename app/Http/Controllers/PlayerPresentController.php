<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlayerPresentListRequest;
use App\Http\Requests\PlayerPresentTakeRequest;
use App\Services\PlayerPresentService;
use App\Services\SessionService;

class PlayerPresentController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerPresentListRequest $request
	 * @return
	 */
	public function list(PlayerPresentListRequest $request)
	{
		$response = PlayerPresentService::list($request);
		return $response->toResponse();
	}

	/**
	 * 受取
	 * @param PlayerPresentTakeRequest $request
	 * @return
	 */
	public function take(PlayerPresentTakeRequest $request)
	{
		$response = PlayerPresentService::take($request);
		return $response->toResponse();
	}
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);

		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;






		
	}
}
