<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\PlayerBan;

use App\Models\PlayerCommon;
use App\Services\SessionService;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PlayerBanController extends Controller
{
	/**
	 * BAN登録
	 * @param Request $request
	 * @return
	 */
    public function modify(Request $request)
    {
        //datetime-localはformat必要！！！
        $startAt = Carbon::parse($request->start_at)->format("Y-m-d H:i:s");
        $endAt = Carbon::parse($request->end_at)->format("Y-m-d H:i:s");
        // レコード追加して登録
        $playerBan = new PlayerBan();
        $playerBan->player_id = $request->player_id;
        $playerBan->ban_status = $request->ban_status;
        $playerBan->message = $request->message;
        $playerBan->delete_flag = 0;
        $playerBan->start_at = $startAt;
        $playerBan->end_at = $endAt;
        $playerBan->save();
    }

    /**
     * 論理削除
     * @param Request $request
     */
    public function delete(Request $request)
    {
        PlayerBan::logicalDelete($request->id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //セッション開始
        if (isset($request->player_id)) {
            SessionService::start(0, SessionService::SS_GMS);
            $playerCommon = PlayerCommon::getByPlayerId($request->player_id);
            SessionService::setDbNo($playerCommon->db_no);
        }

        //アクション分岐
        if (isset($request->action)) {
            switch ($request->action) {
                case 'create_ban':
                    $this->modify($request);
                    break;
                case 'delete_ban':
                    $this->delete($request);
                    break;
                default:
                    break;
            }
        }
        //プレイヤーIDから取得
        $playerBans = [];
        $player = [];
        if (isset($request->player_id)) {
            $playerId = $request->player_id;
            $playerBans = PlayerBan::getByPlayerId($playerId);
            $player = Player::find($playerId);
        }
        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;
        $params = [
            'playerBans' => $playerBans,
            'player' => $player,
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
        ];
        return view('ban.playerBan', $params);
    }
}
