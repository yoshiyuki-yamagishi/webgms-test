<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\GachaDictionary;
use App\Models\MasterModels\GachaLot;
use App\Models\MasterModels\GachaGroup;
use App\GmsServices\GmsGachaService;
use App\Services\GachaService;
use App\Services\SessionService;

use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class GachaMasterDetailsController extends Controller
{

	/**
	 * BBDW_MasterData_grimoire.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		$req = $request->all();
		$host = $request->getHost();
		DebugUtil::e_log('$host', '$host', $request->getHost());
		DebugUtil::e_log('$req', '$req', $req);
		$gachaId = $request->id;
		$count = 0;
		$gachaResults = [];
		$gachaResults[] = [
            'expected' => 0,
            'count' => 0,
		];
		$resultList = [];
		
		if (array_key_exists('gacha_try', $req))
        {
			DebugUtil::e_log('request', 'request', $request->all());
			if ($request->gacha_select == 'single')
			{
				$count = $request->count;
				$isTen = false;

				// DebugUtil::e_log('count', 'count', $count);
				$resultList = GmsGachaService::try($gachaId, $isTen, $count);
				// DebugUtil::e_log('resultList', 'resultList', $resultList);
			}
			elseif ($request->gacha_select == 'ten')
			{
				$count = $request->count;
				$isTen = true;

				// DebugUtil::e_log('count', 'count', $count);
				$resultList = GmsGachaService::try($gachaId, $isTen, $count);
				// DebugUtil::e_log('resultList', 'resultList', $resultList);
			}


			foreach ($resultList as $result)
			{

				if ($result->content_type == 1)
				{
					$resultItem = CharacterBase::getOne($result->content_id);
					// DebugUtil::e_log('resultItem', 'resultItem', $resultItem);
					$resultItemD = CharacterDictionary::getOne($resultItem->character_name_id);
					$result->item_name = $resultItemD->dictionary_ja;;
					// $result->item_name = '"'.$result->item_name.'"';
					// $result->num = '"'.$result->get_count.'"';
				}
				elseif ($result->content_type == 2)
				{
					$resultItem = Grimoire::getOne($result->content_id);
					// DebugUtil::e_log('resultItem2', 'resultItem2', $resultItem);
					$resultItemD = GrimoireDictionary::getOne($resultItem->grimoire_name_id);
					$result->item_name = $resultItemD->dictionary_ja;
					// $result->item_name = '"'.$result->item_name.'"';
					// $result->num = '"'.$result->get_count.'"';
				}
				elseif ($result->content_type == 3)
				{
					$resultItem = Item::getOne($result->content_id);
					DebugUtil::e_log('resultItem', 'resultItem', $resultItem);
					$resultItemD = ItemDictionary::getOne($resultItem->item_name_id);
					$result->item_name = $resultItemD->dictionary_ja;
					// $result->item_name = '"'.$result->item_name.'"';
					// $result->num = '"'.$result->get_count.'"';
				}
				else
				{
					$result->item_name = '不明なアイテム';
				}

			}
			DebugUtil::e_log('resultList', 'resultList', $resultList);

		}
		// $array_count_values = array_count_values($resultList->content_id);
		$sum[1] = [];
		$sum[2] = [];
		$sum[3] = [];
		
		foreach ($resultList as $result)
		{
			if (array_key_exists($result->content_id, $sum[$result->content_type]))
			{
				$sum[$result->content_type][$result->content_id]['num'] += 1;
			}
			else
			{
				$sum[$result->content_type][$result->content_id] = [];
				$sum[$result->content_type][$result->content_id]['num'] = 1;
				$sum[$result->content_type][$result->content_id]['name'] = $result->item_name;
			}

			// $  ($result->content_type)
		}

		$totalCount = count($resultList);

		foreach ($sum as &$group)
		{
			foreach ($group as &$item)
			{
				$item['rate'] = round($item['num'] / $totalCount*100, 3);
				$item['rate'] = $item['rate'].'%';
			}
		}
		unset($item, $group);

		// DebugUtil::e_log('sumResult', 'sumResult', $sumResult);
		DebugUtil::e_log('sum', 'sum', $sum);


		// ガチャ情報表示
		// DebugUtil::e_log('gacha_request', 'gacha_request', $request->all());
		$gachaName = $request->gacha_name;
		$releaseDay = $request->release_day;
		$endDay = $request->end_day;

		$gacha = Gacha::getOne($gachaId);
		$useItemId = $gacha->item_id;
		$gacha->item_id = 'ID：'.$useItemId;
		$gachaLotId = $gacha->gacha_lot_id;
		DebugUtil::e_log('gacha', 'gacha', $gacha);

		$gacha->gacha_name = $gachaName;
		$gacha->release_day = $releaseDay;
		$gacha->end_day = $endDay;

		// バナー表示
		$banner = $gacha->gacha_banner;
		if ($host == 'localhost')
		{
			$gacha->img = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com:8080/assetbundle_rsc/Banner/Gacha/'.$banner.'.png';
		}
		else
		{
			$gacha->img = '/assetbundle_rsc/Banner/Gacha/'.$banner.'.png';
		}

		if ($gacha->single_item > 0)
		{
			$gacha->single_item = '単発消費：'.$gacha->single_item;
		}
		else
		{
			$gacha->single_item = '単発消費：なし';
		}

		if ($gacha->ten_item > 0)
		{
			$gacha->ten_item = '10連消費：'.$gacha->ten_item;
		}
		else
		{
			$gacha->ten_item = '10連消費：なし';
		}

		$item = Item::getOne($useItemId);
		$itemD = ItemDictionary::getOne($item->item_name_id);

		// DebugUtil::e_log('itemD', 'itemD', $itemD);
		$gacha->use_item_name = $itemD->dictionary_ja;
		

		// 通常枠ラインナップ
		$gachaLotList = GachaLot::getAll($gachaLotId);
		$gachaGroupList = GmsGachaService::calcRates($gachaLotList);
		DebugUtil::e_log('gachaGroupList', 'gachaGroupList', $gachaGroupList);

		// 確定枠ラインナップ
		$confirm = $gacha->confirm;
		$gachaLotConfirmList = GachaLot::getAll($confirm);
		$confirmList = GmsGachaService::calcRates($gachaLotConfirmList);
		DebugUtil::e_log('confirmList', 'confirmList', $confirmList);


		$params = [
			'gachaLotList' => $gachaLotList,
			'gachaGroupList' => $gachaGroupList,
			'gacha' => $gacha,
			'confirmList' => $confirmList,
			'sum' => $sum,
		];

		return view('master_check.gacha_detail_master', $params);
	}





}
