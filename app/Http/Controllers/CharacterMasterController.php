<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterBattleInfo;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\FlavorDictionary;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\Services\PlayerItemService;
use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class CharacterMasterController extends Controller
{
	/**
	 * BBDW_MasterData_character.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		$characters = CharacterBase::getAll();
		$host = $request->getHost();
		DebugUtil::e_log('$host', '$host', $request->getHost());
		
		foreach ($characters as $character)
		{
			$characterNameId = $character->character_name_id;
			$flavorId = $character->flavor_id;
			$active1 = $character->active1;
			$active2 = $character->active2;
			$rarity = $character->character_initial_rarity;
			$characterBattleInfoId = $character->character_battle_info_id;
			$character->cost = 'コスト：'.$character->cost;

			$characterNameD = CharacterDictionary::getOne($characterNameId);
			$character->character_name = $characterNameD->dictionary_ja;
			$characterFlavorD = FlavorDictionary::getOne($flavorId);
			$character->character_flavor = $characterFlavorD->flavor_dictionary_ja;
			DebugUtil::e_log('character', 'character', $character);

			$aSkill1 = Skill::getOne($active1);
			$aSkill2 = Skill::getOne($active2);

			$aSkill1D = SkillDictionary::getOne($aSkill1->skill_name_id);
			$aSkill2D = SkillDictionary::getOne($aSkill2->skill_name_id);

			$character->skill_1 = 'AS1：'.$aSkill1D->dictionary_ja;
			$character->skill_2 = 'AS2：'.$aSkill2D->dictionary_ja;

			for ($i = 1; $i <= 10; ++ $i)
			{
				$passive = 'passive'.$i;
				$passiveName = 'passive_name_'.$i;
				DebugUtil::e_log('passive', 'passive', $passive);
				if ($character->$passive != -1)
				{
					$passiveS = Skill::getOne($character->$passive);
					DebugUtil::e_log('passiveS', 'passiveS', $passiveS);
					$passiveD = SkillDictionary::getOne($passiveS->skill_name_id);
					$character->$passiveName = 'P'.$i.'：'.$passiveD->dictionary_ja;
				}
				if (!isset($character->$passiveName))
				{
					$character->$passiveName = 'P'.$i.'：'.'なし';
				}
			}



			$characterBattleInfo = CharacterBattleInfo::getOne($characterBattleInfoId);
			DebugUtil::e_log('characterBattleInfo', 'characterBattleInfo', $characterBattleInfo);
			$r = $characterBattleInfo->revolver_command_emission_rate/1000*100;
			$d = $characterBattleInfo->drive_command_emission_rate/1000*100;
			$sp = $characterBattleInfo->special_command_emission_rate/1000*100;

			$character->r_command = 'R：'.$r.'%';
			$character->d_command = 'D：'.$d.'%';
			$character->sp_command = 'SP：'.$sp.'%';

			$dd = Skill::getOne($characterBattleInfo->distortion_skill_id);
			DebugUtil::e_log('dd', 'dd', $dd);
			$ddName = SkillDictionary::getOne($dd->skill_name_id);
			DebugUtil::e_log('ddName', 'ddName', $ddName);
			$character->dd_name = 'DD：'.$ddName->dictionary_ja;

			$ah = Skill::getOne($characterBattleInfo->astral_skill_id);
			$ahName = SkillDictionary::getOne($ah->skill_name_id);
			$character->ah_name = 'AH：'.$ahName->dictionary_ja;

			if ($rarity == 1)
			{
				$character->character_rarity = '★：A2';
			}
			elseif ($rarity == 2)
			{
				$character->character_rarity = '★：A1';
			}
			elseif ($rarity == 3)
			{
				$character->character_rarity = '★：S';
			}
			elseif ($rarity == 4)
			{
				$character->character_rarity = '★：SS';
			}
			elseif ($rarity == 5)
			{
				$character->character_rarity = '★：SS+';
			}
			elseif ($rarity == 6)
			{
				$character->character_rarity = '★：SS++';
			}
			elseif ($rarity == 7)
			{
				$character->character_rarity = '★：SS+++';
			}
			$iId = sprintf('%06d', $character->id) . '0';

			// アイコン表示
			if ($host == 'localhost')
			{
				// ローカル用
				$character->img = 'http://bbdw-dev-lb-1372753899.ap-northeast-1.elb.amazonaws.com:8080/assetbundle_rsc/Reinforce/Character/rif_chr_'.$iId.'.png';
			}
			else
			{
				// 開発用サーバー用
				$character->img = '/assetbundle_rsc/Reinforce/Character/rif_chr_'.$iId.'.png';
			}

		}

		DebugUtil::e_log('characters', 'characters', $characters);
		$params = [
			'characters' => $characters,

		];

		return view('master_check.character_master', $params);
	}





}
