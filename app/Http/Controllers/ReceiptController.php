<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReceiptCheckRequest;
use App\Services\ReceiptService;

class ReceiptController extends Controller
{
	/**
	 * チェック
	 * @param ReceiptCheckRequest $request
	 * @return
	 */
	public function check(ReceiptCheckRequest $request)
	{
		// レシートをチェックし、正しければ、蒼の結晶を増やす
		$response = ReceiptService::check($request);
		return $response->toResponse();
	}
}
