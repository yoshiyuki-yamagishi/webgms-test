<?php

namespace App\Http\Controllers;

use App\Http\Responses\BattleEnemyListResponse;
use App\Models\MasterModels\QuestDictionary;
use App\Services\QuestService;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class SimulateController extends Controller
{
	/**
	 * GMS シミュレート
	 * @param Request $request
	 * @return
	 */


     public $dropItems = [];


    public function result(Request $request)
    {
        // if ($request->num == 0)
        // {
        //     throw \App\Exceptions\DataException::make(
        //         'num = 0'
        //     );
        // }


        $this->dropItems = [];

        $this->dropItems[] = [
            'name' => '金',
            'expected' => 0,
            'count' => 0,
        ];

        $this->dropItems[] = [
            'name' => '銀',
            'expected' => 0,
            'count' => 0,
        ];

        $this->dropItems[] = [
            'name' => '銅',
            'expected' => 0,
            'count' => 0,
        ];

        if ($request->selectQuest == 'story')
        {
            $questNameId = $request->story_quest_name_id;
            $questCategory = QuestService::QUEST_CATEGORY_STORY;
        }
        else if ($request->selectQuest == 'chara')
        {
            $questNameId = $request->character_quest_name_id;
            $questCategory = QuestService::QUEST_CATEGORY_CHARACTER;
        }
        else
        {
            // TODO: エラー処理
        }
        
        $pairId = explode(":", $questNameId);
        $chapterId = $pairId[0];
        $questId = $pairId[1];
        $questService = QuestService::make($questCategory);
        
        $quest = $questService->getQuest(
            $chapterId, 
            $questId
        );
        $battleList = $questService->getBattleByQuest($quest);
        $enemyCount = count($battleList);
        $questD = QuestDictionary::getOne($quest->quest_name_id);
        $this->_viewQuest = $quest->id.'：'.$questD->dictionary_ja;
        $this->num = '周回数：'.$request->num;

        foreach ($battleList as $battle)
        {
            $copperRate = $battle->copper_rate/1000;
            $silverRate =  $battle->silver_rate/1000;
            $goldRate =  $battle->gold_rate/1000;
            
            $this->dropItems[0]['expected'] += $goldRate;
            $this->dropItems[1]['expected'] += $silverRate;
            $this->dropItems[2]['expected'] += $copperRate;
        }

        for ($i = 0; $i < $request->num; ++ $i)
        {
            $_battleList = BattleEnemyListResponse::make($battleList);
            $dropItems = $questService->calcDropReward($quest, $_battleList);

            // DebugUtil::e_log('_battleList', '_battleList', $_battleList);
            // DebugUtil::e_log('dropItems', 'dropItems', $dropItems);

            foreach ($_battleList as $battle)
            {
                $type = $battle['drop_box_type'];
                if ($type < 1 || $type > 3)
                    continue;

                $this->dropItems[3 - $type]['count'] += 1;
            }
        }

        if ($request->num > 0)
        {
            for ($i = 0; $i < 3; ++ $i)
                $this->dropItems[$i]['count'] /= $request->num;
        }

    }

    
    public function index(Request $request)
    {

        // $questCategory = $request->quest_category;
        // $questService = QuestService::make($questCategory);
        $storyQuest = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
        $storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
        $characterQuest = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);
        $characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);

        $this->storyQuests = $storyQuestService->getAllQuests();
        $storyQuestNames = [];

        $this->characterQuests = $characterQuestService->getAllQuests();
        $characterQuestNames = [];
        $this->_viewQuest = '';
        $this->num = '';

        if (isset($request->action))
        {
            $this->result($request);
        }
        $num = $this->num;
        $viewQuest = $this->_viewQuest;
        foreach ($this->storyQuests as $storyQuest)
        {
            $qd = QuestDictionary::getOne($storyQuest->id);
            $key = $storyQuest->story_quest_chapter_id . ':' . $storyQuest->id;

            // $qd = QuestDictionary::getOne($storyQuest->id);
            if (isset($qd))
            {
                $name = $qd->dictionary_ja;
            }
            else
            {
                $name = 'クエスト名がありません: ' . $storyQuest->id;
            }
            
            $storyQuestNames[$key] = $name;
        }

        foreach ($this->characterQuests as $characterQuest)
        {
            $qd = QuestDictionary::getOne($characterQuest->id);
            $key = $characterQuest->character_quest_chapter_id . ':' . $characterQuest->id;

            // $characterQuestNames[$key] = $qd->dictionary_ja;
            if (isset($qd))
            {
                $name = $qd->dictionary_ja;
            }
            else
            {
                $name = 'クエスト名がありません: ' . $characterQuest->id;
            }
            
            $characterQuestNames[$key] = $name;
            // DebugUtil::e_log('characterQuestNames', 'characterQuestNames', $characterQuestNames);
        }
        
        // SDebugUtil::e_log('dropItems', 'dropItems', $this->dropItems);
        $params = [
            'dropItems' => $this->dropItems,
            'storyQuestNames' => $storyQuestNames,
            'characterQuestNames' => $characterQuestNames,
            'aveDropCounts' => '',
            'totalCopper' => '',
            'totalSilver' => '',
            'totalGold' => '',
            'enemyCount' => '',
            'dropCounts' => '',
            'viewQuest' => $viewQuest,
            'num' => $num,
        ];

        return view('simulate.simulate', $params);
        
    }
}
