<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FollowService;
use App\Services\FollowerService;
use App\Http\Requests\FollowListRequest;

class FollowController extends Controller
{
	/**
	 * 一覧
	 * @return string[]
	 */
	public function list(FollowListRequest $request)
	{
		$response = FollowService::list($request);
		return $response->toArray();
	}

	/**
	 * 追加
	 * @return string[]
	 */
	public function add()
	{
		$result = ["add" => "OK"];
		return $result;
	}

	/**
	 * 削除
	 * @return string[]
	 */
	public function delete()
	{
		$result = ["delete" => "OK"];
		return $result;
	}
}
