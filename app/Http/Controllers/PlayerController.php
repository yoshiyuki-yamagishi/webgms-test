<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerTransitDstRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerController extends Controller
{
	/**
	 * GMS プレイヤー一覧
	 * @param Request $request
	 * @return
	 */
    public function list(Request $request)
    {
        $players =
			Player::orderBy('id', 'asc')
            ->get();
        DebugUtil::e_log('hc', 'players', $players);
        # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('player.player_list', ['players' => $players]);

        $commons =
            PlayerCommon::orderBy('id', 'asc')
            ->get();
            # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('player.player_list', ['players' => $players]);
    }

    // GMS
    public function details(Request $request)
    {
        $players =
			Player::orderBy('id', 'asc')
            ->get();
        DebugUtil::e_log('hc', 'players', $players);
        # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('player.player_details', ['players' => $players]);

        $commons =
            PlayerCommon::orderBy('id', 'asc')
            ->get();
            # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('player.player_details', ['players' => $players]);
    }

    public function debug(Request $request)
    {
        $players =
			Player::orderBy('id', 'asc')
            ->get();
        DebugUtil::e_log('hc', 'players', $players);
        # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('debug.debug', ['players' => $players]);

        $commons =
            PlayerCommon::orderBy('id', 'asc')
            ->get();
            # data連想配列に代入&Viewファイルをlist.blade.phpに指定
        return view('debug.debug', ['players' => $players]);
    }

}
