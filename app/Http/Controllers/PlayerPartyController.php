<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerPartyListRequest;
use App\Http\Requests\PlayerPartyUpdateRequest;
use App\Services\PlayerPartyService;

class PlayerPartyController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerPartyListRequest $request
	 * @return
	 */
	public function list(PlayerPartyListRequest $request)
	{
		$response = PlayerPartyService::list($request);
		return $response->toResponse();
	}

	/**
	 * 更新
	 * @param PlayerPartyUpdateRequest $request
	 * @return
	 */
	public function update(PlayerPartyUpdateRequest $request)
	{
		$response = PlayerPartyService::update($request);
		return $response->toResponse();
	}
}
