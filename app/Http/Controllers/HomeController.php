<?php

namespace App\Http\Controllers;

use App\Http\Requests\HomeRequest;
use App\Services\HomeService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	/**
	 * ホーム
	 * @param HomeRequest $request
	 * @return
	 */
	public function index(HomeRequest $request)
	{
		$response = HomeService::index($request);
		return $response->toResponse();
	}
}
