<?php

namespace App\Http\Controllers;


use App\Models\Information;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class InfoRegisterController extends Controller
{
	/**
	 * GMS お知らせ管理
	 * @param Request $request
	 * @return
	 */


    public function modify(Request $request)
    {
        $startedAt = $request->started_at . ' ' . $request->started_at_hh . ':' .  $request->started_at_mm;
        $expiredAt = $request->started_at . ' ' . $request->expired_at_hh . ':' .  $request->expired_at_mm;

        $platformFlg = $request->platform1 + $request->platform2 + $request->platform3;

        if ($request->action == 'register')
        {
            Information::register(
                $request->infoType,
                $platformFlg,
                $request->title,
                $request->content,
                $request->imageName,
                $request->priority,
                $startedAt,
                $expiredAt
            );
        }
    }

    public function index(Request $request)
    {
        if (isset($request->action))
        {
            switch ($request->action){
                case "register":
                    $this->modify($request);
                    break;
                case "preview":
                    $param = [
                        'info' => $request
                    ];
                    return view('info.preview', $param);
                default:
                    break;
            }
        }

        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;

        $params = [
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
        ];

        return view('info.infoRegister', $params);

    }

}
