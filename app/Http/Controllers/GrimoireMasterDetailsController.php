<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterBattleInfo;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\CharacterFlavorDictionary;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;

use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\GmsServices\GmsGrimoireService;
use App\Services\PlayerItemService;
use App\Services\SessionService;

use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class GrimoireMasterDetailsController extends Controller
{
	/**
	 * BBDW_MasterData_grimoire.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('dc', 'request', $request->all());
		$grimoireId = $request->id;
		$grimoireName = $request->grimoire_name;
		$grimoire = $grimoireId.'：'.$grimoireName;

		if (!isset($awake))
		{
			$awake = 0;
		}

		$maxAwake = GmsGrimoireService::maxGrimoireAwake();

		$grimoireParams = [];
		for ($i = 0; $i <= $maxAwake; ++ $i)
		{
			$grimoireParams[] = GmsGrimoireService::calcGrimoireParam($grimoireId, $i);
		}
		DebugUtil::e_log('grimoireParams', 'grimoireParams', $grimoireParams);

		$params = [
			'grimoireParams' => $grimoireParams,
			'grimoireId' => $grimoireId,
			'grimoire' => $grimoire
		];

		return view('master_check.grimoire_detail_master', $params);
	}





}
