<?php

namespace App\Http\Controllers;

use App\Models\PlayerItemBuy;

use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerGrimoireService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerItemBuyLogController extends Controller
{
	
	public function index(Request $request)
	{
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$itemBuyLogs = PlayerItemBuy::logGetByPlayerId($playerId);
		// DebugUtil::e_log('itemBuyLogs', 'itemBuyLogs', $itemBuyLogs);

		foreach ($itemBuyLogs as $itemBuyLog)
		{
			if ($itemBuyLog->shop_category == 1)
			{
				$itemBuyLog->shop_name = 'P$ショップ';
			}
			elseif ($itemBuyLog->shop_category == 2)
			{
				$itemBuyLog->shop_name = '欠片交換所';
			}
			elseif ($itemBuyLog->shop_category == 3)
			{
				$itemBuyLog->shop_name = '魔道書交換所';
			}
			elseif ($itemBuyLog->shop_category == 4)
			{
				$itemBuyLog->shop_name = 'イベント交換所';
			}
			elseif ($itemBuyLog->shop_category == 5)
			{
				$itemBuyLog->shop_name = '蒼の結晶ショップ';
			}
			elseif ($itemBuyLog->shop_category == 6)
			{
				$itemBuyLog->shop_name = '有償通貨購入';
			}

			$item = Item::getOne($itemBuyLog->item_id);
			$itemD = ItemDictionary::getOne($item->item_name_id);
			$itemBuyLog->item_name = $itemD->dictionary_ja;

			$useItem = Item::getOne($itemBuyLog->pay_item_contents_id);
			$useItemD = ItemDictionary::getOne($useItem->item_name_id);
			$itemBuyLog->use_item_name = $useItemD->dictionary_ja;
		}
		
		DebugUtil::e_log('itemBuyLogs', 'itemBuyLogs', $itemBuyLogs);

		$params = [
            'itemBuyLogs' => $itemBuyLogs,
        ];

		return view('player_log.item_buy_log', $params);

	}



	
}
