<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterBattleInfo;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\CharacterFlavorDictionary;
use App\Models\MasterModels\Enemy;
use App\Models\MasterModels\ResistAbnormal;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;
use App\Http\Requests\PlayerItemListRequest;
use App\Http\Requests\PlayerItemSellRequest;
use App\GmsServices\GmsCharacterService;
use App\Services\PlayerItemService;
use App\Services\SessionService;
use App\Services\QuestService;

use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class QuestMasterDetailsController extends Controller
{
	/**
	 * BBDW_MasterData_character.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		$storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
		$characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);

		DebugUtil::e_log('dc', 'request', $request->all());
		$battleId = $request->battle_id;
		$questName = $request->quest_name;
		$questId = $request->id;

		if ($request->type == 1)
		{
			$battles = $storyQuestService->getBattle($battleId);
		}
		elseif ($request->type == 2)
		{
			$battles = $characterQuestService->getBattle($battleId);
		}


		foreach ($battles as $battle)
		{
			$battle->battle_enemy_id = 'ID：'.$battle->enemy_id;
			$enemy = Enemy::getOne($battle->enemy_id);
			DebugUtil::e_log('enemy', 'enemy', $enemy);
			$enemyD = CharacterDictionary::getOne($enemy->enemy_name_id);
			$battle->enemy_name = $enemyD->dictionary_ja;
			$battle->hp_coefficient = 'HP：'.$battle->hp_coefficient;
			$battle->atk_coefficient = 'ATK：'.$battle->atk_coefficient;
			$battle->def_coefficient = 'DEF：'.$battle->def_coefficient;

			$goldRate = $battle->gold_rate/1000*100;
			$battle->gold_rate = '金：'.$goldRate.'%';

			$silverRate = $battle->silver_rate/1000*100;
			$battle->silver_rate = '銀：'.$silverRate.'%';

			$copperRate = $battle->copper_rate/1000*100;
			$battle->copper_rate = '銅：'.$copperRate.'%';

			// 状態異常耐性
			$resistAbnormal = ResistAbnormal::getOne($battle->resist_abnormal_id);
			DebugUtil::e_log('resistAbnormal', 'resistAbnormal', $resistAbnormal);
			$battle->poison = '毒：'.$resistAbnormal->resist_poison;
			$battle->combustion = '燃焼：'.$resistAbnormal->resist_combustion;
			$battle->laceration = '裂傷：'.$resistAbnormal->resist_laceration;
			$battle->swoon = '気絶：'.$resistAbnormal->resist_swoon;
			$battle->restraint = '拘束：'.$resistAbnormal->resist_restraint;
			$battle->frieze = '凍結：'.$resistAbnormal->resist_frieze;
			$battle->electric_shock = '感電：'.$resistAbnormal->resist_electric_shock;
			$battle->negative = 'ネガティブ：'.$resistAbnormal->resist_negative;

			if ($battle->position == 1)
			{
				$battle->position_name = '前衛上';
			}
			elseif ($battle->position == 2)
			{
				$battle->position_name = '前衛中央';
			}
			elseif ($battle->position == 3)
			{
				$battle->position_name = '前衛下';
			}
			elseif ($battle->position == 4)
			{
				$battle->position_name = '後衛上';
			}
			elseif ($battle->position == 5)
			{
				$battle->position_name = '後衛中央';
			}
			elseif ($battle->position == 6)
			{
				$battle->position_name = '後衛下';
			}
			else
			{
				$battle->position_name = '不明';
			}

			if ($battle->active1 == -1)
			{
				$battle->active1_id = '';
				$battle->active1_name = 'スキル１：なし';
			}
			else
			{
				$active1 = $battle->active1;
				$battle->active1_id = 'ID：'.$active1;
				$skill1 = Skill::getOne($battle->active1);
				$skill1D = SkillDictionary::getOne($skill1->skill_description_id);
				$battle->active1_name = 'スキル１：'.$skill1D->dictionary_ja;
			}

			if ($battle->active2 == -1)
			{
				$battle->active2_id = '';
				$battle->active2_name = 'スキル２：なし';
			}
			else
			{
				$active2 = $battle->active2;
				$battle->active2_id = 'ID：'.$active2;
				$skill2 = Skill::getOne($battle->active2);
				$skill2D = SkillDictionary::getOne($skill2->skill_description_id);
				$battle->active2_name = 'スキル２：'.$skill2D->dictionary_ja;
			}

			if ($battle->element == 1)
			{
				$battle->element_type = '火';
			}
			elseif ($battle->element == 2)
			{
				$battle->element_type = '水';
			}
			elseif ($battle->element == 3)
			{
				$battle->element_type = '土';
			}
			elseif ($battle->element == 4)
			{
				$battle->element_type = '風';
			}
			elseif ($battle->element == 5)
			{
				$battle->element_type = '光';
			}
			elseif ($battle->element == 6)
			{
				$battle->element_type = '闇';
			}
			else
			{
				$battle->element_type = '不明';
			}
		}



		// $characterId = $request->id;
		// $characterName = $request->character_name;
		// $character = $characterId.'：'.$characterName;


		// DebugUtil::e_log('characterParam', 'characterParam', $characterParam);
		DebugUtil::e_log('battles', 'battles', $battles);

		$params = [
			'questName' => $questName,
			'battles' => $battles,

		];

		return view('master_check.quest_detail_master', $params);
	}





}
