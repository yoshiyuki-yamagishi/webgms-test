<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerOrbListRequest;
use App\Services\PlayerOrbService;

class PlayerOrbController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerOrbListRequest $request
	 * @return
	 */
	public function list(PlayerOrbListRequest $request)
	{
		$response = PlayerOrbService::list($request);
		return $response->toResponse();
	}
}
