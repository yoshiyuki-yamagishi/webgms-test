<?php

namespace App\Http\Controllers;

use App\Models\PlayerItemLog;

use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;

use App\Http\Requests\PlayerGrimoireLimitUpRequest;
use App\Http\Requests\PlayerGrimoireListRequest;
use App\Http\Requests\PlayerGrimoireReinforceRequest;
use App\Http\Requests\PlayerGrimoireSellRequest;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerGrimoireService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerItemLogController extends Controller
{
	
	public function index(Request $request)
	{
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$itemLogs = PlayerItemLog::getByPlayerId($playerId);
		

		foreach ($itemLogs as $itemLog)
		{
			if ($itemLog->type == 1)
			{
				$itemLog->_type = '取得';
			}
			elseif ($itemLog->type == 2)
			{
				$itemLog->_type = '消費';
			}
			$item = Item::getOne($itemLog->value3);
			$itemD = ItemDictionary::getOne($item->item_name_id);
			DebugUtil::e_log('itemD', 'itemD', $itemD);
			$itemLog->item_name = $itemD->dictionary_ja;
		}
		
		DebugUtil::e_log('itemLogs', 'itemLogs', $itemLogs);

		$params = [
            'itemLogs' => $itemLogs,
        ];

		return view('player_log.player_item_log', $params);

	}



	
}
