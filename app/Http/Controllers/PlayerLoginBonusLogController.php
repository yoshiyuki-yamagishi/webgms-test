<?php

namespace App\Http\Controllers;

use App\Models\PlayerCharacterLog;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Models\PlayerLog;
use App\Models\PlayerLoginBonus;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\LoginBonusDictionary;
use App\Models\MasterModels\LoginBonus;
use App\Services\SessionService;
use App\Utils\DebugUtil;

class PlayerLoginBonusLogController extends Controller
{
	
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$playerLoginBonusLogs = PlayerLoginBonus::getByPlayerId($playerId);
		
		$loginBonusNames = [];
        $prevLoginId = 0;
        $prevLoginCnt = 0;
		
		foreach ($playerLoginBonusLogs as $playerLoginBonusLog)
		{
			$loginBonus = LoginBonus::getOne($playerLoginBonusLog->login_bonus_id);
			$loginBonusD = LoginBonusDictionary::getOne($loginBonus->login_bonus_description);
			DebugUtil::e_log('loginBonusD', 'loginBonusD', $loginBonusD);
			$playerLoginBonusLog->loginBonus_name = $loginBonusD->dictionary_ja;
			
			$item = Item::getOne($playerLoginBonusLog->remuneration);
			$itemD = ItemDictionary::getOne($item->item_name_id);
			$playerLoginBonusLog->item_name = $itemD->dictionary_ja;
			DebugUtil::e_log('playerLoginBonusLog', 'playerLoginBonusLog', $playerLoginBonusLog);
		
		}
		DebugUtil::e_log('playerLoginBonusLogs', 'playerLoginBonusLogs', $playerLoginBonusLogs);
		// DebugUtil::e_log('loginBonusNames', 'loginBonusNames', $loginBonusNames);
		$params = [
			// 'loginBonusNames' => $loginBonusNames,
			'playerLoginBonusLogs' => $playerLoginBonusLogs,
        ];

		return view('player_log.player_login_bonus_log', $params);

	}

}