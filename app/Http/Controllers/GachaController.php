<?php

namespace App\Http\Controllers;

use App\Http\Requests\GachaListRequest;
use App\Http\Requests\GachaExecRequest;
use App\Http\Requests\GachaRetryRequest;
use App\Services\GachaService;



class GachaController extends Controller
{
	/**
	 * 実行
	 * @param GachaListRequest $request
	 * @return
	 */
	public function list(GachaListRequest $request)
	{
		$response = GachaService::list($request);
		return $response->toResponse();
	}

	/**
	 * 実行
	 * @param GachaExecRequest $request
	 * @return
	 */
	public function exec(GachaExecRequest $request)
	{
		$response = GachaService::exec($request);
		return $response->toResponse();
	}

	/**
	 * 引き直し
	 * @param GachaRetryRequest $request
	 * @return
	 */
	public function retry(GachaRetryRequest $request)
	{
		$response = GachaService::retry($request);
		return $response->toResponse();
	}
}
