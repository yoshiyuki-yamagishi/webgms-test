<?php

namespace App\Http\Controllers;

use App\Http\Requests\FollowerListRequest;
use App\Services\FollowerService;
use Illuminate\Http\Request;

class FollowerController extends Controller
{
	/**
	 * 一覧
	 * @return string[]
	 */
	public function list(FollowerListRequest $request)
	{
		$response = FollowerService::list($request);
		return $response->toArray();
	}

	/**
	 * 削除
	 * @return string[]
	 */
	public function delete()
	{
		$result = ["delete" => "OK"];
		return $result;
	}
}
