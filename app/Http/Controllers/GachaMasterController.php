<?php

namespace App\Http\Controllers;

use App\Http\Responses\BattleEnemyListResponse;
use App\Models\MasterModels\GachaDictionary;
use App\Models\MasterModels\Gacha;
use App\Models\MasterModels\GachaLot;
use App\Models\MasterModels\GachaGroup;
use App\Services\GachaService;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class GachaMasterController extends Controller
{
	/**
	 * GMS ガチャマスター
	 * @param Request $request
	 * @return
	 */
    
    public function index(Request $request)
    {
        SessionService::start(0, SessionService::SS_GMS);
        DebugUtil::e_log('dc', 'request', $request->all());
        $requestGachaType = $request->gacha_type;
        $_gachaList = Gacha::getAllRaw();

        $gachaTypes = [];
        $gachaSearch = [];

        foreach ($_gachaList as $gacha)
        {
            $gachaName = GachaDictionary::getOne($gacha->gacha_name_id);
            $gacha->gacha_name = $gachaName->dictionary_ja;
            $gacha->release_day = '開始日：'.$gacha->release_day;
            $gacha->end_day = '終了日：'.$gacha->end_day;
            $gacha->gacha_type_convert = '種別：'.$gacha->gacha_type;
            $key = $gacha->gacha_type;
            $gachaTypes[$key] = true;
        }


        // ガチャ種別ソート
        if ($requestGachaType > 0)
        {
            foreach ($_gachaList as $_gacha)
            {
                if ($_gacha->gacha_type == $requestGachaType)
                {
                    $gachaSearch[] = $_gacha;
                }
            }
            $gachaList = $gachaSearch;
        }
        else
        {
            $gachaList = $_gachaList;
        }

        // ガチャ種別の数だけドロップダウンの選択肢追加
        $i = 0;
        $type = [];
        $type[0] = '全て';
        foreach ($gachaTypes as $gachaType)
        {
            $i = ++ $i;
            DebugUtil::e_log('gachaType', 'gachaType', $gachaType);
            $type[$i] = $i;
        }

        $params = [
            'gachaList' => $gachaList,
            'type' => $type,

        ];

        return view('master_check.gacha_master', $params);
        
    }
}
