<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\PlayerBattle;
use App\Models\PlayerBlueCrystal;
use App\Models\PlayerCharacter;
use App\Models\PlayerCommon;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\PlayerParty;
use App\Models\PlayerPresent;

use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Models\MasterModels\CharacterQuestChapter;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireDictionary;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\LoginBonusDictionary;
use App\Models\MasterModels\LoginBonusReward;
use App\Models\MasterModels\Skill;
use App\Models\MasterModels\SkillDictionary;
use App\Models\MasterModels\StoryQuestChapter;

use App\Models\MasterModels\QuestDictionary;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Services\PlayerService;
use App\Services\SessionService;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\Auth;

class PlayerDetailsController extends Controller
{
	/**
	 * GMS プレイヤー詳細
	 * @param Request $request
	 * @return
	 */

    public $player = null;

    public function index(Request $request)
    {
        SessionService::start(0, SessionService::SS_GMS);
        $now = DateTimeUtil::getNOW();

        $playerId = $request->player_id;

        $playerCommon = PlayerCommon::getByPlayerId($playerId);
        SessionService::setDbNo($playerCommon->db_no);

        $_playerBattle = PlayerBattle::where('player_id', $playerId)
                                        ->orderBy('created_at', 'desk')
                                        ->first();

        $player = Player::find($playerId);
        $player ['player_disp_id'] = $playerCommon->player_disp_id;
        $player ['passcode'] = $playerCommon->passcode;
        $player ['password'] = $playerCommon->password;
        $player ['unique_id'] = $playerCommon->unique_id;
        $player ['free_crystal'] = PlayerBlueCrystal::getFreeNum($playerId, $now);
        $player ['charged_crystal'] = PlayerBlueCrystal::getChargedNum($playerId, $now);
        $player ['last_play_stage'] = $_playerBattle['quest_id'];


        // キャラクター
        $playerCharacters = PlayerCharacter::getByPlayerId($playerId);

        $viewPlayerCharacters = [];
        foreach ($playerCharacters as $playerCharacter)
        {
            $character = CharacterBase::getOne($playerCharacter->character_id);
            $charaNameD = CharacterDictionary::getOne($character->character_name_id);

            $rarity = $character->character_initial_rarity + $playerCharacter->grade;
            $as1Id = $character->active1;
            $as2Id = $character->active2;
            $as1 = Skill::getOne($as1Id);
            $as2 = Skill::getOne($as2Id);
            $skill1NameD = SkillDictionary::getOne($as1->skill_name_id);
            $skill2NameD = SkillDictionary::getOne($as2->skill_name_id);
            $playerCharacter['as1Name'] = $skill1NameD->dictionary_ja;
            $playerCharacter['as2Name'] = $skill2NameD->dictionary_ja;

            if ($rarity == 1)
                $playerCharacter['rarity'] = 'A2';
            elseif
                ($rarity == 2)
                    $playerCharacter['rarity'] = 'A1';
            elseif
                ($rarity == 3)
                    $playerCharacter['rarity'] = 'S';
            elseif
                ($rarity == 4)
                    $playerCharacter['rarity'] = 'SS';
            elseif
                ($rarity == 5)
                    $playerCharacter['rarity'] = 'SS+';
            elseif
                ($rarity == 6)
                    $playerCharacter['rarity'] = 'SS++';
            elseif
                ($rarity == 7)
                    $playerCharacter['rarity'] = 'SS+++';
            else
                $playerCharacter['rarity'] = 'その他';


            if ($character->element == 1)
                    $playerCharacter['element'] = '火';
            elseif
                ($character->element == 2)
                    $playerCharacter['element'] = '水';
            elseif
                ($character->element == 3)
                    $playerCharacter['element'] = '土';
            elseif
                ($character->element == 4)
                    $playerCharacter['element'] = '風';
            elseif
                ($character->element == 5)
                    $playerCharacter['element'] = '光';
            elseif
                ($character->element == 6)
                    $playerCharacter['element'] = '闇';
            else
                $playerCharacter['element'] = 'その他の属性';

            $playerCharacter['name'] = $charaNameD->dictionary_ja;
            $viewPlayerCharacters [] =  $playerCharacter;
        }


        // 魔導書
        $playerGrimoires = PlayerGrimoire::getByPlayerId($playerId);
        $viewPlayerGrimoires = [];

        foreach ($playerGrimoires as $playerGrimoire)
        {
            $grimoire = Grimoire::getOne($playerGrimoire->grimoire_id);
            $grimoireNameD = GrimoireDictionary::getOne($grimoire->grimoire_name_id);
            $playerGrimoire['name'] = $grimoireNameD->dictionary_ja;
            $playerGrimoire['rarity'] = '★'. $grimoire->grimoire_initial_rarity;

            $ps1Id = $grimoire->passive_id_1;
            $ps2Id = $grimoire->passive_id_2;
            $ps1 = Skill::getOne($ps1Id);
            $ps2 = Skill::getOne($ps2Id);
            // スキル名
            $pSkill1NameD = SkillDictionary::getOne($ps1->skill_name_id);
            $pSkill2NameD = SkillDictionary::getOne($ps2->skill_name_id);
            $playerGrimoire['ps1Name'] = $pSkill1NameD->dictionary_ja;
            $playerGrimoire['ps2Name'] = $pSkill2NameD->dictionary_ja;
            // スキル説明文
            $pSkill1DetailD = SkillDictionary::getOne($ps1->skill_description_id);
            $pSkill2DetailD = SkillDictionary::getOne($ps2->skill_description_id);
            $playerGrimoire['ps1Detail'] = $pSkill1DetailD->dictionary_ja;
            $playerGrimoire['ps2Detail'] = $pSkill2DetailD->dictionary_ja;

            $viewPlayerGrimoires[] =  $playerGrimoire;
        }
        // if (empty($viewPlayerGrimoires))
        // {
        //     $playerGrimoire['player_id'] = $playerId;
        //     $playerGrimoire['acquired_at'] = '--';
        //     $playerGrimoire['grimoire_id'] = '--';
        //     $playerGrimoire['name'] = '--';
        //     $playerGrimoire['rarity'] = '--';
        //     $playerGrimoire['awake'] = '--';
        //     $playerGrimoire['ps1Name'] = '--';
        //     $playerGrimoire['ps1Detail'] = '--';
        //     $playerGrimoire['ps2Name'] = '--';
        //     $playerGrimoire['ps2Detail'] = '--';
        //     $viewPlayerGrimoires [] =  (object)$playerGrimoire;
        // }
        // DebugUtil::e_log('viewPlayerGrimoires', 'viewPlayerGrimoires', $viewPlayerGrimoires);

        // パーティー編成
        $playerPartys = PlayerParty::getByPlayerId($playerId);

        foreach ($playerPartys as $playerParty)
        {
            for ($i = 1; $i <= 6; ++ $i)
            {
                $idFunc = 'player_character_id_' . $i;
                $nameFunc = 'player_party_name_' . $i;

                if (isset($playerParty->$idFunc))
                {
                    $playerCharacter = PlayerCharacter::find($playerParty->$idFunc);
                    $partyCharacter = CharacterBase::getOne($playerCharacter->character_id);
                    $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
                    $playerParty[$nameFunc] = $partyCharaNameD->dictionary_ja;
                }
            }

            // if (isset($playerParty->player_character_id_1))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_1);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_1'] = $partyCharaNameD->dictionary_ja;
            // }

            // if (isset($playerParty->player_character_id_2))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_2);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_2'] = $partyCharaNameD->dictionary_ja;;
            // }

            // if (isset($playerParty->player_character_id_3))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_3);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_3'] = $partyCharaNameD->dictionary_ja;
            // }

            // if (isset($playerParty->player_character_id_4))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_4);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_4'] = $partyCharaNameD->dictionary_ja;
            // }

            // if (isset($playerParty->player_character_id_5))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_5);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_5'] = $partyCharaNameD->dictionary_ja;
            // }

            // if (isset($playerParty->player_character_id_6))
            // {
            //     $partyCharacter = CharacterBase::getOne($playerParty->player_character_id_6);
            //     $partyCharaNameD = CharacterDictionary::getOne($partyCharacter->character_name_id);
            //     $playerParty['player_party_name_6'] = $partyCharaNameD->dictionary_ja;
            // }
        }


        // アイテム種別分け
        $playerItems = PlayerItem::getByPlayerId($playerId);
        $mItems = Item::getall();
        $pOrbs = [];
        $pFragments = [];
        $pItems = [];

        $itemNames = [];
        $orbNames = [];
        $fragmentNames = [];

        foreach ($playerItems as $playerItem)
        {
            $pItemCat = intdiv($playerItem->item_id, 1000000);

            $item = Item::getOne($playerItem->item_id);
            if (empty($item))
            {
                $playerItem['name'] = '存在しないアイテム: ' . $playerItem->item_id;
                $playerItem['detail'] = '存在しないアイテム';
            }
            else
            {
                $itemNameD = ItemDictionary::getOne($item->item_name_id);
                $itemDetailD = ItemDictionary::getOne($item->item_detail_id);
                $playerItem['name'] = $itemNameD->dictionary_ja;
                $playerItem['detail'] = $itemDetailD->dictionary_ja;
            }

            // オーブ
            if($pItemCat == 4)
            {
                $playerItem['category'] = 'オーブ';
                $pOrbs[] = $playerItem;
            }
            // 欠片
            elseif ($pItemCat == 6)
            {
                $playerItem['category'] = 'キャラクター欠片';
                $pFragments[] = $playerItem;
            }
            // アイテム
            elseif ($pItemCat == 2 || $pItemCat == 3 || $pItemCat == 5 || $pItemCat == 7)
            {
                $category = Item::categoryFromId($playerItem->item_id);
                if ($category == 2)
                    $category = 'ガチャチケット';
                elseif
                    ($category == 3)
                    $category = '強化アイテム';
                elseif
                    ($category == 5)
                    $category = 'スキップチケット';
                elseif
                    ($category == 7)
                    $category = '回復アイテム';
                $playerItem['category'] = $category;
                $pItems[] = $playerItem;
            }
        }


        // プレゼント
        $playerPresents = PlayerPresent::get($playerId, $now);

        foreach ($playerPresents as $playerPresent)
        {
            if ($playerPresent->src_type == 1)
                $playerPresent->src_type = 'ログインボーナス';
            elseif ($playerPresent->src_type == 2)
                $playerPresent->src_type = 'クエスト汎用';
            elseif ($playerPresent->src_type == 3)
                $playerPresent->src_type = 'クエスト初回';
            elseif ($playerPresent->src_type == 4)
                $playerPresent->src_type = 'クエスト固定';
            elseif ($playerPresent->src_type == 5)
                $playerPresent->src_type = 'クエストドロップ';
            elseif ($playerPresent->src_type == 6)
                $playerPresent->src_type = 'クエストミッション';
            elseif ($playerPresent->src_type == 7)
                $playerPresent->src_type = 'ミッション';
            elseif ($playerPresent->src_type == 8)
                $playerPresent->src_type = 'ガチャ';
            elseif ($playerPresent->src_type == 9)
                $playerPresent->src_type = 'ショップ購入';
            elseif ($playerPresent->src_type == 10)
                $playerPresent->src_type = 'リアルマネー購入';
            elseif ($playerPresent->src_type == 11)
                $playerPresent->src_type = 'デイリーパック付与';
            elseif ($playerPresent->src_type == 12)
                $playerPresent->src_type = 'プレゼントボックス';
            elseif ($playerPresent->src_type == 13)
                $playerPresent->src_type = '初回設定';
            elseif ($playerPresent->src_type == 14)
                $playerPresent->src_type = '管理ツール付与';
            elseif ($playerPresent->src_type == 15)
                $playerPresent->src_type = 'ログインポイント';

            if ($playerPresent->item_type == 1)
            {
                $playerCharacter = CharacterBase::getOne($playerPresent->item_id);
                $presentCharaNameD = CharacterDictionary::getOne($playerCharacter->character_name_id);
                $playerPresent['name'] = $presentCharaNameD->dictionary_ja;
            }
            elseif ($playerPresent->item_type == 2)
            {
                $playerGrimoire = Grimoire::getOne($playerPresent->item_id);
                $presentGrimoireNameD = GrimoireDictionary::getOne($grimoire->grimoire_name_id);
                $playerPresent['name'] = $presentGrimoireNameD->dictionary_ja;
            }
            elseif ($playerPresent->item_type == 3)
            {
                $presentItem = Item::getOne($playerPresent->item_id);
                $presentItemNameD = ItemDictionary::getOne($presentItem->item_name_id);
                $playerPresent['name'] = $presentItemNameD->dictionary_ja;
            }



        }

        // DebugUtil::e_log('playerPresents', 'playerPresents', $playerPresents);
        // DebugUtil::e_log('pItems', 'pItems', $pItems);
        // DebugUtil::e_log('pFragments', 'pFragments', $pFragments);
        // DebugUtil::e_log('pOrbs', 'pOrbs', $pOrbs);

        $loginUserData = Auth::User();
        $authLevel = $loginUserData->manage_user_auth;
        $params = [
            'player' => $player,
            'pOrbs'=> $pOrbs,
            'pFragments'=> $pFragments,
            'pItems'=> $pItems,
            'viewPlayerCharacters' => $viewPlayerCharacters,
            'viewPlayerGrimoires' => $viewPlayerGrimoires,
            'playerPartys' =>$playerPartys,
            'playerPresents' => $playerPresents,
            'loginUserData' => $loginUserData,
            'authLevel' => $authLevel,
        ];
        return view('browse.player', $params);
    }
}
