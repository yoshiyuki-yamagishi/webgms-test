<?php

namespace App\Http\Controllers;

use App\Http\Requests\BattleStartRequest;
use App\Http\Requests\BattleEndRequest;
use App\Http\Requests\BattleContinueRequest;
use App\Http\Requests\BattleSkipRequest;
use App\Http\Requests\BattleInterruptRequest;
use App\Services\BattleService;


class BattleController extends Controller
{
	/**
	 * 開始
	 * @param BattleStartRequest $request
	 * @return
	 */
	public function start(BattleStartRequest $request)
	{
		$response = BattleService::start($request);
		return $response->toResponse();
	}

	/**
	 * 終了
	 * @param BattleEndRequest $request
	 * @return
	 */
	public function end(BattleEndRequest $request)
	{
		$response = BattleService::end($request);
		return $response->toResponse();
	}

	/**
	 * コンティニュー
	 * @param BattleContinueRequest $request
	 * @return
	 */
	public function continue(BattleContinueRequest $request)
	{
		$response = BattleService::continue($request);
		return $response->toResponse();
	}

	/**
	 * スキップ
	 * @param BattleSkipRequest $request
	 * @return
	 */
	public function skip(BattleSkipRequest $request)
	{
		$response = BattleService::skip($request);
		return $response->toResponse();
	}

	/**
	 * 中断
	 * @param BattleInterruptRequest $request
	 * @return
	 */
	public function interrupt(BattleInterruptRequest $request)
	{
		$response = BattleService::interrupt($request);
		return $response->toResponse();
	}
}
