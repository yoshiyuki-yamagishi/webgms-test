<?php

namespace App\Http\Controllers;

use App\Models\MasterModels\DropReward;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\ItemDictionary;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\MissionDictionary;
use App\Models\MasterModels\StoryQuestChapter;
use App\Models\MasterModels\CharacterQuestChapter;
use App\Models\MasterModels\QuestDictionary;
use App\Models\MasterModels\QuestReward;

use App\GmsServices\GmsQuestService;
use App\Services\QuestService;

use App\Services\SessionService;
use App\Utils\DebugUtil;
use Illuminate\Http\Request;

class RewardMasterController extends Controller
{
	/**
	 * BBDW_MasterData_story_quest.xlsm チェック
	 * BBDW_MasterData_character_quest.xlsm チェック
	 * @param Request $request
	 * @return
	 */
    public function index(Request $request)
	{

		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('dc', 'request', $request->all());
		$storyQuestService = QuestService::make(QuestService::QUEST_CATEGORY_STORY);
		$characterQuestService = QuestService::make(QuestService::QUEST_CATEGORY_CHARACTER);
		
		$storyQuests = $storyQuestService->getAllQuests();
		$characterQuests = $characterQuestService->getAllQuests();
		$reward = [];
		if ($request->selectQuest == 'story')
        {
            $questNameId = $request->story_quest_name_id;
			$questCategory = QuestService::QUEST_CATEGORY_STORY;
			
			$pairId = explode(":", $questNameId);
			$chapterId = $pairId[0];
			$questId = $pairId[1];
	
			DebugUtil::e_log('chapterId', 'chapterId', $chapterId);
			DebugUtil::e_log('questId', 'questId', $questId);
	
			$searchQuest = $storyQuestService->getQuest($chapterId, $questId);
	
			DebugUtil::e_log('searchQuest', 'searchQuest', $searchQuest);
        }
        else if ($request->selectQuest == 'chara')
        {
            $questNameId = $request->character_quest_name_id;
			$questCategory = QuestService::QUEST_CATEGORY_CHARACTER;
			
			$pairId = explode(":", $questNameId);
			$chapterId = $pairId[0];
			$questId = $pairId[1];
	
			DebugUtil::e_log('chapterId', 'chapterId', $chapterId);
			DebugUtil::e_log('questId', 'questId', $questId);
	
			$searchQuest = $characterQuestService->getQuest($chapterId, $questId);
	
			DebugUtil::e_log('searchQuest', 'searchQuest', $searchQuest);
		}

		if (isset($request->selectQuest))
		{

			$firstRewards = QuestReward::getAll($searchQuest->first_reward_id);
			$firstRewardItem = [];
			$fixRewardItem = [];
			foreach ($firstRewards as $firstReward)
			{
				$firstReward->type = '初クリア';
				$itemCategory = Item::categoryFromId($firstReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);

				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($firstReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					
					$firstReward->first_reward_item_name = $itemD->dictionary_ja;;
					$firstReward->first_reward_item_num = $firstReward->quest_reward_count;
					// $storyQuest->first_reward_item_name = $itemD->dictionary_ja;
					// $storyQuest->first_reward_item_num = '個数：'.$firstReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($firstReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$firstReward->first_reward_item_name = $itemD->dictionary_ja;
					$firstReward->first_reward_item_num = $firstReward->quest_reward_count;
				}
			}
			DebugUtil::e_log('firstRewards', 'firstRewards', $firstRewards);

			
			$fixRewards = QuestReward::getAll($searchQuest->fix_reward_id);
			foreach ($fixRewards as $fixReward)
			{
				$fixReward->type = '固定';
				$itemCategory = Item::categoryFromId($fixReward->quest_reward_item_id);
				DebugUtil::e_log('itemCategory', 'itemCategory', $itemCategory);

				if ($itemCategory == 1 || $itemCategory == 3)
				{
					$item = Item::getOne($fixReward->quest_reward_item_id);
					$itemD = ItemDictionary::getOne($item->item_name_id);
					$fixReward->fix_reward_item_name = $itemD->dictionary_ja;
					$fixReward->fix_reward_item_num = $fixReward->quest_reward_count;
				}
				elseif ($itemCategory == 2)
				{
					$item = Grimoire::getOne($fixReward->quest_reward_item_id);
					$itemD = GrimoireDictionary::getOne($item->grimoire_name_id);
					$fixReward->fix_reward_item_name = $itemD->dictionary_ja;
					$fixReward->fix_reward_item_num = $fixReward->quest_reward_count;
				}
			}

			DebugUtil::e_log('fixRewards', 'fixRewards', $fixRewards);


			$dropRewards = DropReward::getById($searchQuest->drop_reward_id);
			

			foreach ($dropRewards as $dropReward)
			{
				$dropReward->type = 'ドロップ';
				$itemCategory = Item::categoryFromId($dropReward->drop_item_id);

				if ($itemCategory != 2)
				{
					$dropItem = Item::getOne($dropReward->drop_item_id);
					$dropItemD = ItemDictionary::getOne($dropItem->item_name_id);
					$dropReward->drop_reward_item_name = $dropItemD->dictionary_ja;
					$dropReward->drop_reward_item_num = $dropReward->drop_item_count;
				}
				elseif ($itemCategory == 2)
				{
					$dropItem = Grimoire::getOne($dropReward->drop_item_id);
					$dropItemD = GrimoireDictionary::getOne($dropItem->grimoire_name_id);
					$dropReward->drop_reward_item_name = $dropItemD->dictionary_ja;
					$dropReward->drop_reward_item_num = $dropReward->drop_item_count;
				}
			}
			DebugUtil::e_log('dropRewards', 'dropRewards', $dropRewards);



			$i = 0;

			foreach ($firstRewards as $firstReward)
			{
				++ $i;
				DebugUtil::e_log('firstReward', 'firstReward', $firstReward);

				$reward[$i]['type'] = $firstReward->type;
				$reward[$i]['id'] = 'ID：'.$firstReward->quest_reward_item_id;
				$reward[$i]['item_id'] = $firstReward->quest_reward_item_id;
				$reward[$i]['item_name'] = $firstReward->first_reward_item_name;
				$reward[$i]['count'] = $firstReward->first_reward_item_num;
				// $reward->item_id = $firstReward->quest_reward_item_id;
				// $reward->item_name = $firstReward->first_reward_item_name;
				// $reward->count = $firstReward->first_reward_item_num;
			}
			foreach ($fixRewards as $fixReward)
			{
				// ++ $i;
				++ $i;
				$reward[$i]['type'] = $fixReward->type;
				$reward[$i]['id'] = 'ID：'.$fixReward->quest_reward_item_id;
				$reward[$i]['item_id'] = $fixReward->quest_reward_item_id;
				$reward[$i]['item_name'] = $fixReward->fix_reward_item_name;
				$reward[$i]['count'] = $fixReward->fix_reward_item_num;
			// 	$reward->type = $fixReward->type;
			// 	$reward->item_id = $fixReward->quest_reward_item_id;
			// 	$reward->item_name = $fixReward->fix_reward_item_name;
			// 	$reward->count = $fixReward->fix_reward_item_num;
			}
			foreach ($dropRewards as $dropReward)
			{
				++ $i;
				$reward[$i]['type'] = $dropReward->type;
				$reward[$i]['id'] = 'ID：'.$dropReward->drop_item_id;
				$reward[$i]['item_id'] = $dropReward->drop_item_id;
				$reward[$i]['item_name'] = $dropReward->drop_reward_item_name;
				$reward[$i]['count'] = $dropReward->drop_reward_item_num;
				// $reward->item_id = $dropReward->drop_item_id;
				// $reward->item_name = $dropReward->drop_reward_item_name;
				// $reward->count = $dropReward->drop_reward_item_num;
			}

			DebugUtil::e_log('reward', 'reward', $reward);

		}

		$storyQuestNames = [];
		$characterQuestNames = [];
		foreach ($storyQuests as $storyQuest)
        {
            $qd = QuestDictionary::getOne($storyQuest->id);
            $key = $storyQuest->story_quest_chapter_id . ':' . $storyQuest->id;

            // $qd = QuestDictionary::getOne($storyQuest->id);
            if (isset($qd))
            {
                $name = $qd->dictionary_ja;
            }
            else
            {
                $name = 'クエスト名がありません: ' . $storyQuest->id;
            }
            
            $storyQuestNames[$key] = $name;
        }

        foreach ($characterQuests as $characterQuest)
        {
            $qd = QuestDictionary::getOne($characterQuest->id);
            $key = $characterQuest->character_quest_chapter_id . ':' . $characterQuest->id;

            // $characterQuestNames[$key] = $qd->dictionary_ja;
            if (isset($qd))
            {
                $name = $qd->dictionary_ja;
            }
            else
            {
                $name = 'クエスト名がありません: ' . $characterQuest->id;
            }
            
            $characterQuestNames[$key] = $name;
            // DebugUtil::e_log('characterQuestNames', 'characterQuestNames', $characterQuestNames);
        }

		DebugUtil::e_log('characterQuests', 'characterQuests', $characterQuests);
		DebugUtil::e_log('characterQuestNames', 'characterQuestNames', $characterQuestNames);

		$params = [
			'storyQuests' => $storyQuests,
			'characterQuests' => $characterQuests,
			'storyQuestNames' => $storyQuestNames,
			'characterQuestNames' => $characterQuestNames,
			'reward' => $reward,

		];

		return view('master_check.reward_master', $params);
	}





}
