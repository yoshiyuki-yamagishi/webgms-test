<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerMissionListRequest;
use App\Http\Requests\PlayerMissionRewardTakeRequest;
use App\Services\PlayerMissionService;



class PlayerMissionController extends Controller
{
	/**
	 * 一覧
	 * @param PlayerMissionListRequest $request
	 * @return
	 */
	public function list(PlayerMissionListRequest $request)
	{
		$response = PlayerMissionService::list($request);
		return $response->toResponse();
	}

	/**
	 * 売却
	 * @param PlayerMissionRewardTakeRequest $request
	 * @return
	 */
	public function rewardTake(PlayerMissionRewardTakeRequest $request)
	{
		$response = PlayerMissionService::rewardTake($request);
		return $response->toResponse();
	}
}
