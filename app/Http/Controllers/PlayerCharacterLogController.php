<?php

namespace App\Http\Controllers;

use App\Models\PlayerCharacterLog;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterDictionary;
use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerRegistRequest;
use App\Services\SessionService;
use Illuminate\Http\Request;
use App\Utils\DebugUtil;

class PlayerCharacterLogController extends Controller
{
	
	public function index(Request $request)
	{
		SessionService::start(0, SessionService::SS_GMS);
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$characterLogs = PlayerCharacterLog::getByPlayerId($playerId);
		$characterLogs->player_name = $request->player_name;
		DebugUtil::e_log('characterLogs', 'characterLogs', $characterLogs);
		
		foreach ($characterLogs as $characterLog)
		{
			$charaBase = CharacterBase::getOne($characterLog->value3);
			DebugUtil::e_log('charaBase', 'charaBase', $charaBase);
			$charaD = CharacterDictionary::getOne($charaBase->character_name_id);
			DebugUtil::e_log('charaD', 'charaD', $charaD);
			$characterLog->chara_name = $charaD->dictionary_ja;

		}

		$params = [
            'characterLogs' => $characterLogs,
        ];

		return view('player_log.player_character_log', $params);

	}

}