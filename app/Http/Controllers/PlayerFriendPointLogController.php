<?php

namespace App\Http\Controllers;

use App\Models\PlayerCharacterLog;

use App\Http\Requests\PlayerAlRecoverRequest;
use App\Http\Requests\PlayerGetRequest;
use App\Http\Requests\PlayerTransitSrcRequest;
use App\Http\Requests\PlayerAccountRestoreRequest;
use App\Http\Requests\PlayerUpdateRequest;
use App\Http\Requests\PlayerRegistRequest;
use Illuminate\Http\Request;
use App\Models\PlayerLog;
use App\Utils\DebugUtil;

class PlayerFriendPointLogController extends Controller
{
	
	public function index(Request $request)
	{
		DebugUtil::e_log('request', 'request', $request->all());
		$playerId = $request->player_id;
		DebugUtil::e_log('playerId', 'playerId', $playerId);
		$playerFriendPointLogs = PlayerLog::getPlayerFrienfPointLog($playerId);
		foreach ($playerFriendPointLogs as $playerFriendPointLog)
		{
			DebugUtil::e_log('playerFriendPointLog', 'playerFriendPointLog', $playerFriendPointLog);
			// if ($playerFriendPointLog->type = 1)
			// {
			// 	$playerFriendPointLog->typeView = '増';
			// }
			// elseif ($playerFriendPointLog->type = 2)
			// {
			// 	$playerFriendPointLog->typeView = '減';
			// }
		}
		DebugUtil::e_log('playerFriendPointLogs', 'playerFriendPointLogs', $playerFriendPointLogs);

		$params = [
            'playerFriendPointLogs' => $playerFriendPointLogs,
        ];

		return view('player_log.player_friend_point_log', $params);

	}

}