<?php
/**
 * ShopListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\ShopResponse;



/**
 * ShopListのレスポンス
 *
 */
class ShopListResponse
{
	/**
	 * ShopListのレスポンス作成
	 *
	 * @param array $shopList shopListの配列
	 * @return array ShopListのレスポンス
	 */
	public static function make($shopList)
	{
		$body = [];
		foreach ($shopList as $shop)
		{
			$body[] = ShopResponse::make($shop);
		}
		return $body;
	}
}