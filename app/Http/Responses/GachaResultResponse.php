<?php
/**
 * GachaResultのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * GachaResultのレスポンス
 *
 */
class GachaResultResponse
{
	/**
	 * GachaResultのレスポンス作成
	 *
	 * @param GachaResult $gachaResult GachaResultのインスタンス
	 * @return array GachaResultのレスポンス
	 */
	public static function make($gachaResult)
	{
		$body = [
			'item_type' => $gachaResult->item_type,
			'item_id' => $gachaResult->item_id,
			'item_count' => $gachaResult->item_count,
			'gacha_result_flag' => $gachaResult->gacha_result_flag,
			'take_flag' => $gachaResult->take_flag,
		];
		return $body;
	}
    
}