<?php
/**
 * PlayerOrbのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerOrbのレスポンス
 *
 */
class PlayerOrbResponse
{
	/**
	 * PlayerOrbのレスポンス作成
	 *
	 * @param PlayerOrb $playerOrb PlayerOrbのインスタンス
	 * @return array PlayerOrbのレスポンス
	 */
	public static function make($playerOrb)
	{
		$body = [
			'player_orb_id'	=> $playerOrb->id,
			'orb_id'		=> $playerOrb->orb_id,
			'num'			=> $playerOrb->num,
		];
		return $body;
	}
}