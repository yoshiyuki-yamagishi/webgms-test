<?php
/**
 * PlayerItemのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerItemのレスポンス
 *
 */
class PlayerItemResponse
{
	/**
	 * PlayerItemのレスポンス作成
	 *
	 * @param PlayerItem $playerItem PlayerItemのインスタンス
	 * @return array PlayerItemのレスポンス
	 */
	public static function make($playerItem)
	{
        if (empty($playerItem))
            return static::makeEmpty();
        
		$body = [
			'item_id' => $playerItem->item_id,
			'player_item_id' => $playerItem->id,
			'num' => $playerItem->num,
		];
		return $body;
	}

	/**
	 * PlayerItemのレスポンス作成 (空)
	 *
	 * @return array PlayerItemのレスポンス
	 */
	public static function makeEmpty()
	{
		$body = [
			'item_id' => 0,
			'player_item_id' => 0,
			'num' => 0,
		];
		return $body;
	}
}