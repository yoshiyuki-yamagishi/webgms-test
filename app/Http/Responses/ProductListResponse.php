<?php
/**
 * ProductListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\ProductResponse;
use App\Models\PlayerProductBuy;

/**
 * ProductListのレスポンス
 *
 */
class ProductListResponse
{
	/**
	 * ProductListのレスポンス作成
	 *
	 * @param array $productList ProductShop の配列
	 * @param integer $playerId プレイヤID
	 * @param string $now 現在日付
	 * @return array ProductList のレスポンス
	 */
	public static function make($productList, $playerId, $now)
	{
		$body = [];
		foreach ($productList as $product)
		{
            // 本日の購入回数
            $num = PlayerProductBuy::getNumDay($playerId, $product->id, $now);

            // 最近の購入
            $latestBuy = PlayerProductBuy::getLatest($playerId, $product->id);
            
			$body[] = ProductResponse::make($product, $num, $latestBuy);
		}
		return $body;
	}
    
}
