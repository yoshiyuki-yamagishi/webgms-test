<?php
/**
 * PlayerGrimoireListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerGrimoireListのレスポンス
 *
 *
 */
class PlayerGrimoireListResponse
{
	/**
	 * PlayerGrimoireListのレスポンス作成
	 *
	 * @param array $player PlayerGrimoireの配列
	 * @param array $usedGrimoire 装備中の魔道書の配列 or true
	 * @return array PlayerGrimoireListのレスポンス
	 */
	public static function make($playerGrimoireList, $usedGrimoire)
	{
		$body = [];
		foreach ($playerGrimoireList as $playerGrimoire)
		{
            if (empty($playerGrimoire))
                continue;
            
            if (is_array($usedGrimoire))
                $used = isset($usedGrimoire[$playerGrimoire->id]);
            else
                $used = ($usedGrimoire === true);
            
			$body[] = PlayerGrimoireResponse::make($playerGrimoire, $used);
		}

		return $body;
	}
}