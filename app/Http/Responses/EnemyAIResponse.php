<?php
/**
 * EnemyAIのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Utils\DebugUtil;

/**
 * EnemyAIのレスポンス
 *
 */
class EnemyAIResponse
{
	/**
	 * EnemyAIのレスポンス作成
	 *
	 * @param EnemyAI $enemyAI EnemyAIのレスポンス
	 * @return array EnemyAIのレスポンス
	 */
	public static function make($enemyAI)
	{
        $params = [
            'enemy_ai_id',
            'active1_rate',
            'active2_rate',
            'trigger1_condition',
            'trigger1_value',
            'trigger1_action',
            'trigger1_action_contents',
            'trigger2_condition',
            'trigger2_value',
            'trigger2_action',
            'trigger2_action_contents',
            'trigger3_condition',
            'trigger3_value',
            'trigger3_action',
            'trigger3_action_contents',
        ];

        foreach ($params as $param)
        {
            if ($param == 'enemy_ai_id')
            {
                $body[$param] = $enemyAI->id;
                continue;
            }
            $body[$param] = $enemyAI->$param;
        }
        
		return $body;
	}
}

