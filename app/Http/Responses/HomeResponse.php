<?php
/**
 * Homeのレスポンス
 *
 */

namespace App\Http\Responses;

/**
 * Homeのレスポンス
 *
 */
class HomeResponse
{
    const QUEST_FLAG_NONE = 0;
    const QUEST_FLAG_EXIST = 1;
    
    const GACHA_FLAG_NONE = 0;
    const GACHA_FLAG_EXIST = 1;
}
