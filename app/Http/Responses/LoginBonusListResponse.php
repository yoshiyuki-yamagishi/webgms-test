<?php
/**
 * LoginBonusListのレスポンス
 *
 */

namespace App\Http\Responses;

/**
 * LoginBonusListのレスポンス
 *
 */
class LoginBonusListResponse
{
	/**
	 * login_bonus_list のレスポンス追加
	 *
	 * @param array $loginBonusList マスター login_bonus のリスト
	 * @param array $takeCounts login_id => 取得済み個数
	 * @return array login_bonus_list レスポンス生成
	 */
	public static function append(
        &$ret, $loginBonus, $rewardList, $takeFlag
    )
	{
		foreach ($rewardList as $reward)
		{
			$ret[] = LoginBonusResponse::make(
                $loginBonus, $reward, $takeFlag
            );
		}
        
        return $ret;
	}
    
	/**
	 * login_bonus_list のレスポンス作成
	 *
	 * @param array $loginBonusList マスター login_bonus のリスト
	 * @param array $takeCounts login_id => 取得済み個数
	 * @return array login_bonus_list レスポンス生成
	 */
	public static function make(
        $loginBonus, $rewardList, $takeFlag
    )
	{
        $ret = [];

        self::append($ret, $loginBonus, $rewardList, $takeFlag);
        
        return $ret;
	}
    
	/**
	 * login_bonus_list のレスポンス作成 (指定の番号まで取得状態にして追加)
	 *
	 * @param array $loginBonus マスター login_bonus
	 * @param array $rewardList マスター login_bonus_reward のリスト
	 * @param array $takeCount これ以下までを出力する
	 * @return array login_bonus_list レスポンス生成
	 */
	public static function appendByCount(
        &$ret, $loginBonus, $rewardList, $takeCount
    )
	{
        foreach ($rewardList as $reward)
		{
            $takeFlag = LoginBonusResponse::NOT_TAKE;
            if ($reward->count <= $takeCount)
				$takeFlag = LoginBonusResponse::TAKE;
                
			$ret[] = LoginBonusResponse::make(
                $loginBonus, $reward, $takeFlag
            );
		}
        
        return $ret;
	}

	/**
	 * login_bonus_list のレスポンス作成 (指定の番号まで取得状態にする)
	 *
	 * @param array $loginBonus マスター login_bonus
	 * @param array $rewardList マスター login_bonus_reward のリスト
	 * @param array $takeCount これ以下までを出力する
	 * @return array login_bonus_list レスポンス生成
	 */
	public static function makeByCount(
        $loginBonus, $rewardList, $takeCount
    )
	{
        $ret = [];
        
        self::appendByCount(
            $ret, $loginBonus, $rewardList, $takeCount
        );
        
        return $ret;
	}

}
