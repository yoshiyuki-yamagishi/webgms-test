<?php
/**
 * PlayerGrimoireのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerCharacter;
use App\Models\PlayerGrimoire;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\GrimoireAwakeCoefficient;

/**
 * PlayerGrimoireのレスポンス
 *
 */
class PlayerGrimoireResponse
{
	/**
	 * PlayerGrimoireのレスポンス作成
	 *
	 * @param PlayerGrimoire $playerGrimoire PlayerGrimoireのインスタンス
	 * @return array PlayerGrimoireのレスポンス
	 */
	public static function make($playerGrimoire, $used)
	{
        /*
        // パラメータの計算 【廃止】
        // 魔道書取得
        
        $grimoire = Grimoire::getOne($playerGrimoire->grimoire_id);
        if (empty($grimoire))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'grimoire', 'id', $playerGrimoire->grimoire_id
            );
        }

        // 旧) 覚醒は、0 〜 4 だが、マスタは、1 〜 5
        //
        // 新) 覚醒は、0 〜 5 で、マスタは、1 〜 5 が用意されている
        // このコードは (旧) なので、復活する場合は要修正

        $grimoireAwake = GrimoireAwakeCoefficient::getOne(
            $playerGrimoire->awake + 1
        );
        if (empty($grimoireAwake))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'grimoire_awake_coefficient', 'grimoire_awake',
                $playerGrimoire->awake + 1
            );
        }

        $hp = PlayerCharacter::calcParam(
            $grimoire->min_hp, [$grimoireAwake->hp_coefficient]
        );
        $atk = PlayerCharacter::calcParam(
            $grimoire->min_atk, [$grimoireAwake->atk_coefficient]
        );
        $def = PlayerCharacter::calcParam(
            $grimoire->min_def, [$grimoireAwake->def_coefficient]
        );
        */

        // スロット開放の計算
        
		$slotReleaseNum = 0; // スロット解放数
        
		$slotReleaseFlag1 = self::_getSlotReleaseFlag($playerGrimoire, 1);
		$slotReleaseFlag2 = self::_getSlotReleaseFlag($playerGrimoire, 2);
		$slotReleaseFlag3 = self::_getSlotReleaseFlag($playerGrimoire, 3);
        
		if ($slotReleaseFlag1 > 1)
            ++ $slotReleaseNum;
		if ($slotReleaseFlag2 > 1)
            ++ $slotReleaseNum;
		if ($slotReleaseFlag3 > 1)
            ++ $slotReleaseNum;

		$body = [
			'grimoire_id' => $playerGrimoire->grimoire_id,
			'player_grimoire_id' => $playerGrimoire->id,
			'slot_release_num' => $slotReleaseNum,
			'slot_release_flag_1' => $slotReleaseFlag1,
			'slot_id_1' => $playerGrimoire->slot_1,
			'slot_release_flag_2' => $slotReleaseFlag2,
			'slot_id_2' => $playerGrimoire->slot_2,
			'slot_release_flag_3' => $slotReleaseFlag3,
			'slot_id_3' => $playerGrimoire->slot_3,
			'awake' => $playerGrimoire->awake,
            // 'hp' => $hp,
            // 'atk' => $atk,
            // 'def' => $def,
			'lock_flag' => $playerGrimoire->lock_flag,
			'use_flag' => $used ? 1 : 0,
			'acquired_at' => $playerGrimoire->acquired_at,
		];
		return $body;
	}

	/**
	 * スロット解放フラグを取得
	 *
	 * @param PlayerGrimoire $playerGrimoire
	 * @param integer $slotNo スロット番号
	 * @return integer スロット解放フラグ(0:未解放、1:未装着、2:装着済み)
	 */
	private static function _getSlotReleaseFlag($playerGrimoire, $slotNo)
	{
		$slotReleaseFlag = 0;
		$slotFlag = sprintf('slot_%d_flag', $slotNo);
		$slot = sprintf('slot_%d', $slotNo);

		if ($playerGrimoire->$slotFlag == PlayerGrimoire::SLOT_RELEASE_FLAG_NO)
		{
			$slotReleaseFlag = 0;
		}
		else
		{
			if (!isset($playerGrimoire->$slot))
			{
				$slotReleaseFlag = 1;
			}
			else
			{
				$slotReleaseFlag = 2;
			}
		}
		return $slotReleaseFlag;
	}
}