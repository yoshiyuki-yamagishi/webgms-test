<?php
/**
 * PlayerItemListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerItemListのレスポンス
 *
 */
class PlayerItemListResponse
{
	/**
	 * PlayerItemListのレスポンス
	 *
	 * @param array $playerItemList PlayerItemListの配列
	 * @return array PlayerItemListのレスポンス
	 */
	public static function make($playerItemList)
	{
		$body = [];
		foreach ($playerItemList as $playerItem)
		{
			$body[]	= PlayerItemResponse::make($playerItem);
		}
		return $body;
	}
}