<?php
/**
 * BattleEnemyListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * BattleEnemyListのレスポンス
 *
 */
class BattleEnemyListResponse
{
	/**
	 * BattleEnemyListのレスポンス作成
	 *
	 * @param array $battleEnemyList BattleEnemyの配列
	 * @return array BattleEnemyListのレスポンス
	 */
	public static function make($battleEnemyList)
	{
		$body = [];
		foreach ($battleEnemyList as $battleEnemy)
		{
			$body[] = BattleEnemyResponse::make($battleEnemy);
		}
		return $body;
	}

	/**
	 * レスポンスに不要な情報を削る
	 *
	 * @param array $list BattleEnemyListのレスポンス
	 */
	public static function fixResponse(&$list)
	{
		foreach ($list as &$item)
		{
			BattleEnemyResponse::fixResponse($item);
		}
    }
    
}