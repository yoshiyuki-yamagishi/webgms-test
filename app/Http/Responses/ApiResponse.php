<?php
/**
 * API のレスポンス
 *
 */

namespace App\Http\Responses;

use App\Models\Maintenance;
use App\Models\PlayerBan;
use App\Services\S3Service;
use App\Utils\EncryptUtil;
use App\Utils\DebugUtil;
use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\Response;
use MessagePack\MessagePack;
use MessagePack\Packer;
use Thrift\Thrift\ClassLoader\ThriftClassLoader;

/**
 * API のレスポンス
 *
 *
 * レスポンスの形式
 * ------------------------------
 *	header					ヘッダ
 *		status_code				ステータスコード
 *		master_data				マスタデータ
 *			version					バージョン
 *			path					パス
 *			aes_key					AESキー
 *		error					エラー情報
 *			message					メッセージ
 *		maintenance				メンテナンス情報
 *			started_at				開始日時
 *			ended_at				終了日時
 *			message					メッセージ
 *	body 					ボディ
 *		xxxxx					ボディの内容
 * ------------------------------
 *
 */
class ApiResponse extends BaseResponse
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton))
        {
            self::$singleton = new ApiResponse();
            self::$singleton->checkMaintenance();
        }
        return self::$singleton;
    }
    
	/**
	 * ステータスコード
	 *
	 * @var integer
	 */
	public $statusCode = \App\Exceptions\ApiException::S_OK;

	/**
	 * 現在日付
	 *
	 * @var string
	 */
	public $currentDate = null;

	/**
	 * エラー情報：メッセージ
	 *
	 * @var string
	 */
	public $errorMessage = null;

	/**
	 * メンテナンス情報
	 *
	 * @var Maintenance モデル
	 */
	public $maintenance = null;
    
	/**
	 * BAN情報
	 *
	 * @var PlayerBan モデル
	 */
	public $playerBan = null;

	/**
	 * ボディ
	 *
	 * @var array
	 */
	public $body = null;

	public $loader = null;

	public $response = null;

    function __construct()
    {
        $this->currentDate = new \DateTime();
    }

    public function currentDateDB()
    {
        return DateTimeUtil::formatDB($this->currentDate);
    }

	/**
	 * エラー情報の設定
	 *
	 * @param integer $statusCode ステータスコード
	 * @param string $message メッセージ
	 */
	public function setError($statusCode, $message = '')
	{
		$this->statusCode = $statusCode;
		$this->errorMessage = $message;
	}

	/**
	 * メンテナンスチェック
	 *
	 */
	public function checkMaintenance()
	{
        $now = $this->currentDateDB();
        $mt = Maintenance::getOne($now);
        if (empty($mt))
            return;

        $this->maintenance = $mt;
            
        // ステータスコードは、使われないが、一応セット
		$this->statusCode = \App\Exceptions\ApiException::S_MAINTENANCE;
		$this->errorMessage = 'in mentainance !';

        throw \App\Exceptions\MaintenanceException::make(
            $this->errorMessage
        );
	}

	/**
	 * 警告、アカウント停止チェック
	 *
	 */
	public function checkBan($playerId)
	{
        $now = $this->currentDateDB();
        $playerBan = PlayerBan::getOne($playerId, $now);
        if (empty($playerBan))
            return;
        if ($playerBan->ban_status < PlayerBan::STATUS_WARN)
            return;

        $this->playerBan = $playerBan;
        
        switch ($playerBan->ban_status)
        {
        case PlayerBan::STATUS_WARN:
            $this->statusCode = \App\Exceptions\ApiException::S_WARN;
            break;
        case PlayerBan::STATUS_BAN:
            $this->statusCode = \App\Exceptions\ApiException::S_BAN;
            $this->errorMessage = 'this player is banned: ' . $playerId;
            throw \App\Exceptions\BanException::make(
                $this->errorMessage
            );
            break;
        default:
            throw \App\Exceptions\DataException::makeInvalid(
                'player_ban', 'ban_status', $playerBan->ban_status
            );
            break;
        }
	}
    
	/**
	 * 配列化
	 *
	 * @return array 本クラスの配列
	 */
	public function toArray()
	{
		$ret = [];
        
		$ret['header']['status_code']
            = $this->statusCode;
		$ret['header']['current_date']
            = $this->currentDateDB();
        
        S3Service::setHeader($ret['header'], S3Service::TYPE_MASTER_DATA);
        S3Service::setHeader($ret['header'], S3Service::TYPE_ASSET_BUNDLE);

        $ret['header']['info']['path'] = S3Service::infoUrl();

		if (isset($this->errorMessage))
		{
			$ret['header']['error']['message'] = $this->errorMessage;
		}

		if (isset($this->maintenance))
		{
            $mt = $this->maintenance;
			$ret['header']['maintenance']['started_at'] = $mt->start_at;
			$ret['header']['maintenance']['ended_at'] = $mt->end_at;
			$ret['header']['maintenance']['message'] = $mt->message;
		}

		if (isset($this->playerBan))
		{
            $pb = $this->playerBan;
			$ret['header']['ban']['status'] = $pb->ban_status;
			$ret['header']['ban']['started_at'] = $pb->start_at;
			$ret['header']['ban']['ended_at'] = $pb->end_at;
			$ret['header']['ban']['message'] = $pb->message;
		}
        
		$ret['body'] = $this->body;

		return $ret;
	}

	public function toResponse()
	{
		$ret = $this->toArray();
// 		$headers = ['Content-Type' => 'application/x-msgpack'];
		$headers = ['Content-Type' => 'application/octet-stream'];

		if (EncryptUtil::isEncryptResponse())
		{
			//------------------------------
			// 暗号化
			//------------------------------
			$ret = EncryptUtil::encryptResponse($ret);
		}
        
		return Response::make($ret, 200, $headers);
	}
    
}
