<?php
/**
 * PlayerQuestのレスポンス
 *
 */

namespace App\Http\Responses;

/**
 * PlayerQuestのレスポンス
 *
 */
class PlayerQuestResponse
{
	/**
	 * PlayerQuestのレスポンス作成
	 *
	 * @param PlayerQuest $playerQuest PlayerQuestのインスタンス
	 * @param integer 章ごとのクリア回数 (日毎)
	 * @return array PlayerQuestのレスポンス
	 */
	public static function make($playerQuest, $dailyClearCount)
	{
		$body = [
            'player_quest_id' => $playerQuest->id,
            'quest_category' => $playerQuest->quest_category,
            'chapter_id' => $playerQuest->chapter_id,
            'quest_id' => $playerQuest->quest_id,
            'clear_flag' => $playerQuest->clear_flag,
            'mission_flag_1' => $playerQuest->mission_flag_1,
            'mission_flag_2' => $playerQuest->mission_flag_2,
            'mission_flag_3' => $playerQuest->mission_flag_3,
            'clear_count' => $playerQuest->clear_count,
            'daily_clear_count' => $dailyClearCount,
		];
		return $body;
	}
    
}
