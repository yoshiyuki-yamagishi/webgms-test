<?php
/**
 * PlayerPartyListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerPartyListのレスポンス
 *
 */
class PlayerPartyListResponse
{
	/**
	 * PlayerPartyListのレスポンス作成
	 *
	 * @param array $playerPartyList PlayerCharacterの配列
	 * @return array PlayerPartyListのレスポンス
	 */
	public static function make($playerPartyList)
	{
		$body = [];
		foreach ($playerPartyList as $playerParty)
		{
			$body[] = PlayerPartyResponse::make($playerParty);
		}
		return $body;
	}
}