<?php
/**
 * Productのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Utils\DateTimeUtil;



/**
 * Productのレスポンス
 *
 */
class ProductResponse
{
	/**
	 * Productのレスポンス作成
	 *
	 * @param Product $product ProductShop のインスタンス
	 * @param PlayerProductBuy $latestBuy 最近のプレイヤ製品購入
	 * @return array Productのレスポンス
	 */
	public static function make($product, $num, $latestBuy)
	{
        $params = [
            'id',
            'start_day',
            'end_day',
            'limit',
            'num',
            'buy_at',
            'daily_end_at',
        ];

        foreach ($params as $param)
        {
            if ($param == 'id')
            {
                $body['product_id'] = $product->$param;
                continue;
            }
            if ($param == 'num')
            {
                $body[$param] = $num;
                continue;
            }
            if ($param == 'buy_at')
            {
                if (isset($latestBuy))
                    $body[$param] = DateTimeUtil::formatDB(
                        $latestBuy->created_at
                    );
                else
                    $body[$param] = '';
                continue;
            }
            if ($param == 'daily_end_at')
            {
                if (isset($latestBuy) && $latestBuy->isDaily())
                {
                    $endAt = $latestBuy->dailyEndDate();
                    $body[$param] = DateTimeUtil::formatDB($endAt);
                }
                else
                {
                    $body[$param] = '';
                }
                continue;
            }
            
            $body[$param] = $product->$param;
        }
        
		return $body;
	}
    
}