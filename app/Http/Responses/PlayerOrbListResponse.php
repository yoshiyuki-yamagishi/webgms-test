<?php
/**
 * PlayerOrbListのレスポンス
 *
 */

namespace App\Http\Responses;



/**
 * PlayerOrbListのレスポンス
 *
 */
class PlayerOrbListResponse
{
	/**
	 * PlayerOrbListのレスポンス作成
	 *
	 * @param array $playerOrbList PlayerOrbの配列
	 * @return array $playerOrbListのレスポンス
	 */
	public static function make($playerOrbList)
	{
		$body = [];
		foreach ($playerOrbList as $playerOrb)
		{
			$body[] = PlayerOrbResponse::make($playerOrb);
		}
		return $body;
	}
}