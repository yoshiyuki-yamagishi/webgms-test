<?php
/**
 * QuestRewardListのレスポンス
 *
 */

namespace App\Http\Responses;

use App\Utils\SortUtil;

/**
 * QuestRewardListのレスポンス
 *
 */
class QuestRewardListResponse
{
	/**
	 * QuestRewardListのレスポンス作成
	 *
	 * @param array $questRewardList PlayerBattleReward の配列
	 * @return array QuestRewardListのレスポンス
	 */
	public static function make($playerRewardList, $takeFlag)
	{
		$body = [];
		foreach ($playerRewardList as $playerReward)
		{
			$body[] = QuestRewardResponse::make($playerReward, $takeFlag);
		}
		return $body;
	}

	/**
	 * QuestReward の配列からQuestRewardListのレスポンス作成
	 *
	 * @param array $questRewardList QuestRewardの配列
	 * @return array QuestRewardListのレスポンス
	 */
	public static function makeByQuestReward($questRewardList)
	{
		$body = [];
		foreach ($questRewardList as $questReward)
		{
			$body[] = QuestRewardResponse::makeByQuestReward($questReward);
		}
		return $body;
	}

	/**
	 * DropReward の配列からQuestRewardListのレスポンス作成
	 *
	 * @param array $dropRewardList DropRewardの配列
	 * @return array QuestRewardListのレスポンス
	 */
	public static function makeByDropReward($dropRewardList)
	{
		$body = [];
		foreach ($dropRewardList as $dropReward)
		{
			$body[] = QuestRewardResponse::makeByDropReward($dropReward);
		}
		return $body;
	}

	/**
	 * アイテム毎に集計する
	 *
	 * @return array QuestRewardListのレスポンス
	 */
	public static function makeSum($list)
	{
        // 欠片変換は考慮済み //
        
        $_sort = $list;

        usort($_sort, function ($a, $b) {
            $cmp = SortUtil::val_cmp($a['item_type'], $b['item_type']);
            if ($cmp != 0)
                return $cmp;
            return SortUtil::val_cmp($a['item_id'], $b['item_id']);
        });

        $ret = [];
        $last = null;
        foreach ($_sort as $item)
        {
            if (isset($last) &&
                $last['item_type'] == $item['item_type'] &&
                $last['item_id'] == $item['item_id'])
            {
                $last['item_num'] += $item['item_num'];
                continue;
            }

            $last = $item;
            $last['frame_count'] = 0; // 無効化
            $last['take_flag'] = 0; // 無効化
            $ret[] = $last;
        }

        return $ret;
	}
    
}