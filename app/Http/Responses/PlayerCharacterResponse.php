<?php
/**
 * PlayerCharacterのレスポンス
 *
 */

namespace App\Http\Responses;

use App\Models\PlayerItem;
use App\Models\PlayerParty;
use App\Models\PlayerCharacter;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\CharacterExpTable;
use App\Models\MasterModels\LevelCoefficient;
use App\Models\MasterModels\RarityCoefficient;
use App\Utils\DebugUtil;

/**
 * PlayerCharacterのレスポンス
 *
 */
class PlayerCharacterResponse
{
	// パーティ使用中フラグ
	const PARTY_FLAG_NO		= 0; // 未使用
	const PARTY_FLAG_YES	= 1; // 使用中

	// お気に入りフラグ
	const FAVORITE_FLAG_NO	= 0;	// お気に入りでない
	const FAVORITE_FLAG_YES	= 1;	// お気に入り

	/**
	 * PlayeCharacterのレスポンス作成
	 *
	 * @param PlayeCharacter $playerCharacter PlayeCharacterのインスタンス
	 * @return array PlayeCharacterのレスポンス
	 */
	public static function make($player, $playerCharacter)
	{
        $playerId = $playerCharacter->player_id;
        $charaId = $playerCharacter->character_id;
        
        $charaBase = CharacterBase::getOne($charaId);
        if (empty($charaBase))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'character_base', 'id', $charaId
            );
        }
        // DebugUtil::e_log('PCR', 'charaBase', $charaBase);

        // 経験値の計算
        
        $lv = $playerCharacter->character_lv;
        $exp = $playerCharacter->experience;
        $startExp = 0;
        $nextExp = 0;
        
        $levels = CharacterExpTable::getLevels(
            $charaBase->character_exp_table_id, $lv, 2
        );
        assert($levels > 0);
        $startExp = $levels[0]->exp_character;
        
        if (count($levels) > 1)
        {
            if ($levels[1]->exp_character > $exp)
                $nextExp = $levels[1]->exp_character - $exp;
        }

        $evolve = $playerCharacter->evolve;
        $rarity0 = $charaBase->character_initial_rarity;
        
        // パラメータの計算 【廃止】
        /*
        $rarity = $rarity0 + $evolve;

        $rc = RarityCoefficient::getOne($rarity);
        if (!isset($rc))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'rarity_coefficient', 'rarity_coefficient_id', $rarity
            );
        }
        // DebugUtil::e_log('PCR', 'rc', $rc);

        $lc = LevelCoefficient::getOne($lv);
        if (!isset($lc))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'level_coefficient', 'level_coefficient_id', $lv
            );
        }
        // DebugUtil::e_log('PCR', 'lc', $lc);
        
        $hp = PlayerCharacter::calcParam(
            $charaBase->min_hp, [$rc->hp_coefficient, $lc->hp_coefficient]
        );
        
        $atk = PlayerCharacter::calcParam(
            $charaBase->min_atk, [$rc->atk_coefficient, $lc->atk_coefficient]
        );
        
        $def = PlayerCharacter::calcParam(
            $charaBase->min_def, [$rc->def_coefficient, $lc->def_coefficient]
        );

        // グレード、オーブ装備によるパラメータ増加量の計算

        $gradeupParams = $playerCharacter->calcGradeupParams(
            $charaBase->gradeup_id
        );
        // DebugUtil::e_log('PCR', 'gradeupParams', $gradeupParams);
        */

		// パーティに入っているか確認
        
		$playerPartyList = PlayerParty::getByPlayerCharacterId(
            $playerCharacter->id
        );
		if ($playerPartyList->count() > 0)
		{
			$partyFlag = self::PARTY_FLAG_YES;
		}
		else
		{
			$partyFlag = self::PARTY_FLAG_NO;
		}

		// 欠片の数 (汎用欠片は含まない、あったとしても)
        
        $fragmentId = Character::fragmentIdFrom($charaId);
        $fragment = PlayerItem::getOne($playerId, $fragmentId);
        $fragmentNum = isset($fragment) ? $fragment->num : 0;

        // イラストID に変換
        
        // $illustId = Character::calcIllustId($charaId, $rarity0, $evolve);
        $spineId = $charaId * 10 + $playerCharacter->spine;
        
        $favoriteFlag = ($player->favorite_character == $playerCharacter->id)
                      ? self::FAVORITE_FLAG_YES : self::FAVORITE_FLAG_NO;

        // レスポンス計算

		$body = [
			'character_id' => $spineId,
			'character_base_id' => $charaId,
			'player_character_id' => $playerCharacter->id,
            'spine' => $playerCharacter->spine,
            'evolve' => $playerCharacter->evolve,
            'grade' => $playerCharacter->grade,
			'level' => $lv,
			// 'hp' => $hp + $gradeupParams['hp'],
			// 'atk' => $atk + $gradeupParams['atk'],
			// 'def' => $def + $gradeupParams['def'],
			'acquired_at' => $playerCharacter->acquired_at,
			'experience' => $exp,
			'start_experience' => $startExp,
			'next_experience' => $nextExp,
			'party_flag' => $partyFlag,
			'favorite_flag' => $favoriteFlag,
			'piece_num' => $fragmentNum,
			'active_skill_1_level' => $playerCharacter->active_skill_1_lv,
			'active_skill_2_level' => $playerCharacter->active_skill_2_lv,
			'player_orb_id_1' => $playerCharacter->player_orb_id_1,
			'player_orb_id_2' => $playerCharacter->player_orb_id_2,
			'player_orb_id_3' => $playerCharacter->player_orb_id_3,
			'player_orb_id_4' => $playerCharacter->player_orb_id_4,
			'player_orb_id_5' => $playerCharacter->player_orb_id_5,
			'player_orb_id_6' => $playerCharacter->player_orb_id_6,
		];
		return $body;
	}
}