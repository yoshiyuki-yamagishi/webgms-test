<?php
/**
 * Gachaのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Models\PlayerGacha;



/**
 * Gachaのレスポンス
 *
 */
class GachaResponse
{
	/**
	 * Gachaのレスポンス作成
	 *
	 * @param integer $playerId プレイヤID
	 * @param Gacha $gacha Gachaのインスタンス
	 * @param string $now 現在日付
	 * @return list Gachaのレスポンス
	 */
	public static function make($playerId, $gacha, $now)
	{
        $limitCount = PlayerGacha::getLimitCount($playerId, $gacha, $now);

        $params = [
            'gacha_id',
            // 'gacha_name_id',
            // 'gacha_type',
            // 'priority',
            // 'item_id',
            // 'single_item',
            // 'single_item_get_count',
            // 'ten_item',
            // 'ten_item_get_count',
            // 'gacha_loop',
            // 'limit_type',
            'limit_count',
            // 'gacha_limit',
            // 'gacha_banner',
            // 'gacha_pic',
            // 'gacha_lot_id',
            // 'confirm',
            // 'release_day',
            // 'end_day',
        ];

        foreach ($params as $param)
        {
            if ($param == 'gacha_id')
            {
                $body[$param] = $gacha->id;
                continue;
            }
            else if ($param == 'limit_count')
            {
                $body[$param] = $limitCount;
                continue;
            }
                
            $body[$param] = $gacha->$param;
        }

		return $body;
	}
}