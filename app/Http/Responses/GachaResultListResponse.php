<?php
/**
 * GachaResultListのレスポンス
 *
 */

namespace App\Http\Responses;
use App\Http\Responses\GachaResultResponse;



/**
 * GachaResultListのレスポンス
 *
 */
class GachaResultListResponse
{
	/**
	 * GachaResultListのレスポンス作成
	 *
	 * @param array $gachaResultList gachaResult の配列
	 * @return array GachaResultListのレスポンス
	 */
	public static function make($gachaResultList)
	{
		$body = [];
		foreach ($gachaResultList as $gachaResult)
		{
			$body[] = GachaResultResponse::make($gachaResult);
        }
		return $body;
	}
}