<?php
/**
 * Enemyのレスポンス
 *
 */

namespace App\Http\Responses;


/**
 * Enemyのレスポンス
 *
 */
class EnemyResponse
{
	/**
	 * Enemyのレスポンス作成
	 *
	 * @param Enemy $enemy Enemyのインスタンス
	 * @return array Enemyのレスポンス
	 */
	public static function make($enemy)
	{
        $params = [
            'enemy_id',
            'enemy_name_id',
            'max_hp',
            'max_atk',
            'max_def',
            'sex',
            'alignment',
            'voice_id',
            'release_at',
        ];

        foreach ($params as $param)
        {
            if ($param == 'enemy_id')
            {
                $body[$param] = $enemy->id;
                continue;
            }
            
            $body[$param] = $enemy->$param;
        }

		return $body;
	}
}