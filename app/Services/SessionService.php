<?php
/**
 * セッション のサービス
 *
 */

namespace App\Services;

use App\Utils\DateTimeUtil;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\ApiException;
use App\Utils\DebugUtil;

/**
 * セッション のサービス
 *
 */
class SessionService extends BaseService
{
    const SS_NORMAL = 0;
    const SS_REGIST = 1;
    const SS_LOGIN = 2;
    const SS_UPDATE = 3;
    const SS_GMS = 100;
    
	/** キャッシュ用のタグ */
    private static $TAGS = ['session'];
    
	/** プレイヤID */
	private static $_mode = -1;
	/** プレイヤID */
	private static $_playerId;
	/** セッションの値 */
	private static $_values;

	/**
	 * コンストラクタ
	 * <p>
	 * コンストラクタは、不要なので、privateにして、呼出し不可とする
	 * </p>
	 *
	 */
	private function __construct()
	{
	}

	/**
	 * セッションの開始
	 * <p>
	 * キャッシュから値を取得する。
	 * ログイン時は、認証コードを更新する。
	 * </p>
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $mode セッションスタートモード
	 * @param integer $dbNo DB 番号
	 */
	public static function start(
        $playerId, $mode = self::SS_NORMAL, $dbNo = null
    )
	{
// 		LogUtils::d('SessionModule: start');

        self::$_mode = $mode;
		if ($mode == self::SS_GMS)
		{
            // 現在のバージョンだと GMS から呼ばれる //
            self::$_values = [];
			return;
		}

        self::$_playerId = $playerId;
        
		if ($mode >= self::SS_REGIST)
		{
            // セッションをリセットする
            // DebugUtil::e_log(
            // 'Ses', 'start', '[' . $mode . '] ' . self::$_playerId 
            // );
            
			self::$_values = [];

			self::setDbNo($dbNo);
			self::_updateAuthCode(); // 認証コードを更新する
		}
        else
        {
            // セッション情報をキャッシュから取得
            
            if (self::$_values !== null)
            {
                // DebugUtil::e_log(
                // 'Ses', 'start', '_values are !== null'
                // );
                
                assert(false); // start が 2 回呼ばれている？
                return;
            }
            
            self::$_values = CacheService::get(self::$TAGS, self::$_playerId);

            // DebugUtil::e_log(
            // 'Ses', 'start',
            // '[' . $mode . '] ' . self::$_playerId 
            // );
            // DebugUtil::e_log(
            // 'Ses', 'start-value', self::$_values
            // );
            
            if (self::$_values === false)
                self::$_values = [];
        }
	}

	/**
	 * セッションの終了
	 * <p>
	 * キャッシュに値を設定する
	 * </p>
	 *
	 */
	public static function end()
	{
// 		LogUtils::d('SessionModule: end');

		if (self::$_values === null)
		{
			return;
		}

		if (self::$_mode == self::SS_GMS)
		{
            // GMS では、何もしない
			return;
		}

        // セッションのタイムアウトをキャッシュにも設定 //
        
		$authCodeExpired = env('AUTH_CODE_EXPIRED'); // 秒
        $timeout = intdiv($authCodeExpired, 60); // 分

        CacheService::setEx(
            self::$TAGS, self::$_playerId, self::$_values, $timeout
        );

        // DebugUtil::e_log(
        // 'Ses', 'end',
        // '[] ' . self::$_playerId 
        // );
        // DebugUtil::e_log(
        // 'Ses', 'end-value', self::$_values
        // );
	}

	/**
	 * セッションの値の取得
	 *
	 * @param string $key セッションのキー
	 * @return string セッションの値
	 */
	private static function _getSessionValue($key)
	{
		if (self::$_values === null)
		{
			return(null);
		}

		if (is_array(self::$_values) && isset(self::$_values[$key]))
		{
			return(self::$_values[$key]);
		}

		return(null);
	}

	/**
	 * セッションの値の設定
	 *
	 * @param string $key セッションのキー
	 * @param string $value セッションの値
	 */
	private static function _setSessionValue($key, $value)
	{
		if (self::$_values === null)
		{
			return;
		}

		self::$_values[$key] = $value;
	}

	/**
	 * 認証コードの生成
	 *
	 * @param integer $playerId プレイヤID
	 * @return string 認証コード
	 */
	private static function _makeAuthCode($playerId)
	{
		$str = '4nTj/8tDs-'
             . sprintf('%04d', rand(0, 9999))
             . '-' . $playerId . '-' .date('YmdHis');
        
		return md5($str);
	}

	/**
	 * 認証コードの更新
	 *
	 */
	private static function _updateAuthCode()
	{
		$_authCode = self::_makeAuthCode(self::$_playerId);
		$authCodeExpired = env('AUTH_CODE_EXPIRED');
		$_authCodeTime = DateTimeUtil::addSecondsToDate(
            DateTimeUtil::getNOW(), $authCodeExpired
        );

		self::_setSessionValue('_auth_code', $_authCode);
		self::_setSessionValue('_auth_code_time', $_authCodeTime);
	}

	/**
	 * 認証コードのチェック
	 *
	 * @param string $authCode 認証コード
	 * @return integer 認証コードが正しい場合、ApiException::S_OK を返す
	 */
	public static function checkAuthCode($authCode)
	{
		$_authCode		= self::_getSessionValue('_auth_code');
		$_authCodeTime	= self::_getSessionValue('_auth_code_time');

		// データの存在チェック
		if (empty($_authCode) || empty($_authCodeTime))
		{
            // LogUtils::e('SessionModule::checkAuthCode E_AUTH_EXPIRED(1) playerId=%s, _authCode=%s, _auth_code_time=%s', self::$_playerId, $_authCode, $_authCodeTime);
			return ApiException::E_AUTH_EXPIRED;
		}

		// 認証コードのチェック
		if (strcmp($authCode, $_authCode) != 0)
		{
			return ApiException::E_AUTH;
		}

		// 有効期限チェック
        $now = DateTimeUtil::getNOW();
		if (DateTimeUtil::compareDate($_authCodeTime, $now) < 0)
		{
			return ApiException::E_AUTH_EXPIRED;
		}
		else
		{
			return ApiException::S_OK;
		}
	}

	/**
	 * プレイヤIDの取得
	 *
	 * @return string プレイヤID
	 */
	public static function getPlayerId()
	{
		return(self::$_playerId);
	}

	/**
	 * 認証コードの取得
	 *
	 * @return string 認証コード
	 */
	public static function getAuthCode()
	{
		return(self::_getSessionValue('_auth_code'));
	}

	/**
	 * データベース番号の取得
	 *
	 * @return integer データベース番号
	 */
	public static function getDbNo()
	{
		return(self::_getSessionValue('_db_no'));
	}

	/**
	 * データベース番号の設定
	 *
	 * @param integer $dbNo データベース番号
	 */
	public static function setDbNo($dbNo)
	{
		self::_setSessionValue('_db_no', $dbNo);
	}
    
}

