<?php
/**
 * プレイヤキャラクタ のサービス
 *
 */

namespace App\Services;

use App\Config\Constants;

use App\Http\Responses\ApiResponse;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerCharacter;
use App\Models\PlayerCharacterLog;
use App\Models\PlayerCommonCache;
use App\Models\PlayerItem;
use App\Models\MasterModels\Character;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Gradeup;
use App\Models\MasterModels\Item;
use App\Http\Responses\PlayerCharacterListResponse;
use App\Http\Responses\PlayerCharacterResponse;
use App\Services\MissionChecker\MissionCheckerCharacterEvolve;
use App\Services\MissionChecker\MissionCheckerCharacterReinforce;
use App\Services\MissionChecker\MissionCheckerCharacterSkillReinforce;
use App\Services\MissionChecker\MissionCheckerCharacterOrbEquip;


/**
 * プレイヤキャラクタ のサービス
 *
 */
class PlayerCharacterService extends BaseService
{
	// パーティ使用中フラグ
	const PARTY_FLAG_NO		= 0; // 未使用
	const PARTY_FLAG_YES	= 1; // 使用中

	/**
	 * 一覧
	 *
	 * @param PlayerCharacterListRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
        $response = self::getResponse($request);

		// プレイヤ取得

		$player = Player::find_($request->player_id);

		// プレイヤキャラクタ取得

		$playerCharacterList = PlayerCharacter::getByPlayerId(
            $request->player_id
        );

		$body = [
			'player_character_list'	=> PlayerCharacterListResponse::make(
                $player, $playerCharacterList
            ),
			'player_character_num'	=> count($playerCharacterList),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 更新
	 *
	 * @param PlayerCharacterUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
        $response = self::getResponse($request);

		// プレイヤ取得
		$player = Player::find_($request->player_id);

		// プレイヤキャラクタ取得
		$playerCharacter = PlayerCharacter::find_($request->player_character_id);
        if ($playerCharacter->player_id != $request->player_id)
        {
            throw \App\Exceptions\ParamException::make(
                '該当プレイヤーキャラデータがのプレイヤーIDと呼び出しプレイヤーIDが違います: player_character->player_id = '
                . $playerCharacter->player_id . ' $request->player_id' .
                $request->player_id
            );
        }

        $character = CharacterBase::getOne_($playerCharacter->character_id);

        {// Spine判定
            // 開放済み最大スパインを取得
            $selectableSpineMax = Character::calcSpineLv(
                $character->character_initial_rarity,
                $playerCharacter->evolve
            );

            if ($request->spine > $selectableSpineMax)
            {
                throw \App\Exceptions\GameException::make(
                    '未開放のSpineを指定しています: selectableSpineMax = '
                    . $selectableSpineMax . ' selectSpine = '
                    . $request->spine
                );
            }

            // スパインの設定
            $playerCharacter->spine = $request->spine;
        }

        {// IllustNumber判定
            // 開放済み最大イラスト番号を取得
            $selectableIllustNumMax = Character::calcIllustLv(
                $character->character_initial_rarity,
                $playerCharacter->evolve
            );

            if ($request->illust_number > $selectableIllustNumMax)
            {
                throw \App\Exceptions\GameException::make(
                    '未開放のイラスト番号を指定しています: selectableIllustNumMax = '
                    . $selectableIllustNumMax . ' selectIllustNum = '
                    . $request->illust_number
                );
            }

            // イラスト番号の設定
            $playerCharacter->illust_number = $request->illust_number;
        }

        // 保存 (今のところ、トランザクション不要)
        $playerCharacter->save();

		$body = [
			'player_character'	=> PlayerCharacterResponse::make($player, $playerCharacter),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 進化
	 *
	 * @param PlayerCharacterEvolveRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function evolve($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化
        $missionChecker = MissionCheckerCharacterEvolve::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得
		$player = Player::find_($request->player_id);

		// プレイヤキャラクタ取得
		$playerCharacter = PlayerCharacter::find_($request->player_character_id);

        // キャラID
        $characterId = $playerCharacter->character_id;

        // キャラクタ取得(マスターから取得)
        $characterBase = CharacterBase::getOne_($characterId);

        // キャラの初期レアリティ
        $characterInitialRarity = $characterBase->character_initial_rarity;
        // キャラの現在進化数
        $evolve = $playerCharacter->evolve;
        // キャラの現在レアリティ
        $rarity = $characterInitialRarity + $evolve;
        // キャラの更新進化数
        $nextEvolve = $evolve + 1;

        // レアリティの最大は越えない
        if ($rarity >= Character::MAX_RARITY)
        {
            throw \App\Exceptions\GameException::make(
                'character has already max rarity: ' . Character::MAX_RARITY
            );
        }

		// 決め打ちの表から、必要数を取得
        $constModels = Constant::characterRarityUpConstModel($rarity);
        $needNum = $constModels->value1;

        // アイテム ID を取得し、ついでに所持数が足りているかチェックする
        $useItems = $request->player_item_list;
        PlayerItemService::setupUseItems($useItems);

		// 使用する合計欠片数
		$pieceNum = 0;
		foreach ($useItems as $useItem)
        {
            CharacterBase::checkFragmentId($characterBase, $useItem['item_id']);
			$pieceNum += $useItem['item_num'];
        }

		// 欠片の所持を確認
		if ($needNum != $pieceNum)
		{
            throw \App\Exceptions\DataException::makeNotEquals(
                '', '', 'sum(item_num)',
                $pieceNum, $needNum
            );
		}

		// スパインの現在値と更新値を計算
        $oldSpine = Character::calcSpineLv($characterInitialRarity, $evolve);
        $newSpine = Character::calcSpineLv($characterInitialRarity, $nextEvolve);

        // イラスト番号の現在値と更新値を計算
        $oldIllustNumber = Character::calcIllustLv($characterInitialRarity, $evolve);
        $newIllustNumber = Character::calcIllustLv($characterInitialRarity, $nextEvolve);

		{
			BaseGameModel::beginTransaction();

			// プレイヤキャラクタ進化
            $playerCharacter->evolve = $nextEvolve;

            // スパインが開放された場合のみ、設定する
            if ($newSpine != $oldSpine) $playerCharacter->spine = $newSpine;
            // イラストが開放された場合のみ、設定する
            if ($oldIllustNumber != $newIllustNumber) $playerCharacter->illust_number = $newIllustNumber;

			$playerCharacter->save();

            $log = LogService::playerCharacterEvolve(
                $playerCharacter, SrcType::EVOLVE, 0
            );

            // アイテムを消費する
            foreach ($useItems as $useItem)
            {
                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = Item::TYPE_ITEM;
                $param->itemId = $useItem['item_id'];
                $param->count = - $useItem['item_num'];
                $param->srcType = SrcType::EVOLVE;
                $param->srcId = $log->id;

                PlayerService::giveOrPay($param);
			}

            // ミッション達成チェック
            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerCharacter', $playerCharacter);
            $missionChecker->addData('characterBase', $characterBase);

            $evolveCounts = PlayerCharacterLog::getEvolveCounts(
                $player->id
            );
            $missionChecker->addData('evolveCounts', $evolveCounts);

            $rarityEvolveCounts = PlayerCharacterLog::calcRarityEvolveCounts(
                $evolveCounts
            );
            $missionChecker->addData('rarityEvolveCounts', $rarityEvolveCounts);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		$body = [
			'player_character'	=>
                PlayerCharacterResponse::make($player, $playerCharacter),
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * 強化
	 *
	 * @param PlayerCharacterReinforceRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function reinforce($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerCharacterReinforce::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得
		$player = Player::find_($request->player_id);

		// プレイヤキャラクタ取得
		$playerCharacter = PlayerCharacter::find_(
            $request->player_character_id
        );

        if ($playerCharacter->character_lv >= $player->player_lv)
        {
            throw \App\Exceptions\GameException::make(
                'character level is already player_lv: ' . $player->player_lv
            );
        }

        // アイテム ID を取得し、ついでに所持数が足りているかチェックする

        $useItems = $request->player_item_list;
        PlayerItemService::setupUseItems($useItems);

        // 経験値を計算する

        $exp = 0;
        foreach ($useItems as $useItem)
        {
            // アイテム取得
            $item = Item::getOne_($useItem['item_id']);

            // アイテムカテゴリチェック
            $category = Item::categoryFromId($useItem['item_id']);
            if ($category != Item::CATEGORY_EXP)
            {
                throw \App\Exceptions\GameException::make(
                    'item category is invalid: ' . $useItem['item_id']
                );
            }

            // 経験値を計算
            $exp += $useItem['item_num'] * $item->value;
        }

		//------------------------------
		// プレイヤキャラクタ強化
		//------------------------------

		{
			BaseGameModel::beginTransaction();

            // プレイヤキャラクターに経験値を加算

            $playerCharacter->experience += $exp;

            $playerCharacterLog = LogService::playerCharacterExp(
                $playerCharacter, $exp, SrcType::REINFORCE, 0
            );

			// プレイヤキャラクタのレベルを更新

            $prevCharaLv = $playerCharacter->character_lv;
            $playerCharacter->updateLevel($player->player_lv);

            if ($playerCharacter->character_lv != $prevCharaLv)
            {
                LogService::playerCharacterLv(
                    $playerCharacter,
                    $playerCharacter->character_lv - $prevCharaLv,
                    SrcType::REINFORCE, 0
                );
            }

            $playerCharacter->save(); // 経験値は増えてる

            // アイテムを消費する

            foreach ($useItems as $useItem)
            {
                $param = new GiveOrPayParam();
                $param->playerId = $request->player_id;
                $param->itemType = Item::TYPE_ITEM;
                $param->itemId = $useItem['item_id'];
                $param->count = - $useItem['item_num'];
                $param->srcType = SrcType::REINFORCE;
                $param->srcId = $playerCharacterLog->id;

                PlayerService::giveOrPay($param);
			}

            // ミッション達成チェック

            $missionChecker->addData('prevCharacterLv', $prevCharaLv);
            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerCharacter', $playerCharacter);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		// プレイヤアイテム一覧
		// $playerItemList = PlayerItem::getByPlayerId($request->player_id);

		$body = [
			'player_character'	=>
                PlayerCharacterResponse::make($player, $playerCharacter),
			// 'player_item_list'	=>
            // PlayerItemListResponse::make($playerItemList),
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * スキル強化
	 *
	 * @param PlayerCharacterSkillReinforceRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function skillReinforce($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerCharacterSkillReinforce::getInstance();
        $missionChecker->init($request->player_id, $now);

        // プレイヤ取得

        $player = Player::find_($request->player_id);

		// プレイヤキャラクタ取得

		$playerCharacter = PlayerCharacter::find_(
            $request->player_character_id
        );

		// キャラクタ取得(マスターから取得)

        $characterId = $playerCharacter->character_id;
		$characterBase = CharacterBase::getOne($characterId);
		if (!isset($characterBase))
		{
            throw \App\Exceptions\MasterException::makeNotFound(
                'character_base', 'character_base_id', $characterId
            );
		}

		// 次のスキルレベル等の計算

        $skillNo = 0;
		switch ($request->skill_id)
		{
			case $characterBase->active1:
                $skillNo = 1;
				break;
			case $characterBase->active2:
                $skillNo = 2;
				break;
			default:
                throw \App\Exceptions\MasterException::makeNotFound(
                    'character_base',
                    ['active1', 'active2'],
                    $request->skill_id
                );
		}

        $skillColumn = 'active_skill_' . $skillNo . '_lv';
        $skillLv = $playerCharacter->$skillColumn + 1;

        // グレードがスキルレベルの上限

        if ($skillLv > $playerCharacter->grade)
        {
            throw \App\Exceptions\GameException::make(
                'skill level is already grade: ' . $playerCharacter->grade
            );
        }

		// スキルのレベルアップに必要なp$取得

        $needDollars = Constant::skillUpConsts();
        $need = $needDollars[$skillLv - 2]->value1; // 最初の行が 2 になる用

        if ($need != $request->platinum_dollar)
        {
            throw \App\Exceptions\ParamException::makeNotEquals(
                '', '', 'platinum_dollar',
                $request->platinum_dollar, $need
            );
        }

		// p$が足りているか

		if ($player->platinum_dollar < $need)
		{
            throw \App\Exceptions\DataException::makeNotEnough(
                '', '', 'platinum_dollar',
                $need, $player->platinum_dollar
            );
		}

		{
			BaseGameModel::beginTransaction();

			// プレイヤキャラクタスキル強化

			$playerCharacter->$skillColumn = $skillLv;
			$playerCharacter->save();

            $playerCharacterLog = LogService::playerCharacterSkillLv(
                $playerCharacter, $skillNo, SrcType::SKILL_REINFORCE, 0
            );

			// p$消費

            $param = new GiveOrPayParam();
            $param->player = $player;
            $param->playerId = $request->player_id;
            $param->itemId = Item::ID_P_DOLLAR;
            $param->count = - $need;
            $param->srcType = SrcType::SKILL_REINFORCE;
            $param->srcId = $playerCharacterLog->id;

            PlayerService::giveOrPay($param);

            // ミッション達成チェック

            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerCharacter', $playerCharacter);

            $skillReinforceCount = PlayerCharacter::countSkillReinforce(
                $player->id
            );
            $missionChecker->addData(
                'skillReinforceCount', $skillReinforceCount
            );

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		$body = [
			'player_character'	=>
                PlayerCharacterResponse::make($player, $playerCharacter),
			'platinum_dollar'	=>
                $player->platinum_dollar,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * オーブ装備
	 *
	 * @param PlayerCharacterOrbEquipRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function orbEquip($request)
	{
        $response = self::getResponse($request);
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerCharacterOrbEquip::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤ取得

        $playerId = $request->player_id;

		$player = Player::find_($playerId);

		// プレイヤキャラクタ取得

		$playerCharacter = PlayerCharacter::find_(
            $request->player_character_id
        );

        $characterId = $playerCharacter->character_id;
        $grade = $playerCharacter->grade;

        // グレード上限では、装備もできない

        $maxGrade = Constant::gradeCurrentLimit();
        if ($grade >= $maxGrade->value1)
        {
            throw \App\Exceptions\GameException::make(
                'grade is already max: ' . $playerCharacter->grade
            );
        }

		// キャラクタ取得 (マスタ)

		$characterBase = CharacterBase::getOne($characterId);
		if (empty($characterBase))
		{
            throw \App\Exceptions\MasterException::makeNotFound(
                'character_base', 'character_base_id', $characterId
            );
		}

        $gradeupId = $characterBase->gradeup_id;

        // オーブ装備取得 (マスタ)

        $gradeup = Gradeup::getOne_([$gradeupId, $grade]);

        // アイテム ID を取得し、ついでに所持数が足りているかチェックする

        $useItems = $request->player_item_list;
        PlayerItemService::setupUseItems($useItems);

        // 消費前の個数を保存しておく

        $useItemsOrg = PlayerItemService::cloneItems($useItems);

        // オーブ装備枠は、6 枠

        $equipFlags = 0;
        $equipIds = array_pad([], PlayerCharacter::MAX_ORB_EQUIP, 0);
        $totalPrice = 0;
        $canGradeup = true;

        $equipMask = $request->equip_mask;

        for ($i = 1; $i <= PlayerCharacter::MAX_ORB_EQUIP; ++ $i)
        {
            $pcOrbProp = 'player_orb_id_' . $i;
            if (isset($playerCharacter->$pcOrbProp))
                continue; // 装備済み

            if ((($equipMask >> ($i - 1)) & 1) == 0)
            {
                // 装備予定の枠で無い場合
                $canGradeup = false; // 空きがある
                continue;
            }

            // 装備できるオーブを取得 //

            $orbProp = 'orb_equipment' . $i;
            $orbId = $gradeup->$orbProp;

            // 重複は無いはずなので、1 回も消費できないか、全部消費できるか

            $synthesized = OrbService::synthesize(
                $price, $useItems, [$orbId => 1]
            );

            $totalPrice += $price;

            if ($synthesized)
            {
                $equipFlags |= (1 << ($i - 1));
                $equipIds[$i - 1] = $orbId;
            }
            else
            {
                $canGradeup = false; // 空きがある
            }
        }

        // 合成費用が正しいかチェックする //

        if ($totalPrice != $request->platinum_dollar)
        {
            throw \App\Exceptions\ParamException::make(
                'platinum_dollar is too many / few: '
                . $totalPrice . ' != ' . $request->platinum_dollar
            );
        }

        // 消費して 0 になったかチェックする //

        PlayerItemService::checkUseItemsZero($useItems);

		{
			BaseGameModel::beginTransaction();

            $log = null;
            if ($canGradeup && $request->gradeUp())
            {
                // グレードアップする場合は、装備を全てはずす

                for ($i = 1; $i <= PlayerCharacter::MAX_ORB_EQUIP; ++ $i)
                {
                    $pcOrbProp = 'player_orb_id_' . $i;
                    $playerCharacter->$pcOrbProp = null;
                }

                $playerCharacter->grade += 1;

                $log = LogService::playerCharacterGradeUp(
                    $playerCharacter, $equipFlags, SrcType::GRADE_UP, 0
                );
            }
            else
            {
                // 装備する //

                for ($i = 1; $i <= PlayerCharacter::MAX_ORB_EQUIP; ++ $i)
                {
                    if (empty($equipIds[$i - 1]))
                        continue;

                    $itemId = $equipIds[$i - 1];

                    // 持っていないものを合成し、装備する場合、
                    // プレイヤアイテムの行が無い場合もある

                    $playerItem = PlayerItem::getOne($playerId, $itemId);
                    if (empty($playerItem))
                    {
                        $playerItem = new PlayerItem();
                        $playerItem->player_id = $playerId;
                        $playerItem->item_id = $itemId;
                        $playerItem->num = 0; // 個数は 0
                        $playerItem->save();
                    }

                    $pcOrbProp = 'player_orb_id_' . $i;
                    $playerCharacter->$pcOrbProp = $playerItem->id;
                }

                $log = LogService::playerCharacterOrbEquip(
                    $playerCharacter, $equipFlags, SrcType::ORB_EQUIP, 0
                );
            }

            $playerCharacter->save();

            // P$ を消費する //

            $param = new GiveOrPayParam();
            $param->playerId = $playerId;
            $param->itemId = Item::ID_P_DOLLAR;
            $param->count = - $request->platinum_dollar;
            $param->srcType = $log->src_type;
            $param->srcId = $log->id;

            PlayerService::giveOrPay($param);

            // アイテムを消費する //

            foreach ($useItemsOrg as $useItem)
            {
                $param = new GiveOrPayParam();
                $param->playerId = $playerId;
                $param->itemId = $useItem['item_id'];
                $param->count = - $useItem['item_num'];
                $param->srcType = SrcType::ORB_EQUIP;
                $param->srcId = $log->id;

                PlayerService::giveOrPay($param);
            }

            // ミッション達成チェック

            $missionChecker->addData('player', $player);
            $missionChecker->addData('playerCharacter', $playerCharacter);

            $orbEquipCount = PlayerCharacter::countOrbEquip(
                $player->id
            );
            $missionChecker->addData('orbEquipCount', $orbEquipCount);

            $gradeUpCounts = PlayerCharacterLog::getGradeUpCounts(
                $player->id
            );
            $missionChecker->addData('gradeUpCounts', $gradeUpCounts);

            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();
		}

		$body = [
			'player_character' =>
                PlayerCharacterResponse::make($player, $playerCharacter),
            'platinum_dollar' =>
                Player::getPDollar($playerId)
		];
		$response->body = $body;
		return $response;
	}

	/**
	 * お気に入り選択
	 *
	 * @param PlayerCharacterFavoriteSelectRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function favoriteSelect($request)
	{
        $response = self::getResponse($request);

        $playerId = $request->player_id;
        $charaId = $request->player_character_id;

		// プレイヤ取得

		$player = Player::find_($playerId);

		// プレイヤキャラクタ取得

		$playerCharacter = PlayerCharacter::find_($charaId);

        $set = $request->favorite_flag !=
                 PlayerCharacterResponse::FAVORITE_FLAG_NO;

        if (!$set && $player->favorite_character != $charaId)
        {
            throw \App\Exceptions\ParamException::make(
                'specified character is not favorite: ' . $charaId
            );
        }

		{
			BaseGameModel::beginTransaction();

			// お気に入り選択
            $player->favorite_character = $set ? $charaId : null;
			$player->save();

            // 1 SQL なので、トランザクション不要
            PlayerCommonCache::updateCache($player);

			BaseGameModel::commit();
		}

		$body = [
			'player_character' =>
              PlayerCharacterResponse::make($player, $playerCharacter),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * キャラクター個数取得
	 *
	 * @param object $param 付与/消費パラメータ
	 * @return integer 個数
	 */
	public static function getCount($param)
    {
        assert($param->itemType == Item::TYPE_CHARACTER);

        $model = PlayerCharacter::getOne($param->playerId, $param->itemId);
        return isset($model) ? 1 : 0;
    }

	/**
	 * キャラクター付与/消費
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPay($param)
    {
        if ($param->count == 0)
            return;

        if ($param->count < 0)
        {
            throw \App\Exceptions\GameException::make(
                'player_character can\'t be pay: ' . $param->itemId
            );
        }

        assert($param->count > 0);

        // プレイヤキャラクタを取得

        $playerCharacter = PlayerCharacter::getOne(
            $param->playerId, $param->itemId
        );

        if (!isset($playerCharacter))
        {
            if (!$param->onlyCheck)
            {
                // 新規追加

                $playerCharacter = PlayerCharacter::regist(
                    $param->playerId, $param->itemId
                );

                // 付与ログ

                $log = LogService::playerCharacterGiveOrPay(
                    $playerCharacter, $param->itemId, 1, 1,
                    $param->srcType, $param->srcId
                );
                $param->logs[] = $log;
            }

            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_CHARACTER;
            -- $param->count;
        }else{
            //重複ボーナス
            if($playerCharacter->repeat < Constants::REPEAT_BONUS_GOLD){
                $repeat = $playerCharacter->repeat + 1;
                PlayerCharacter::updateRepeatCount($playerCharacter->id, $repeat);
                $param->repeat = $repeat;
           }
        }

        // 欠片付与 //

        if ($param->count > 0)
        {
            // 欠片の数をマスタから取得し、かける
            $charaBase = CharacterBase::getOne_($param->itemId);
            $param->count *= $charaBase->piece_conversion;

            $fragmentId = Character::fragmentIdFrom($param->itemId);
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_CONVERT;
            $param->itemId = $fragmentId;

            return PlayerService::giveOrPayItem($param);
        }
    }

}
