<?php


namespace App\Services\KpiLogger;
use App\Utils\AwsSqsUtil;

/**
 * KPIログ出力の基底クラス
 * Class BaseKpiLogger
 * @package App\Services\KpiLogger
 */
class BaseKpiLogger
{
    static $table_name = '';

    /**
     * KPIログの登録
     * @param $data
     * @return bool
     */
    public function resistImpl($data)
    {
      return AwsSqsUtil::send(static::$table_name, $data);
    }
}
