<?php


namespace App\Services\KpiLogger;

/**
 * アイテム交換情報の出力クラス
 * Class ItemBuyLogger
 * @package App\Services\KpiLogger
 */
class ItemBuyLogger extends BaseKpiLogger
{
    static $table_name = 'item_buy_logger';

    /**
     * アイテム交換情報を配列に整形し、保存
     * @param $playerId
     * @param $playerLv
     * @param $shop
     * @param $shopExchange
     * @param $now
     * @return bool
     */
    public function register($playerId, $playerLv, $shop, $shopExchange, $now)
    {
        $data = [
            'player_id'=>$playerId,
            'player_lv'=>$playerLv,
            'shop_category'=>$shop->shop_category,
            'shop_id'=>$shop->id,
            'shop_exchange_id'=>$shopExchange->id,
            'lineup_category'=>$shopExchange->lineup_category,
            'lineup_id'=>$shopExchange->lineup_id,
            'pay_item_contents_id'=>$shopExchange->pay_item_contents_id,
            'pay_item_count'=>$shopExchange->pay_item_count,
            'now'=>$now
        ];

        return static::resistImpl($data);
    }

}
