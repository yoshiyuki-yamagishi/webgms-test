<?php


namespace App\Services\KpiLogger;


class QuestLogger extends BaseKpiLogger
{
    static $table_name = 'quest_logger';

    public function register($player, $playerBattle, $result, $missionId1, $missionId2, $missionId3,
                             $playerQuest, $playerBattleResult, $now)
    {
        $data = [
            'player_id'=>$player->player_id,
            'player_lv'=>$player->player_lv,
            'player_battle_id'=>$playerBattle->id,
            'quest_id'=>$playerBattle->quest_id,
            'continue_count'=>$playerBattle->continue_count,
            'result'=>$result,
            'mission_id_1'=>$missionId1,
            'mission_id_2'=>$missionId2,
            'mission_id_3'=>$missionId3,
            'mission_flag_1'=>$playerQuest->mission_flag_1,
            'mission_flag_2'=>$playerQuest->mission_flag_2,
            'mission_flag_3'=>$playerQuest->mission_flag_3,
            'total_damage_1'=>$playerBattleResult->total_damage_1,
            'total_damage_2'=>$playerBattleResult->total_damage_2,
            'total_damage_3'=>$playerBattleResult->total_damage_3,
            'total_receive_damage_1'=>$playerBattleResult->total_receive_damage_1,
            'total_receive_damage_2'=>$playerBattleResult->total_receive_damage_3,
            'total_receive_damage_3'=>$playerBattleResult->total_receive_damage_2,
            'turn_count_1'=>$playerBattleResult->turn_count_1,
            'turn_count_2'=>$playerBattleResult->turn_count_2,
            'turn_count_3'=>$playerBattleResult->turn_count_3,
            'now'=>$now
        ];

        return static::resistImpl($data);
    }


}
