<?php


namespace App\Services\KpiLogger;


class PlayerQuestCharacterLogger extends BaseKpiLogger
{
    static $table_name = 'player_quest_character_logger';

    public function register($playerBattleId, $playerCharacter, $now)
    {
        $data = [
            'player_battle_id'=>$playerBattleId,
            'player_character_id'=>$playerCharacter->player_character_id,
            'character_id'=>$playerCharacter->character_id,
            'evolve'=>$playerCharacter->evolve,
            'grade'=>$playerCharacter->grade,
            'character_lv'=>$playerCharacter->character_lv,
            'experience'=>$playerCharacter->experience,
            'player_orb_id_1'=>$playerCharacter->player_orb_id_1,
            'player_orb_id_2'=>$playerCharacter->player_orb_id_2,
            'player_orb_id_3'=>$playerCharacter->player_orb_id_3,
            'player_orb_id_4'=>$playerCharacter->player_orb_id_4,
            'player_orb_id_5'=>$playerCharacter->player_orb_id_5,
            'player_orb_id_6'=>$playerCharacter->player_orb_id_6,
            'active_skill_1_lv'=>$playerCharacter->active_skill_1_lv,
            'active_skill_2_lv'=>$playerCharacter->active_skill_2_lv,
            'now'=>$now

        ];
        return static::resistImpl($data);
    }




}
