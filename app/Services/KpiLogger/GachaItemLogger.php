<?php


namespace App\Services\KpiLogger;

/**
 * ガチャアイテムの情報出力クラス
 * Class GachaItemLogger
 * @package App\Services\KpiLogger
 */
class GachaItemLogger extends BaseKpiLogger
{
    static $table_name = 'gacha_item_logger';

    /**
     * ガチャアイテムの情報を配列に整形して保存。DBに保存されているデータと同じもの。
     * @param $result
     * @param $gacha
     * @param $now
     */
    public function register($result, $gacha, $now)
    {
        $data = [
            'player_gacha_id'=>$result->player_gacha_id,
            'item_type'=>$result->item_type,
            'item_id'=>$result->item_id,
            'item_count'=>$result->item_count,
            'gacha_result_flag'=>$gacha->gacha_result_flag,
            'take_flag'=>$gacha->take_flag,
            'now'=>$now
        ];

        static::resistImpl($data);
    }
}
