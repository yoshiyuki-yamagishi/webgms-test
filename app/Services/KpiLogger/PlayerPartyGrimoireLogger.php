<?php


namespace App\Services\KpiLogger;


class PlayerPartyGrimoireLogger extends BaseKpiLogger
{
    static $table_name = 'player_party_grimoire_logger';

    /**
     * @param $playerBattleId
     * @param $grimoire
     * @param $now
     * @return mixed
     */
    public function register($playerBattleId, $grimoire, $now)
    {
        $data = [
            'player_battle_id'=>$playerBattleId,
            'player_grimore_id'=>$grimoire->player_grimore_id,
            'grimoire_id'=>$grimoire->grimoire_id,
            'awake'=>$grimoire->awake,
            'slot_1'=>$grimoire->slot_1,
            'slot_2'=>$grimoire->slot_2,
            'slot_3'=>$grimoire->slot_3,
            'now'=>$now
        ];

        return static::resistImpl($data);
    }
}
