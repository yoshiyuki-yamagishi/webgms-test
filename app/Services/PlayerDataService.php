<?php
/**
 * プレイヤデータ のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Models\PlayerData;
use App\Models\BaseGameModel;



/**
 * プレイヤデータ のサービス
 *
 */
class PlayerDataService extends BaseService
{
	/**
	 * 実行
	 *
	 * @param PlayerDataGetRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function get($request)
	{
		$response = ApiResponse::getInstance();

		$playerDataList = PlayerData::getByPlayerId($request->player_id);

		$_playerDataList	= [];
		foreach ($playerDataList as $playerData)
		{
			$_playerDataList[] = [
				'type'	=> $playerData->type,
				'value'	=> $playerData->value,
			];
		}

		$body = [
			'player_data_list'	=> $_playerDataList,
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * 設定
	 *
	 * @param PlayerDataSetRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function set($request)
	{
		$response = ApiResponse::getInstance();
        
		$playerData = PlayerData::getByType(
            $request->player_id, $request->type
        );

		{
			BaseGameModel::beginTransaction();

			if (!isset($playerData))
			{
				$playerData = new PlayerData();
				$playerData->player_id	= $request->player_id;
				$playerData->type		= $request->type;
			}

			$playerData->value = $request->value;
			$playerData->save();

			BaseGameModel::commit();
		}

		return $response;
	}
}
