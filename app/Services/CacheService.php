<?php
/**
 * キャッシュ のサービス
 *
 */

namespace App\Services;

use App\Utils\DebugUtil;
use Illuminate\Support\Facades\Cache;

/**
 * キャッシュ のサービス
 *
 */
class CacheService extends BaseService
{
	/**
	 * タグを使用するかどうかを返す
	 *
	 * @return boolean true: タグを使う
	 */
    public static function useTags()
    {
        // タグを使わない場合のコード
        // return false; 
        
        $cacheDriver = config('cache.default');
        // DebugUtil::e_log('CS', 'cacheDriver', $cacheDriver);
        
        if ($cacheDriver == 'file')
            return false;
        return true;
    }
    
	/**
	 * キャッシュから値を取得する
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 * @return mixed キャッシュから取得した値
	 */
	public static function get($tags, $key)
	{
        if (self::useTags())
            return Cache::tags($tags)->get($key);
        else
            return Cache::get($key);
	}

	/**
	 * キャッシュに値を設定する
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 * @param string $value キャッシュに保存する値
	 */
	public static function set($tags, $key, $value)
	{
        // キャッシュは無期限で OK
        if (self::useTags())
            return Cache::tags($tags)->forever($key, $value);
        else
            return Cache::forever($key, $value);
	}

	/**
	 * キャッシュに値を設定する
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 * @param string $value キャッシュに保存する値
	 * @param integer/datetime $timeout 期限
	 */
	public static function setEx($tags, $key, $value, $timeout)
	{
        if (self::useTags())
            return Cache::tags($tags)->put($key, $value, $timeout);
        else
            return Cache::put($key, $value, $timeout);
	}
    
	/**
	 * キャッシュから値を削除する
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 */
	public static function clear($tags, $key)
	{
        if (self::useTags())
            return Cache::tags($tags)->forget($key);
        else
            return Cache::forget($key);
    }

	/**
	 * キャッシュから値を削除する
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 */
	public static function clearAll($tags)
	{
        if (self::useTags())
            return Cache::tags($tags)->flush();
        else
            return Cache::flush();
    }
    
	/**
	 * キャッシュの値存在チェック
	 *
	 * @param string $tags タグの配列
	 * @param string $key キー
	 */
	public static function has($tags, $key)
	{
        if (self::useTags())
            return Cache::tags($tags)->has($key);
        else
            return Cache::has($key);
    }
    
}
