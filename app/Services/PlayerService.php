<?php
/**
 * プレイヤ のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerResponse;
use App\Models\BaseCommonModel;
use App\Models\BaseGameModel;
use App\Models\Player;
use App\Models\PlayerCommon;
use App\Models\PlayerCommonCache;
use App\Models\PlayerData;
use App\Models\PlayerGrimoire;
use App\Models\PlayerItem;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Item;
use App\Services\MissionChecker\MissionCheckerPlayerUpdate;
use App\Services\PlayerSetupService;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * プレイヤ のサービス
 *
 */
class PlayerService extends BaseService
{
    private static $AES_METHOD = 'aes-128-cbc'; // 変更は不可
    private static $AES_KEY = 'fi*Eo4j#@G09>2sQ'; // 変更は不可
    private static $AES_OPTIONS = OPENSSL_RAW_DATA; // 変更は不可
    
	/**
	 * 登録
	 * @param PlayerRegistRequest $request
	 * @return ApiResponse
	 */
	public static function regist($request)
	{
		$response = ApiResponse::getInstance();

		{
			BaseCommonModel::beginTransaction();

			// プレイヤ共通登録
			$playerCommon = PlayerCommon::register(
                $request->unique_id,
                $request->os_type,
                $request->os_version,
                $request->model_name
            );

			// セッション開始
			SessionService::start(
                $playerCommon->player_id,
                SessionService::SS_REGIST,
                $playerCommon->db_no
            );

			BaseGameModel::beginTransaction();

            // 初期設定を行う

            $setupService = new PlayerSetupService($playerCommon->player_id);
            $setupService->setup();

            // 法律上の誕生日を更新する

            if (!empty($request->legal_birthday))
            {
                $setupService->player->legal_birthday
                    = $request->legal_birthday;
                $setupService->player->save();
            }

            // 共通 DB のプレイヤ共通キャッシュを更新する //
            PlayerCommonCache::updateCache($setupService->player);

			BaseGameModel::commit();
			BaseCommonModel::commit();
		}

		SessionService::end();

		$body = [
			'player_id'	=> $playerCommon->player_id,
		];

		$response->body = $body;

		return $response;
	}

	/**
	 * 取得
	 *
	 * @param PlayerGetRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function get($request)
	{
		$response = ApiResponse::getInstance();

		// プレイヤの取得
		$player = Player::find_($request->player_id);

		$response->body = [
			'player' => PlayerResponse::make($player)
		];

		return $response;
	}

	/**
	 * 更新
	 *
	 * @param PlayerUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
		$response = ApiResponse::getInstance();
        $now = $response->currentDateDB();

        // ミッションチェッカーを初期化

        $missionChecker = MissionCheckerPlayerUpdate::getInstance();
        $missionChecker->init($request->player_id, $now);

		// プレイヤの取得
        
		$player = Player::find_($request->player_id);

        // 法律上の誕生日は変更できない

        if (!empty($request->legal_birthday) &&
            isset($player->legal_birthday))
        {
            $dbYmd = DateTimeUtil::DB_to_YMD($player->legal_birthday);
            if ($request->legal_birthday != $dbYmd)
            {
                throw \App\Exceptions\GameException::make(
                    'player\'s legal_birthday can\'t be changed'
                );
            }
        }

        // 使用不可文言のチェック

        $resWordService = RestrictionWordService::getInstance();
        $resWordService->check('message', $request->message);
        $resWordService->check('player_name', $request->player_name);

		// プレイヤの更新
		{
			BaseGameModel::beginTransaction();
            
			if (!empty($request->player_name)) // 空文字列 も empty
                $player->player_name = $request->player_name;
			if (!empty($request->gender)) // 0 も empty
                $player->gender = $request->gender;
			if (!empty($request->message)) // 空文字列 も empty
                $player->message = $request->message;
			if (!empty($request->birthday)) // 空文字列 も empty
                $player->birthday = $request->birthday;
			if (!empty($request->legal_birthday) &&
                !isset($player->legal_birthday))
                $player->legal_birthday = $request->legal_birthday;
            
			$player->save();

            // ホームミッション更新
            
            $missionChecker->addData(
                'player', $player
            );
                
            $achiveMissionList = [];
            $missionChecker->updateMission($achiveMissionList, false);

			BaseGameModel::commit();

            // 1 SQL なので、トランザクション不要
            // 失敗しても、さほど、問題無し
            PlayerCommonCache::updateCache($player);
		}

		$response->body = [
			'player' => PlayerResponse::make($player),
		];

		return $response;
	}

	/**
	 * チュートリアル進行度更新
	 *
	 * @param PlayerTutorialUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function tutorialUpdate($request)
	{
        // DebugUtil::e_log('PS_tutorialUpdate', 'request', $request->all());
		$response = ApiResponse::getInstance();

        $player = Player::find_($request->player_id);

        // 元には戻らない

        if ($player->tutorial_progress > $request->tutorial_progress)
        {
            throw \App\Exceptions\GameException::make(
                'tutorial progress must be larger than now: '
                . $player->tutorial_progress
            );
        }

        $upNum = $request->tutorial_progress - $player->tutorial_progress;
        if ($player->tutorial_progress != $request->tutorial_progress)
		{
			// トランザクション開始
            
			BaseGameModel::beginTransaction();

            // データ更新

            $player->tutorial_progress = $request->tutorial_progress;
            $player->tutorial_flag = $request->tutorial_flag;
            $player->save();

            // ログ更新

            LogService::playerTutorialUpdate(
                $player, $upNum, 0, 0
            );

            // コミット
            
			BaseGameModel::commit();
		}
        
		return $response;
    }
    

	/**
	 * AL回復
	 *
	 * @param PlayerAlRecoverRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function alRecover($request)
	{
		$response = ApiResponse::getInstance();
        $now = $response->currentDateDB();

        $player = Player::find_($request->player_id);

        $item = null;
        $playerItem = null;
        $alPer1 = 0;
        $alCount = 0;
        $useId = 0;
        $useCount = 0;
        
        if (isset($request->crystal_num))
        {
            $useId = Item::ID_FREE_BLUE_CRYSTAL;
            $useCount = $request->crystal_num;
            
            // 蒼の結晶消費
            
            $alRecoverCrystal = Constant::getOne_(Constant::AL_RECOVER_CRYSTAL);

            $unitNum = $alRecoverCrystal->value1;
            $alPer1 = $alRecoverCrystal->value2;

            // 回復量の計算
            
            if ($alPer1 <= 0)
            {
                // 0 以下の場合は、現在の AL 最大値分回復する
                $alPer1 = $player->max_al;
            }

            // 個数チェック
            
            if ($request->crystal_num < $unitNum)
            {
                throw \App\Exceptions\ParamException::make(
                    'crysal_num must be larger or equals: ' . $unitNum
                );
            }
            if (($request->crystal_num % $unitNum) != 0)
            {
                throw \App\Exceptions\ParamException::make(
                    'crysal_num must be multiple of : ' . $unitNum
                );
            }
            
            static::canPay(
                $request->player_id, Item::TYPE_ITEM, $useId, $useCount
            );

            $alCount = intdiv($request->crystal_num, $unitNum);
        }
        else if (isset($request->item_id, $request->item_num))
        {
            $useId = $request->item_id;
            $useCount = $request->item_num;
            
            // アイテム消費
            
            $itemCategory = Item::categoryFromId($request->item_id);

            if ($itemCategory != Item::CATEGORY_AL_RECOVER)
            {
                throw \App\Exceptions\ParamException::makeInvalid(
                    '', 'item_id', $request->item_id
                );
            }

            $item = Item::getOne($request->item_id);
            if (!isset($item))
            {
                throw \App\Exceptions\MasterException::makeInvalid(
                    'item', 'item_id', $request->item_id
                );
            }

            $playerItem = PlayerItem::getOne(
                $request->player_id, $request->item_id
            );

            if (!isset($playerItem))
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'item_id', $request->item_id, 
                    $request->item_num, 0
                );
            }
            else if ($playerItem->num < $request->item_num)
            {
                throw \App\Exceptions\DataException::makeNotEnough(
                    'player_item', 'item_id', $request->item_id, 
                    $request->item_num, $playerItem->num
                );
            }

            $alPer1 = $item->value;
            $alCount = $request->item_num;
        }
        else
        {
            throw \App\Exceptions\ParamException::make(
                '(crystal_num) / (item_id and item_num) is need'
            );
        }

		{
			// トランザクション開始
			BaseGameModel::beginTransaction();

            // 自然回復

            $player->updateAl($now, true);

            // AL回復
                
            $player->giveAl($now, $alPer1 * $alCount, true, true);

            // ログ更新

            $log = LogService::playerAlRecover(
                $player, $alPer1 * $alCount, $useId, SrcType::AL_RECOVER, 0
            );
            
            // アイテムの消費

            $param = new GiveOrPayParam();
            $param->playerId = $request->player_id;
            $param->itemType = Item::TYPE_ITEM;
            $param->itemId = $useId;
            $param->count = - $useCount;
            $param->srcType = SrcType::AL_RECOVER;
            $param->srcId = $log->id;
            static::giveOrPay($param);

			BaseGameModel::commit();
		}

		$body = [
			'al' => $player->al,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * 引継ぎ元
	 *
	 * @param PlayerTransitSrcRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function transitSrc($request)
	{
		$response = ApiResponse::getInstance();

		// TODO:引継ぎコード生成方法
		$passcode = self::_createPasscode(
            $request->player_id, $request->password
        );

		{
			BaseCommonModel::beginTransaction();
            
			PlayerCommon::registPasscode(
                $request->player_id, $passcode, $request->password
            );
            
			BaseCommonModel::commit();
		}

		$body = [
			'passcode'	=> $passcode,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * 引継ぎ先
	 *
	 * @param PlayerTransitDstRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function transitDst($request)
	{
		$response = ApiResponse::getInstance();

		// 引継ぎ元 (ややこしいが、transit_dst で指定のプレイヤ)
		$playerCommonSrc = PlayerCommon::getByPlayerId($request->player_id);
		if (!isset($playerCommonSrc))
		{
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $request->player_id
            );
		}

		// 引継ぎ先 (ややこしいが、transit_src で指定のプレイヤ)
		$playerCommonDst = PlayerCommon::getByPasscodeAndPassword(
            $request->passcode, $request->password
        );
		if (!isset($playerCommonDst))
		{
            throw \App\Exceptions\WrongPassException::makeNotFound(
                'player_common',
                ['passcode', 'password'],
                [$request->passcode, $request->password]
            );
		}

		// 元と先のプレイヤが同じ
        if ($playerCommonSrc->player_id == $playerCommonDst->player_id)
        {
            throw \App\Exceptions\ParamException::make(
                'passcode\'s player_id is the same: ' . $request->player_id
            );
        }

		// トランザクション開始
        {
            BaseCommonModel::beginTransaction();

            $playerCommonDst->unique_id	= $playerCommonSrc->unique_id;
            $playerCommonDst->os_type = $request->os_type;
            $playerCommonDst->os_version = $request->os_version;
            $playerCommonDst->model_name = $request->model_name;
            $playerCommonDst->passcode	= null;
            $playerCommonDst->password	= null;
            $playerCommonDst->save();
            
            $playerCommonSrc->valid_flag = PlayerCommon::VALID_FLAG_NO;
            $playerCommonSrc->save();
        
            BaseCommonModel::commit();
        }

		$body = [
			'player_id'	=> $playerCommonDst->player_id,
		];

		$response->body = $body;
		return $response;
	}

	/**
	 * アカウント開始
	 *
	 * @param PlayerAccountStartRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function accountStart($request)
	{
		$response = ApiResponse::getInstance();

        $playerCommon = PlayerCommon::getByPlayerId($request->player_id);
        if (!isset($playerCommon))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $request->player_id
            );
        }

        if ($playerCommon->os_type != $request->os_type)
        {
            throw \App\Exceptions\GameException::make(
                'os_type is different: ' . $playerCommon->os_type
            );
        }

        // トランザクション開始
        {
            BaseCommonModel::beginTransaction();

            $playerCommon->account_token = $request->token;
            $playerCommon->save();

            BaseCommonModel::commit();
        }
        
		$body = [
		];
		$response->body = $body;
		return $response;
    }

	/**
	 * アカウント終了
	 *
	 * @param PlayerAccountEndRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function accountEnd($request)
	{
		$response = ApiResponse::getInstance();

        $playerCommon = PlayerCommon::getByPlayerId($request->player_id);
        if (!isset($playerCommon))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $request->player_id
            );
        }

        // トランザクション開始
        {
            BaseCommonModel::beginTransaction();

            $playerCommon->account_token = null;
            $playerCommon->save();

            BaseCommonModel::commit();
        }
        
		$body = [
		];
		$response->body = $body;
		return $response;
    }
    
	/**
	 * アカウント情報
	 *
	 * @param PlayerAccountInfoRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function accountInfo($request)
	{
        // DebugUtil::e_log('PS_accountInfo', 'request', $request->all());
        return static::_accountInfoRestore($request);
    }
    
	/**
	 * アカウント復元
	 *
	 * @param PlayerAccountRestoreRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function accountRestore($request)
	{
        // DebugUtil::e_log('PS_accountRestore', 'request', $request->all());
        return static::_accountInfoRestore($request);
    }
    
	/**
	 * アカウント復元/復元
	 *
	 * @param PlayerAccount(Info/Restore)Request $request
	 * @return ApiResponse レスポンス
	 */
	private static function _accountInfoRestore($request)
	{
		$response = ApiResponse::getInstance();

        $restore = false;
        if ($request instanceof \App\Http\Requests\PlayerAccountRestoreRequest)
            $restore = true;

        // 引継ぎ元
            
        $playerCommonSrc = PlayerCommon::getByPlayerId($request->player_id);
        if (!isset($playerCommonSrc))
        {
            throw \App\Exceptions\DataException::makeNotFound(
                'player_common', 'player_id', $request->player_id
            );
        }

        // OS 種別のチェック

        if ($playerCommonSrc->os_type != $request->os_type)
        {
            throw \App\Exceptions\GameException::make(
                'os_type (now) is different: ' . $playerCommonSrc->os_type
            );
        }

        // 引継ぎ先
        
        $playerCommonDst = PlayerCommon::getByAccountToken(
            $request->os_type, $request->token
        );
        if (!isset($playerCommonDst))
        {
            throw \App\Exceptions\ParamException::makeNotFound(
                'player_common',
                ['os_type', 'player_id'],
                [$request->os_type, $request->token]
            );
        }

        // OS 種別のチェック
        
        if ($playerCommonDst->os_type != $request->os_type)
        {
            throw \App\Exceptions\GameException::make(
                'os_type (dst) is different: ' . $playerCommonDst->os_type
            );
        }

        // 同一プレイヤチェック
            
        if ($restore && ($request->player_id == $playerCommonDst->player_id))
        {
            // レストアしない場合は、同じプレイヤIDでも問題ない //
            throw \App\Exceptions\ParamException::make(
                'dst_player_id is same'
            );
        }

        // プレイヤ取得
        
        $player = Player::find_($playerCommonDst->player_id);

        // トランザクション開始
        
        if ($restore)
        {
            BaseCommonModel::beginTransaction();

            $playerCommonDst->unique_id	= $playerCommonSrc->unique_id;
            $playerCommonDst->save();

            $playerCommonSrc->valid_flag = PlayerCommon::VALID_FLAG_NO;
            $playerCommonSrc->save();
                
            BaseCommonModel::commit();
        }

		$body = [
            'player_id' => $playerCommonDst->player_id,
            'player_disp_id' => $playerCommonDst->player_disp_id,
			'player' => PlayerResponse::make($player)
		];

		$response->body = $body;
		return $response;
	}


	/**
	 * 引継ぎコードを生成する。
	 *
	 * @return string 引継ぎコード
	 */
	private static function _createPasscode($playerId, $password)
	{
		// TODO:引継ぎコード生成方法
// 		$passcode = mt_rand(1000000000, 999999999);
		$passcode = "";
		$str = "0123456789ABCDEFGHJKLMNPQRSTUVWXYZ";
		$len = strlen($str) - 1;
		for ($i = 0; $i < 10; $i++)
		{
			$n = (int)mt_rand(0, $len - 1);
			$passcode .= substr($str, $n, 1);
		}

		return $passcode;
	}

	/**
	 * 汎用消費チェック
	 *
	 * @param integer $playerId プレイヤID
	 * @param integer $itemType アイテム種別
	 * @param integer $itemId アイテム
	 * @param integer $count 個数
	 */
	public static function canPay(
        $playerId, $itemType, $itemId, $count
    )
    {
        $param = new GiveOrPayParam();
        $param->playerId = $playerId;
        $param->itemType = $itemType;
        $param->itemId = $itemId;
        $param->count = - $count;
        $param->onlyCheck = true;
        return static::giveOrPay($param);
    }

	/**
	 * 汎用付与チェック
	 *
	 * @param integer $takeFlag 取得フラグ
	 * @param integer $playerId プレイヤID
	 * @param integer $itemType アイテム種別
	 * @param integer $itemId アイテム
	 * @param integer $count 個数
	 */
	public static function canGive(
        &$takeFlag, $playerId, $itemType, $itemId, $count
    )
    {
        $param = new GiveOrPayParam();
        $param->playerId = $playerId;
        $param->itemType = $itemType;
        $param->itemId = $itemId;
        $param->count = $count;
        $param->onlyCheck = true;
        $ret = static::giveOrPay($param);
        $takeFlag = $param->takeFlag;
        return $ret;
    }

	/**
	 * 汎用個数取得
	 *
	 * @param object $param 付与/消費パラメータ
	 * @return integer 個数
	 */
	public static function getCount($param)
	{
        switch ($param->itemType)
        {
		case Item::TYPE_CHARACTER:
			// キャラクタ
			return PlayerCharacterService::getCount($param);
		case Item::TYPE_GRIMOIRE:
			// 魔道書
            return PlayerGrimoireService::getCount($param);
		case Item::TYPE_ITEM:
            // アイテム
            return PlayerItemService::getCount($param);
        default:
            throw \App\Exceptions\ParamException::makeInvalid(
                '', 'item_type', $param->itemType
            );
        }
    }
    

	/**
	 * 汎用付与/消費
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPay($param)
	{
		switch ($param->itemType)
		{
		case Item::TYPE_CHARACTER:
			// キャラクタ
			return PlayerCharacterService::giveOrPay($param);
		case Item::TYPE_GRIMOIRE:
			// 魔道書
            return PlayerGrimoireService::giveOrPay($param);
		case Item::TYPE_ITEM:
            // アイテム            
            return static::giveOrPayItem($param);
        default:
            throw \App\Exceptions\ParamException::makeInvalid(
                '', 'item_type', $param->itemType
            );
		}
    }

	/**
	 * 汎用付与/消費 (アイテム)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPayItem($param)
    {
        // TODO: 将来は、廃止する過渡的な処理
        // 有償の蒼の結晶で -1 を指定してある場合がある
        if ($param->itemId == -1)
            $param->itemId = Item::ID_BLUE_CRYSTAL;

        $category = Item::categoryFromId($param->itemId);
        switch ($category)
        {
        case Item::CATEGORY_FREE_BLUE_CRYSTAL:
        case Item::CATEGORY_BLUE_CRYSTAL:
            return PlayerBlueCrystalService::giveOrPay($param);
        case Item::CATEGORY_FRIEND_POINT:
            return static::giveOrPayFriendPoint($param);
        case Item::CATEGORY_P_DOLLAR:
            return static::giveOrPayPDollar($param);
        case Item::CATEGORY_TICKET:
        case Item::CATEGORY_EXP:
        case Item::CATEGORY_ORB:
        case Item::CATEGORY_SKIP_TICKET:
        case Item::CATEGORY_FRAGMENT:
        case Item::CATEGORY_AL_RECOVER:
        case Item::CATEGORY_EVENT:
            return PlayerItemService::giveOrPay($param);
        case Item::CATEGORY_ELEMENT:
            return static::giveOrPayElement($param);
        case Item::CATEGORY_POWDER:
            return static::giveOrPayPowder($param);
        default:
            throw \App\Exceptions\ParamException::makeNotFound(
                'item (category)', 'item_id', $param->itemId
            );
        }
    }

	/**
	 * 汎用付与/消費 (プレイヤ内のポイント)
	 *
	 * @param object $param 付与/消費パラメータ
	 * @param string $colName 列名
	 */
	public static function giveOrPayPlayerPoint($param, $colName)
    {
        if (isset($param->player))
            $player = $param->player;
        else
            $player = Player::find_($param->playerId);

        if ($param->count < 0)
        {
            if ($player->$colName < - $param->count)
            {
                throw \App\Exceptions\NotEnoughException::makeNotEnough(
                    'player', '', $colName,
                    - $param->count, $player->$colName
                );
            }
        }

        if (!$param->onlyCheck)
        {
            // TODO: 上限カット処理
            
            $player->$colName += $param->count;
            $player->save();
            $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;

            // 消費ログ

            $log = LogService::playerGiveOrPay(
                $player, $param->itemId, $param->count, $player->$colName,
                $param->srcType, $param->srcId
            );
            $param->logs[] = $log;
        }
        
    }

	/**
	 * 汎用付与/消費 (P$)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPayPDollar($param)
    {
        return static::giveOrPayPlayerPoint($param, 'platinum_dollar');
    }
    
	/**
	 * 汎用付与/消費 (欠片パウダー)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPayPowder($param)
    {
        return static::giveOrPayPlayerPoint($param, 'powder');
    }
    
	/**
	 * 汎用付与/消費 (魔素)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPayElement($param)
    {
        return static::giveOrPayPlayerPoint($param, 'magic_num');
    }

	/**
	 * 汎用付与/消費 (フレンドポイント)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPayFriendPoint($param)
    {
        return static::giveOrPayPlayerPoint($param, 'friend_point');
    }

}
