<?php
/**
 * ガチャ履歴 のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Utils\DateTimeUtil;



/**
 * ガチャ履歴 のサービス
 *
 */
class GachaHistoryService extends BaseService
{
	/**
	 * 一覧
	 *
	 * @param GachaHistoryListRequest $request リクエスト
	 * @return ApiResponse レスポンス
	 */
	public static function list($request)
	{
		$response = ApiResponse::getInstance();

		// TODO:ガチャ履歴取得

		// TODO:レスポンス生成
		$gachaHistoryList = [];
		$gachaHistoryList[] = [
			'gacha_id'		=> 1,
			'gacha_name'	=> 'ガチャ名_01',
			'gacha_date'	=> DateTimeUtil::getNOW(),
			'gacha_type'	=> 2,
			'general_id'	=> 1,
		];
		$gachaHistoryList[] = [
			'gacha_id'		=> 2,
			'gacha_name'	=> 'ガチャ名_02',
			'gacha_date'	=> DateTimeUtil::getNOW(),
			'gacha_type'	=> 2,
			'general_id'	=> 2,
		];

		$_gachaHistoryList = [];
		foreach ($gachaHistoryList as $gachaHistory)
		{
			$_gachaHistoryList[]	= [
				'gacha_id'	=> $gachaHistory['gacha_id'],
				'gacha_name'	=> $gachaHistory['gacha_name'],
				'gacha_date'	=> $gachaHistory['gacha_date'],
				'gacha_type'	=> $gachaHistory['gacha_type'],
				'general_id'	=> $gachaHistory['general_id'],
			];
		}

		$body = [
			'gacha_history_list'	=> $_gachaHistoryList,
		];

		$response->body = $body;

		return $response;
	}
}