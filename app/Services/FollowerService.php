<?php
/**
 * フォロワー のサービス
 *
 */

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Http\Responses\ApiResponse;
use App\Models\PlayerFollow;



/**
 * フォロワー のサービス
 *
 */
class FollowerService extends BaseService
{
	public static function list($request)
	{
		$response = ApiResponse::getInstance();

		// フォロワー一覧
		$followerList = PlayerFollow::playerIdEqual($request->player_id)->get();

		$followerPlayer = [];
		foreach($followerList as $follower)
		{
			if (isset($follower->follower))
			{
				$followerPlayer[] = [
					'player_id'		=> $follower->follower_id,
					'player_name'	=> $follower->follower->player_name
				];
			}
		}

		$body = [
			'follower_list'	=> $followerPlayer
		];

		$response->body = $body;

		return $response;
	}
}
