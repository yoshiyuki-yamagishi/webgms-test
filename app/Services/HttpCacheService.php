<?php
/**
 * HTTP キャッシュサービス
 *
 */

namespace App\Services;
use App\Utils\DebugUtil;

/**
 * HTTP キャッシュサービス
 *
 */
class HttpCacheService extends BaseService
{
	/**
	 * HTTP 取得
     *
	 * @param list $tags キャッシュタグ
	 * @param string $key キー
	 * @param string $url URL
	 * @param integer $cacheTimeout キャッシュの有効期間 (分)
	 * @return HTTP レスポンス
	 */
	public static function get($tags, $key, $url, $cacheTimeout)
    {
        $value = CacheService::get($tags, $key);
        if (!is_null($value))
        {
            // DebugUtil::e_log('HCS', 'return cache', $value);
            return $value;
        }
        
        $curl = curl_init($url);
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        ]);

        $value = curl_exec($curl);
        curl_close($curl);

        CacheService::setEx($tags, $key, $value, $cacheTimeout);
        // DebugUtil::e_log('HCS', 'return http', $value);
        return $value;
    }

	/**
	 * HTTP キャッシュクリア
     *
	 * @param list $tags キャッシュタグ
	 * @param string $key キー
	 */
	public static function clear($tags, $key)
    {
        CacheService::clear($tags, $key);
    }
    
}
