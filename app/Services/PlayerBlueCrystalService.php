<?php
/**
 * プレイヤ蒼の結晶 のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Item;
use App\Models\PlayerBlueCrystal;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * プレイヤ蒼の結晶 のサービス
 *
 */
class PlayerBlueCrystalService extends BaseService
{
	/**
	 * 汎用付与/消費 (蒼の結晶)
	 *
	 * @param object $param 付与/消費パラメータ
	 */
	public static function giveOrPay($param)
    {
        if ($param->count == 0)
            return;

        $charged = false;
        $category = Item::categoryFromId($param->itemId);
        switch ($category)
        {
        case Item::CATEGORY_FREE_BLUE_CRYSTAL:
            $charged = false;
            break;
        case Item::CATEGORY_BLUE_CRYSTAL:
            $charged = true;
            break;
        default:
            throw \App\Exceptions\UnknownException::make(
                'bad PlayerBlueCrystal::giveOrPay call item_id: '
                . $param->itemId
            );
        }

        $now = ApiResponse::getInstance()->currentDateDB();

        // 所持個数を取得

        $freeNum = 0;
        $bothNum = PlayerBlueCrystal::getBothNum(
            $freeNum, $param->playerId, $now
        );
        $chargeNum = $bothNum - $freeNum;
        
        if ($param->count > 0)
            return self::_give($param, $now, $charged, $freeNum, $chargeNum);
        
        return self::_pay($param, $now, $charged, $freeNum, $chargeNum);
    }

	/**
	 * 汎用付与 (蒼の結晶)
	 *
	 * @param object $param 付与/消費パラメータ
	 * @param string $now 現在日付
	 * @param boolean $charged true: 有償のみ
	 * @param integer $freeNum 蒼の結晶の個数(無償)
	 * @param integer $chargeNum 蒼の結晶の個数(有償)
	 */
	public static function _give($param, $now, $charged, $freeNum, $chargeNum)
    {
        if (!$param->onlyCheck)
        {
            $playerBlueCrystal = PlayerBlueCrystal::regist(
                $param->playerId, $param->paymentType, 
                $param->srcType, $param->srcId, $param->count, null
            );

            // 付与ログ
                
            $log = LogService::playerBlueCrystalGiveOrPay(
                $playerBlueCrystal, $param->itemId, $param->count,
                $charged ? $chargeNum : $freeNum,
                $param->srcType, $param->srcId
            );
            $param->logs[] = $log;
        }
        
        $param->takeFlag |= GiveOrPayParam::TAKE_FLAG_TAKE;
    }

	/**
	 * 汎用消費 (蒼の結晶)
	 *
	 * @param object $param 付与/消費パラメータ
	 * @param string $now 現在日付
	 * @param boolean $charged true: 有償のみ
	 * @param integer $freeNum 蒼の結晶の個数(無償)
	 * @param integer $chargeNum 蒼の結晶の個数(有償)
	 */
	public static function _pay($param, $now, $charged, $freeNum, $chargeNum)
    {
        $payNum = - $param->count;
            
        // 個数チェック

        $checkNum = $charged ? $chargeNum : ($freeNum + $chargeNum);
        
        if ($checkNum < $payNum)
        {
            throw \App\Exceptions\DataException::makeNotEnough(
                '', '', 'player_blue_crystal',
                - $param->count, $checkNum
            );
        }

        if ($param->onlyCheck)
            return;

        // 消費

        $playerBlueCrystals = PlayerBlueCrystal::getForPay(
            $param->playerId, PlayerBlueCrystal::bothOrCharged($charged)
        );

		foreach ($playerBlueCrystals as $playerBlueCrystal)
		{
			if ($playerBlueCrystal->num >= $payNum)
            {
                // 減らす数以上だったら、そのまま減らして終了
                $substractNum = $payNum;
            }
            else
            {
                // 減らす数の方が大きかったら、
                // そのレコードは 0 にして次のレコードを減らす
                $substractNum = $playerBlueCrystal->num;
            }
                
            // 蒼の結晶数を減らす
                
            $playerBlueCrystal->num -= $substractNum;
            $playerBlueCrystal->save();

            // 消費ログ

            $_charged = $playerBlueCrystal->isCharged(); // 行ごとの有償/無償
            
            if ($_charged)
            {
                $itemId = Item::ID_BLUE_CRYSTAL;
                $chargeNum -= $substractNum;
            }
            else
            {
                $itemId = Item::ID_FREE_BLUE_CRYSTAL;
                $freeNum -= $substractNum;
            }

            $log = LogService::playerBlueCrystalGiveOrPay(
                $playerBlueCrystal, $itemId, - $substractNum,
                $_charged ? $chargeNum : $freeNum,
                $param->srcType, $param->srcId
            );
            $param->logs[] = $log;

            // 支払い数を減少

            $payNum -= $substractNum;
            assert($payNum >= 0);

            if ($payNum <= 0)
            {
                return;
            }
		}

        if ($payNum > 0)
        {
            // ココには来ないハズだが一応・・・
            throw \App\Exceptions\DataException::makeNotEnough(
                '', '', 'player_blue_crystal',
                - $param->count, $checkNum
            );
        }
    }

}
