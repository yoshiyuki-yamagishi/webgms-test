<?php
/**
 * オーブ のサービス
 *
 */

namespace App\Services;
use App\Utils\DebugUtil;
use App\Models\MasterModels\Item;

/**
 * オーブ のサービス
 *
 */
class OrbService extends BaseService
{
    public static function synthesize(&$price, &$useItems, $useIds)
    {
        $price = 0;
        $updated = false;
        $ok = self::_synthesizeImpl($updated, $price, $useItems, $useIds);

        if (!$ok)
        {
            if ($updated) // 完成していないのに、消費した場合は、エラー
            {
                throw \App\Exceptions\ParamException::make(
                    'player_item_list is too many / few'
                );
            }
            $price = 0; // 合成にかかる費用は 0
            return false;
        }

        return $ok;
    }
    
    protected static function _synthesizeImpl(
        &$updated, &$price, &$useItems, $useIds
    )
    {
        // DebugUtil::e_log('synthesize', 'updated', $updated);
        // DebugUtil::e_log('synthesize', 'price', $price);
        // DebugUtil::e_log('synthesize', 'useItems', $useItems);
        // DebugUtil::e_log('synthesize', 'useIds', $useIds);
        
        $nextIds = [];
        
        foreach ($useIds as $key => $count)
        {
            foreach ($useItems as &$useItem)
            {
                if ($useItem['item_id'] == $key)
                {
                    // 見つかった場合は、減らす

                    $sub = min($count, $useItem['item_num']);

                    $useItem['item_num'] -= $sub;
                    $useIds[$key] -= $sub;
                    $updated = true;
                }
            }

            if ($useIds[$key] > 0) // 残ってる場合
            {
                $synthCount = $useIds[$key];
                            
                // 分割して、追加する //

                $item = Item::getOne($key);
                if (empty($item))
                {
                    throw \App\Exceptions\MasterException::makeNotFound(
                        'item', 'item_id', $key
                    );
                }

                for ($i = 1; $i <= 4; ++ $i)
                {
                    $synthProp = 'synthesis' . $i;
                    $synthCountProp = 'synthesis_count' . $i;

                    if ($i == 1 && $item->$synthProp <= 0)
                        return false; // 合成不可なアイテムが残ってる

                    if ($item->$synthProp <= 0)
                        continue; // 合成に不要

                    // 合成元の個数
                    $nextIds[$item->$synthProp]
                        = $item->$synthCountProp * $synthCount;
                }
                
                // 合成にかかる費用
                $price += $item->synthesis_cost * $synthCount;
            }
        }

        if (empty($nextIds))
            return true;

        return self::_synthesizeImpl($updated, $price, $useItems, $nextIds);
    }
    
}