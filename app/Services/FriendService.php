<?php
/**
 * フレンド のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\FriendListResponse;
use App\Models\PlayerFriend;
use App\Utils\DebugUtil;

/**
 * フレンド のサービス
 *
 */
class FriendService extends BaseService
{
    const TYPE_FOLLOW = 1;
    const TYPE_FOLLOWER = 2;
    const TYPE_FRIEND = 3;
    const TYPE_RECOMEND = 4;

    const SORT_LAST_LOGIN_AT = 1; // : 最終ログイン時間：昇順
    const SORT_LAST_LOGIN_AT_REV = 2; // : 最終ログイン時間：降順
    const SORT_PLAYER_LV = 3; // : プレイヤーレベル：昇順
    const SORT_PLAYER_LV_REV = 4; // : プレイヤーレベル：降順
    
	/**
	 * 一覧
	 *
	 * @param FriendListRequest $request
	 * @return ApiResponse
	 */
	public static function list($request)
	{
        // DebugUtil::e_log('FS_list', 'request', $request->all());
		$response = ApiResponse::getInstance();
        $now = $response->currentDateDB();

        $useFollow = true;
        $list = PlayerFriend::get(
            $request->player_id,
            $request->friend_type,
            $request->search_id,
            $now,
            $request->sort_order,
            $request->from, 
            $request->count,
            $useFollow
        );
        // DebugUtil::e_log('FS_list', 'list', $list);

        //  ナイーブな実装

        $followNum = PlayerFriend::getCount(
            $request->player_id, self::TYPE_FOLLOW
        );
        $followerNum = PlayerFriend::getCount(
            $request->player_id, self::TYPE_FOLLOWER
        );
        $friendNum = PlayerFriend::getCount(
            $request->player_id, self::TYPE_FRIEND
        );

		$body = [
            'follow_num' => $followNum - $friendNum,
            'follower_num' => $followerNum - $friendNum,
            'friend_num' => $friendNum,
            'friend_list' => FriendListResponse::make($list, $useFollow),
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * ソート種別、降順を昇順に変換する
	 *
	 * @param integer $sortType ソート種別
	 * @return integer ソート種別 (昇順の方)
	 */
	public static function sortColumnNo($sortType)
    {
        return $sortType - (1 - ($sortType % 2));
    }

	/**
	 * ソート種別から DB 列名を取得する
	 *
	 * @param integer $sortType ソート種別
	 * @return string DB 列名
	 */
	public static function sortColumn($sortType)
    {
        $sortColNo = static::sortColumnNo($sortType);
                  
        switch ($sortColNo)
        {
        case self::SORT_LAST_LOGIN_AT:
            return 'last_login_at';
        case self::SORT_PLAYER_LV:
            return 'player_lv';
        }
        assert(false);
        return '';
    }
    
	/**
	 * ソート種別から、昇順、降順を計算する
	 *
	 * @param integer $sortType ソート種別
	 * @return string ソートオーダー
	 */
	public static function sortOrder($sortType)
    {
        return ($sortType % 2 == 1) ? 'asc' : 'desc';
    }
    
}
