<?php
/**
 * バージョン のサービス
 *
 */

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Responses\ApiResponse;




/**
 * バージョン のサービス
 *
 */
class AppVersionService extends BaseService
{
	public static function get($request)
	{
        $response = ApiResponse::getInstance();

		$body = [
			'version'	=> '1'
		];

		$response->body = $body;

		return $response;
	}
}
