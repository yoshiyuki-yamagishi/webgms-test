<?php
/**
 * ログイン のサービス
 *
 */

namespace App\Services;

use App\Http\Responses\ApiResponse;
use App\Http\Responses\PlayerResponse;
use App\Models\PlayerCommon;
use App\Models\BaseGameModel;
use App\Models\BaseCommonModel;
use App\Models\Player;
use App\Models\PlayerBattle;
use App\Utils\DebugUtil;

/**
 * ログイン のサービス
 *
 */
class LoginService extends BaseService
{
	/**
	 * ログイン
	 *
	 * @param LoginRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function index($request)
	{
		$response = ApiResponse::getInstance();

		// プレイヤ共通テーブル (共通DB) からプレイヤ情報を取得
		$playerCommon = PlayerCommon::getByPlayerId_($request->player_id);

		// セッション開始
		SessionService::start(
            $playerCommon->player_id,
            SessionService::SS_LOGIN,
            $playerCommon->db_no
        );

        // プレイヤ取得
        $player = Player::find_($request->player_id);

		// セッションから認証コード取得
		$authCode = SessionService::getAuthCode();

		// バトルコードの確認
		$playerBattle = PlayerBattle::getActive($playerCommon->player_id);
		$battleCode = null;
		if (PlayerBattle::isValidBattleCode($playerBattle))
			$battleCode = $playerBattle->battle_code;

		SessionService::end();

		$body = [
            'player_disp_id' => $playerCommon->player_disp_id,
			'player' => PlayerResponse::make($player),
			'auth_code' => $authCode,
			'battle_code' => $battleCode,
		];
		$response->body = $body;

		return $response;
	}

	/**
	 * 認証コード更新
	 *
	 * @param LoginUpdateRequest $request
	 * @return ApiResponse レスポンス
	 */
	public static function update($request)
	{
        // DebugUtil::e_log('LS_update', 'request', $request->all());
		$response = ApiResponse::getInstance();

		// プレイヤ共通テーブル (共通DB) からプレイヤ情報を取得
		$playerCommon = PlayerCommon::getByPlayerId_($request->player_id);

		// セッション開始
		SessionService::start(
            $playerCommon->player_id,
            SessionService::SS_UPDATE,
            $playerCommon->db_no
        );

		// セッションから認証コード取得
		$authCode = SessionService::getAuthCode();

		SessionService::end();
        
		$body = [
			'auth_code' => $authCode,
		];
		$response->body = $body;

		return $response;
    }
    
}
