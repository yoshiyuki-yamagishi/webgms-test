<?php
/**
 * ミッション - キャラクターグレードチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerCharacter;
use App\Utils\DebugUtil;

/**
 * ミッション - キャラクターグレードチェッカー クラス
 *
 */
class CharacterGradeChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_CHARACTER_GRADE)
            return false;

        $mscs = $this->data("mscs");
        $grade = -1;
        if ($mscs)
        {
            // グレードは複数指定できない
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            $grade = $mscs[0];
        }

        $gradeUpCounts = $this->data('gradeUpCounts');
        
        $gradeUpCount = 0;
        foreach ($gradeUpCounts as $_gradeUpCount)
        {
            if ($grade < 0 || $grade == $_gradeUpCount->grade)
                $gradeUpCount += $_gradeUpCount->grade_up_count;
        }

        if ($gradeUpCount <= 0)
            return false;

        if ($this->setAchived($mission, $playerMission, $gradeUpCount))
            $updated = true;

        // DebugUtil::e_log('CGC', 'mission', $mission);
        // DebugUtil::e_log('CGC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
