<?php
/**
 * ミッション - 魔道書強化チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Mission;
use App\Models\PlayerGrimoire;
use App\Services\PlayerGrimoireService;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * ミッション - 魔道書強化チェッカー クラス
 *
 */
class GrimoireReinforceChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        $reinforceType = null;
        switch ($mission->mission_success_type)
        {
        case Mission::ST_GRIMOIRE_AWAKE:
            $reinforceType = PlayerGrimoireService::REINFORCE_TYPE_AWAKE;
            break;
        default:
            return false;
        }

        // 強化種別が違う場合は、チェックしない
        
        if ($reinforceType != $this->data('reinforceType'))
            return false;
        
        $rarity = -1;
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // レアリティは複数指定できない
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            $rarity = $mscs[0];
        }

        $awakeCounts = $this->data('rarityAwakeCounts');

        $awakeCount = 0;
        if ($rarity < 0)
        {
            // レアリティの指定なし、全魔道書
            foreach ($awakeCounts as $_rarity => $count)
            {
                $awakeCount += $count;
            }
        }
        else if (array_key_exists($rarity, $awakeCounts))
        {
            $awakeCount = $awakeCounts[$rarity];
        }
        else
        {
            $awakeCount = 0;
        }

        if ($awakeCount <= 0)
            return false;

        if ($this->setAchived($mission, $playerMission, $awakeCount))
            $updated = true;
        
        // DebugUtil::e_log('GRC', 'mission', $mission);
        // DebugUtil::e_log('GRC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
