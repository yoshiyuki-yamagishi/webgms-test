<?php
/**
 * ミッション - バトル属性チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\CharacterBase;
use App\Utils\DebugUtil;

/**
 * ミッション - バトル属性チェッカー クラス
 *
 */
class BattleElementChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        $finish = false;
        switch ($mission->mission_success_type)
        {
        case Mission::ST_ELEMENT_CLEAR:
            break;
        case Mission::ST_ELEMENT_FINISH:
            $finish = true;
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerParty = $this->data('playerParty');
        if (empty($playerParty))
            return false; // スキップ
        
        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        $element = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $element = $mscs[0];
        }
        else
        {
            // 属性の指定は必要 
            
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') need mission_success_contents id: '
                . $mission->id
            );
        }

        // 空きポジションは null //
        $charas = $this->data('partyCharacterList');

        if ($finish)
        {
            $finishNo = $playerBattleResult->finish_character_no;
            $prop = 'player_character_id_' . $finishNo;
            $pcId = $playerParty->$prop;

            $chara = $charas->find($pcId);
            $mChara = CharacterBase::getOne($chara->character_id);
            if ($mChara->element != $element)
                return false;
        }
        else
        {
            $found = false;
            foreach ($charas as $chara)
            {
                if (empty($chara))
                    continue;
                
                $mChara = CharacterBase::getOne($chara->character_id);
                if ($mChara->element == $element)
                {
                    $found = true;
                    break;
                }
            }
            if (!$found)
                return false;
        }

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        return false; // 何もしない
	}
}
