<?php
/**
 * ミッション - バトルスキルチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルスキルチェッカー クラス
 *
 */
class BattleSkillChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        $code = "";
        switch ($mission->mission_success_type)
        {
        case Mission::ST_ACTIVE_SKILL:
            $code = "skill";
            break;
        case Mission::ST_OD_USE:
            $code = "od";
            break;
        case Mission::ST_DD_USE:
            $code = "dd";
            break;
        case Mission::ST_AH_USE:
            $code = "ah";
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        $skillCount = 1; // ディフォルト
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 複数回数の指定は無し
            
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            
            $skillCount = $mscs[0];
        }

        $prop = 'used_' . $code . '_count';
        if ($playerBattleResult->$prop < $skillCount)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('BSC', 'mission', $mission);
        // DebugUtil::e_log('BSC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
