<?php
/**
 * ミッション - プレイヤ更新チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\MasterModels\PlayerInitialSettings;
use App\Utils\DebugUtil;

/**
 * ミッション - プレイヤ更新チェッカー クラス
 *
 */
class PlayerUpdateChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        switch ($mission->mission_success_type)
        {
        case Mission::ST_PLAYER_UPDATE:
            break;
        default:
            return false;
        }
        
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 指定には未対応
            
            throw \App\Exceptions\MasterException::make(
                'mission_success_type ('
                . $mission->mission_success_type
                . ') can\'t use mission_success_contents id: '
                . $mission->id
            );
        }

        $player = $this->data("player");
        // DebugUtil::e_log('PUC', 'player', $player);
        $iniS = PlayerInitialSettings::getOne_(1);
        // DebugUtil::e_log('PUC', 'iniS', $iniS);

        $updateCount = 0;        
        if ($player->message != $iniS->message)
            ++ $updateCount; // 初期設定のメッセージと違う場合 + 1
        
        if ($this->setAchived($mission, $playerMission, $updateCount))
            $updated = true;
        
        // DebugUtil::e_log('PUC', 'mission', $mission);
        // DebugUtil::e_log('PUC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
