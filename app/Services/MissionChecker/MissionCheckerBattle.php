<?php
/**
 * ミッションチェッカーの統合用クラス (バトル用)
 *
 */

namespace App\Services\MissionChecker;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーのの統合用クラス (バトル用)
 *
 */
class MissionCheckerBattle extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerBattle();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new PlayerLevelChecker($this);
        $this->checkerList[] = new QuestClearChecker($this);

        // ミッションは最後
        $this->checkerList[] = new MissionClearChecker($this);
    }

}
