<?php
/**
 * ミッションチェッカーの統合用クラス (キャラクター進化用)
 *
 */

namespace App\Services\MissionChecker;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーのの統合用クラス (キャラクター進化用)
 *
 */
class MissionCheckerCharacterEvolve extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerCharacterEvolve();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new CharacterEvolveChecker($this);

        // ミッションは最後
        $this->checkerList[] = new MissionClearChecker($this);
    }

}
