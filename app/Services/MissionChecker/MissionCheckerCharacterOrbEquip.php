<?php
/**
 * ミッションチェッカーの統合用クラス (キャラクターオーブ装備用)
 *
 */

namespace App\Services\MissionChecker;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーのの統合用クラス (キャラクターオーブ装備用)
 *
 */
class MissionCheckerCharacterOrbEquip extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerCharacterOrbEquip();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new CharacterOrbEquipChecker($this);
        $this->checkerList[] = new CharacterGradeChecker($this);

        // ミッションは最後
        $this->checkerList[] = new MissionClearChecker($this);
    }

}
