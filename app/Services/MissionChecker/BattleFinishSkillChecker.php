<?php
/**
 * ミッション - バトルとどめスキルチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerBattleResult;
use App\Utils\DebugUtil;

/**
 * ミッション - バトルとどめスキルチェッカー クラス
 *
 */
class BattleFinishSkillChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        $code = 0;
        switch ($mission->mission_success_type)
        {
        case Mission::ST_OD_FINISH:
            $code = PlayerBattleResult::FINISH_CODE_OD;
        case Mission::ST_DD_FINISH:
            $code = PlayerBattleResult::FINISH_CODE_DD;
        case Mission::ST_AH_FINISH:
            $code = PlayerBattleResult::FINISH_CODE_AH;
            break;
        default:
            return false;
        }
        
        if (!$this->isWin())
            return false;

        $playerBattleResult = $this->data("playerBattleResult");
        assert(isset($playerBattleResult));

        if (($playerBattleResult->finish_skill_type & $code) == 0)
            return false;

        if ($this->incAchived($mission, $playerMission))
            $updated = true;
        
        // DebugUtil::e_log('BFSC', 'mission', $mission);
        // DebugUtil::e_log('BFSC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
