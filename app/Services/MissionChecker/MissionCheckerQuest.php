<?php
/**
 * ミッションチェッカーの統合用クラス (クエストミッション用)
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerQuest;
use App\Models\PlayerQuestMission;
use App\Utils\DebugUtil;

/**
 * ミッションチェッカーの統合用クラス (クエストミッション用)
 *
 */
class MissionCheckerQuest extends MissionChecker
{
    // シングルトン //
    
    private static $singleton;

    public $playerQuest = null;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new MissionCheckerQuest();
        }
        return self::$singleton;
    }

    // 実装 //
   
    private function __construct()
    {
        // チェッカーリストを用意する
        $this->checkerList[] = new BattleDeadChecker($this);
        $this->checkerList[] = new BattleSkillChecker($this);
        $this->checkerList[] = new BattleNoSkillChecker($this);
        $this->checkerList[] = new BattleFinishSkillChecker($this);
        $this->checkerList[] = new BattleComboChecker($this);
        $this->checkerList[] = new BattleClearChecker($this);
        $this->checkerList[] = new BattlePartyChecker($this);
        $this->checkerList[] = new BattleElementChecker($this);
    }

    public function initQuest($playerQuest, $quest, $now)
    {
        $this->now = $now;
        $this->playerQuest = $playerQuest;
        
        $missionIds = [
            $quest->quest_mission_id_01,
            $quest->quest_mission_id_02,
            $quest->quest_mission_id_03,
        ];
        $this->missionList = Mission::getQuest($now, $missionIds);

        $this->playerMissionList = PlayerQuestMission::getByPlayerQuestId(
            $playerQuest->id, $now
        );
    }
    
    public function updateMission(&$achiveMissionList, $report)
    {
        for ($i = 1; $i <= 3; ++ $i)
        {
            $mission = $this->missionList[$i - 1];
            if (empty($mission))
                continue; // ミッション設定無し

            $playerMission = $this->playerMissionList->where(
                'mission_id', $mission->id
            )->first();
            
            // 達成済みチェック

            if ($this->playerQuest->isMissionClear($i))
                continue;

            $isPm = isset($playerMission);

            // ミッション毎のデータを設定

            $this->addMissionData($mission, $playerMission);

            // 達成判定

            $updated = false;
            $this->check($mission, $playerMission, $updated);

            if (!$updated)
                continue;

            // プレイヤクエスト ID を設定する

            $playerMission->player_quest_id = $this->playerQuest->id;

            // 達成したか？

            $achived = ($playerMission->progress_count >= $mission->count);

            // 今回達成リストに追加する

            if ($achived)
                $achiveMissionList[] = $playerMission;
            
            // もともと行が無かった場合、リストに追加する
            
            if (!$isPm)
                $this->playerMissionList[] = $playerMission;

            if ($achived && $report)
                $playerMission->setReported();

            // ミッション達成フラグを更新

            if ($achived)
            {
                $this->playerQuest->setMissionFlag($i);
                $this->playerQuest->save();
            }

            // プレイヤクエストミッション 更新

            if ($achived)
            {
                $playerMission->cleared_at = $this->now;
                $playerMission->taked_at = $this->now;
            }
            
            $playerMission->save();
        }
    }

}
