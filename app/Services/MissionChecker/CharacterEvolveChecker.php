<?php
/**
 * ミッション - キャラクター進化チェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Models\MasterModels\Mission;
use App\Models\PlayerCharacter;
use App\Utils\DebugUtil;

/**
 * ミッション - キャラクター進化チェッカー クラス
 *
 */
class CharacterEvolveChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_CHARACTER_EVOLVE)
            return false;

        $rarity = -1;
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // レアリティは複数指定できない
            if (count($mscs) > 1)
            {
                throw \App\Exceptions\MasterException::make(
                    'mission_success_type ('
                    . $mission->mission_success_type
                    . ') can\'t use multiple mission_success_contents id: '
                    . $mission->id
                );
            }
            $rarity = $mscs[0];
        }

        $evolveCounts = $this->data('rarityEvolveCounts');
        $evolveCount = 0;
        if ($rarity < 0)
        {
            // レアリティの指定なし、全キャラ
            foreach ($evolveCounts as $_rarity => $count)
            {
                $evolveCount += $count;
            }
        }
        else if (array_key_exists($rarity, $evolveCounts))
        {
            $evolveCount = $evolveCounts[$rarity];
        }
        else
        {
            $evolveCount = 0;
        }

        if ($evolveCount <= 0)
            return false;

        if ($this->setAchived($mission, $playerMission, $evolveCount))
            $updated = true;
        
        // DebugUtil::e_log('CEC', 'mission', $mission);
        // DebugUtil::e_log('CEC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
    
}
