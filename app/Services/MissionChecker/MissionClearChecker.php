<?php
/**
 * ミッション - ミッションクリアチェッカー 
 *
 */

namespace App\Services\MissionChecker;
use App\Http\Responses\ApiResponse;
use App\Models\MasterModels\Mission;
use App\Models\PlayerMission;
use App\Utils\DateTimeUtil;
use App\Utils\DebugUtil;

/**
 * ミッション - ミッションクリアチェッカー クラス
 *
 */
class MissionClearChecker extends BaseMissionChecker
{
    /**
	 * ミッション達成状況を更新する
	 *
	 * @param Mission $mission ミッション
	 * @param PlayerMission $playerMission プレイヤミッション
	 * @param boolean $updated true:更新した,false: 更新してない
	 * @return boolean true:処理終了,false:処理続行
	 */
	public function check($mission, &$playerMission, &$updated)
	{
        if ($mission->mission_success_type != Mission::ST_MISSION_CLEAR)
            return false;

        $count = 0;
        $mscs = $this->data("mscs");
        if ($mscs)
        {
            // 特定のミッション
            
			$clears = $this->rootChecker->playerMissionList
                    // ->where('progress_count', '>=', 'count')
                    ->whereIn('mission_id', $mscs);

            // DebugUtil::e_log('MCC', 'clears1', $clears);
            // $count = count($clears);
            $count = PlayerMission::clearedCount($clears);
        }
        else if (Mission::isDaily($mission))
        {
            // 今日クリアしていれば何でもいい
            
            $now = ApiResponse::getInstance()->currentDateDB();
            $dayStartDate = DateTimeUtil::dailyMissionStartDate($now);
            $dayStart = DateTimeUtil::formatDB($dayStartDate);

			$clears = $this->rootChecker->playerMissionList
                    // ->where('progress_count', '>=', 'count')
                    ->where('created_at', '>=', $dayStart);

            // DebugUtil::e_log('MCC', 'clears2', $clears);
            // $count = count($clears);
            $count = PlayerMission::clearedCount($clears);
        }
        else
        {
            // 何でもよい
            
			$clears = $this->rootChecker->playerMissionList;
            // ->where('progress_count', '>=', 'count');
            
            // DebugUtil::e_log('MCC', 'clears3', $clears);
            // $count = count($clears);
            $count = PlayerMission::clearedCount($clears);
        }

        $player = $this->data("player");

        if ($this->setAchived($mission, $playerMission, $count))
            $updated = true;
        
        // DebugUtil::e_log('MCC', 'mission', $mission);
        // DebugUtil::e_log('MCC', 'playerMission', $playerMission);
        return false; // 何もしない
	}
}
