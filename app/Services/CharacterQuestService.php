<?php
/**
 * キャラクタークエスト のサービス
 *
 */

namespace App\Services;

/**
 * キャラクタークエスト のサービス
 *
 */
class CharacterQuestService extends QuestService
{
    protected $questCategory = self::QUEST_CATEGORY_CHARACTER;
	protected $chapterModel = 'CharacterQuestChapter';
	protected $questModel = 'CharacterQuestQuest';
	protected $battleModel = 'CharacterQuestBattle';
    
	protected $colNamePrefix = 'character_';
    protected $colNameFirstRewardId = 'first_reward_id';
    protected $colNameFixRewardId = 'fix_reward_id';
    protected $colNameDropRewardId = 'drop_reward_id';
}
