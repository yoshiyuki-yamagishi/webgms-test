<?php
/**
 * S3 のサービス
 *
 */

namespace App\Services;
use App\Models\MasterModelManager;
use App\Utils\DebugUtil;

/**
 * S3 のサービス
 *
 */
class S3Service extends BaseService
{
	/** キャッシュ用のタグ */
    private static $TAGS = ['version'];
    
    const CACHE_TIMEOUT = 1; // 1 分
    const TYPE_MASTER_DATA = 1;
    const TYPE_ASSET_BUNDLE = 2;
    
	/**
	 * 基底 URL を取得する
     *
	 * @return string 基底 URL
	 */
	public static function infoUrl()
    {
        // TODO: 環境によって分岐する必要がある
        return 'http://s3-ap-northeast-1.amazonaws.com/bbdw-dev-info';
    }

	/**
	 * 基底 URL を取得する
     *
	 * @return string 基底 URL
	 */
	public static function baseUrl()
    {
        // TODO: 環境によって分岐する必要がある
        return 'http://s3-ap-northeast-1.amazonaws.com/bbdw-dev-contents';
    }

	/**
	 * タイプ文字列を取得する
     *
	 * @param integer $type タイプ
	 */
	public static function typeName($type)
    {
        switch ($type)
        {
        case self::TYPE_MASTER_DATA:
            return 'master_data';
            break;
        case self::TYPE_ASSET_BUNDLE:
            return 'asset_bundle';
        }
        assert(false);
        return '';
    }
    

	/**
	 * 基底 URL を取得する
     *
	 * @param integer $type タイプ
	 * @return string 基底 URL
	 */
	public static function baseTypeUrl($type, $version = 0)
    {
        $url = static::baseUrl();
        
        $name = static::typeName($type);
        if (isset($name))
            $url .= '/' . $name;
        if ($version > 0)
            $url .= '/v' . $version;

        return $url;
    }

	/**
	 * バージョンを取得する
     *
	 * @param integer $type タイプ
	 * @return integer バージョン
	 */
	public static function getVersion($type)
    {
        $name = static::typeName($type);
        $url = static::baseTypeUrl($type) . '/version.txt';
        
        $response = HttpCacheService::get(
            static::$TAGS, $name, $url, static::CACHE_TIMEOUT
        );

        if (!preg_match('|^[0-9]+$|', $response))
        {
            // キャッシュクリア
            HttpCacheService::clear(static::$TAGS, $name);
            throw \App\Exceptions\UnknownException::make(
                'version.txt response is invalid: ' . $response
            );
        }

        return intval($response);
    }

	/**
	 * ヘッダー情報を設定する
     *
	 * @param list $header 設定先連想配列
	 * @param integer $type タイプ
	 */
	public static function setHeader(&$header, $type)
    {
        $name = static::typeName($type);
        $key = '';
        switch ($type)
        {
        case self::TYPE_MASTER_DATA:
            $key = MasterModelManager::$AES_IV;
            break;
        case self::TYPE_ASSET_BUNDLE:
            $key = MasterModelManager::$AES_IV;
            break;
        default:
            assert(false);
            break;
        }

        $version = static::getVersion($type);
        $url = static::baseTypeUrl($type, $version);
        
        $header[$name]['version'] = $version;
		$header[$name]['path'] = $url;
		$header[$name]['key'] = $key;
    }
    
}
