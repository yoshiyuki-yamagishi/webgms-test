<?php
/**
 * ストーリークエスト のサービス
 *
 */

namespace App\Services;

/**
 * ストーリークエスト のサービス
 *
 */
class StoryQuestService extends QuestService
{
    protected $questCategory = self::QUEST_CATEGORY_STORY;
	protected $chapterModel = 'StoryQuestChapter';
	protected $questModel = 'StoryQuestQuest';
	protected $battleModel = 'StoryQuestBattle';
    
	protected $colNamePrefix = 'story_';
    protected $colNameFirstRewardId = 'first_reward_id';
    protected $colNameFixRewardId = 'fix_reward_id';
    protected $colNameDropRewardId = 'drop_reward_id';
}
