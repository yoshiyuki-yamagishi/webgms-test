<?php
/**
 * 使用不可文言 のサービス
 *
 */

namespace App\Services;

use App\Models\MasterModels\RestrictionWord;

/**
 * 使用不可文言 のサービス
 *
 */
class RestrictionWordService extends BaseService
{
    // シングルトン //
    
    private static $singleton;

    public static function getInstance()
    {
        if (!isset(self::$singleton)) {
            self::$singleton = new RestrictionWordService();
        }
        return self::$singleton;
    }
    
	/**
	 * チェック
	 *
	 * @param string $targetName ターゲットの名前
	 * @param string $target ターゲット文字列
	 * @return ApiResponse
	 */
	public static function check($targetName, $target)
	{
        if (empty($target))
            return;

        $words = RestrictionWord::getAll();
        foreach ($words as $word)
        {
            if (strpos($target, $word->restriction_dictionary) === false)
                continue;

            throw \App\Exceptions\RestrictionWordException::makeRestrictionWord(
                $targetName, 
                $word->id,
                $word->restriction_dictionary
            );
        }
    }

}
