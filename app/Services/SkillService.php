<?php
/**
 * スキル のサービス
 *
 */

namespace App\Services;
use App\Models\PlayerGrimoire;
use App\Models\MasterModels\CharacterBase;
use App\Models\MasterModels\Constant;
use App\Models\MasterModels\Grimoire;
use App\Models\MasterModels\Skill;
use App\Utils\DebugUtil;
use App\Utils\StrUtil;

/**
 * スキル のサービス
 *
 */
class SkillService extends BaseService
{
	/**
	 * キャラクターのスキル効果を計算する
	 *
	 * @param PlayerCharacter $playerCharacter プレイヤキャラクター
	 * @return array SkillEffect の配列
	 */
    public static function calcCharacterEffects($playerCharacter)
    {
        // DebugUtil::e_log('SkillS', 'playerCharacter', $playerCharacter);
        if (empty($playerCharacter))
            return [];

        $character = CharacterBase::getOne_($playerCharacter->character_id);
        // DebugUtil::e_log('SkillS', 'character', $character);

        // 無理やり入れると、6 キャラ分加算される
        // $character->passive5 = 40731; // debug

        $all = [];
        for ($i = 0; $i < CharacterBase::MAX_PASSIVE_COUNT; ++ $i)
        {
            $colName = 'passive' . ($i + 1);
            $skillId = $character->$colName;
            if ($skillId <= 0)
                continue;

            $effects = static::calcSkillEffects($skillId);
            static::merge($all, $effects);
        }
        
        return $all;
    }
    
	/**
	 * 魔道書のスキル効果を計算する
	 *
	 * @param PlayerGrimoire $playerGrimoire プレイヤ魔道書
	 * @return array SkillEffect の配列
	 */
    public static function calcGrimoireEffects($playerGrimoire)
    {
        // DebugUtil::e_log('SkillS', 'playerGrimoire', $playerGrimoire);
        if (empty($playerGrimoire))
            return [];

        $grimoire = Grimoire::getOne_($playerGrimoire->grimoire_id);
        // DebugUtil::e_log('SkillS', 'grimoire', $grimoire);

        $skillId = $grimoire->passive_id_1;

        // 覚醒最大値を取得
        $awakeLimit = Constant::grimoireAwakeLimit();
        
        if ($playerGrimoire->awake >= $awakeLimit->value1)
            $skillId = $grimoire->passive_id_2;

        return static::calcSkillEffects($skillId);
    }

	/**
	 * スキル効果を計算する
	 *
	 * @param integer $skillId スキルID
	 * @return array SkillEffect の配列
	 */
    public static function calcSkillEffects($skillId)
    {
        if ($skillId <= 0)
            return [];

        // サーバーで使用するスキルは限られているため、
        // あらかじめ、フィルタリングしておくことで高速化できる
        //
        // フィルタリングして除去したときのことを考慮し、null チェックする
        
        $skill = Skill::getOne($skillId);
        if (empty($skill)) 
            return [];

        $params = StrUtil::decodeGeneralParams($skill->effect_detail_1);
        // DebugUtil::e_log('SkillS', 'params', $params);
        if (empty($params))
            return [];

        $effects = [];
        foreach ($params as $param)
        {
            $effect = SkillEffect::make($param);
            if (empty($effect))
                continue;
            $effects[] = $effect;
        }
        
        // DebugUtil::e_log('SkillS', 'effects', $effects);
        return $effects;
    }

    public static function merge(&$effects, $effect)
    {
        if (empty($effect))
            return;
        
        if (is_array($effect))
        {
            foreach ($effect as $one)
            {
                self::merge($effects, $one);
            }
            return;
        }
        
        if (isset($effects[$effect->type]))
        {
            // 現状、加算で OK そう
            $effects[$effect->type]->value += $effect->value;
        }
        else
        {
            $effects[$effect->type] = $effect;
        }
    }
    
}
