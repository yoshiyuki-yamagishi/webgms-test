<?php
/**
 * クエスト の基本サービス
 *
 */

namespace App\Services;
use App\Models\PlayerPresent;
use App\Models\PlayerQuest;
use App\Models\PlayerBattleReward;
use App\Models\MasterModels\Item;
use App\Models\MasterModels\QuestReward;
use App\Models\MasterModels\DropReward;
use App\Http\Responses\QuestRewardListResponse;
use App\Utils\DebugUtil;
use App\Utils\RandUtil;

/**
 * クエスト の基本サービス
 *
 */
class QuestService extends BaseService
{
	// クエスト種類
	const QUEST_CATEGORY_ALL = 0; // 全て
	const QUEST_CATEGORY_STORY = 1; // ストーリ
	const QUEST_CATEGORY_CHARACTER = 2; // キャラクタ
	const QUEST_CATEGORY_EVENT = 3; // イベント
    
    const QUEST_CATEGORY_FIRST = 1;
    const QUEST_CATEGORY_LAST = 3;

	// クエストタイプ
    const QUEST_TYPE_STORY = 0;
    const QUEST_TYPE_STORY_BATTLE = 1;
    const QUEST_TYPE_BATTLE = 2;

	// 報酬種別
	const REWARD_TYPE_FIRST = 1; // 初回報酬
	const REWARD_TYPE_FIX = 2; // 固定報酬
	const REWARD_TYPE_DROP  = 3; // ドロップ報酬

	// クエストクリアフラグ
	const CLEAR_FLAG_NO	= 0; // 未クリア
	const CLEAR_FLAG_YES = 1; // クリア済み

    // オーバーライドする //
	protected $chapterModel = '';
	protected $questModel = '';
	protected $battleModel = '';

	/**
	 * PlayerQuest から、クリア済みかを計算する
	 * @param PlayerQuest $playerQuest
	 * @return クリア済みなら true
	 */
	public static function IsCleared_PQ($playerQuest)
	{
        return
            isset($playerQuest) &&
            $playerQuest->clear_flag == self::CLEAR_FLAG_YES;
    }

	/**
	 * PlayerQuest から、ミッションを含めてクリア済みかを計算する
	 * @param PlayerQuest $playerQuest
	 * @return クリア済みなら true
	 */
	public static function IsAllCleared_PQ($playerQuest)
	{
        if (!self::IsCleared_PQ($playerQuest))
            return false;

        return 
            $playerQuest->mission_flag_1 == self::CLEAR_FLAG_YES &&
            $playerQuest->mission_flag_2 == self::CLEAR_FLAG_YES &&
            $playerQuest->mission_flag_3 == self::CLEAR_FLAG_YES;
    }
    
	/**
	 * サービスの作成
	 * @param int $questCategory
	 * @return QuestService
	 */
	public static function make($questCategory)
	{
		switch ($questCategory)
		{
        case self::QUEST_CATEGORY_STORY:
            return new StoryQuestService();
        case self::QUEST_CATEGORY_CHARACTER:
            return new CharacterQuestService();
        case self::QUEST_CATEGORY_EVENT:
            return new EventQuestService();
        }

        throw \App\Exceptions\ParamException::makeNotFound(
            '', 'quest_category', $questCategory
        );
	}
    
	/**
	 * 章の取得
	 * @param int $chapterId
	 * @return チャプターモデル
	 */
	public function getChapter($chapterId)
	{
        $cname = "App\\Models\\MasterModels\\" . $this->chapterModel;
        $chapter = $cname::getOne_($chapterId);
        return $chapter;
        
    }
    
	/**
	 * 章の取得
	 * @return チャプターモデル
	 */
	public function getChapters()
	{
        $cname = "App\\Models\\MasterModels\\" . $this->chapterModel;
        return $cname::getAll();
	}

	/**
	 * クエストの取得
	 * @param int $chapterId
	 * @param int $questId
	 * @return クエストモデル
	 */
	public function getQuest($chapterId, $questId)
	{
        $cname = "App\\Models\\MasterModels\\" . $this->questModel;
        $quest = $cname::getOne_([$questId, $chapterId]);
        return $quest;
    }

    /**
	 * クエストの取得
	 * @param int $chapterId
	 * @return クエストモデル
	 */
	public function getQuests($chapterId)
	{
        $cname = "App\\Models\\MasterModels\\" . $this->questModel;
        return $cname::getByChapterId($chapterId);
    }

    /**
	 * クエストの取得
	 * @return クエストモデル
	 */
	public function getAllQuests()
	{
        $cname = "App\\Models\\MasterModels\\" . $this->questModel;
        return $cname::getAll();
    }

	/**
	 * バトルの取得
	 * @param int $battleId
	 * @return バトルモデル
	 */
	public function getBattle($battleId)
	{
        $cname = "App\\Models\\MasterModels\\" . $this->battleModel;
        return $cname::getAll($battleId);
	}

	/**
	 * バトルの取得
	 * @param StoryQuestQuest / CharacterQuestQuest $quest
	 * @return バトルモデル
	 */
	public function getBattleByQuest($quest)
	{
        $funcName = $this->composeIdName('quest_battle_id');
        $battleId = $quest->$funcName;

        $battleList = $this->getBattle($battleId);

        if (!isset($battleList))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'xx_quest_battle',
                $funcName,
                $battleId
            );
        }

        return $battleList;
	}
    
	/**
	 * 列名の計算
	 * @param string $postfix 列名のボディ
	 * @return 列名
	 */
    public function composeIdName($postfix)
    {
        return $this->colNamePrefix . $postfix;
    }

    public function getFirstRewardId($quest) {
        $method = $this->colNameFirstRewardId;
        return $quest->$method;
    }

    public function getFixRewardId($quest) {
        $method = $this->colNameFixRewardId;
        return $quest->$method;
    }
    
    public function getDropRewardId($quest) {
        $method = $this->colNameDropRewardId;
        return $quest->$method;
    }
    
	/**
	 * 報酬アイテムリストの計算
	 * @param Reward $reward 初回報酬テーブルのインスタンス
	 * @return 報酬アイテムのリスト
	 */
	public static function calcRewardItems($reward)
	{
        $rewardItems = [];
        
        $count = $reward->first_reward_count;
        for ($i = 0; $i < $count; ++ $i)
        {
            $no = sprintf('%02d', ($i + 1));
            $itemIdFn = 'first_reward' . $no;
            $itemCountFn = 'reward_count' . $no;

            $rewardItems[$i] = [
                'reward_type' => self::REWARD_TYPE_FIRST,
                'reward_no' => $i + 1,
                'item_type' => Item::TYPE_ITEM,
                'item_id' => $reward->$itemIdFn,
                'item_num' => $reward->$itemCountFn,
            ];
        }

        return $rewardItems;
	}

	/**
	 * 初回、固定報酬の計算
	 * @param object $quest Qeust マスタ
	 * @param string $colName 列名
	 * @return 報酬リスト
	 */
    public function calcQuestReward($quest, $colName)
    {
        $rewardItems = [];
        
        $rewardId = $quest->$colName;
        if ($rewardId < 1)
        {
            return $rewardItems;
        }

        $rewards = QuestReward::getAll($rewardId);
        if (!isset($rewards))
        {
            throw \App\Exceptions\MasterException::makeNotFound(
                'quest_reward', 'id', $rewardId
            );
        }

        $rewardItems = QuestRewardListResponse::makeByQuestReward(
            $rewards
        );
        return $rewardItems;
    }
    
	/**
	 * 初回報酬の計算
	 * @param object $quest クエストマスタ
	 * @return 報酬リスト
	 */
    public function calcFirstReward($quest)
    {
        return static::calcQuestReward(
            $quest, $this->colNameFirstRewardId
        );
    }

	/**
	 * 固定報酬の計算
	 * @param object $quest クエストマスタ
	 * @return 報酬リスト
	 */
    public function calcFixReward($quest)
    {
        return static::calcQuestReward(
            $quest, $this->colNameFixRewardId
        );
    }
    
	/**
	 * ドロップ報酬の計算
	 * @param Reward $enemyList バトルエネミーリスト
	 * @return 報酬リスト
	 */
    public function calcDropReward($quest, &$battleList)
    {
        $dropRewardId = $this->getDropRewardId($quest);
        
        $dropItems = [];
        foreach ($battleList as &$battle)
        {
            $battle['drop_box_type'] = 0;
            $battle['drop_items'] = [];
            
            if ($dropRewardId < 1)
                continue;

            // ドロップ報酬マスタ、drop_box_type は、銅:1、銀:2、金:3
            $rates = [
                $battle['copper_rate'],
                $battle['silver_rate'],
                $battle['gold_rate'],
            ];

            // 1000 分率
            $rand = mt_rand(0, 999);
            // DebugUtil::e_log('QS', 'rand', $rand);
            // DebugUtil::e_log('QS', 'rates', $rates);
            
            $sum = 0;
            
            $count = count($rates);
            for ($i = 0; $i < $count; ++ $i)
            {
                $sum += $rates[$i];
                if ($rand < $sum)
                    break;
            }

            // 宝箱が出たか？
            if ($i >= $count)
                continue;

            // ドロップ報酬を設定
            $dropBoxType = $i + 1;
            $rewards = DropReward::getAll($dropRewardId, $dropBoxType);
            // DebugUtil::e_log('QS', 'dropBoxType', $dropBoxType);

            // 確率で 1 アイテムを選定
            $reward = RandUtil::calcOneItem($rewards, 'drop_rate', null);

            if (empty($reward))
                continue;

            // 1 アイテムだが、配列に変換する
            $_rewards = [];
            $_rewards[] = $reward;

            $battle['drop_box_type'] = $dropBoxType;
            $battle['drop_items'] = QuestRewardListResponse::makeByDropReward(
                $_rewards
            );

            // 全ドロップアイテムに追加
            if (is_array($battle['drop_items']))
                $dropItems = array_merge($dropItems, $battle['drop_items']);
        }
        
        return $dropItems;
    }
    
	/**
	 * プレイヤークエストの取得
	 * @param int $playerId
	 * @param int $chapterId
	 * @param int $questId
	 * @return クエストモデル
	 */
	public function getPlayerQuest($playerId, $chapterId, $questId)
	{
        return PlayerQuest::getOne(
            $playerId, $this->questCategory, $chapterId, $questId
        );
	}

	/**
	 * プレイヤークエストの準備
	 * @param object $playerQuest プレイヤクエストモデル
	 * @param int $playerId
	 * @param int $chapterId
	 * @param int $questId
	 */
	public function preparePlayerQuest(
        &$playerQuest, $playerId, $chapterId, $questId
    )
	{
		if (empty($playerQuest))
		{
			$playerQuest = new PlayerQuest();
			$playerQuest->player_id = $playerId;
			$playerQuest->quest_category = $this->questCategory;
			$playerQuest->chapter_id = $chapterId;
			$playerQuest->quest_id = $questId;
			$playerQuest->clear_flag = self::CLEAR_FLAG_NO;
			$playerQuest->mission_flag_1 = self::CLEAR_FLAG_NO;
			$playerQuest->mission_flag_2 = self::CLEAR_FLAG_NO;
			$playerQuest->mission_flag_3 = self::CLEAR_FLAG_NO;

            if ($this->questCategory == self::QUEST_CATEGORY_EVENT)
            {
                $chapter = $this->getChapter($chapterId);
                $playerQuest->close_day = $chapter->close_day;
            }
        }
	}
    
	/**
	 * プレイヤークエスト報酬の登録
	 * @param int $playerId
	 * @param int $battleCode
	 * @param int $chapterId
	 * @param int $questId
	 * @param int $rewardList
	 * @return 報酬リスト
	 */
	public function registPlayerBattleReward(
        $playerBattle, $skipNo, $rewardType, $rewardList
    )
	{
        return PlayerBattleReward::registAll(
            $playerBattle->id, $skipNo, $rewardType, $rewardList
        );
	}

	/**
	 * プレイヤークエスト報酬の取得
	 * @param integer $battleId バトルコード
	 * @param integer $rewardType 報酬種別
	 * @return array プレイヤバトル報酬モデルの配列
	 */
	public function getPlayerBattleReward($playerBattleId, $rewardType)
	{
        return PlayerBattleReward::getByBattleId($playerBattleId, $rewardType);
    }

	/**
	 * 開始日時、終了日時のチェック
	 * @param object $quest クエストマスタ
	 * @param string $now 現在日付
	 */
	public function checkStartEndDay($quest, $now)
	{
        $startDay = new \DateTime($quest->start_day);
        $endDay = new \DateTime($quest->end_day);
        $_now = new \DateTime($now);

        if ($startDay > $_now || $endDay < $_now)
        {
            throw \App\Exceptions\GameException::make(
                'xx_quest_quest is out of date'
            );
        }
    }
    
    
}
