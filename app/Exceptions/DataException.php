<?php
/**
 * ゲームデータ の例外
 *
 */

namespace App\Exceptions;

class DataException extends ApiException
{
    public static $CODE = self::E_DATA;
}
