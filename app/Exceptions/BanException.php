<?php
/**
 * BAN による例外
 *
 */

namespace App\Exceptions;

class BanException extends ApiException
{
    public static $CODE = self::S_BAN;
}
