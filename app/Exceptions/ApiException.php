<?php
/**
 * API の例外
 *
 */

namespace App\Exceptions;

class ApiException extends \Exception
{
    public static $CODE = self::E_UNKNOWN;

    const S_OK = 0; // エラー無し
    const S_WARN = 1; // 警告中
    const S_MAINTENANCE = 100; // メンテナンス中
    const S_BAN = 200; // 警告、アカウント停止中
    const E_UNKNOWN = 1000; // 不明な例外
	const E_PARAM = 2000; // パラメータ異常
    const E_DATABASE = 3000; // データベースエラー
    const E_MASTER = 4000; // マスタ異常
    const E_DATA = 5000; // ゲームデータ異常
    const E_NOT_ENOUGH = 5001; // ゲームデータ異常(足りない)
    const E_OVERFLOW = 5002; // ゲームデータ異常(持ち切れない)
    const E_GAME = 6000; // ゲームの仕様違反
    const E_RESTRICTION_WORD = 6001; // 使用不可文言エラー
	const E_AUTH = 7000; // 認証エラー
	const E_AUTH_EXPIRED = 7001; // 認証コードの有効期限切れ
    const E_WRONG_PASS = 7002; // 引き継ぎパスコード、パスワードが違う
    const E_RECEIPT = 7100; // レシート認証エラー
    const E_RECEIPT_USED = 7101; // レシート使用済みエラー
    
    public $code;
    public $message;

    public function __construct($message)
    {
        $this->code = static::$CODE;
        $this->message = $message;
    }

    public static function make($message)
    {
        return new static($message);
    }
    
    public static function makeTable($table, $name, $value, $postfix)
    {
        if (is_array($name))
            $name = '(' . implode(",", $name) . ')';
        if (is_array($value))
            $value = '(' . implode(",", $value) . ')';
        
        $msg = '';
        if (!empty($table) && !empty($name))
            $msg .= "[" . $table . "." . $name . "] ";
        else if (!empty($table) || !empty($name))
            $msg .= "[" . $table . $name . "] ";

        $msg .= $value . $postfix;
        
        return static::make($msg);
    }

    public static function makeInvalid($table, $name, $value)
    {
        return static::makeTable($table, $name, $value, " is invalid");
    }

    public static function makeNotFound($table, $name, $value)
    {
        return static::makeTable($table, $name, $value, " not found");
    }

    public static function makeNotEnough(
        $table, $name, $value, $use, $remain
    )
    {
        return static::makeTable(
            $table, $name, $value,
            ' is not enough: ' . $use . ' > ' . $remain
        );
    }

    public static function makeNotEquals(
        $table, $name, $value, $use, $remain
    )
    {
        return static::makeTable(
            $table, $name, $value,
            ' is not equals: ' . $use . ' need: ' . $remain
        );
    }
}
