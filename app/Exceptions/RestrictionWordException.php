<?php
/**
 * 使用不可文言 例外
 *
 */

namespace App\Exceptions;

class RestrictionWordException extends ApiException
{
    public static $CODE = self::E_RESTRICTION_WORD;

    public static function makeRestrictionWord(
        $name, $id, $word
    )
    {
        return static::make(
            $name . ' has restriction word hit: [' . $id . '] ' . $word
        );
    }
}
