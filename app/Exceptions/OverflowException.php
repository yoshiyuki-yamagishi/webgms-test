<?php
/**
 * ゲームデータ の例外 (オーバー系)
 *
 */

namespace App\Exceptions;

class OverflowException extends ApiException
{
    public static $CODE = self::E_OVERFLOW;
    
    public $itemType = 0;
    public $item = 0;
    public $count = 0;
    public $userCount = 0;

    public static function makeOverflow(
        $table, $name, $value, $itemType, $item, $count, $userCount
    )
    {
        $_this = static::makeTable(
            $table, $name, $value,
            ' is too many: ' . $userCount . ' + ' . $count
        );
        $_this->itemType = $itemType;
        $_this->item = $item;
        $_this->count = $count;
        $_this->userCount = $userCount;
        return $_this;
    }
}
