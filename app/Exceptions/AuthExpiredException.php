<?php
/**
 * 認証による例外
 *
 */

namespace App\Exceptions;

class AuthExpiredException extends ApiException
{
    public static $CODE = self::E_AUTH_EXPIRED;
}
