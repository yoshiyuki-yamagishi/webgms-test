<?php
/**
 * マスターデータ の例外
 *
 */

namespace App\Exceptions;

class MasterException extends ApiException
{
    public static $CODE = self::E_MASTER;
}
