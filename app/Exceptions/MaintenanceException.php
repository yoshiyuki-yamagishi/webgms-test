<?php
/**
 * メンテナンス中による例外
 *
 */

namespace App\Exceptions;

class MaintenanceException extends ApiException
{
    public static $CODE = self::S_MAINTENANCE;
}
