<?php
/**
 * App\Models のファクトリ
 *
 */

use Faker\Generator as Faker;

use Carbon\Carbon;



// player_common:プレイヤ共通
$factory->define(App\Models\PlayerCommon::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'		=> $playerId,
		'unique_id'		=> 'unique_id_' . $random,
		'db_no'			=> 1,
		'passcode'		=> null,
		'password'		=> null,
		'valid_flag'	=> App\Models\PlayerCommon::VALID_FLAG_YES,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player:プレイヤ
$factory->define(App\Models\Player::class, function (Faker $faker) {
	$now = Carbon::now();
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		//'id'					=> ,
		'player_lv'				=> 1,
		'player_name'			=> 'プレイヤ名_' . $random,
		'birthday'				=> $now,
		'gender'				=> App\Models\Player::GENDER_FEMALE,
		'tutorial_progress'		=> 1,
		//'leader_character_id'	=> 1,
		'created_at'			=> $now,
		'updated_at'			=> $now,
	];
});


// player_battle:プレイヤバトル
$factory->define(App\Models\PlayerBattle::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	return [
		'player_id'		=> $playerId,
		'battle_code'	=> null,
		'stage_id'		=> null,
		'quest_type'	=> null,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_blue_crystal:プレイヤ蒼の結晶
$factory->define(App\Models\PlayerBlueCrystal::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'		=> $playerId,
		'payment_id'	=> 'payemnt_id_' . $random,
		'payment_num'	=> $random,
		'num'			=> $random,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_character
$factory->define(App\Models\PlayerCharacter::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	$characterId = 150034000;
	$seriesId = 34;
	return [
		'player_id'			=> $playerId,
		'series_id'			=> $seriesId,
		'character_id'		=> $characterId,
		'character_lv'		=> 1,
		'experience'		=> 0,
		'player_orb_id_1'	=> null,
		'player_orb_id_2'	=> null,
		'player_orb_id_3'	=> null,
		'player_orb_id_4'	=> null,
		'player_orb_id_5'	=> null,
		'player_orb_id_6'	=> null,
		'active_skill_1_lv'	=> 1,
		'active_skill_2_lv'	=> 1,
		'favorite_flag'		=> 0,
		'acquired_at'		=> $now,
		'created_at'		=> $now,
		'updated_at'		=> $now,
	];
});

// player_character_quest
$factory->define(App\Models\PlayerCharacterQuest::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'			=> $playerId,
		'stage_id'			=> $random,
		'mission_flag_1'	=> 0,
		'mission_flag_2'	=> 0,
		'mission_flag_3'	=> 0,
		'clear_flag'		=> 0,
		'created_at'		=> $now,
		'updated_at'		=> $now,
	];
});

// player_character_quest_reward
$factory->define(App\Models\PlayerCharacterQuestReward::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$rewardId = $faker->numberBetween(1, 999);
	$rewardNum = $faker->numberBetween(1, 100);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'		=> $playerId,
		'battle_code'	=> 'battle_code_' . $random,
		'stage_id'		=> $random,
		'reward_id'		=> $rewardId,
		'reward_num'	=> $rewardNum,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_data
$factory->define(App\Models\PlayerData::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	return [
		'player_id'		=> $playerId,
		'type'			=> App\Models\PlayerData::TYPE_BGM,
		'value'			=> 100,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_grimoire
$factory->define(App\Models\PlayerGrimoire::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$grimoireId = $faker->numberBetween(1, 999);
	return [
		'player_id'		=> $playerId,
		'grimoire_id'	=> $grimoireId,
		'awake'			=> 0,
		'slot_1'		=> null,
		'slot_1_flag'	=> App\Models\PlayerGrimoire::SLOT_RELEASE_FLAG_NO,
		'slot_2'		=> null,
		'slot_2_flag'	=> App\Models\PlayerGrimoire::SLOT_RELEASE_FLAG_NO,
		'slot_3'		=> null,
		'slot_3_flag'	=> App\Models\PlayerGrimoire::SLOT_RELEASE_FLAG_NO,
		'lock_flag'		=> App\Models\PlayerGrimoire::LOCK_FLAG_NO,
		'delete_flag'	=> App\Models\PlayerGrimoire::DELETE_FLAG_NO,
		'acquired_at'	=> $now,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_item
$factory->define(App\Models\PlayerItem::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$itemId = $faker->numberBetween(1, 999);
	$num = $faker->numberBetween(1, 100);
	return [
		'player_id'		=> $playerId,
		'item_id'		=> $itemId,
		'num'			=> $num,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_login_bonus_history
$factory->define(App\Models\PlayerLoginBonusHistory::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$loginBonusId = $faker->numberBetween(1, 999);
	$num = $faker->numberBetween(1, 100);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'			=> $playerId,
		'login_bonus_id'	=> $loginBonusId,
		'login_bonus_name'	=> 'login_bonus_name_' . $random,
		'started_at'		=> $now,
		'ended_at'			=> $now,
		'count'				=> 1,
		'player_count'		=> 1,
		'remuneration'		=> 1,
		'num'				=> 1,
		'created_at'		=> $now,
		'updated_at'		=> $now,
	];
});

// player_login_log
$factory->define(App\Models\PlayerLoginLog::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	return [
		'player_id'		=> $playerId,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_orb
$factory->define(App\Models\PlayerOrb::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$orbId = $faker->numberBetween(1, 999);
	$num = $faker->numberBetween(1, 100);
	return [
		'player_id'		=> $playerId,
		'orb_id'		=> $orbId,
		'num'			=> $num,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_party
$factory->define(App\Models\PlayerParty::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$partyNo = $faker->numberBetween(1, 10);
	$playerCharacterId1 = $faker->numberBetween(1, 999999);
	$num = $faker->numberBetween(1, 100);
	return [
		'player_id'				=> $playerId,
		'party_no'				=> $partyNo,
		'player_character_id_1'	=> $playerCharacterId1,
		'player_grimoire_id_1'	=> null,
		'player_character_id_2'	=> null,
		'player_grimoire_id_2'	=> null,
		'player_character_id_3'	=> null,
		'player_grimoire_id_3'	=> null,
		'player_character_id_4'	=> null,
		'player_grimoire_id_4'	=> null,
		'player_character_id_5'	=> null,
		'player_grimoire_id_5'	=> null,
		'player_character_id_6'	=> null,
		'player_grimoire_id_6'	=> null,
		'created_at'			=> $now,
		'updated_at'			=> $now,
	];
});

// player_present
$factory->define(App\Models\PlayerPresent::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$generalId = $faker->numberBetween(1, 10);
	$num = $faker->numberBetween(1, 100);
	return [
		'player_id'		=> $playerId,
		'type'			=> App\Services\PlayerService::ITEM_TYPE_ITEM,
		'general_id'	=> $generalId,
		'num'			=> $num,
		'take_flag'		=> App\Models\PlayerPresent::TAKE_FLAG_NO,
		'taked_at'		=> null,
		'expired_at'	=> null,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_story_quest
$factory->define(App\Models\PlayerStoryQuest::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$stageId = $faker->numberBetween(1, 10);
	return [
		'player_id'			=> $playerId,
		'stage_id'			=> $stageId,
		'mission_flag_1'	=> 0,
		'mission_flag_2'	=> 0,
		'mission_flag_3'	=> 0,
		'clear_flag'		=> App\Services\BattleService::QUEST_CLEAR_FLAG_NO,
		'created_at'		=> $now,
		'updated_at'		=> $now,
	];
});

// player_story_quest_reward
$factory->define(App\Models\PlayerStoryQuestReward::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$stageId = $faker->numberBetween(1, 10);
	$rewardId = $faker->numberBetween(1, 999);
	$rewardNum = $faker->numberBetween(1, 100);
	$random = sprintf('%04', $faker->numberBetween(0, 9999));
	return [
		'player_id'		=> $playerId,
		'battle_code'	=> 'battle_code_' . $random,
		'stage_id'		=> $stageId,
		'reward_id'		=> $rewardId,
		'reward_num'	=> $rewardNum,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_ticket
$factory->define(App\Models\PlayerTicket::class, function (Faker $faker) {
	$now = Carbon::now();
	$playerId = $faker->numberBetween(100000000, 999999999);
	$num = $faker->numberBetween(1, 100);
	return [
		'player_id'		=> $playerId,
		'num'			=> $num,
		'created_at'	=> $now,
		'updated_at'	=> $now,
	];
});

// player_follow
$factory->define(App\Models\PlayerFollow::class, function (Faker $faker) {
	$now = Carbon::now();
	return [
		//'id'				=>
		'player_id'			=> 1,
		'follower_id'		=> 2,
		'created_at'		=> $now,
		'updated_at'		=> $now,
	];
});