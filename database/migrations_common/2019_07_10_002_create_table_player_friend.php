<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerFriend extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_friend (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤフレンドID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , follow_id BIGINT UNSIGNED not null comment 'フォローID'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_friend_PKC primary key (id)
) comment 'プレイヤフレンド' AUTO_INCREMENT=10001;

create index player_friend_IX1
  on player_friend(player_id);

create index player_friend_IX2
  on player_friend(follow_id);

create unique index player_friend_IX3
  on player_friend(player_id,follow_id);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_friend');
	}

}
