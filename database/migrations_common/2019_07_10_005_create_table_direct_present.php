<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTableDirectPresent extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table direct_present (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment '直接プレゼントID'
  , type TINYINT UNSIGNED not null comment '付与条件種別'
  , player_id BIGINT UNSIGNED default 0 comment 'プレイヤID'
  , date1 DATETIME comment '付与条件日付1'
  , date2 DATETIME comment '付与条件日付2'
  , item_type TINYINT UNSIGNED not null comment 'アイテム種別'
  , item_id INT UNSIGNED not null comment 'アイテムID'
  , item_num INT UNSIGNED not null comment 'アイテム個数'
  , info_type TINYINT UNSIGNED not null comment '情報種別'
  , info_id VARCHAR(64) not null comment '情報ID'
  , message VARCHAR(512) not null comment 'メッセージ'
  , takable_days INT UNSIGNED not null comment '受取り可能日数'
  , delete_flag TINYINT UNSIGNED default 0 not null comment '削除フラグ'
  , start_at DATETIME not null comment '開始時刻'
  , end_at DATETIME not null comment '終了時刻'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint direct_present_PKC primary key (id)
) comment '直接プレゼント' AUTO_INCREMENT=10001;

create index direct_present_IX1
  on direct_present(player_id);

create index direct_present_IX2
  on direct_present(start_at);

create index direct_present_IX3
  on direct_present(end_at);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('direct_present');
	}

}
