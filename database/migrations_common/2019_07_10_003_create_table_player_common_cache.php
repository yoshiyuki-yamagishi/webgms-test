<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Utils\SqlUtil;

class CreateTablePlayerCommonCache extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
$query = <<<QUERY
create table player_common_cache (
  id BIGINT UNSIGNED AUTO_INCREMENT not null comment 'プレイヤ共通キャッシュID'
  , player_id BIGINT UNSIGNED not null comment 'プレイヤID'
  , player_name VARCHAR(255) not null comment 'プレイヤ名'
  , player_lv INT UNSIGNED not null comment 'プレイヤレベル'
  , main_character INT UNSIGNED not null comment 'メインキャラクター'
  , message VARCHAR(512) not null comment 'メッセージ'
  , last_login_at DATETIME not null comment '最終ログイン日時'
  , created_at DATETIME not null comment '作成日時'
  , updated_at DATETIME not null comment '更新日時'
  , constraint player_common_cache_PKC primary key (id)
) comment 'プレイヤ共通キャッシュ' AUTO_INCREMENT=10001;

create unique index player_common_cache_IX1
  on player_common_cache(player_id);

create index player_common_cache_IX2
  on player_common_cache(player_lv);

create index player_common_cache_IX3
  on player_common_cache(last_login_at);

QUERY;

        SqlUtil::execRawSqls($query);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_common_cache');
	}

}
