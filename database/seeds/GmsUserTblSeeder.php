<?php

use Illuminate\Database\Seeder;
use App\Models\GmsUserModel;

class GmsUserTblSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // BaseData
        $keys = ['name', 'email', 'password', 'manage_user_auth'];
        $value = [
            'admin',
            'admin@admin.com',
            '$2y$10$AD9KnnAk8TxniR98ABiDyOCWTa0CaOXhFOa1cQkahrciADwMbBcSO',
            2
        ];
        $insValue = array_combine($keys, $value);
        GmsUserModel::firstOrCreate(['id'=>1], $insValue);
    }

}
