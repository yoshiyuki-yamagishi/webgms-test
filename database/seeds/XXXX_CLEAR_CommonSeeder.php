<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class XXXX_CLEAR_CommonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$query = <<<QUERY
delete from `player_common`;
delete from `player_common_cache`;
delete from `player_friend`;
delete from `maintenance`;
delete from `direct_present`;
QUERY;

        SqlUtil::execRawSqls($query);
    }
}
