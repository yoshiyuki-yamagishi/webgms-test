<?php

use Illuminate\Database\Seeder;
use App\Utils\SqlUtil;

class XXXX_CLEAR_GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$query = <<<QUERY
delete from `player`;
delete from `player_ban`;
delete from `player_battle`;
delete from `player_battle_character`;
delete from `player_battle_grimoire`;
delete from `player_battle_result`;
delete from `player_battle_reward`;
delete from `player_blue_crystal`;
delete from `player_blue_crystal_log`;
delete from `player_character`;
delete from `player_character_log`;
delete from `player_data`;
delete from `player_direct_present`;
delete from `player_gacha`;
delete from `player_gacha_result`;
delete from `player_grimoire`;
delete from `player_grimoire_log`;
delete from `player_item`;
delete from `player_item_buy`;
delete from `player_item_log`;
delete from `player_log`;
delete from `player_login`;
delete from `player_login_bonus`;
delete from `player_login_point`;
delete from `player_mission`;
delete from `player_party`;
delete from `player_present`;
delete from `player_product_buy`;
delete from `player_product_buy_item`;
delete from `player_quest`;
delete from `player_quest_mission`;
delete from `player_shop_item`;

QUERY;

        SqlUtil::execRawSqls($query);
    }
}
