<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// v1
Route::group(['prefix' => '/v1'], function () {

    Route::get('/home', 'HomeController@index');
	// Route::get('/hello', 'HelloController@index');
	// Route::post('/hello', 'HelloController@post');
	// Route::get('/top', 'HelloController@top');
	Route::get('/player_list/index', 'PlayerListController@index');
	Route::post('/player_list/index', 'PlayerListController@index');

	Route::get('/ban/index', 'PlayerListController@index');
	Route::post('/ban/index', 'PlayerListController@index');

	Route::get('/debug/index', 'DebugController@index');
	Route::post('/debug/index', 'DebugController@index');

	Route::get('/simulate/index', 'SimulateController@index');
	Route::post('/simulate/index', 'SimulateController@index');

	Route::get('/player/index', 'PlayerDetailsController@index');
	Route::post('/player/index', 'PlayerDetailsController@index');

	Route::get('/player_ban/index', 'PlayerBanController@index');
	Route::post('/player_ban/index', 'PlayerBanController@index');

	Route::get('/info/index', 'InfoController@index');
	Route::post('/info/index', 'InfoController@index');

    Route::get('/info_register/index', 'InfoRegisterController@index');
    Route::post('/info_register/index', 'InfoRegisterController@index');

	Route::get('/test/index', 'TestController@index');
	Route::post('/test/index', 'TestController@index');

	Route::get('/publish/index', 'PublishController@index');
	Route::post('/publish/index', 'PublishController@index');

	Route::get('/maintenance/index', 'MaintenanceController@index');
	Route::post('/maintenance/index', 'MaintenanceController@index');

	Route::get('/player_exp_log/index', 'PlayerExpLogController@index');
	Route::post('/player_exp_log/index', 'PlayerExpLogController@index');

	Route::get('/player_lv_log/index', 'PlayerLvLogController@index');
	Route::post('/player_lv_log/index', 'PlayerLvLogController@index');

	Route::get('/grimoire_log/index', 'PlayerGrimoireLogController@index');
	Route::post('/grimoire_log/index', 'PlayerGrimoireLogController@index');

	Route::get('/character_log/index', 'PlayerCharacterLogController@index');
	Route::post('/character_log/index', 'PlayerCharacterLogController@index');

	Route::get('/item_log/index', 'PlayerItemLogController@index');
	Route::post('/item_log/index', 'PlayerItemLogController@index');

	Route::get('/player_friend_point_log/index', 'PlayerFriendPointLogController@index');
	Route::post('/player_friend_point_log/index', 'PlayerFriendPointLogController@index');

	Route::get('/player_login_bonus_log/index', 'PlayerLoginBonusLogController@index');
	Route::post('/player_login_bonus_log/index', 'PlayerLoginBonusLogController@index');

	Route::get('/item_buy_log/index', 'PlayerItemBuyLogController@index');
	Route::post('/item_buy_log/index', 'PlayerItemBuyLogController@index');

	Route::get('/product_buy_log/index', 'ProductBuyLogController@index');
	Route::post('/product_buy_log/index', 'ProductBuyLogController@index');

	Route::get('/player_mission_log/index', 'PlayerMissionLogController@index');
	Route::post('/player_mission_log/index', 'PlayerMissionLogController@index');

	Route::get('/player_present_log/index', 'PlayerPresentLogController@index');
	Route::post('/player_present_log/index', 'PlayerPresentLogController@index');

	Route::get('/direct_present/index', 'DirectPresentController@index');
	Route::post('/direct_present/index', 'DirectPresentController@index');

    Route::get('/direct_present/multiItemPresentPlayers', 'DirectPresentController@multiItemPresentPlayers');
    Route::post('/direct_present/multiItemPresentPlayers', 'DirectPresentController@multiItemPresentPlayers');

	Route::get('/item_master/index', 'ItemMasterController@index');
	Route::post('/item_master/index', 'ItemMasterController@index');

	Route::get('/item_detail_master/index', 'ItemMasterDetailsController@index');
	Route::post('/item_detail_master/index', 'ItemMasterDetailsController@index');

	Route::get('/character_master/index', 'CharacterMasterController@index');
	Route::post('/character_master/index', 'CharacterMasterController@index');

	Route::get('/character_detail_master/index', 'CharacterMasterDetailsController@index');
	Route::post('/character_detail_master/index', 'CharacterMasterDetailsController@index');

	Route::get('/grimoire_master/index', 'GrimoireMasterController@index');
	Route::post('/grimoire_master/index', 'GrimoireMasterController@index');

	Route::get('/grimoire_detail_master/index', 'GrimoireMasterDetailsController@index');
	Route::post('/grimoire_detail_master/index', 'GrimoireMasterDetailsController@index');

	Route::get('/skill_master/index', 'SkillMasterController@index');
	Route::post('/skill_master/index', 'SkillMasterController@index');

	Route::get('/gacha_master/index', 'GachaMasterController@index');
	Route::post('/gacha_master/index', 'GachaMasterController@index');

	Route::get('/gacha_simulate/index', 'GachaMasterDetailsController@index');
	Route::post('/gacha_simulate/index', 'GachaMasterDetailsController@index');

	Route::get('/quest_master/index', 'QuestMasterController@index');
	Route::post('/quest_master/index', 'QuestMasterController@index');

	Route::get('/quest_detail_master/index', 'QuestMasterDetailsController@index');
	Route::post('/quest_detail_master/index', 'QuestMasterDetailsController@index');

	Route::get('/reward_master/index', 'RewardMasterController@index');
	Route::post('/reward_master/index', 'RewardMasterController@index');

	Route::get('/reward_detail_master/index', 'RewardMasterDetailsController@index');
	Route::post('/reward_detail_master/index', 'RewardMasterDetailsController@index');

	// Route::get('/gacha_simulate/index', 'GachaMasterDetailsController@tryGacha');
	// Route::post('/gacha_simulate/index', 'GachaMasterDetailsController@tryGacha');

	// Route::get('hello', function() {
	// 	return view('hello.index');
	// });

	Route::get('/app/version', 'AppController@version');

	Route::post('/player/regist', 'PlayerController@regist');
	Route::post('/player/get', 'PlayerController@get');
	Route::post('/player/update', 'PlayerController@update');
	Route::post('/player/al_recover', 'PlayerController@alRecover');
	Route::post('/player/transit_src', 'PlayerController@transitSrc');
	Route::post('/player/transit_dst', 'PlayerController@transitDst');
	Route::post('/player/account_restore', 'PlayerController@accountRestore');

	Route::post('/login', 'LoginController@login');;

	Route::post('/home', 'HomeController@index');

	Route::post('/player_present/list', 'PlayerPresentController@list');
	Route::post('/player_present/take', 'PlayerPresentController@take');

	Route::post('/player_mission/list', 'PlayerMissionController@list');
	Route::post('/player_mission/reward_take', 'PlayerMissionController@rewardTake');

	Route::post('/player_character/list', 'PlayerCharacterController@list');
	Route::post('/player_character/evolve', 'PlayerCharacterController@evolve');
	Route::post('/player_character/reinforce', 'PlayerCharacterController@reinforce');
	Route::post('/player_character/skill_reinforce', 'PlayerCharacterController@skillReinforce');
	Route::post('/player_character/orb_equip', 'PlayerCharacterController@orbEquip');
	Route::post('/player_character/leader_select', 'PlayerCharacterController@leaderSelect');
	Route::post('/player_character/favorite_select', 'PlayerCharacterController@favoriteSelect');

	Route::post('/player_grimoire/list', 'PlayerGrimoireController@list');
	Route::post('/player_grimoire/reinforce', 'PlayerGrimoireController@reinforce');
	Route::post('/player_grimoire/limit_up', 'PlayerGrimoireController@limitUp');
	Route::post('/player_grimoire/sell', 'PlayerGrimoireController@sell');

	Route::post('/player_party/list', 'PlayerPartyController@list');
	Route::post('/player_party/update', 'PlayerPartyController@update');

	Route::post('/player_orb/list', 'PlayerOrbController@list');

	Route::post('/player_quest/list', 'PlayerQuestController@list');
	Route::post('/player_quest/update', 'PlayerQuestController@update');

	Route::post('/battle/start', 'BattleController@start');
	Route::post('/battle/end', 'BattleController@end');
	Route::post('/battle/continue', 'BattleController@continue');
	Route::post('/battle/skip', 'BattleController@skip');
	Route::post('/battle/interrupt', 'BattleController@interrupt');

	Route::post('/gacha/list', 'GachaController@list');
	Route::post('/gacha/exec', 'GachaController@exec');
	Route::post('/gacha/retry', 'GachaController@retry');

	Route::post('/gacha_history/list', 'GachaHistoryController@list');

	Route::post('/receipt/check', 'ReceiptController@check');

	Route::post('/shop/list', 'ShopController@list');
	Route::post('/shop/lineup_update', 'ShopController@lineupUpdate');
	Route::post('/shop/buy', 'ShopController@buy');

	Route::post('/player_item/list', 'PlayerItemController@list');
	Route::post('/player_item/sell', 'PlayerItemController@sell');

	Route::post('/player_story/list', 'PlayerStoryController@list');

	Route::post('/player_data/get', 'PlayerDataController@get');
	Route::post('/player_data/set', 'PlayerDataController@set');

	Route::post('/follow/list', 'FollowController@list');
	Route::post('/follow/add', 'FollowController@add');
	Route::post('/follow/delete', 'FollowController@delete');

	Route::post('/follower/list', 'FollowerController@list');
	Route::post('/follower/delete', 'FollowerController@block');
});

Auth::routes();

